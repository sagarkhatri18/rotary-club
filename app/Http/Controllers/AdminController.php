<?php

namespace App\Http\Controllers;

use App\Model\AdminMark;
use App\Model\AnnualFund;
use App\Model\BasicEducation;
use App\Model\ClubBulletin;
use App\Model\ClubFlagship;
use App\Model\ClubMeeting;
use App\Model\ClubParticipation;
use App\Model\ClubRegistration;
use App\Model\Complete;
use App\Model\DiseasePrevention;
use App\Model\EconomicCommunity;
use App\Model\MaternalChild;
use App\Model\MediaCoverage;
use App\Model\MembershipClassification;
use App\Model\MembershipDevelopment;
use App\Model\MembershipGrowth;
use App\Model\Month;
use App\Model\Participation;
use App\Model\Promotion;
use App\Model\WaterSupply;
use App\Model\YouthService;
use Illuminate\Http\Request;
use App\User;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $month = Month::all();
        $club = User::where('role', '!=', 'admin')->get();
        $month_id = 7;
        $club_id = 0;
        //$route = explode("/", \Route::current()->uri())[0];
        return view('admin_home', compact('month', 'club', 'month_id', 'club_id'));
    }

    public function clubView($month_id)
    {
        $month = Month::all();
        $club = User::where('role', '!=', 'admin')->get();
        $month_name = Month::find($month_id);

        return view('pages.admin_preview', compact('month_id', 'month', 'club', 'month_name'));
    }

    public function reportSearch($month_id, $user_id)
    {
        $data = [
            'club_meetings' =>  ClubMeeting::where('user_id', $user_id)->where('month_id', $month_id)->first(),
            'club_bulletins' => ClubBulletin::where('user_id', $user_id)->where('month_id', $month_id)->first(),
            'club_participation' => ClubParticipation::with('club_participants_data')->where('user_id', $user_id)->where('month_id', $month_id)->first(),
            'club_registration' => ClubRegistration::where('user_id', $user_id)->where('month_id', $month_id)->first(),
            'membership_class' => MembershipClassification::with('membership_classification_data')->where('user_id', $user_id)->where('month_id', $month_id)->first(),
            'membership_growth' => MembershipGrowth::where('user_id', $user_id)->where('month_id', $month_id)->first(),
            'membership_development' => MembershipDevelopment::where('user_id', $user_id)->where('month_id', $month_id)->first(),
            'water_supply' => WaterSupply::with('water_supply_data')->where('user_id', $user_id)->where('month_id', $month_id)->first(),
            'disease_prev' => DiseasePrevention::where('user_id', $user_id)->where('month_id', $month_id)->first(),
            'economic_comm' => EconomicCommunity::where('user_id', $user_id)->where('month_id', $month_id)->first(),
            'maternal_child' => MaternalChild::where('user_id', $user_id)->where('month_id', $month_id)->first(),
            'basic_edu' => BasicEducation::with('basic_education_data')->where('user_id', $user_id)->where('month_id', $month_id)->first(),
            'youth_service' => YouthService::where('user_id', $user_id)->where('month_id', $month_id)->first(),
            'media_coverage' => MediaCoverage::with('media_coverage_data')->where('user_id', $user_id)->where('month_id', $month_id)->first(),
            'club_flagship' => ClubFlagship::with('club_flagship_data')->where('user_id', $user_id)->where('month_id', $month_id)->first(),
            'promotion' => Promotion::where('user_id', $user_id)->where('month_id', $month_id)->first(),
            'annual_fund' => AnnualFund::where('user_id', $user_id)->where('month_id', $month_id)->first(),
            'participation' => Participation::where('user_id', $user_id)->where('month_id', $month_id)->first(),
        ];

        $month = Month::all();
        $club = User::where('role', '!=', 'admin')->get();

        $club_name = User::find($user_id)->club_name;
        $month_name = Month::find($month_id);
        $club_id = $user_id;
        return view('pages.form_preview', compact('month','club', 'data', 'club_name', 'month_name', 'month_id', 'club_id'));
    }

    public function mark(Request $request)
    {
        $club_info = [
            'club_id' => $request->club_id,
            'month_id' => $request->month_id
        ];

        $mark = ['mark' => $request->mark];
        $store_mark = AdminMark::updateOrCreate($club_info, $mark);

        if($store_mark){
            $notification = array(
                'message' => "Mark successfully saved.",
                'type' => 'success'
            );
        }else{
            $notification = array(
                'message' => "Failed to save mark.",
                'type' => 'error'
            );
        }
        return redirect()->back()->with($notification);
    }
}
