<?php

namespace App\Http\Controllers\ClubAdministration;

use App\Model\ClubBulletin;
use App\Model\Complete;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClubBulletinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tick = User::tickMark();
        $month = Complete::showMonth();
        $club_bulletins = ClubBulletin::where('user_id', \Auth::user()->id)->where('month_id', 7)->first();
        $month_id = 7;
        $route = explode("/", \Route::current()->uri())[0];
        return view('pages.club_bulletin', compact('month', 'club_bulletins', 'month_id',
            'route', 'tick'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        $validatedData = $request->validate([
//            'no_of_members' => 'required|int',
//        ]);

        if(!Complete::checkChecked($request->month_id)){

            $no_of_issue = [
                'first' => $request->no_of_issue_first,
                'second' => $request->no_of_issue_second,
                'third' => $request->no_of_issue_third,
                'fourth' => $request->no_of_issue_fourth,
                'fifth' => $request->no_of_issue_fifth,
            ];

            $data = [
                'id' => $request->id,
                'user_id' => \Auth::user()->id,
                'month_id' => $request->month_id,
                'no_of_members' => $request->no_of_members,
                'club_attendance' => $request->club_attendance,
                'annual_target_percent' => $request->annual_target_percent,
                'title' => $request->title,
                'no_of_issue' => json_encode($no_of_issue)
            ];

            if($request->id == ""){
                ClubBulletin::create($data);
                $notification = array(
                    'message' => 'Form has been successfully saved.',
                    'type' => 'success'
                );
            }else{
                $value = ClubBulletin::findOrFail($request->id);
                $value->update($data);
                $notification = array(
                    'message' => 'Form has been successfully updated.',
                    'type' => 'success'
                );
            }

        }else{
            $notification = array(
                'message' => "Failed. The form has already been submitted finally.",
                'type' => 'error'
            );
        }

        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tick = User::tickMark();
        if(Complete::checkMonth($id)){
            $month = Complete::showMonth();
            $month_id = $id;
            $route = explode("/", \Route::current()->uri())[0];
            $club_bulletins = ClubBulletin::where('user_id', \Auth::user()->id)->where('month_id', $id)->first();
            return view('pages.club_bulletin', compact('month', 'club_bulletins', 'month_id',
                'route', 'tick'));
        }else{
            return redirect()->back();
        }
    }
}
