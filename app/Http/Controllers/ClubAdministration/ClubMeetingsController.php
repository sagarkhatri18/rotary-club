<?php

namespace App\Http\Controllers\ClubAdministration;

use App\Model\ClubMeeting;
use App\Model\Complete;
use App\Model\Month;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ClubMeetingsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tick = User::tickMark();
        $month = Complete::showMonth();
        $club_meetings = ClubMeeting::where('user_id', \Auth::user()->id)->where('month_id', 7)->first();
        $month_id = 7;
        $route = explode("/", \Route::current()->uri())[0];
        return view('pages.club_meetings_conducted', compact('month', 'club_meetings',
            'month_id', 'route', 'tick'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Complete::checkChecked($request->month_id)){

            $validator = Validator::make($request->all(), [
                'no_of_members' => 'required|integer'
            ]);

            if($validator->passes()){
                $guest_speaker = [
                    'first' => $request->guest_speaker_first,
                    'second' => $request->guest_speaker_second,
                    'third' => $request->guest_speaker_third,
                    'fourth' => $request->guest_speaker_fourth,
                    'fifth' => $request->guest_speaker_fifth,
                ];
                $classification_talks = [
                    'first' => $request->classification_talks_first,
                    'second' => $request->classification_talks_second,
                    'third' => $request->classification_talks_third,
                    'fourth' => $request->classification_talks_fourth,
                    'fifth' => $request->classification_talks_fifth,
                ];
                $rotary_program = [
                    'first' => $request->rotary_program_first,
                    'second' => $request->rotary_program_second,
                    'third' => $request->rotary_program_third,
                    'fourth' => $request->rotary_program_fourth,
                    'fifth' => $request->rotary_program_fifth,
                ];
                $meeting_combined = [
                    'first' => $request->meeting_combined_first,
                    'second' => $request->meeting_combined_second,
                    'third' => $request->meeting_combined_third,
                    'fourth' => $request->meeting_combined_fourth,
                    'fifth' => $request->meeting_combined_fifth,
                ];
                $club_business = [
                    'first' => $request->club_business_first,
                    'second' => $request->club_business_second,
                    'third' => $request->club_business_third,
                    'fourth' => $request->club_business_fourth,
                    'fifth' => $request->club_business_fifth,
                ];
                $member_presence = [
                    'first' => $request->member_presence_first,
                    'second' => $request->member_presence_second,
                    'third' => $request->member_presence_third,
                    'fourth' => $request->member_presence_fourth,
                    'fifth' => $request->member_presence_fifth,
                ];
                $total_percent = [
                    'first' => $request->total_percent_first,
                    'second' => $request->total_percent_second,
                    'third' => $request->total_percent_third,
                    'fourth' => $request->total_percent_fourth,
                    'fifth' => $request->total_percent_fifth,
                ];
                $board = [
                    'first' => $request->board_first,
                    'second' => $request->board_second,
                    'third' => $request->board_third,
                    'fourth' => $request->board_fourth,
                    'fifth' => $request->board_fifth,
                ];
                $club_assembly = [
                    'first' => $request->club_assembly_first,
                    'second' => $request->club_assembly_second,
                    'third' => $request->club_assembly_third,
                    'fourth' => $request->club_assembly_fourth,
                    'fifth' => $request->club_assembly_fifth,
                ];
                $club_committee = [
                    'first' => $request->club_committee_first,
                    'second' => $request->club_committee_second,
                    'third' => $request->club_committee_third,
                    'fourth' => $request->club_committee_fourth,
                    'fifth' => $request->club_committee_fifth,
                ];

                $data = [
                    'id' => $request->id,
                    'user_id' => \Auth::user()->id,
                    'month_id' => $request->month_id,
                    'no_of_members' => $request->no_of_members,
                    'club_attendance' => $request->club_attendance,
                    'annual_target_percent' => $request->annual_target_percent,
                    'guest_speaker' => json_encode($guest_speaker),
                    'classification_talks' => json_encode($classification_talks),
                    'rotary_program' => json_encode($rotary_program),
                    'meeting_combined' => json_encode($meeting_combined),
                    'club_business' => json_encode($club_business),
                    'member_presence' => json_encode($member_presence),
                    'total_percent' => json_encode($total_percent),
                    'board' => json_encode($board),
                    'club_assembly' => json_encode($club_assembly),
                    'club_committee' => json_encode($club_committee),
                    'month_attendance' => $request->month_attendance,
                    'annual_target' => $request->annual_target
                ];

                if($request->id == ""){
                    ClubMeeting::create($data);
                    $notification = array(
                        'message' => 'Form has been successfully saved.',
                        'type' => 'success'
                    );
                }else{
                    $value = ClubMeeting::findOrFail($request->id);
                    $value->update($data);
                    $notification = array(
                        'message' => 'Form has been successfully updated.',
                        'type' => 'success'
                    );
                }
            }
            else{
                $notification = array(
                    'message' => "The number of members field cannot be empty.",
                    'type' => 'error'
                );
            }

        }else{
            $notification = array(
                'message' => "Failed. The form has already been submitted finally.",
                'type' => 'error'
            );
        }

        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tick = User::tickMark();
        if(Complete::checkMonth($id)){
            $month = Complete::showMonth();
            $month_id = $id;
            $route = explode("/", \Route::current()->uri())[0];
            $club_meetings = ClubMeeting::where('user_id', \Auth::user()->id)->where('month_id', $id)->first();
            return view('pages.club_meetings_conducted', compact('month', 'club_meetings',
                'month_id', 'route', 'tick'));
        }else{
            return redirect()->back();
        }
    }
}
