<?php

namespace App\Http\Controllers\ClubAdministration;

use App\Model\ClubParticipation;
use App\Model\ClubParticipationData;
use App\Model\Complete;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\User;

class ClubParticipationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tick = User::tickMark();
        $month = Complete::showMonth();
        $club_participation = ClubParticipation::where('user_id', \Auth::user()->id)->where('month_id', 7)->first();
        if($club_participation){
            $club_participation_data = ClubParticipationData::where('club_participation_id', $club_participation->id)->get();
        }else{$club_participation_data = null;}
        $month_id = 7;
        $route = explode("/", \Route::current()->uri())[0];
        return view('pages.club_participation', compact('month', 'club_participation',
            'month_id', 'route', 'club_participation_data', 'tick'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Complete::checkChecked($request->month_id)){

            $take['no_of_members'] = $request->no_of_members;
            $take['district_name'] = array_slice($request->district_name, 0, -1);

            $validator = Validator::make($take, [
                'no_of_members' => 'required|integer',
                'district_name.*' => 'required'
            ]);

            if($validator->passes()){
                $data = [
                    'id' => $request->id,
                    'user_id' => \Auth::user()->id,
                    'month_id' => $request->month_id,
                    'no_of_members' => $request->no_of_members,
                    'club_attendance' => $request->club_attendance,
                    'annual_target_percent' => $request->annual_target_percent
                ];

                if($request->id == ""){
                    $add = ClubParticipation::create($data);

                    for($i=0; $i<(count($request->district_name)-1); $i++){
                        $data1 = [
                            'club_participation_id' => $add->id,
                            'district_name' => $request->district_name[$i],
                            'date' => $request->date[$i],
                            'no_of_participants' => $request->no_of_participants[$i],
                            'event_sponsored' => $request->event_sponsored[$i]
                        ];
                        ClubParticipationData::create($data1);
                    }

                    $notification = array(
                        'message' => 'Form has been successfully saved.',
                        'type' => 'success'
                    );

                }else{
                    $value = ClubParticipation::findOrFail($request->id);
                    $value->update($data);

                    for($i=0; $i<(count($request->district_name)-1); $i++){
                        $data1 = [
                            'id' => $request->participation_data_id[$i],
                            'club_participation_id' => $value->id,
                            'district_name' => $request->district_name[$i],
                            'date' => $request->date[$i],
                            'no_of_participants' => $request->no_of_participants[$i],
                            'event_sponsored' => $request->event_sponsored[$i]
                        ];
                        if($request->participation_data_id[$i] == ""){
                            ClubParticipationData::create($data1);
                        }else{
                            $check = ClubParticipationData::findOrFail($request->participation_data_id[$i]);
                            $check->update($data1);
                        }
                    }
                    $notification = array(
                        'message' => 'Form has been successfully updated.',
                        'type' => 'success'
                    );
                }
            }else{
                $notification = array(
                    'message' => 'District name or no of members cannot be left empty.',
                    'type' => 'error'
                );
            }

        }else{
            $notification = array(
                'message' => "Failed. The form has already been submitted finally.",
                'type' => 'error'
            );
        }

        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tick = User::tickMark();
        if(Complete::checkMonth($id)){
            $month = Complete::showMonth();
            $month_id = $id;
            $route = explode("/", \Route::current()->uri())[0];
            $club_participation = ClubParticipation::where('user_id', \Auth::user()->id)->where('month_id', $id)->first();
            if($club_participation){
                $club_participation_data = ClubParticipationData::where('club_participation_id', $club_participation->id)->get();
            }else{$club_participation_data = null;}
            return view('pages.club_participation', compact('month', 'club_participation',
                'month_id', 'route', 'club_participation_data', 'tick'));
        }else{
            return redirect()->back();
        }
    }

}
