<?php

namespace App\Http\Controllers\ClubAdministration;

use App\Model\ClubRegistration;
use App\Model\Complete;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
class ClubRegistrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tick = User::tickMark();
        $month = Complete::showMonth();
        $club_registration = ClubRegistration::where('user_id', \Auth::user()->id)->where('month_id', 7)->first();
        $month_id = 7;
        $route = explode("/", \Route::current()->uri())[0];
        return view('pages.club_registration', compact('month', 'club_registration',
            'month_id', 'route', 'tick'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Complete::checkChecked($request->month_id)){

            $data = [
                'id' => $request->id,
                'user_id' => \Auth::user()->id,
                'month_id' => $request->month_id,
                'no_of_members' => $request->no_of_members,
                'club_attendance' => $request->club_attendance,
                'annual_target_percent' => $request->annual_target_percent,
                'members_of_july' => $request->members_of_july,
                'members_of_this_month' => $request->members_of_this_month,
                'club_goals_uploaded' => $request->club_goals_uploaded,
                'club_goals_upgraded' => $request->club_goals_upgraded
            ];

            if($request->id == ""){
                ClubRegistration::create($data);
                $notification = array(
                    'message' => 'Form has been successfully saved.',
                    'type' => 'success'
                );
            }else{
                $value = ClubRegistration::findOrFail($request->id);
                $value->update($data);
                $notification = array(
                    'message' => 'Form has been successfully updated.',
                    'type' => 'success'
                );
            }
        }else{
            $notification = array(
                'message' => "Failed. The form has already been submitted finally.",
                'type' => 'error'
            );
        }

        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tick = User::tickMark();
        if(Complete::checkMonth($id)){
            $month = Complete::showMonth();
            $month_id = $id;
            $route = explode("/", \Route::current()->uri())[0];
            $club_registration = ClubRegistration::where('user_id', \Auth::user()->id)->where('month_id', $id)->first();
            return view('pages.club_registration', compact('month', 'club_registration',
                'month_id', 'route', 'tick'));
        }else{
            return redirect()->back();
        }
    }

}
