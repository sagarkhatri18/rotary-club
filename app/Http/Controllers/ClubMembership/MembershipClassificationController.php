<?php

namespace App\Http\Controllers\ClubMembership;

use App\Model\Complete;
use App\Model\MembershipClassification;
use App\Model\MembershipClassificationData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\User;
class MembershipClassificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tick = User::tickMark();
        $month = Complete::showMonth();
        $membership_classification = MembershipClassification::where('user_id', \Auth::user()->id)->where('month_id', 7)->first();
        if($membership_classification){
            $membership_classification_data = MembershipClassificationData::where('mem_class_id', $membership_classification->id)->get();
        }else{$membership_classification_data = null;}
        $month_id = 7;
        $route = explode("/", \Route::current()->uri())[0];
        return view('pages.membership_classification', compact('month', 'membership_classification',
            'month_id', 'route', 'membership_classification_data', 'tick'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Complete::checkChecked($request->month_id)){

            $take['no_of_members'] = $request->no_of_members;
            $take['classification_categories_fulfilled'] = array_slice($request->classification_categories_fulfilled, 0, -1);

            $validator = Validator::make($take, [
                'no_of_members' => 'required|integer',
                'classification_categories_fulfilled.*' => 'required'
            ]);

            if($validator->passes()){
                $data = [
                    'id' => $request->id,
                    'user_id' => \Auth::user()->id,
                    'month_id' => $request->month_id,
                    'no_of_members' => $request->no_of_members,
                    'annual_target_no' => $request->annual_target_no,
                    'annual_target_percent' => $request->annual_target_percent
                ];

                if($request->id == ""){
                    $add = MembershipClassification::create($data);

                    for($i=0; $i<(count($request->classification_categories_fulfilled)-1); $i++){
                        $data1 = [
                            'mem_class_id' => $add->id,
                            'classification_categories_fulfilled' => $request->classification_categories_fulfilled[$i],
                            'no_of_club_members' => $request->no_of_club_members[$i],
                            'classification_categories_unfulfilled' => $request->classification_categories_unfulfilled[$i]
                        ];
                        MembershipClassificationData::create($data1);
                    }

                    $notification = array(
                        'message' => 'Form has been successfully saved.',
                        'type' => 'success'
                    );

                }else{
                    $value = MembershipClassification::findOrFail($request->id);
                    $value->update($data);

                    for($i=0; $i<(count($request->classification_categories_fulfilled)-1); $i++){
                        $data1 = [
                            'id' => $request->classification_data_id[$i],
                            'mem_class_id' => $value->id,
                            'classification_categories_fulfilled' => $request->classification_categories_fulfilled[$i],
                            'no_of_club_members' => $request->no_of_club_members[$i],
                            'classification_categories_unfulfilled' => $request->classification_categories_unfulfilled[$i]
                        ];
                        if($request->classification_data_id[$i] == ""){
                            MembershipClassificationData::create($data1);
                        }else{
                            $check = MembershipClassificationData::findOrFail($request->classification_data_id[$i]);
                            $check->update($data1);
                        }
                    }
                    $notification = array(
                        'message' => 'Form has been successfully updated.',
                        'type' => 'success'
                    );
                }
            }else{
                $notification = array(
                    'message' => 'Classification Categories or no of members cannot be left empty.',
                    'type' => 'error'
                );
            }

        }else{
            $notification = array(
                'message' => "Failed. The form has already been submitted finally.",
                'type' => 'error'
            );
        }

        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tick = User::tickMark();
        if(Complete::checkMonth($id)){
            $month = Complete::showMonth();
            $month_id = $id;
            $route = explode("/", \Route::current()->uri())[0];
            $membership_classification = MembershipClassification::where('user_id', \Auth::user()->id)->where('month_id', $id)->first();
            if($membership_classification){
                $membership_classification_data = MembershipClassificationData::where('mem_class_id', $membership_classification->id)->get();
            }else{$membership_classification_data = null;}
            return view('pages.membership_classification', compact('month', 'membership_classification',
                'month_id', 'route', 'membership_classification_data', 'tick'));
        }else{
            return redirect()->back();
        }
    }
}
