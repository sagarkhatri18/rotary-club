<?php

namespace App\Http\Controllers\ClubMembership;

use App\Model\Complete;
use App\Model\MembershipDevelopment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
class MembershipDevelopmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tick = User::tickMark();
        $month = Complete::showMonth();
        $membership_development = MembershipDevelopment::where('user_id', \Auth::user()->id)->where('month_id', 7)->first();
        $month_id = 7;
        $route = explode("/", \Route::current()->uri())[0];
        return view('pages.membership_development', compact('month', 'membership_development',
            'month_id', 'route', 'tick'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Complete::checkChecked($request->month_id)){

            $new_members_orientation = [
                'club_event' => $request->new_members_orientation_club_event,
                'district_event' => $request->new_members_orientation_district_event,
                'date' => $request->new_members_orientation_date
            ];
            $club_officers = [
                'club_event' => $request->club_officers_club_event,
                'district_event' => $request->club_officers_district_event,
                'date' => $request->club_officers_date
            ];
            $district_membership = [
                'club_event' => $request->district_membership_club_event,
                'district_event' => $request->district_membership_district_event,
                'date' => $request->membership_others_date
            ];
            $membership_others = [
                'club_event' => $request->membership_others_club_event,
                'district_event' => $request->membership_others_district_event,
                'date' => $request->district_membership_date
            ];

            $rotary_club = [
                'date' => $request->rotary_club_date,
                'charter_members' => $request->rotary_club_charter_members
            ];
            $rotaract_club = [
                'date' => $request->rotaract_club_date,
                'charter_members' => $request->rotaract_club_charter_members
            ];
            $interact_club = [
                'date' => $request->interact_club_date,
                'charter_members' => $request->interact_club_charter_members
            ];
            $rotary_community = [
                'date' => $request->rotary_community_date,
                'charter_members' => $request->rotary_community_charter_members
            ];

            $members_satisfaction = [
                'date' => $request->members_satisfaction_date,
                'no' => $request->members_satisfaction_no
            ];
            $health_check = [
                'date' => $request->health_check_date,
                'no' => $request->health_check_no
            ];
            $development_others = [
                'date' => $request->development_others_date,
                'no' => $request->development_others_no
            ];

            $data = [
                'id' => $request->id,
                'user_id' => \Auth::user()->id,
                'month_id' => $request->month_id,
                'no_of_members' => $request->no_of_members,
                'annual_target_no' => $request->annual_target_no,
                'annual_target_percent' => $request->annual_target_percent,

                'new_members_orientation' => json_encode($new_members_orientation),
                'club_officers' => json_encode($club_officers),
                'district_membership' => json_encode($district_membership),
                'membership_others' => json_encode($membership_others),

                'rotary_club' => json_encode($rotary_club),
                'rotaract_club' => json_encode($rotaract_club),
                'interact_club' => json_encode($interact_club),
                'rotary_community' => json_encode($rotary_community),

                'members_satisfaction' => json_encode($members_satisfaction),
                'health_check' => json_encode($health_check),
                'development_others' => json_encode($development_others)
            ];

            if($request->id == ""){
                MembershipDevelopment::create($data);
                $notification = array(
                    'message' => 'Form has been successfully saved.',
                    'type' => 'success'
                );
            }else{
                $value = MembershipDevelopment::findOrFail($request->id);
                $value->update($data);
                $notification = array(
                    'message' => 'Form has been successfully updated.',
                    'type' => 'success'
                );
            }

        }else{
            $notification = array(
                'message' => "Failed. The form has already been submitted finally.",
                'type' => 'error'
            );
        }

        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tick = User::tickMark();
        if(Complete::checkMonth($id)){
            $month = Complete::showMonth();
            $month_id = $id;
            $route = explode("/", \Route::current()->uri())[0];
            $membership_development = MembershipDevelopment::where('user_id', \Auth::user()->id)->where('month_id', $id)->first();
            return view('pages.membership_development', compact('month', 'membership_development',
                'month_id', 'route', 'tick'));
        }else{
            return redirect()->back();
        }
    }
}
