<?php

namespace App\Http\Controllers\ClubMembership;

use App\User;
use App\Model\Complete;
use App\Model\MembershipGrowth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MembershipGrowthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tick = User::tickMark();
        $month = Complete::showMonth();
        $membership_growth = MembershipGrowth::where('user_id', \Auth::user()->id)->where('month_id', 7)->first();
        $month_id = 7;
        $route = explode("/", \Route::current()->uri())[0];
        return view('pages.membership_growth', compact('month', 'membership_growth',
            'month_id', 'route', 'tick'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Complete::checkChecked($request->month_id)){

            $loss_of_members = [
                'first' => $request->loss_of_members_first,
                'second' => $request->loss_of_members_second,
                'third' => $request->loss_of_members_third,
                'fourth' => $request->loss_of_members_fourth
            ];
            $loss_of_female_members = [
                'first' => $request->loss_of_female_members_first,
                'second' => $request->loss_of_female_members_second,
                'third' => $request->loss_of_female_members_third,
                'fourth' => $request->loss_of_female_members_fourth
            ];
            $loss_of_youth_members = [
                'first' => $request->loss_of_youth_members_first,
                'second' => $request->loss_of_youth_members_second,
                'third' => $request->loss_of_youth_members_third,
                'fourth' => $request->loss_of_youth_members_fourth
            ];

            $data = [
                'id' => $request->id,
                'user_id' => \Auth::user()->id,
                'month_id' => $request->month_id,
                'no_of_members' => $request->no_of_members,
                'annual_target_no' => $request->annual_target_no,
                'annual_target_percent' => $request->annual_target_percent,
                'loss_of_members' => json_encode($loss_of_members),
                'loss_of_female_members' => json_encode($loss_of_female_members),
                'loss_of_youth_members' => json_encode($loss_of_youth_members)
            ];

            if($request->id == ""){
                MembershipGrowth::create($data);
                $notification = array(
                    'message' => 'Form has been successfully saved.',
                    'type' => 'success'
                );
            }else{
                $value = MembershipGrowth::findOrFail($request->id);
                $value->update($data);
                $notification = array(
                    'message' => 'Form has been successfully updated.',
                    'type' => 'success'
                );
            }

        }else{
            $notification = array(
                'message' => "Failed. The form has already been submitted finally.",
                'type' => 'error'
            );
        }

        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tick = User::tickMark();
        if(Complete::checkMonth($id)){
            $month = Complete::showMonth();
            $month_id = $id;
            $route = explode("/", \Route::current()->uri())[0];
            $membership_growth = MembershipGrowth::where('user_id', \Auth::user()->id)->where('month_id', $id)->first();
            return view('pages.membership_growth', compact('month', 'membership_growth',
                'month_id', 'route', 'tick'));
        }else{
            return redirect()->back();
        }
    }
}
