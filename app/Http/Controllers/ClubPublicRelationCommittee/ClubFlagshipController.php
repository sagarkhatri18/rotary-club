<?php

namespace App\Http\Controllers\ClubPublicRelationCommittee;

use App\User;
use App\Model\Complete;
use App\Model\ClubFlagship;
use App\Model\ClubFlagshipData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class ClubFlagshipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tick = User::tickMark();
        $month = Complete::showMonth();
        $club_flagship = ClubFlagship::where('user_id', \Auth::user()->id)->where('month_id', 7)->first();
        if($club_flagship){
            $club_flagship_data = ClubFlagshipData::where('club_flagship_id', $club_flagship->id)->get();
        }else{$club_flagship_data = null;}
        $month_id = 7;
        $route = explode("/", \Route::current()->uri())[0];
        return view('pages.club_flagship', compact('month', 'club_flagship', 'month_id', 'route',
            'club_flagship_data', 'tick'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Complete::checkChecked($request->month_id)){

            $take['name_of_club_flagship'] = $request->name_of_club_flagship;
            $take['activities'] = array_slice($request->activities, 0, -1);

            $validator = Validator::make($take, [
                'name_of_club_flagship' => 'required',
                'activities.*' => 'required'
            ]);

            if($validator->passes()){
                $data = [
                    'id' => $request->id,
                    'user_id' => \Auth::user()->id,
                    'month_id' => $request->month_id,
                    'name_of_club_flagship' => $request->name_of_club_flagship,
                    'partner_name' => $request->partner_name
                ];

                if($request->id == ""){
                    $add = ClubFlagship::create($data);

                    for($i=0; $i<(count($request->activities)-1); $i++){
                        $data1 = [
                            'club_flagship_id' => $add->id,
                            'activities' => $request->activities[$i],
                            'project_check' => $request->project_check[$i],
                            'beneficiaries' => $request->beneficiaries[$i],
                            'area' => $request->area[$i],
                            'funding_type' => $request->funding_type[$i]
                        ];
                        ClubFlagshipData::create($data1);
                    }

                    $notification = array(
                        'message' => 'Form has been successfully saved.',
                        'type' => 'success'
                    );

                }else{
                    $value = ClubFlagship::findOrFail($request->id);
                    $value->update($data);

                    for($i=0; $i<(count($request->activities)-1); $i++){
                        $data1 = [
                            'id' => $request->club_flagship_data_id[$i],
                            'club_flagship_id' => $value->id,
                            'activities' => $request->activities[$i],
                            'project_check' => $request->project_check[$i],
                            'beneficiaries' => $request->beneficiaries[$i],
                            'area' => $request->area[$i],
                            'funding_type' => $request->funding_type[$i]
                        ];
                        if($request->club_flagship_data_id[$i] == ""){
                            ClubFlagshipData::create($data1);
                        }else{
                            $check = ClubFlagshipData::findOrFail($request->club_flagship_data_id[$i]);
                            $check->update($data1);
                        }
                    }
                    $notification = array(
                        'message' => 'Form has been successfully updated.',
                        'type' => 'success'
                    );
                }
            }else{
                $notification = array(
                    'message' => 'Club Flagship name or activities field cannot be left empty.',
                    'type' => 'error'
                );
            }
        }else{
            $notification = array(
                'message' => "Failed. The form has already been submitted finally.",
                'type' => 'error'
            );
        }

        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tick = User::tickMark();
        if(Complete::checkMonth($id)){
            $month = Complete::showMonth();
            $month_id = $id;
            $route = explode("/", \Route::current()->uri())[0];
            $club_flagship = ClubFlagship::where('user_id', \Auth::user()->id)->where('month_id', $id)->first();
            if($club_flagship){
                $club_flagship_data = ClubFlagshipData::where('club_flagship_id', $club_flagship->id)->get();
            }else{$club_flagship_data = null;}
            return view('pages.club_flagship', compact('month', 'club_flagship',
                'month_id', 'route', 'club_flagship_data', 'tick'));
        }else{
            return redirect()->back();
        }
    }
}
