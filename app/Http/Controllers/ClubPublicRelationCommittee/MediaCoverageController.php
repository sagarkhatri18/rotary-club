<?php

namespace App\Http\Controllers\ClubPublicRelationCommittee;

use App\User;
use App\Model\Complete;
use Illuminate\Support\Facades\Validator;
use App\Model\MediaCoverage;
use App\Model\MediaCoverageData;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MediaCoverageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tick = User::tickMark();
        $month = Complete::showMonth();
        $media_coverage = MediaCoverage::where('user_id', \Auth::user()->id)->where('month_id', 7)->first();
        if($media_coverage){
            $media_coverage_data = MediaCoverageData::where('media_coverage_id', $media_coverage->id)->get();
        }else{$media_coverage_data = null;}
        $month_id = 7;
        $route = explode("/", \Route::current()->uri())[0];
        return view('pages.media_coverage', compact('month', 'media_coverage', 'month_id',
            'route', 'media_coverage_data', 'tick'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Complete::checkChecked($request->month_id)){

            $take['message'] = array_slice($request->message, 0, -1);

            $validator = Validator::make($take, [
                'message.*' => 'required'
            ]);

            if($validator->passes()){
                $data = [
                    'id' => $request->id,
                    'user_id' => \Auth::user()->id,
                    'month_id' => $request->month_id
                ];

                if($request->id == ""){
                    $add = MediaCoverage::create($data);

                    for($i=0; $i<(count($request->message)-1); $i++){
                        $data1 = [
                            'media_coverage_id' => $add->id,
                            'message' => $request->message[$i],
                            'date' => $request->date[$i],
                            'times_coverage' => $request->times_coverage[$i],
                            'print_name' => $request->print_name[$i],
                            'visual_name' => $request->visual_name[$i],
                            'audio_name' => $request->audio_name[$i],
                            'social_media' => $request->social_media[$i]
                        ];
                        MediaCoverageData::create($data1);
                    }

                    $notification = array(
                        'message' => 'Form has been successfully saved.',
                        'type' => 'success'
                    );

                }else{
                    $value = MediaCoverage::findOrFail($request->id);
                    $value->update($data);

                    for($i=0; $i<(count($request->message)-1); $i++){
                        $data1 = [
                            'id' => $request->media_coverage_data_id[$i],
                            'media_coverage_id' => $value->id,
                            'message' => $request->message[$i],
                            'date' => $request->date[$i],
                            'times_coverage' => $request->times_coverage[$i],
                            'print_name' => $request->print_name[$i],
                            'visual_name' => $request->visual_name[$i],
                            'audio_name' => $request->audio_name[$i],
                            'social_media' => $request->social_media[$i]
                        ];
                        if($request->media_coverage_data_id[$i] == ""){
                            MediaCoverageData::create($data1);
                        }else{
                            $check = MediaCoverageData::findOrFail($request->media_coverage_data_id[$i]);
                            $check->update($data1);
                        }
                    }
                    $notification = array(
                        'message' => 'Form has been successfully updated.',
                        'type' => 'success'
                    );
                }
            }else{
                $notification = array(
                    'message' => 'Description Message field cannot be left empty.',
                    'type' => 'error'
                );
            }
        }else{
            $notification = array(
                'message' => "Failed. The form has already been submitted finally.",
                'type' => 'error'
            );
        }

        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tick = User::tickMark();
        if(Complete::checkMonth($id)){
            $month = Complete::showMonth();
            $month_id = $id;
            $route = explode("/", \Route::current()->uri())[0];
            $media_coverage = MediaCoverage::where('user_id', \Auth::user()->id)->where('month_id', $id)->first();
            if($media_coverage){
                $media_coverage_data = MediaCoverageData::where('media_coverage_id', $media_coverage->id)->get();
            }else{$media_coverage_data = null;}
            return view('pages.media_coverage', compact('month', 'media_coverage', 'month_id',
                'route', 'media_coverage_data', 'tick'));
        }else{
            return redirect()->back();
        }
    }
}
