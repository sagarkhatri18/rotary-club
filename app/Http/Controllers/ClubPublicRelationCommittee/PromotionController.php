<?php

namespace App\Http\Controllers\ClubPublicRelationCommittee;

use App\Model\Complete;
use App\Model\Promotion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class PromotionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tick = User::tickMark();
        $month = Complete::showMonth();
        $promotion = Promotion::where('user_id', \Auth::user()->id)->where('month_id', 7)->first();
        $month_id = 7;
        $route = explode("/", \Route::current()->uri())[0];
        return view('pages.promotion', compact('month', 'promotion', 'month_id', 'route', 'tick'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Complete::checkChecked($request->month_id)){

            $test_banners = [
                'first' => $request->test_banners_first,
                'second' => $request->test_banners_second,
                'third' => $request->test_banners_third
            ];
            $project_information = [
                'first' => $request->project_information_first,
                'second' => $request->project_information_second,
                'third' => $request->project_information_third
            ];

            $data = [
                'id' => $request->id,
                'user_id' => \Auth::user()->id,
                'month_id' => $request->month_id,
                'name_of_school' => $request->name_of_school,
                'name_of_business' => $request->name_of_business,
                'name_of_public_places' => $request->name_of_public_places,

                'test_banners' => json_encode($test_banners),
                'project_information' => json_encode($project_information),
                'club_brochures' => $request->club_brochures,
                'club_pamphlets' => $request->club_pamphlets
            ];

            if($request->id == ""){
                Promotion::create($data);
                $notification = array(
                    'message' => 'Form has been successfully saved.',
                    'type' => 'success'
                );
            }else{
                $value = Promotion::findOrFail($request->id);
                $value->update($data);
                $notification = array(
                    'message' => 'Form has been successfully updated.',
                    'type' => 'success'
                );
            }
        }else{
            $notification = array(
                'message' => "Failed. The form has already been submitted finally.",
                'type' => 'error'
            );
        }

        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tick = User::tickMark();
        if(Complete::checkMonth($id)){
            $month = Complete::showMonth();
            $month_id = $id;
            $route = explode("/", \Route::current()->uri())[0];
            $promotion = Promotion::where('user_id', \Auth::user()->id)->where('month_id', $id)->first();
            return view('pages.promotion', compact('month', 'promotion', 'month_id', 'route', 'tick'));
        }else{
            return redirect()->back();
        }
    }
}
