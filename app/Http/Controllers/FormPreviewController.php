<?php

namespace App\Http\Controllers;

use App\Model\AnnualFund;
use App\Model\BasicEducation;
use App\Model\ClubBulletin;
use App\Model\ClubFlagship;
use App\Model\ClubMeeting;
use App\Model\ClubParticipation;
use App\Model\ClubRegistration;
use App\Model\DiseasePrevention;
use App\Model\EconomicCommunity;
use App\Model\MaternalChild;
use App\Model\MediaCoverage;
use App\Model\MembershipClassification;
use App\Model\MembershipDevelopment;
use App\Model\MembershipGrowth;
use App\Model\Month;
use App\Model\Participation;
use App\Model\Promotion;
use App\Model\WaterSupply;
use App\Model\YouthService;
use App\User;
use App\Model\Complete;
use Illuminate\Http\Request;

class FormPreviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'club_meetings' => ClubMeeting::where('user_id', \Auth::user()->id)->where('month_id', 7)->first(),
            'club_bulletins' => ClubBulletin::where('user_id', \Auth::user()->id)->where('month_id', 7)->first(),
            'club_participation' => ClubParticipation::with('club_participants_data')->where('user_id', \Auth::user()->id)->where('month_id', 7)->first(),
            'club_registration' => ClubRegistration::where('user_id', \Auth::user()->id)->where('month_id', 7)->first(),
            'membership_class' => MembershipClassification::with('membership_classification_data')->where('user_id', \Auth::user()->id)->where('month_id', 7)->first(),
            'membership_growth' => MembershipGrowth::where('user_id', \Auth::user()->id)->where('month_id', 7)->first(),
            'membership_development' => MembershipDevelopment::where('user_id', \Auth::user()->id)->where('month_id', 7)->first(),
            'water_supply' => WaterSupply::with('water_supply_data')->where('user_id', \Auth::user()->id)->where('month_id', 7)->first(),
            'disease_prev' => DiseasePrevention::where('user_id', \Auth::user()->id)->where('month_id', 7)->first(),
            'economic_comm' => EconomicCommunity::where('user_id', \Auth::user()->id)->where('month_id', 7)->first(),
            'maternal_child' => MaternalChild::where('user_id', \Auth::user()->id)->where('month_id', 7)->first(),
            'basic_edu' => BasicEducation::with('basic_education_data')->where('user_id', \Auth::user()->id)->where('month_id', 7)->first(),
            'youth_service' => YouthService::where('user_id', \Auth::user()->id)->where('month_id', 7)->first(),
            'media_coverage' => MediaCoverage::with('media_coverage_data')->where('user_id', \Auth::user()->id)->where('month_id', 7)->first(),
            'club_flagship' => ClubFlagship::with('club_flagship_data')->where('user_id', \Auth::user()->id)->where('month_id', 7)->first(),
            'promotion' => Promotion::where('user_id', \Auth::user()->id)->where('month_id', 7)->first(),
            'annual_fund' => AnnualFund::where('user_id', \Auth::user()->id)->where('month_id', 7)->first(),
            'participation' => Participation::where('user_id', \Auth::user()->id)->where('month_id', 7)->first(),
        ];

        $tick = User::tickMark();
        $month = Complete::showMonth();
        $club_name = \Auth::user()->club_name;
        $month_id = 7;
        $month_name = Month::find(7);
        $route = explode("/", \Route::current()->uri())[0];
        return view('pages.form_preview', compact('month', 'month_id', 'route', 'tick', 'data', 'month_name', 'club_name'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tick = User::tickMark();
        if(Complete::checkMonth($id)){
            $data = [
                'club_meetings' =>  ClubMeeting::where('user_id', \Auth::user()->id)->where('month_id', $id)->first(),
                'club_bulletins' => ClubBulletin::where('user_id', \Auth::user()->id)->where('month_id', $id)->first(),
                'club_participation' => ClubParticipation::with('club_participants_data')->where('user_id', \Auth::user()->id)->where('month_id', $id)->first(),
                'club_registration' => ClubRegistration::where('user_id', \Auth::user()->id)->where('month_id', $id)->first(),
                'membership_class' => MembershipClassification::with('membership_classification_data')->where('user_id', \Auth::user()->id)->where('month_id', $id)->first(),
                'membership_growth' => MembershipGrowth::where('user_id', \Auth::user()->id)->where('month_id', $id)->first(),
                'membership_development' => MembershipDevelopment::where('user_id', \Auth::user()->id)->where('month_id', $id)->first(),
                'water_supply' => WaterSupply::with('water_supply_data')->where('user_id', \Auth::user()->id)->where('month_id', $id)->first(),
                'disease_prev' => DiseasePrevention::where('user_id', \Auth::user()->id)->where('month_id', $id)->first(),
                'economic_comm' => EconomicCommunity::where('user_id', \Auth::user()->id)->where('month_id', $id)->first(),
                'maternal_child' => MaternalChild::where('user_id', \Auth::user()->id)->where('month_id', $id)->first(),
                'basic_edu' => BasicEducation::with('basic_education_data')->where('user_id', \Auth::user()->id)->where('month_id', $id)->first(),
                'youth_service' => YouthService::where('user_id', \Auth::user()->id)->where('month_id', $id)->first(),
                'media_coverage' => MediaCoverage::with('media_coverage_data')->where('user_id', \Auth::user()->id)->where('month_id', $id)->first(),
                'club_flagship' => ClubFlagship::with('club_flagship_data')->where('user_id', \Auth::user()->id)->where('month_id', $id)->first(),
                'promotion' => Promotion::where('user_id', \Auth::user()->id)->where('month_id', $id)->first(),
                'annual_fund' => AnnualFund::where('user_id', \Auth::user()->id)->where('month_id', $id)->first(),
                'participation' => Participation::where('user_id', \Auth::user()->id)->where('month_id', $id)->first(),
            ];

            $month = Complete::showMonth();
            $month_id = $id;
            $club_name = \Auth::user()->club_name;
            $month_name = Month::find($id);
            $route = explode("/", \Route::current()->uri())[0];
            return view('pages.form_preview', compact('month',
                'month_id', 'route', 'tick', 'data', 'month_name', 'club_name'));
        }else{
            return redirect()->back();
        }
    }

    public function finalSubmit(Request $request)
    {

        $check = Complete::where('user_id', $request->user_id)->where('month_id', $request->month)->count();

        if($check <= 0){
            $data = [
                'user_id' => $request->user_id,
                'month_id' => $request->month,
                'check' => 1
            ];
            Complete::create($data);
            $notification = array(
                'title' => 'Submitted!',
                'text' => 'Form has been finally submitted successfully.',
                'type' => 'success'
            );
        }
        else{
            $notification = array(
                'title' => 'failed!',
                'text' => 'Form has already submitted finally.',
                'type' => 'error'
            );
        }

        return response()->json($notification);

    }

}
