<?php

namespace App\Http\Controllers;

use App\Model\Complete;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $tick = User::tickMark();
        $month = Complete::showMonth();
        $month_id = 0;
        $route = explode("/", \Route::current()->uri())[0];
        return view('home', compact('month', 'month_id', 'route', 'tick'));
    }

    public function view($id)
    {
        return \Redirect::route('home');
    }
}
