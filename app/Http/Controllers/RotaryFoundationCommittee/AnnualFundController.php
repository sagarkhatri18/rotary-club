<?php

namespace App\Http\Controllers\RotaryFoundationCommittee;

use App\Model\AnnualFund;
use Illuminate\Http\Request;
use App\Model\Complete;
use App\User;
use App\Http\Controllers\Controller;

class AnnualFundController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tick = User::tickMark();
        $month = Complete::showMonth();
        $annual_fund = AnnualFund::where('user_id', \Auth::user()->id)->where('month_id', 7)->first();
        $month_id = 7;
        $route = explode("/", \Route::current()->uri())[0];
        return view('pages.annual_fund', compact('month', 'annual_fund', 'month_id', 'route', 'tick'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Complete::checkChecked($request->month_id)){

            $club_contribution = [
                'first' => $request->club_contribution_first,
                'second' => $request->club_contribution_second
            ];
            $rotary_foundation = [
                'first' => $request->rotary_foundation_first,
                'second' => $request->rotary_foundation_second
            ];
            $phf = [
                'first' => $request->phf_first,
                'second' => $request->phf_second
            ];
            $mphf = [
                'first' => $request->mphf_first,
                'second' => $request->mphf_second
            ];
            $phs = [
                'first' => $request->phs_first,
                'second' => $request->phs_second
            ];
            $major_donor = [
                'first' => $request->major_donor_first,
                'second' => $request->major_donor_second
            ];
            $others = [
                'first' => $request->others_first,
                'second' => $request->others_second
            ];


            $data = [
                'id' => $request->id,
                'user_id' => \Auth::user()->id,
                'month_id' => $request->month_id,
                'annual_target' => $request->annual_target,

                'club_contribution' => json_encode($club_contribution),
                'rotary_foundation' => json_encode($rotary_foundation),
                'phf' => json_encode($phf),
                'mphf' => json_encode($mphf),
                'phs' => json_encode($phs),
                'major_donor' => json_encode($major_donor),
                'others' => json_encode($others)
            ];

            if($request->id == ""){
                AnnualFund::create($data);
                $notification = array(
                    'message' => 'Form has been successfully saved.',
                    'type' => 'success'
                );
            }else{
                $value = AnnualFund::findOrFail($request->id);
                $value->update($data);
                $notification = array(
                    'message' => 'Form has been successfully updated.',
                    'type' => 'success'
                );
            }
        }else{
            $notification = array(
                'message' => "Failed. The form has already been submitted finally.",
                'type' => 'error'
            );
        }

        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tick = User::tickMark();
        if(Complete::checkMonth($id)){
            $month = Complete::showMonth();
            $month_id = $id;
            $route = explode("/", \Route::current()->uri())[0];
            $annual_fund = AnnualFund::where('user_id', \Auth::user()->id)->where('month_id', $id)->first();
            return view('pages.annual_fund', compact('month', 'annual_fund', 'month_id', 'route', 'tick'));
        }else{
            return redirect()->back();
        }
    }
}
