<?php

namespace App\Http\Controllers\RotaryFoundationCommittee;

use App\User;
use App\Model\Complete;
use App\Model\Participation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ParticipationInTRFController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tick = User::tickMark();
        $month = Complete::showMonth();
        $participation = Participation::where('user_id', \Auth::user()->id)->where('month_id', 7)->first();
        $month_id = 7;
        $route = explode("/", \Route::current()->uri())[0];
        return view('pages.participation_in_trf', compact('month', 'participation', 'month_id',
            'route', 'tick'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Complete::checkChecked($request->month_id)){

            $global_grant = [
                'first' => $request->global_grant_first,
                'second' => $request->global_grant_second,
                'third' => $request->global_grant_third
            ];
            $district_grant = [
                'first' => $request->district_grant_first,
                'second' => $request->district_grant_second,
                'third' => $request->district_grant_third
            ];
            $club_level = [
                'first' => $request->club_level_first,
                'second' => $request->club_level_second,
                'third' => $request->club_level_third
            ];
            $district_grand = [
                'first' => $request->district_grand_first,
                'second' => $request->district_grand_second,
                'third' => $request->district_grand_third
            ];
            $district_trf = [
                'first' => $request->district_trf_first,
                'second' => $request->district_trf_second,
                'third' => $request->district_trf_third
            ];
            $no_of_peace = [
                'first' => $request->no_of_peace_first,
                'second' => $request->no_of_peace_second,
                'third' => $request->no_of_peace_third
            ];
            $no_of_youth = [
                'first' => $request->no_of_youth_first,
                'second' => $request->no_of_youth_second,
                'third' => $request->no_of_youth_third
            ];



            $data = [
                'id' => $request->id,
                'user_id' => \Auth::user()->id,
                'month_id' => $request->month_id,
                'annual_target' => $request->annual_target,

                'global_grant' => json_encode($global_grant),
                'district_grant' => json_encode($district_grant),
                'club_level' => json_encode($club_level),
                'district_grand' => json_encode($district_grand),
                'district_trf' => json_encode($district_trf),
                'no_of_peace' => json_encode($no_of_peace),
                'no_of_youth' => json_encode($no_of_youth)
            ];

            if($request->id == ""){
                Participation::create($data);
                $notification = array(
                    'message' => 'Form has been successfully saved.',
                    'type' => 'success'
                );
            }else{
                $value = Participation::findOrFail($request->id);
                $value->update($data);
                $notification = array(
                    'message' => 'Form has been successfully updated.',
                    'type' => 'success'
                );
            }
        }else{
            $notification = array(
                'message' => "Failed. The form has already been submitted finally.",
                'type' => 'error'
            );
        }

        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tick = User::tickMark();
        if(Complete::checkMonth($id)){
            $month = Complete::showMonth();
            $month_id = $id;
            $route = explode("/", \Route::current()->uri())[0];
            $participation = Participation::where('user_id', \Auth::user()->id)->where('month_id', $id)->first();
            return view('pages.participation_in_trf', compact('month', 'participation',
                'month_id', 'route', 'tick'));
        }else{
            return redirect()->back();
        }
    }
}
