<?php

namespace App\Http\Controllers\ServiceProjectCommittee;

use App\Model\BasicEducation;
use App\Model\BasicEducationData;
use Illuminate\Http\Request;
use App\Model\Complete;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\User;
class BasicEducationController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tick = User::tickMark();
        $month = Complete::showMonth();
        $basic_education = BasicEducation::where('user_id', \Auth::user()->id)->where('month_id', 7)->first();
        if($basic_education){
            $basic_education_data = BasicEducationData::where('basic_edu_id', $basic_education->id)->get();
        }else{$basic_education_data = null;}
        $month_id = 7;
        $route = explode("/", \Route::current()->uri())[0];
        return view('pages.basic_education', compact('month', 'basic_education', 'month_id',
            'route', 'basic_education_data', 'tick'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Complete::checkChecked($request->month_id)){

            $take['project_name'] = $request->project_name;
            $take['project_activities'] = array_slice($request->project_activities, 0, -1);

            $validator = Validator::make($take, [
                'project_name' => 'required',
                'project_activities.*' => 'required'
            ]);

            if($validator->passes()){
                $data = [
                    'id' => $request->id,
                    'user_id' => \Auth::user()->id,
                    'month_id' => $request->month_id,
                    'project_name' => $request->project_name,
                    'project_address' => $request->project_address
                ];

                if($request->id == ""){
                    $add = BasicEducation::create($data);

                    for($i=0; $i<(count($request->date)-1); $i++){
                        $data1 = [
                            'basic_edu_id' => $add->id,
                            'project_activities' => $request->project_activities[$i],
                            'date' => $request->date[$i],
                            'beneficiaries' => $request->beneficiaries[$i],
                            'outputs' => $request->outputs[$i],
                            'funding_type' => $request->funding_type[$i],
                            'fund_contributions' => $request->fund_contributions[$i]
                        ];
                        BasicEducationData::create($data1);
                    }

                    $notification = array(
                        'message' => 'Form has been successfully saved.',
                        'type' => 'success'
                    );

                }else{
                    $value = BasicEducation::findOrFail($request->id);
                    $value->update($data);

                    for($i=0; $i<(count($request->project_activities)-1); $i++){
                        $data1 = [
                            'id' => $request->basic_edu_data_id[$i],
                            'basic_edu_id' => $value->id,
                            'project_activities' => $request->project_activities[$i],
                            'date' => $request->date[$i],
                            'beneficiaries' => $request->beneficiaries[$i],
                            'outputs' => $request->outputs[$i],
                            'funding_type' => $request->funding_type[$i],
                            'fund_contributions' => $request->fund_contributions[$i]
                        ];
                        if($request->basic_edu_data_id[$i] == ""){
                            BasicEducationData::create($data1);
                        }else{
                            $check = BasicEducationData::findOrFail($request->basic_edu_data_id[$i]);
                            $check->update($data1);
                        }
                    }
                    $notification = array(
                        'message' => 'Form has been successfully updated.',
                        'type' => 'success'
                    );
                }
            }else{
                $notification = array(
                    'message' => 'Project name or project activities field cannot be left empty.',
                    'type' => 'error'
                );
            }
        }else{
            $notification = array(
                'message' => "Failed. The form has already been submitted finally.",
                'type' => 'error'
            );
        }

        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tick = User::tickMark();
        if(Complete::checkMonth($id)){
            $month = Complete::showMonth();
            $month_id = $id;
            $route = explode("/", \Route::current()->uri())[0];
            $basic_education = BasicEducation::where('user_id', \Auth::user()->id)->where('month_id', $id)->first();
            if($basic_education){
                $basic_education_data = BasicEducationData::where('basic_edu_id', $basic_education->id)->get();
            }else{$basic_education_data = null;}
            return view('pages.basic_education', compact('month', 'basic_education',
                'month_id', 'route', 'basic_education_data', 'tick'));
        }else{
            return redirect()->back();
        }
    }
}
