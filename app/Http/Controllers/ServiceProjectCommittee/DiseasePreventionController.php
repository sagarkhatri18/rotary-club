<?php

namespace App\Http\Controllers\ServiceProjectCommittee;

use Illuminate\Http\Request;
use App\Model\Complete;
use App\Model\DiseasePrevention;
use App\Http\Controllers\Controller;
use App\User;
class DiseasePreventionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tick = User::tickMark();
        $month = Complete::showMonth();
        $disease_prevention = DiseasePrevention::where('user_id', \Auth::user()->id)->where('month_id', 7)->first();
        $month_id = 7;
        $route = explode("/", \Route::current()->uri())[0];
        return view('pages.disease_prevention', compact('month', 'disease_prevention', 'month_id',
            'route', 'tick'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Complete::checkChecked($request->month_id)){

            $health_camp = [
                'first' => $request->health_camp_first,
                'second' => $request->health_camp_second,
                'third' => $request->health_camp_third,
                'fourth' => $request->health_camp_fourth,
                'fifth' => $request->health_camp_fifth,
                'sixth' => $request->health_camp_sixth
            ];
            $blood_donation = [
                'first' => $request->blood_donation_first,
                'second' => $request->blood_donation_second,
                'third' => $request->blood_donation_third,
                'fourth' => $request->blood_donation_fourth,
                'fifth' => $request->blood_donation_fifth,
                'sixth' => $request->blood_donation_sixth
            ];
            $training_development = [
                'first' => $request->training_development_first,
                'second' => $request->training_development_second,
                'third' => $request->training_development_third,
                'fourth' => $request->training_development_fourth,
                'fifth' => $request->training_development_fifth,
                'sixth' => $request->training_development_sixth
            ];
            $health_equipment = [
                'first' => $request->health_equipment_first,
                'second' => $request->health_equipment_second,
                'third' => $request->health_equipment_third,
                'fourth' => $request->health_equipment_fourth,
                'fifth' => $request->health_equipment_fifth,
                'sixth' => $request->health_equipment_sixth
            ];
            $health_hygiene = [
                'first' => $request->health_hygiene_first,
                'second' => $request->health_hygiene_second,
                'third' => $request->health_hygiene_third,
                'fourth' => $request->health_hygiene_fourth,
                'fifth' => $request->health_hygiene_fifth,
                'sixth' => $request->health_hygiene_sixth
            ];
            $others = [
                'first' => $request->others_first,
                'second' => $request->others_second,
                'third' => $request->others_third,
                'fourth' => $request->others_fourth,
                'fifth' => $request->others_fifth,
                'sixth' => $request->others_sixth
            ];


            $data = [
                'id' => $request->id,
                'user_id' => \Auth::user()->id,
                'month_id' => $request->month_id,
                'project_name' => $request->project_name,
                'project_address' => $request->project_address,

                'health_camp' => json_encode($health_camp),
                'blood_donation' => json_encode($blood_donation),
                'training_development' => json_encode($training_development),
                'health_equipment' => json_encode($health_equipment),
                'health_hygiene' => json_encode($health_hygiene),
                'others' => json_encode($others)
            ];

            if($request->id == ""){
                DiseasePrevention::create($data);
                $notification = array(
                    'message' => 'Form has been successfully saved.',
                    'type' => 'success'
                );
            }else{
                $value = DiseasePrevention::findOrFail($request->id);
                $value->update($data);
                $notification = array(
                    'message' => 'Form has been successfully updated.',
                    'type' => 'success'
                );
            }
        }else{
            $notification = array(
                'message' => "Failed. The form has already been submitted finally.",
                'type' => 'error'
            );
        }

        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tick = User::tickMark();
        if(Complete::checkMonth($id)){
            $month = Complete::showMonth();
            $month_id = $id;
            $route = explode("/", \Route::current()->uri())[0];
            $disease_prevention = DiseasePrevention::where('user_id', \Auth::user()->id)->where('month_id', $id)->first();
            return view('pages.disease_prevention', compact('month', 'disease_prevention',
                'month_id', 'route', 'tick'));
        }else{
            return redirect()->back();
        }
    }
}
