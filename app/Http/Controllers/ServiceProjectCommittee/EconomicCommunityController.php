<?php

namespace App\Http\Controllers\ServiceProjectCommittee;

use App\User;
use App\Model\EconomicCommunity;
use Illuminate\Http\Request;
use App\Model\Complete;
use App\Http\Controllers\Controller;

class EconomicCommunityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tick = User::tickMark();
        $month = Complete::showMonth();
        $economic_community = EconomicCommunity::where('user_id', \Auth::user()->id)->where('month_id', 7)->first();
        $month_id = 7;
        $route = explode("/", \Route::current()->uri())[0];
        return view('pages.economic_community', compact('month', 'economic_community', 'month_id',
            'route', 'tick'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Complete::checkChecked($request->month_id)){

            $skill_development = [
                'first' => $request->skill_development_first,
                'second' => $request->skill_development_second,
                'third' => $request->skill_development_third,
                'fourth' => $request->skill_development_fourth,
                'fifth' => $request->skill_development_fifth
            ];
            $micro_credit = [
                'first' => $request->micro_credit_first,
                'second' => $request->micro_credit_second,
                'third' => $request->micro_credit_third,
                'fourth' => $request->micro_credit_fourth,
                'fifth' => $request->micro_credit_fifth
            ];
            $support_for_income = [
                'first' => $request->support_for_income_first,
                'second' => $request->support_for_income_second,
                'third' => $request->support_for_income_third,
                'fourth' => $request->support_for_income_fourth,
                'fifth' => $request->support_for_income_fifth
            ];
            $tree_plantation = [
                'first' => $request->tree_plantation_first,
                'second' => $request->tree_plantation_second,
                'third' => $request->tree_plantation_third,
                'fourth' => $request->tree_plantation_fourth,
                'fifth' => $request->tree_plantation_fifth
            ];
            $community_institution = [
                'first' => $request->community_institution_first,
                'second' => $request->community_institution_second,
                'third' => $request->community_institution_third,
                'fourth' => $request->community_institution_fourth,
                'fifth' => $request->community_institution_fifth
            ];
            $others = [
                'first' => $request->others_first,
                'second' => $request->others_second,
                'third' => $request->others_third,
                'fourth' => $request->others_fourth,
                'fifth' => $request->others_fifth
            ];

            $data = [
                'id' => $request->id,
                'user_id' => \Auth::user()->id,
                'month_id' => $request->month_id,
                'project_name' => $request->project_name,
                'project_address' => $request->project_address,

                'skill_development' => json_encode($skill_development),
                'micro_credit' => json_encode($micro_credit),
                'support_for_income' => json_encode($support_for_income),
                'tree_plantation' => json_encode($tree_plantation),
                'community_institution' => json_encode($community_institution),
                'others' => json_encode($others)
            ];

            if($request->id == ""){
                EconomicCommunity::create($data);
                $notification = array(
                    'message' => 'Form has been successfully saved.',
                    'type' => 'success'
                );
            }else{
                $value = EconomicCommunity::findOrFail($request->id);
                $value->update($data);
                $notification = array(
                    'message' => 'Form has been successfully updated.',
                    'type' => 'success'
                );
            }
        }else{
            $notification = array(
                'message' => "Failed. The form has already been submitted finally.",
                'type' => 'error'
            );
        }

        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tick = User::tickMark();
        if(Complete::checkMonth($id)){
            $month = Complete::showMonth();
            $month_id = $id;
            $route = explode("/", \Route::current()->uri())[0];
            $economic_community = EconomicCommunity::where('user_id', \Auth::user()->id)->where('month_id', $id)->first();
            return view('pages.economic_community', compact('month', 'economic_community',
                'month_id', 'route', 'tick'));
        }else{
            return redirect()->back();
        }
    }
}
