<?php

namespace App\Http\Controllers\ServiceProjectCommittee;

use App\Model\MaternalChild;
use Illuminate\Http\Request;
use App\Model\Complete;
use App\Http\Controllers\Controller;
use App\User;

class MaternalChildController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tick = User::tickMark();
        $month = Complete::showMonth();
        $maternal_child = MaternalChild::where('user_id', \Auth::user()->id)->where('month_id', 7)->first();
        $month_id = 7;
        $route = explode("/", \Route::current()->uri())[0];
        return view('pages.maternal_child', compact('month', 'maternal_child', 'month_id', 'route', 'tick'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Complete::checkChecked($request->month_id)){

            $maternal_health_service = [
                'first' => $request->maternal_health_service_first,
                'second' => $request->maternal_health_service_second,
                'third' => $request->maternal_health_service_third,
                'fourth' => $request->maternal_health_service_fourth,
                'fifth' => $request->maternal_health_service_fifth
            ];
            $polio_campaign = [
                'first' => $request->polio_campaign_first,
                'second' => $request->polio_campaign_second,
                'third' => $request->polio_campaign_third,
                'fourth' => $request->polio_campaign_fourth,
                'fifth' => $request->polio_campaign_fifth
            ];
            $health_campaign = [
                'first' => $request->health_campaign_first,
                'second' => $request->health_campaign_second,
                'third' => $request->health_campaign_third,
                'fourth' => $request->health_campaign_fourth,
                'fifth' => $request->health_campaign_fifth
            ];
            $child_health_services = [
                'first' => $request->child_health_services_first,
                'second' => $request->child_health_services_second,
                'third' => $request->child_health_services_third,
                'fourth' => $request->child_health_services_fourth,
                'fifth' => $request->child_health_services_fifth
            ];
            $sustainable_immunization = [
                'first' => $request->sustainable_immunization_first,
                'second' => $request->sustainable_immunization_second,
                'third' => $request->sustainable_immunization_third,
                'fourth' => $request->sustainable_immunization_fourth,
                'fifth' => $request->sustainable_immunization_fifth
            ];
            $others = [
                'first' => $request->others_first,
                'second' => $request->others_second,
                'third' => $request->others_third,
                'fourth' => $request->others_fourth,
                'fifth' => $request->others_fifth
            ];

            $data = [
                'id' => $request->id,
                'user_id' => \Auth::user()->id,
                'month_id' => $request->month_id,
                'project_name' => $request->project_name,
                'project_address' => $request->project_address,

                'maternal_health_service' => json_encode($maternal_health_service),
                'polio_campaign' => json_encode($polio_campaign),
                'health_campaign' => json_encode($health_campaign),
                'child_health_services' => json_encode($child_health_services),
                'sustainable_immunization' => json_encode($sustainable_immunization),
                'others' => json_encode($others)
            ];

            if($request->id == ""){
                MaternalChild::create($data);
                $notification = array(
                    'message' => 'Form has been successfully saved.',
                    'type' => 'success'
                );
            }else{
                $value = MaternalChild::findOrFail($request->id);
                $value->update($data);
                $notification = array(
                    'message' => 'Form has been successfully updated.',
                    'type' => 'success'
                );
            }
        }else{
            $notification = array(
                'message' => "Failed. The form has already been submitted finally.",
                'type' => 'error'
            );
        }

        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tick = User::tickMark();
        if(Complete::checkMonth($id)){
            $month = Complete::showMonth();
            $month_id = $id;
            $route = explode("/", \Route::current()->uri())[0];
            $maternal_child = MaternalChild::where('user_id', \Auth::user()->id)->where('month_id', $id)->first();
            return view('pages.maternal_child', compact('month', 'maternal_child', 'month_id',
                'route', 'tick'));
        }else{
            return redirect()->back();
        }
    }
}
