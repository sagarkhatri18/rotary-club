<?php

namespace App\Http\Controllers\ServiceProjectCommittee;

use App\Model\Complete;
use App\Model\WaterSupply;
use App\Model\WaterSupplyData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\User;

class WaterSupplySanitationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tick = User::tickMark();
        $month = Complete::showMonth();
        $water_supply = WaterSupply::where('user_id', \Auth::user()->id)->where('month_id', 7)->first();
        if($water_supply){
            $water_supply_data = WaterSupplyData::where('water_supply_id', $water_supply->id)->get();
        }else{$water_supply_data = null;}
        $month_id = 7;
        $route = explode("/", \Route::current()->uri())[0];
        return view('pages.water_supply', compact('month', 'water_supply', 'month_id', 'route',
            'water_supply_data', 'tick'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Complete::checkChecked($request->month_id)){

            $take['wash_project_name'] = $request->wash_project_name;
            $take['date'] = array_slice($request->date, 0, -1);

            $validator = Validator::make($take, [
                'wash_project_name' => 'required',
                'date.*' => 'required'
            ]);

            if($validator->passes()){
                $data = [
                    'id' => $request->id,
                    'user_id' => \Auth::user()->id,
                    'month_id' => $request->month_id,
                    'wash_project_name' => $request->wash_project_name,
                    'project_address' => $request->project_address
                ];

                if($request->id == ""){
                    $add = WaterSupply::create($data);

                    for($i=0; $i<(count($request->date)-1); $i++){
                        $data1 = [
                            'water_supply_id' => $add->id,
                            'date' => $request->date[$i],
                            'beneficiaries' => $request->beneficiaries[$i],
                            'outputs' => $request->outputs[$i],
                            'funding_type' => $request->funding_type[$i],
                            'fund_contributions' => $request->fund_contributions[$i]
                        ];
                        WaterSupplyData::create($data1);
                    }

                    $notification = array(
                        'message' => 'Form has been successfully saved.',
                        'type' => 'success'
                    );

                }else{
                    $value = WaterSupply::findOrFail($request->id);
                    $value->update($data);

                    for($i=0; $i<(count($request->date)-1); $i++){
                        $data1 = [
                            'id' => $request->water_supply_data_id[$i],
                            'water_supply_id' => $value->id,
                            'date' => $request->date[$i],
                            'beneficiaries' => $request->beneficiaries[$i],
                            'outputs' => $request->outputs[$i],
                            'funding_type' => $request->funding_type[$i],
                            'fund_contributions' => $request->fund_contributions[$i]
                        ];
                        if($request->water_supply_data_id[$i] == ""){
                            WaterSupplyData::create($data1);
                        }else{
                            $check = WaterSupplyData::findOrFail($request->water_supply_data_id[$i]);
                            $check->update($data1);
                        }
                    }
                    $notification = array(
                        'message' => 'Form has been successfully updated.',
                        'type' => 'success'
                    );
                }
            }else{
                $notification = array(
                    'message' => 'Wash Project name or project completion date cannot be left empty.',
                    'type' => 'error'
                );
            }

        }else{
            $notification = array(
                'message' => "Failed. The form has already been submitted finally.",
                'type' => 'error'
            );
        }

        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tick = User::tickMark();
        if(Complete::checkMonth($id)){
            $month = Complete::showMonth();
            $month_id = $id;
            $route = explode("/", \Route::current()->uri())[0];
            $water_supply = WaterSupply::where('user_id', \Auth::user()->id)->where('month_id', $id)->first();
            if($water_supply){
                $water_supply_data = WaterSupplyData::where('water_supply_id', $water_supply->id)->get();
            }else{$water_supply_data = null;}
            return view('pages.water_supply', compact('month', 'water_supply', 'month_id', 'route',
                'water_supply_data', 'tick'));
        }else{
            return redirect()->back();
        }

    }
}
