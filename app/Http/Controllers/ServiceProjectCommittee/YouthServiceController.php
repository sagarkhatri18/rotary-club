<?php

namespace App\Http\Controllers\ServiceProjectCommittee;

use App\Model\YouthService;
use App\Model\Complete;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class YouthServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tick = User::tickMark();
        $month = Complete::showMonth();
        $youth_service = YouthService::where('user_id', \Auth::user()->id)->where('month_id', 7)->first();
        $month_id = 7;
        $route = explode("/", \Route::current()->uri())[0];
        return view('pages.youth_service', compact('month', 'youth_service', 'month_id', 'route', 'tick'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!Complete::checkChecked($request->month_id)){

            $sponsoring = [
                'first' => $request->sponsoring_first,
                'second' => $request->sponsoring_second,
                'third' => $request->sponsoring_third,
                'fourth' => $request->sponsoring_fourth,
                'fifth' => $request->sponsoring_fifth
            ];
            $joint_service = [
                'first' => $request->joint_service_first,
                'second' => $request->joint_service_second,
                'third' => $request->joint_service_third,
                'fourth' => $request->joint_service_fourth,
                'fifth' => $request->joint_service_fifth
            ];
            $career_counseling = [
                'first' => $request->career_counseling_first,
                'second' => $request->career_counseling_second,
                'third' => $request->career_counseling_third,
                'fourth' => $request->career_counseling_fourth,
                'fifth' => $request->career_counseling_fifth
            ];
            $professional_skills = [
                'first' => $request->professional_skills_first,
                'second' => $request->professional_skills_second,
                'third' => $request->professional_skills_third,
                'fourth' => $request->professional_skills_fourth,
                'fifth' => $request->professional_skills_fifth
            ];
            $others = [
                'first' => $request->others_first,
                'second' => $request->others_second,
                'third' => $request->others_third,
                'fourth' => $request->others_fourth,
                'fifth' => $request->others_fifth
            ];

            $data = [
                'id' => $request->id,
                'user_id' => \Auth::user()->id,
                'month_id' => $request->month_id,
                'project_name' => $request->project_name,
                'project_address' => $request->project_address,

                'sponsoring' => json_encode($sponsoring),
                'joint_service' => json_encode($joint_service),
                'career_counseling' => json_encode($career_counseling),
                'professional_skills' => json_encode($professional_skills),
                'others' => json_encode($others)
            ];

            if($request->id == ""){
                YouthService::create($data);
                $notification = array(
                    'message' => 'Form has been successfully saved.',
                    'type' => 'success'
                );
            }else{
                $value = YouthService::findOrFail($request->id);
                $value->update($data);
                $notification = array(
                    'message' => 'Form has been successfully updated.',
                    'type' => 'success'
                );
            }
        }else{
            $notification = array(
                'message' => "Failed. The form has already been submitted finally.",
                'type' => 'error'
            );
        }

        return redirect()->back()->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tick = User::tickMark();
        if(Complete::checkMonth($id)){
            $month = Complete::showMonth();
            $month_id = $id;
            $route = explode("/", \Route::current()->uri())[0];
            $youth_service = YouthService::where('user_id', \Auth::user()->id)->where('month_id', $id)->first();
            return view('pages.youth_service', compact('month', 'youth_service', 'month_id',
                'route', 'tick'));
        }else{
            return redirect()->back();
        }
    }
}
