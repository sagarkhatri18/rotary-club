<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AdminMark extends Model
{
    protected $fillable = ['club_id','month_id','mark'];

    public static function getMark($club_id, $month_id)
    {
        $admin_mark = AdminMark::where('club_id', $club_id)->where('month_id', $month_id)->first();
        if($admin_mark == null){
            return null;
        }
        else{
            return $admin_mark->mark;
        }
    }

}
