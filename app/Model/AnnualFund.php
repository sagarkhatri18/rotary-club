<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AnnualFund extends Model
{
    protected $fillable = ['user_id','month_id','annual_target','club_contribution','rotary_foundation','phf',
        'mphf','phs','major_donor','others'];
}
