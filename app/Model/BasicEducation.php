<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BasicEducation extends Model
{
    protected $fillable = ['user_id','month_id','project_name','project_address'];

    public function basic_education_data(){
        return $this->hasMany(BasicEducationData::class, 'basic_edu_id', 'id');
    }
}
