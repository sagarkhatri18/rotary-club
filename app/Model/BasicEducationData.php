<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BasicEducationData extends Model
{
    protected $fillable = ['basic_edu_id','project_activities','date','beneficiaries','outputs','funding_type'
                            ,'fund_contributions'];

    public function basic_education(){
        return $this->belongsTo(BasicEducation::class, 'basic_edu_id', 'id');
    }
}
