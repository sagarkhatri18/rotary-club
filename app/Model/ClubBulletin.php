<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ClubBulletin extends Model
{
    protected $fillable = ['user_id', 'month_id', 'no_of_members', 'club_attendance', 'annual_target_percent', 'title',
        'no_of_issue'];
}
