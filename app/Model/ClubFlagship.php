<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ClubFlagship extends Model
{
    protected $fillable = ['user_id','month_id','name_of_club_flagship','partner_name'];

    public function club_flagship_data(){
        return $this->hasMany(ClubFlagshipData::class, 'club_flagship_id', 'id');
    }
}
