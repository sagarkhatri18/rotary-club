<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ClubFlagshipData extends Model
{
    protected $fillable = ['club_flagship_id','activities','project_check','beneficiaries','area','funding_type'];

    public function club_flagships(){
        return $this->belongsTo(ClubFlagship::class, 'id', 'club_flagship_id');
    }
}
