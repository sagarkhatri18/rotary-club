<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ClubMeeting extends Model
{
    protected $fillable = ['user_id', 'month_id', 'no_of_members', 'club_attendance', 'annual_target_percent',
        'guest_speaker', 'classification_talks', 'rotary_program', 'meeting_combined', 'club_business', 'member_presence',
        'total_percent', 'board', 'club_assembly', 'club_committee', 'month_attendance', 'annual_target'];
}
