<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ClubParticipation extends Model
{
    protected $fillable = ['user_id', 'month_id', 'no_of_members', 'club_attendance', 'annual_target_percent'];

    public function club_participants_data(){
        return $this->hasMany(ClubParticipationData::class, 'club_participation_id', 'id');
    }
}
