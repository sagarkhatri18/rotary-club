<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ClubParticipationData extends Model
{
    protected $fillable = ['club_participation_id', 'district_name', 'date', 'no_of_participants', 'event_sponsored'];

    public function club_participants(){
        return $this->belongsTo(ClubParticipation::class, 'id', 'club_participation_id');
    }
}
