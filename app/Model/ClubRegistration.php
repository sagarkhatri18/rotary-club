<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ClubRegistration extends Model
{
    protected $table = "club_registrations";

    protected $fillable = ['user_id', 'month_id', 'no_of_members', 'club_attendance', 'annual_target_percent',
        'members_of_july', 'members_of_this_month', 'club_goals_uploaded', 'club_goals_upgraded'];
}
