<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Complete extends Model
{
    public $timestamps = false;

    protected $fillable = ['user_id', 'month_id', 'check'];

    public function months(){
        return $this->belongsTo(Month::class, 'month_id', 'id');
    }

    public static function showMonth(){
        $month = Complete::with('months')->where('user_id', \Auth::user()->id)->where('check', 1)->get();
        if($month->isEmpty()){
            $m = Month::find(7);
            return array($m);
        }else{
            $arr = array();
            foreach ($month as $m){
                $arr[] = $m->months;
                $nextMonth = Month::where('id', '>', $m->months->id)->first();
                $nextCompleteMonth = Complete::where('id', '>', $m->id)->first();
                if(is_null($nextMonth) AND is_null($nextCompleteMonth))
                {
                    $first = Month::find(1)->first();
                    array_push($arr, $first);
                }
            }
            if(!is_null($nextMonth)){
                array_push($arr, $nextMonth);
            }
            return $arr;
        }
    }

    public static function checkChecked($month_id){
        $value = Complete::where('month_id', $month_id)->where('user_id', \Auth::user()->id)->first();

        if(!empty($value)){
            return boolval($value->check);
        }else{
            return false;
        }
    }

    public static function checkMonth($id){
        $month = Complete::showMonth();
        $arr = [];
        foreach ($month as $m){
            $arr[] = $m->id;
        }
        return boolval(in_array($id, $arr));
    }

    public static function adminClubCheck($month_id, $user_id){
        $value = Complete::where('month_id', $month_id)->where('user_id', $user_id)->first();

        if(!empty($value)){
            return boolval($value->check);
        }else{
            return false;
        }
    }
}
