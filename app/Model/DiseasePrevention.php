<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DiseasePrevention extends Model
{
    protected $fillable = ['user_id','month_id','project_name','project_address','health_camp','blood_donation','training_development',
        'health_equipment','health_hygiene','others'];
}
