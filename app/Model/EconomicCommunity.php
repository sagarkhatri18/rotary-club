<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EconomicCommunity extends Model
{
    protected $fillable = ['user_id','month_id','project_name','project_address','skill_development','micro_credit',
        'support_for_income','tree_plantation','community_institution','others'];
}
