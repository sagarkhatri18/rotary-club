<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MaternalChild extends Model
{
    protected $fillable = ['user_id','month_id','project_name','project_address','maternal_health_service','polio_campaign',
        'health_campaign','child_health_services','sustainable_immunization','others'];
}
