<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MediaCoverage extends Model
{
    protected $fillable = ['user_id','month_id'];

    public function media_coverage_data(){
        return $this->hasMany(MediaCoverageData::class, 'media_coverage_id', 'id');
    }
}
