<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MediaCoverageData extends Model
{
    protected $fillable = ['media_coverage_id','message','date','times_coverage','print_name','visual_name','audio_name',
        'social_media'];

    public function media_coverage(){
        return $this->belongsTo(MediaCoverage::class, 'id', 'media_coverage_id');
    }
}
