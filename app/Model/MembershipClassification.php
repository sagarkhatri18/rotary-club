<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MembershipClassification extends Model
{
    protected $fillable = ['user_id', 'month_id', 'no_of_members', 'annual_target_no', 'annual_target_percent'];

    public function membership_classification_data(){
        return $this->hasMany(MembershipClassificationData::class, 'mem_class_id', 'id');
    }
}
