<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MembershipClassificationData extends Model
{
    protected $fillable = ['mem_class_id', 'classification_categories_fulfilled',
        'no_of_club_members', 'classification_categories_unfulfilled'];

    public function membership_classification(){
        return $this->belongsTo(MembershipClassification::class, 'id', 'mem_class_id');
    }
}
