<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MembershipDevelopment extends Model
{
    protected $fillable = ['user_id', 'month_id', 'no_of_members', 'annual_target_no', 'annual_target_percent',
        'new_members_orientation', 'club_officers', 'district_membership', 'membership_others', 'rotary_club', 'rotaract_club',
        'interact_club', 'rotary_community', 'members_satisfaction', 'health_check', 'development_others'];
}
