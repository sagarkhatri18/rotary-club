<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MembershipGrowth extends Model
{
    protected $fillable = ['user_id', 'month_id', 'no_of_members', 'annual_target_no', 'annual_target_percent',
        'loss_of_members', 'loss_of_female_members', 'loss_of_youth_members'];

}
