<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Month extends Model
{
    public $timestamps = false;

    protected $fillable = ['name'];

    public function completes(){
        return $this->hasMany(Complete::class, 'id', 'month_id');
    }
}
