<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Participation extends Model
{
    protected $fillable = ['user_id','month_id','annual_target','global_grant','district_grant','club_level',
        'district_grand','district_trf','no_of_peace','no_of_youth'];
}
