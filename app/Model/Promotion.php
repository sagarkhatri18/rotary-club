<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    protected $fillable = ['user_id','month_id','name_of_school','name_of_business','name_of_public_places','test_banners',
        'project_information','club_brochures','club_pamphlets'];
}
