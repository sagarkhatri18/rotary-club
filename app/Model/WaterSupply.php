<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class WaterSupply extends Model
{
    protected $fillable = ['user_id','month_id','wash_project_name','project_address'];

    public function water_supply_data(){
        return $this->hasMany(WaterSupplyData::class, 'water_supply_id', 'id');
    }
}
