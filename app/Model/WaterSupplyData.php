<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class WaterSupplyData extends Model
{
    protected $fillable = ['water_supply_id','date','beneficiaries','outputs','funding_type','fund_contributions'];

    public function water_supply(){
        return $this->belongsTo(WaterSupply::class, 'water_supply_id', 'id');
    }
}
