<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class YouthService extends Model
{
    protected $fillable = ['user_id','month_id','project_name','project_address','sponsoring','joint_service',
        'career_counseling','professional_skills','others'];
}
