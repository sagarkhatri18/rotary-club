<?php

namespace App;

use App\Model\AnnualFund;
use App\Model\BasicEducation;
use App\Model\ClubFlagship;
use App\Model\DiseasePrevention;
use App\Model\EconomicCommunity;
use App\Model\MaternalChild;
use App\Model\MediaCoverage;
use App\Model\MembershipClassification;
use App\Model\MembershipDevelopment;
use App\Model\MembershipGrowth;
use App\Model\Participation;
use App\Model\Promotion;
use App\Model\WaterSupply;
use App\Model\YouthService;
use Illuminate\Support\Facades\Auth;
use App\Model\ClubBulletin;
use App\Model\ClubMeeting;
use App\Model\ClubParticipation;
use App\Model\ClubRegistration;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['username', 'password', 'club_name', 'role'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function tickMark(){
        $id = Auth::user()->id;
        $arr = [];
        !ClubMeeting::where('user_id', $id)->get()->isEmpty()? $arr['club_meeting'] = true : $arr['club_meeting'] = false;

        !ClubBulletin::where('user_id', $id)->get()->isEmpty()? $arr['club_bulletin'] = true : $arr['club_bulletin'] = false;

        !ClubParticipation::where('user_id', $id)->get()->isEmpty()? $arr['club_participation'] = true : $arr['club_participation'] = false;

        !ClubRegistration::where('user_id', $id)->get()->isEmpty()? $arr['club_registration'] = true : $arr['club_registration'] = false;

        !MembershipClassification::where('user_id', $id)->get()->isEmpty()? $arr['membership_class'] = true : $arr['membership_class'] = false;

        !MembershipGrowth::where('user_id', $id)->get()->isEmpty()? $arr['membership_growth'] = true : $arr['membership_growth'] = false;

        !MembershipDevelopment::where('user_id', $id)->get()->isEmpty()? $arr['membership_dev'] = true : $arr['membership_dev'] = false;

        !WaterSupply::where('user_id', $id)->get()->isEmpty()? $arr['water_supply'] = true : $arr['water_supply'] = false;

        !DiseasePrevention::where('user_id', $id)->get()->isEmpty()? $arr['disease_prevention'] = true : $arr['disease_prevention'] = false;

        !EconomicCommunity::where('user_id', $id)->get()->isEmpty()? $arr['eco_comm'] = true : $arr['eco_comm'] = false;

        !MaternalChild::where('user_id', $id)->get()->isEmpty()? $arr['maternal_child'] = true : $arr['maternal_child'] = false;


        !BasicEducation::where('user_id', $id)->get()->isEmpty()? $arr['basic_edu'] = true : $arr['basic_edu'] = false;

        !YouthService::where('user_id', $id)->get()->isEmpty()? $arr['youth_service'] = true : $arr['youth_service'] = false;

        !MediaCoverage::where('user_id', $id)->get()->isEmpty()? $arr['media_coverage'] = true : $arr['media_coverage'] = false;

        !ClubFlagship::where('user_id', $id)->get()->isEmpty()? $arr['club_flagship'] = true : $arr['club_flagship'] = false;

        !Promotion::where('user_id', $id)->get()->isEmpty()? $arr['promotion'] = true : $arr['promotion'] = false;


        !AnnualFund::where('user_id', $id)->get()->isEmpty()? $arr['annual_fund'] = true : $arr['annual_fund'] = false;

        !Participation::where('user_id', $id)->get()->isEmpty()? $arr['participation'] = true : $arr['participation'] = false;
        return $arr;
    }
}
