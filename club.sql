-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 17, 2019 at 06:16 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `club`
--

-- --------------------------------------------------------

--
-- Table structure for table `annual_funds`
--

CREATE TABLE `annual_funds` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `month_id` bigint(20) UNSIGNED NOT NULL,
  `annual_target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `club_contribution` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `rotary_foundation` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `phf` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `mphf` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `phs` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `major_donor` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `others` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `basic_education`
--

CREATE TABLE `basic_education` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `month_id` bigint(20) UNSIGNED NOT NULL,
  `project_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `basic_education_data`
--

CREATE TABLE `basic_education_data` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `basic_edu_id` bigint(20) UNSIGNED NOT NULL,
  `project_activities` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `beneficiaries` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `outputs` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `funding_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fund_contributions` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `club_bulletins`
--

CREATE TABLE `club_bulletins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `month_id` bigint(20) UNSIGNED NOT NULL,
  `no_of_members` int(11) NOT NULL,
  `club_attendance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `annual_target_percent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_of_issue` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `club_flagships`
--

CREATE TABLE `club_flagships` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `month_id` bigint(20) UNSIGNED NOT NULL,
  `name_of_club_flagship` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `partner_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `club_flagship_data`
--

CREATE TABLE `club_flagship_data` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `club_flagship_id` bigint(20) UNSIGNED NOT NULL,
  `activities` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_check` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `beneficiaries` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `funding_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `club_meetings`
--

CREATE TABLE `club_meetings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `month_id` bigint(20) UNSIGNED NOT NULL,
  `no_of_members` int(11) NOT NULL,
  `club_attendance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `annual_target_percent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guest_speaker` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `classification_talks` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `rotary_program` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `meeting_combined` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `club_business` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `member_presence` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `total_percent` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `board` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `club_assembly` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `club_committee` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `month_attendance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `annual_target` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `club_participations`
--

CREATE TABLE `club_participations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `month_id` bigint(20) UNSIGNED NOT NULL,
  `no_of_members` int(11) NOT NULL,
  `club_attendance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `annual_target_percent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `club_participation_data`
--

CREATE TABLE `club_participation_data` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `club_participation_id` bigint(20) UNSIGNED NOT NULL,
  `district_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date DEFAULT NULL,
  `no_of_participants` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_sponsored` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `club_registrations`
--

CREATE TABLE `club_registrations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `month_id` bigint(20) UNSIGNED NOT NULL,
  `no_of_members` int(11) NOT NULL,
  `club_attendance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `annual_target_percent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `members_of_july` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `members_of_this_month` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `club_goals_uploaded` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `club_goals_upgraded` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `completes`
--

CREATE TABLE `completes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `month_id` bigint(20) UNSIGNED DEFAULT NULL,
  `check` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `disease_preventions`
--

CREATE TABLE `disease_preventions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `month_id` bigint(20) UNSIGNED NOT NULL,
  `project_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `health_camp` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `blood_donation` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `training_development` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `health_equipment` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `health_hygiene` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `others` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `economic_communities`
--

CREATE TABLE `economic_communities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `month_id` bigint(20) UNSIGNED NOT NULL,
  `project_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skill_development` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `micro_credit` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `support_for_income` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `tree_plantation` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `community_institution` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `others` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `maternal_children`
--

CREATE TABLE `maternal_children` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `month_id` bigint(20) UNSIGNED NOT NULL,
  `project_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `maternal_health_service` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `polio_campaign` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `health_campaign` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `child_health_services` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `sustainable_immunization` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `others` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `media_coverages`
--

CREATE TABLE `media_coverages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `month_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `media_coverage_data`
--

CREATE TABLE `media_coverage_data` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `media_coverage_id` bigint(20) UNSIGNED NOT NULL,
  `message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `times_coverage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `print_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visual_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `audio_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_media` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `membership_classifications`
--

CREATE TABLE `membership_classifications` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `month_id` bigint(20) UNSIGNED NOT NULL,
  `no_of_members` int(11) NOT NULL,
  `annual_target_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `annual_target_percent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `membership_classification_data`
--

CREATE TABLE `membership_classification_data` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `mem_class_id` bigint(20) UNSIGNED NOT NULL,
  `classification_categories_fulfilled` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_of_club_members` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `classification_categories_unfulfilled` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `membership_developments`
--

CREATE TABLE `membership_developments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `month_id` bigint(20) UNSIGNED NOT NULL,
  `no_of_members` int(11) NOT NULL,
  `annual_target_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `annual_target_percent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `new_members_orientation` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `club_officers` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `district_membership` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `membership_others` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `rotary_club` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `rotaract_club` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `interact_club` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `rotary_community` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `members_satisfaction` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `health_check` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `development_others` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `membership_growths`
--

CREATE TABLE `membership_growths` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `month_id` bigint(20) UNSIGNED NOT NULL,
  `no_of_members` int(11) NOT NULL,
  `annual_target_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `annual_target_percent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `loss_of_members` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `loss_of_female_members` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `loss_of_youth_members` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_09_03_144822_create_months_table', 1),
(4, '2019_09_03_151102_create_club_meetings_table', 1),
(5, '2019_09_03_160004_create_completes_table', 1),
(6, '2019_09_06_141555_create_club_bulletins_table', 1),
(7, '2019_09_06_155840_create_club_participations_table', 1),
(8, '2019_09_06_160921_create_club_participation_data_table', 1),
(9, '2019_09_07_060427_create_club_registrations_table', 1),
(10, '2019_09_07_071055_create_membership_classifications_table', 1),
(11, '2019_09_07_071110_create_membership_classification_data_table', 1),
(12, '2019_09_07_082425_create_membership_growths_table', 1),
(13, '2019_09_07_090059_create_membership_developments_table', 1),
(14, '2019_09_07_105528_create_water_supplies_table', 1),
(15, '2019_09_07_105537_create_water_supply_data_table', 1),
(16, '2019_09_07_115738_create_disease_preventions_table', 1),
(17, '2019_09_07_124051_create_economic_communities_table', 1),
(18, '2019_09_07_132259_create_maternal_children_table', 1),
(19, '2019_09_07_135356_create_basic_education_table', 1),
(20, '2019_09_07_135403_create_basic_education_data_table', 1),
(21, '2019_09_07_155716_create_youth_services_table', 1),
(22, '2019_09_07_161945_create_annual_funds_table', 1),
(23, '2019_09_07_164424_create_participations_table', 1),
(24, '2019_09_07_170516_create_promotions_table', 1),
(25, '2019_09_08_145848_create_club_flagships_table', 1),
(26, '2019_09_08_145916_create_club_flagship_data_table', 1),
(27, '2019_09_08_161106_create_media_coverages_table', 1),
(28, '2019_09_08_161529_create_media_coverage_data_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `months`
--

CREATE TABLE `months` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `months`
--

INSERT INTO `months` (`id`, `name`) VALUES
(1, 'January'),
(2, 'February'),
(3, 'March'),
(4, 'April'),
(5, 'May'),
(6, 'June'),
(7, 'July'),
(8, 'August'),
(9, 'September'),
(10, 'October'),
(11, 'November'),
(12, 'December');

-- --------------------------------------------------------

--
-- Table structure for table `participations`
--

CREATE TABLE `participations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `month_id` bigint(20) UNSIGNED NOT NULL,
  `annual_target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `global_grant` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `district_grant` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `club_level` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `district_grand` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `district_trf` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `no_of_peace` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `no_of_youth` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `promotions`
--

CREATE TABLE `promotions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `month_id` bigint(20) UNSIGNED NOT NULL,
  `name_of_school` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_of_business` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_of_public_places` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `test_banners` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `project_information` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `club_brochures` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `club_pamphlets` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `club_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `club_name`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', '$2y$10$Y2LzXwk1043fZs4zYMtVB.G5Ar8LgTvj8nok50upNfr8dQiW1QZzi', '', 'admin', NULL, '2019-09-17 10:29:51', '2019-09-17 10:29:51'),
(2, '16203', '$2y$10$eqneoMF/K1UNNpcXiIWGzO4LrSZRFZ.5HRlqZbee/6b9/hxiTd7L2', 'Biratnagar', 'user', NULL, '2019-09-17 10:29:51', '2019-09-17 10:29:51'),
(3, '16204', '$2y$10$HhVp138rgvU0hKaBidaGWubbNkGQ3QYaURRVrfmSq3E2nZpxQZvcO', 'Kathmandu', 'user', NULL, '2019-09-17 10:29:51', '2019-09-17 10:29:51'),
(4, '22892', '$2y$10$/RTpHWR3NsmrfeYWglI5eesnhwPb0DhI1kMzADGXDbD4Z/k4EfHki', 'Dharan', 'user', NULL, '2019-09-17 10:29:51', '2019-09-17 10:29:51'),
(5, '23126', '$2y$10$xEOnrk2J7dQGmnpwlWz6Ju6eJ9tnsNCKhBl2d0eiJ2z937Lq6fNZS', 'Patan', 'user', NULL, '2019-09-17 10:29:51', '2019-09-17 10:29:51'),
(6, '25212', '$2y$10$QqNbRsGY1jrrluoBaxPwpe9CRsQqTD21C319OQCH/4RwQsjMDGWWa', 'Birganj', 'user', NULL, '2019-09-17 10:29:51', '2019-09-17 10:29:51'),
(7, '26776', '$2y$10$wMSmhPjX9VKL4LQp.FmAr.TBZV4lx839sT/5G9dXrP8sdBmnbGi0S', 'Kathmandu Mid-Town', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(8, '27964', '$2y$10$XLnfUgbW31ss5ChYvQGbquw80Zo/ts.4hfp6EzX0Qd9Uktfsv6Imm', 'Chitwan', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(9, '28748', '$2y$10$56gifYPi8OYWSLHW1QJ2ZeAAWNpNz2FhsA4PsRadmSTK4gRvHEZd.', 'Hetauda', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(10, '29857', '$2y$10$cR1wKMwF5uF/9/41N2onIuHHMznfWBR30sAklNxS4Hz2Zk2ZrASQi', 'Butwal', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(11, '29915', '$2y$10$mnpnmkaYGFh.dRsq1uaD/OmOprkgMyMDg2WB6XrXnMdKLJNK.Zo0m', 'Kathmandu North', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(12, '31143', '$2y$10$Y5oebO8uola8YTeg.THpvueI6iEZK9JGzwuip9n2fkDcyRTxM.g.O', 'Patan West', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(13, '31524', '$2y$10$XIokrH8EOr8WRNhLPwIqH.nyqFVnnoT9TnwqBXi1bGKLVi4TOEmty', 'Lumbini Siddharthanagar', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(14, '31764', '$2y$10$9/pDvEupgCYnuBni1uMmoeb/VZlcuVvbwx70HRqWZRi5UUbKkpz1C', 'Yala', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(15, '50406', '$2y$10$IdxlREb8IHJP7tAHi412lOeRwdIBNRXcyJvqz.IeCFgZ5GTYL/Z92', 'Itahari', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(16, '50507', '$2y$10$vogBU82lZvkcddLkWIfdrO6.eO7/MwJ6Rn4f9MW95J.9VyL4.ftiy', 'Pokhara', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(17, '50576', '$2y$10$d0SM0sXQhWrtyaN/ps7avejOwIIU5oHUJWvJBhbKFIXN2lOGHJUCi', 'Dhulikhel', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(18, '50694', '$2y$10$zDc6oN5a0zA8yvqMa58CI.yUWyH7WmVVwLOEygD.mt/ftI4knj5uG', 'Patan South', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(19, '50801', '$2y$10$FVduxSqytn8L3HJ0yyMz6.GSBVdFzmtDTKSROOyNy25EpZCFY/Orm', 'Lalitpur', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(20, '51038', '$2y$10$npblS8MH6eYZwIHpWX0Bn.whHm3Xy6A8Ly4k0tXU.yx737cUvlEGm', 'Pashupati Kathmandu', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(21, '52113', '$2y$10$xhg1PXDt6cao9U8RynUjFekn3lQPnAsmhm3y.ue7MeOIEvcXZDkla', 'Dillibazar-Kathmandu', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(22, '53328', '$2y$10$yR.t1VJUw5/ctUVpXqNblO71LL5OVXgW9xySQxe1Jzj1fSSLAx2kW', 'Kantipur Kathmandu', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(23, '53734', '$2y$10$FjvLiDTxzB2iJypsjcPjwOUTzzHTkgvIuIfop55nh2Fr1HNZlpQvG', 'Ratnanagar', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(24, '53984', '$2y$10$eqsDCX1ryVuraky0lKO3UOXhTGEYus4tKY2vjjmHdoCPlNED9bXP2', 'Damauli', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(25, '53985', '$2y$10$G4W1Bi0YIJC4Yx2jth2nR.I/4qlWAHQsbnEmmxvDjq4IPC48qcsBq', 'Mount Everest Lalitpur', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(26, '55855', '$2y$10$Io3Knx.krPVcO0d.lUCg0OzZR5fB/HAxivXNmFWJ8nVkXt.gb83vy', 'Balaju', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(27, '56011', '$2y$10$JI4YzFavSPpH53II01XXn.h9fXqf0tRk/5B9fzGr4FKz8/APpzRf2', 'Kasthamandap Kathmandu', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(28, '56012', '$2y$10$c5wOSFiwCMRdGn8JLS4Iw.8D5iQoBSriEzp9TSBUfYAnopL.WXhn2', 'Biratnagar Mid Town', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(29, '56013', '$2y$10$1bptBg/okCAgsPMis7.3geIHGjYs.8YacjGImA2VEi3V0hiVTFB6G', 'Kopundol', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(30, '56014', '$2y$10$IF6TW0mcuAiu5iRJzssVsOZy90UpxzknjZA/gyu7oI1xh8CJQNJSq', 'Palpa Tansen', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(31, '56015', '$2y$10$no0d3tcD7fxceyOboainZ.HI7CifZFtoDZIEZ7o0oqLsSYetuICku', 'Kathmandu West', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(32, '56016', '$2y$10$JXrgqnSRF0lIBIm60SA8iOMWgw3pJMxBGxj3XS4YpBVu05CR5q26m', 'Butwal South', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(33, '56780', '$2y$10$yM/hvCdNWQse0FmvPwGxBezlfUEw6oDICCSyr7AOzaYi.xbihXBm2', 'Kavre-Banepa', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(34, '57536', '$2y$10$7Y5qK97ppLfngU91Xq9dJ.B4TB2dLwFpm6qvx2VfdiUOT67pzUM02', 'Thamel-Kathmandu', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(35, '58448', '$2y$10$oU3Mqp3ofDxc5xss7HSRL.pQ38vrZBaUiHfw7fLynaL2twor0Eucy', 'Butwal-Downtown', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(36, '60167', '$2y$10$PtIpDp5wYRmCYQko9R4Yx.4RBLRhmgI2.pYNtvmI9eYnIziTiCCYO', 'Rajdhani', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(37, '60434', '$2y$10$YTFeYnh2ie4vjmpSRStXyOfeNDVldA/ZLIFoxBgc53ylhlw58qsGe', 'Narayangarh', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(38, '60435', '$2y$10$hcS3toPSYgypm2vUIy/K4e1bGHKMLS6ndfha/xw1WbqNJXPfSBCcC', 'Gorkha', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(39, '60675', '$2y$10$kcVZ2wwWuh6MstCJvYcGUOZ.51G6JoQLtoHz2PXjxNqWroe2RraoC', 'Pokhara Mid Town', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(40, '60815', '$2y$10$HZ6uSsn/Tt9QNBDNMT87TuroOae7qpkzfvXHI2t.Enpr8Kg8B4Ul2', 'Charumati Kathmandu', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(41, '60878', '$2y$10$MzPt00WZLkFz70h.L1GuGeCOc76xL6hwz5TuW2NoXrypPDUc.bsgC', 'Tripureswor', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(42, '60968', '$2y$10$G4Y1JR4UQG9IEffOBs1JSu02ykF2MKSqBWizwYAZZrwY6eQs8X8Hy', 'Dharan Ghopa', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(43, '62759', '$2y$10$XVdKlbR.z7enu.4sjEoWJ.k6cc5dkby1wF7advF01rIkFSGEqjkDi', 'Jawalakhel', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(44, '62760', '$2y$10$P2ZOsZX48Ve5fOMdD9te8esEcIdBqE9YihRrLHplOBfLwnBAFFEKS', 'Bhaktapur', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(45, '62761', '$2y$10$3eqz7yKT5ln0eCAqJ4ooJeT1qI1WQet4oSCq3V19H1OulDn/V8Gkm', 'Patan Durbar Square', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(46, '62762', '$2y$10$08EPLTq60IdcAf28/JW7q.HISgjebj53msqMHOK4IyVNyTDs3BezC', 'Narayani Mid Town', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(47, '62763', '$2y$10$tNgiYfJdexErrtGG6o1MWOntaanUsDfLSiTmkFbqG/vR46BvPGjv.', 'Mahabouddha', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(48, '63296', '$2y$10$rYus57.kz65aNnpm5m8sd.RNfUym5zAhBmnjGwRxFJ/OA/w9BwRsG', 'The Himalayan Gurkhas', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(49, '63378', '$2y$10$wO.ZqPjnmp1049oNRBsQLuvYAbFyfe25g8pS6G8jkoem6h0da2nQS', 'Bharatpur', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(50, '63380', '$2y$10$YJ90hQxnJoIna8Rt5TKM/.k8Pevx5nXQ5GyoaWXeiIiMBtaaYkJTW', 'Birgunj Metropolis', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(51, '63381', '$2y$10$s0VjOS9uUTYa.Hawtb9Jde8flwAFhumOnhaCYSUHzU0UVu5oLt33W', 'Himalaya Patan', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(52, '63636', '$2y$10$80xmI9z8hWWdK8S2dQeq5Os7T9vlVrxB8FrU/9qc6YG8gqghYpohi', 'Swoyambhu', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(53, '63637', '$2y$10$pqepLF0qKTxwqsEPxe3NYe6EDLYlTjdDlplqchG0gkQspUuAnTm2W', 'Bagmati Kathmandu', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(54, '63638', '$2y$10$eZLso8tbtBV/RhNWm9nkQe2KWzyV8wcj2ess9/HVfbS0aJNOdy0y6', 'Pokhra Fishtail', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(55, '63859', '$2y$10$7fBGr9GXShYNCkMOyRe/hOVnSXRZfOwuFS4ZCEgRO2k5eDGI8EM.K', 'Kathmandu North East', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(56, '64060', '$2y$10$ctbufffAavqUZbsGHr7cgulxltcCq1ljeo9b4lyoGs8UVb11pFbdi', 'Kathmandu Metro', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(57, '64061', '$2y$10$1AOuzRqckszY.UxuNvekzuFQMPj76fv6LGLaxKvwB/bVsLLS.Dx7G', 'Budhanilkantha', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(58, '64062', '$2y$10$0W89Z/jz4YUS/iqw91L9nuHzmbQl8eTtwc80CFl.57SnDh2eD3XQm', 'Rudramati Kathmandu', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(59, '64357', '$2y$10$L6JE4QcFWIISHaAYsI7uLOEZvPGZz5tnF/DJQCgI.3ngV/wJbmuOe', 'Nagarjun', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(60, '65321', '$2y$10$YOotMbaKyCB3Lq/eoxdWyuvlfLLCpCSPqhX33VdBrIGjR6ebf.4Ge', 'New Road City Kathmandu', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(61, '69937', '$2y$10$V4Am.Yan6p4MM8CSbewFc.8EXRZYmW7SvYvnpHrqUdz2H5O3XLb.q', 'Biratnagar Down Town', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(62, '71663', '$2y$10$haVz.U5ThnlmeUt06NTHDOAiZWsvFlAtNOdEgd00ysQjh3ZGaPmxS', 'Lamjung', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(63, '71857', '$2y$10$OsTqRHenkcYMT2AETUTwb.8OONaVZbKsV.7oAcYdbTC07nybjCZ/.', 'Pokhara Annapurna', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(64, '74282', '$2y$10$RJjETGb9dOObBL6j2ulMjeG8mce.iE03dw9uExXniQ5kFBfM9iTWW', 'Baglung', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(65, '76761', '$2y$10$x.1oDrrTnVUqtmw1Ws1KMOeW43Bs2d9ECInxN9vATlyyXcTP/UXsi', 'Tulsipur City', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(66, '79555', '$2y$10$RzJiLW.HVrYWtmL.Ruq0qek8ycH1lvGrPKeAoRLC2sQ6rK5pKuz/a', 'Jawalakhel-Manjushree', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(67, '79915', '$2y$10$U49qTaY9tSlMu/PZ7gNvce2Zs/EEDiJCxNyO.T/E6wxnGGcDmZ.b.', 'Baneshwor', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(68, '80893', '$2y$10$1F016l6yPhVSNDLaAjfFpOCE4MQd3KwUANrj9pwOy0Gj9oIcLTtwO', 'Chandragiri', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(69, '81318', '$2y$10$SrDKmeQA2TiyF/ci4xOiduw77Qp8EPkOwlkW.hacUuwvxitFRJ5qW', 'Lalitpur Mid Town', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(70, '82616', '$2y$10$75IRezBLS3NkLOGYc4p2IOhjlu0Pez.ZRjiq6LsV6Txm1LDm0z78i', 'Sainbu Bhainsepati', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(71, '82923', '$2y$10$71S988VbxxbF1mpE.5afKO9qFuseddVVP4FWJPH0NVGPAQdv.s5.W', 'Kakarvitta', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(72, '83172', '$2y$10$rJaScj13ddrSUWq7oNGl1e.x0qvWUnKcrIjPgw695yEoNwzwrOLwW', 'Durbarmarg', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(73, '83175', '$2y$10$6tBGPjq0aw1JoMrGqodwF.chjBLbSnKfFKtudDHEiDvvfLTjNXpLW', 'Parbat', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(74, '83243', '$2y$10$Y4M1cQVgoBPOaLFCMZesX.gFGc6fWR.Y3tENwvB6ealXqxU4QZ1K.', 'Birtamode', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(75, '83560', '$2y$10$JQLHZ/P0raqT0KkqFnUyIuvAm60ws/TpSYn3LqRaBOkDv74AMqKQy', 'Lekhnath', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(76, '83601', '$2y$10$RdTBLSR.Ip9gi1pq4BH0OOcDsI8cOBDZfzJ0MEhEwvVvbQ9n/lOUe', 'Pokhara Newroad', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(77, '83999', '$2y$10$8ejPoeU1TQzfz1CLOV0KT.zGuT2AC3Sfv77ND6/VKIFHlIQollI96', 'Dang', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(78, '84013', '$2y$10$Pewx8qJVOW1qj5LtW5gJnePe13V4N8pWIQYyxTgX7ecrYTnLYnauK', 'Madhyapur', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(79, '84190', '$2y$10$AnghIXRw2WXoZWMpV6fgqey9aR4Qy2dfKo3plPZWIdtLQWgMzB7X6', 'Damak', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(80, '84382', '$2y$10$EL9g3iw8SI.eH7MfOlEq6.YcLH/CeKQ6bseL2pY6V0Kpguezzxfmq', 'Tilottama-Rupandehi', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(81, '84427', '$2y$10$MEG67.VIcZMxltaktEc7Ge6Pr4OtJWw2vzz1aYVvD5Lx44syaPBZi', 'Thimphu', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(82, '84437', '$2y$10$9cBNN86zBiGDpcB23suof.3HOnGtbhEgV2dO4v8D.Cb/N.Q90rJji', 'Rupandehi', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(83, '85160', '$2y$10$AaHjsqlcN0ynJvvk4FgHdOceW6nVHxOVJJwujpLP8H824JbnEsiny', 'E-Club of District 3292', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(84, '85219', '$2y$10$zlkcTuFnXdMIJNubu.Xr.u3yPAo3Ph1Km3H6ryfrcc0prHKXUkuO.', 'Dhangadhi', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(85, '85226', '$2y$10$.lSreb5F2g.v86u6gE4znO2i5p9puCpntOsAujxuCKzbkXk/sEX8W', 'Kapilvastu', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(86, '85767', '$2y$10$aeDATxY4IlOUQlTb1G4dje7cmE4DZb/GYcpZoot6jEj0yyRAzxa7i', 'Birtamode Mid-Town', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(87, '85872', '$2y$10$LwZsGJehyDxBHwxqcpwAo.8zfRl7LwkTm0UTqIWRoGG6TgZlHDpZy', 'Bhadrapur', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(88, '85919', '$2y$10$S.A5CNKo1vghaZYmr391wO1bo2G3a0aS9A6uk6HoxJdUfjvTTAzD2', 'Bhadgaon', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(89, '86741', '$2y$10$TC66gTytTtDzXXzvmyyi.eB2IWX.KgliKnklae6Ie0jMRCKh2.4jy', 'Makawanpur', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(90, '87103', '$2y$10$bvQbu6DLzuAqwf78Tlmose29N1Cx2wBOQ0Y3OzauxV78BMSViMgja', 'Sainamaina', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(91, '87222', '$2y$10$98yBwNV3c1UM5qeWGO4z3u2Mk1iR0x8oUCtab89sjZgzzl8u1024e', 'Biratnagar Central', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(92, '87238', '$2y$10$mSENk/J9Z4U/htBj0Hx5RuWlQB6p9mRTmGKMLW/KpNQtDmBi5lv.S', 'Urlabari', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(93, '87245', '$2y$10$8okjph.uo.iKI0gQO2IBCuZ87YX7WSjD32nR84/578GzdDEQHSoi2', 'Waling', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(94, '87257', '$2y$10$alBtoyiIGE.8FVgvtwPjiu7NpGO0aaY3.aL9YGEsLxjryE6EH3CGO', 'Central Butwal', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(95, '87325', '$2y$10$Fs7C8yL3t5SAsEyAbSUSCOCzeM7HV.EVzahW388nsIeyVN.2hviHy', 'Dhading', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(96, '87395', '$2y$10$vbv0oUKSQ8OOvdEaVHWwbOIEcP/FEKvk7L80a57wDj7fWWyEs/yZC', 'Panauti', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(97, '87446', '$2y$10$kJ5h6ijYlZoR8GLdm3S9UObCRXHgExqPDGTyEHQQIYgxnnhywP2ve', 'Arghakhanchi', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(98, '87734', '$2y$10$JvUV/cot1MtlNIO23qd3oulwbqtwUcCcaQTSDtkDvkZ3ls.Vxm5ua', 'Shukalagandaki', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(99, '87737', '$2y$10$2AMFeismjLj6InKz/Jl7VOIf30YETbC9ILgXYJ9l78UrSEpML7YeG', 'Biratnagar Fusion', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(100, '87762', '$2y$10$GtqvvFzC/5dmDIpfFQPuAumHOUbBvDZpAd0lnGjXgdG0MjxoXtMCy', 'Gongabu', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(101, '87769', '$2y$10$q3BRqHecGKqZlSMIHk6t4udRPzSIu3bFBgQTh2/7qLXhJyt1aIJrq', 'Matribhumi Baluwatar', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(102, '88035', '$2y$10$Ez16EyoKCeMUNbF7AhIb0.8/zcIvJQSZGjvAc2Qr84hmxIiqEDSuG', 'Central Lumbini', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(103, '88037', '$2y$10$ns1NekvTAvCZkg3vf/4WqucCJVsdPe6.7UthYwuqfNDDV5ixNWq.K', 'Sankhu', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(104, '88039', '$2y$10$.N0ALIz/MjO/HJ.ejXDzE.7zP1okDMozZkKMGSf5twkROJvBR1PFC', 'Manigram', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(105, '88040', '$2y$10$kMlGK3nbAdmoiWLtkTNtRucS14clxHd7Wjb2E94MxWMn6NR8Gnx2u', 'Kapilvastu Mid Town', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(106, '88042', '$2y$10$SCTBtQgGV0v1YSe2UjYxcuzs373xKuZIj7p3z6iR1aR9U5TLTVzj2', 'Bajra Manasalu Gorkha', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(107, '88043', '$2y$10$Pcdxz4LtKe4MD9Mw8OKZf.sK9DthXnnduu2PIqih1N.kIv6ij.JzG', 'Kapan', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(108, '88111', '$2y$10$kXhhlcGjThuEN9HYBwlf9.HeJdwZmBPa4.cq9LjLEzPCe0AioAtiC', 'Bhairahawa', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(109, '88151', '$2y$10$TSfh32jxgyNBIyTiMsKte.coiaimwqBzvIlIgStY69zR44X5DCduy', 'Tinau Butwal', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(110, '88161', '$2y$10$EZSLUPvM2CZc/KB5dFTVK.9p2pje0i3Ix3nM50lkgxVWb79ZisFcq', 'Devdaha', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(111, '88163', '$2y$10$CFkqdQ0tuASE6BtE4p1rA.vbTqECb5qx.njiqvmrOr2PWn7ODkdN6', 'Pokhara Lakeside', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(112, '88281', '$2y$10$/p4Ju9okjt618wuscONkbuT4PzNdhKkgE8synY3w4vRLTAy/jMNMW', 'Bardibas', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(113, '88552', '$2y$10$Y6eHnQdiquJAqGMLSjubEOyrSK/F4sHvH3ACRLPjApReAh.X1QFTy', 'Gaidakot', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(114, '89255', '$2y$10$fXxfssczZTmIxOyS2.1cleLXojmY0fCOBsQkhnxpaCICS3THkEYmy', 'Maharajgunj', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(115, '89317', '$2y$10$bKqW5nhPAyfwd8mx7D580OSYKuUakE4x7TCOuaQsBWhR.DUer5/ea', 'Nawalpur Kawasoti', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(116, '89336', '$2y$10$./04OtZpS7IRuQMkvmAVcOxlOIc76kr4cAlTSfDGPncgOzcg/3NOa', 'Palpa Lumbini', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(117, '89425', '$2y$10$3MF097IA8qNwwpknlwsPh.i50zi6gBHszNuy60jDybX4.qOeTBDRy', 'Surunga', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52'),
(118, '89526', '$2y$10$Lr.YwG7dK/Ks6uRhFs3yYOuhZlVwR5.8Eb567RwJ9RdOyJ6686wWW', 'Kawasoti', 'user', NULL, '2019-09-17 10:29:52', '2019-09-17 10:29:52');

-- --------------------------------------------------------

--
-- Table structure for table `water_supplies`
--

CREATE TABLE `water_supplies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `month_id` bigint(20) UNSIGNED NOT NULL,
  `wash_project_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `water_supply_data`
--

CREATE TABLE `water_supply_data` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `water_supply_id` bigint(20) UNSIGNED NOT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `beneficiaries` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `outputs` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `funding_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fund_contributions` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `youth_services`
--

CREATE TABLE `youth_services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `month_id` bigint(20) UNSIGNED NOT NULL,
  `project_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sponsoring` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `joint_service` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `career_counseling` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `professional_skills` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `others` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `annual_funds`
--
ALTER TABLE `annual_funds`
  ADD PRIMARY KEY (`id`),
  ADD KEY `annual_funds_user_id_foreign` (`user_id`),
  ADD KEY `annual_funds_month_id_foreign` (`month_id`);

--
-- Indexes for table `basic_education`
--
ALTER TABLE `basic_education`
  ADD PRIMARY KEY (`id`),
  ADD KEY `basic_education_user_id_foreign` (`user_id`),
  ADD KEY `basic_education_month_id_foreign` (`month_id`);

--
-- Indexes for table `basic_education_data`
--
ALTER TABLE `basic_education_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `basic_education_data_basic_edu_id_foreign` (`basic_edu_id`);

--
-- Indexes for table `club_bulletins`
--
ALTER TABLE `club_bulletins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `club_bulletins_user_id_foreign` (`user_id`),
  ADD KEY `club_bulletins_month_id_foreign` (`month_id`);

--
-- Indexes for table `club_flagships`
--
ALTER TABLE `club_flagships`
  ADD PRIMARY KEY (`id`),
  ADD KEY `club_flagships_user_id_foreign` (`user_id`),
  ADD KEY `club_flagships_month_id_foreign` (`month_id`);

--
-- Indexes for table `club_flagship_data`
--
ALTER TABLE `club_flagship_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `club_flagship_data_club_flagship_id_foreign` (`club_flagship_id`);

--
-- Indexes for table `club_meetings`
--
ALTER TABLE `club_meetings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `club_meetings_user_id_foreign` (`user_id`),
  ADD KEY `club_meetings_month_id_foreign` (`month_id`);

--
-- Indexes for table `club_participations`
--
ALTER TABLE `club_participations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `club_participations_user_id_foreign` (`user_id`),
  ADD KEY `club_participations_month_id_foreign` (`month_id`);

--
-- Indexes for table `club_participation_data`
--
ALTER TABLE `club_participation_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `club_participation_data_club_participation_id_foreign` (`club_participation_id`);

--
-- Indexes for table `club_registrations`
--
ALTER TABLE `club_registrations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `club_registrations_user_id_foreign` (`user_id`),
  ADD KEY `club_registrations_month_id_foreign` (`month_id`);

--
-- Indexes for table `completes`
--
ALTER TABLE `completes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `completes_user_id_foreign` (`user_id`),
  ADD KEY `completes_month_id_foreign` (`month_id`);

--
-- Indexes for table `disease_preventions`
--
ALTER TABLE `disease_preventions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `disease_preventions_user_id_foreign` (`user_id`),
  ADD KEY `disease_preventions_month_id_foreign` (`month_id`);

--
-- Indexes for table `economic_communities`
--
ALTER TABLE `economic_communities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `economic_communities_user_id_foreign` (`user_id`),
  ADD KEY `economic_communities_month_id_foreign` (`month_id`);

--
-- Indexes for table `maternal_children`
--
ALTER TABLE `maternal_children`
  ADD PRIMARY KEY (`id`),
  ADD KEY `maternal_children_user_id_foreign` (`user_id`),
  ADD KEY `maternal_children_month_id_foreign` (`month_id`);

--
-- Indexes for table `media_coverages`
--
ALTER TABLE `media_coverages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_coverages_user_id_foreign` (`user_id`),
  ADD KEY `media_coverages_month_id_foreign` (`month_id`);

--
-- Indexes for table `media_coverage_data`
--
ALTER TABLE `media_coverage_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_coverage_data_media_coverage_id_foreign` (`media_coverage_id`);

--
-- Indexes for table `membership_classifications`
--
ALTER TABLE `membership_classifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `membership_classifications_user_id_foreign` (`user_id`),
  ADD KEY `membership_classifications_month_id_foreign` (`month_id`);

--
-- Indexes for table `membership_classification_data`
--
ALTER TABLE `membership_classification_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `membership_classification_data_mem_class_id_foreign` (`mem_class_id`);

--
-- Indexes for table `membership_developments`
--
ALTER TABLE `membership_developments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `membership_developments_user_id_foreign` (`user_id`),
  ADD KEY `membership_developments_month_id_foreign` (`month_id`);

--
-- Indexes for table `membership_growths`
--
ALTER TABLE `membership_growths`
  ADD PRIMARY KEY (`id`),
  ADD KEY `membership_growths_user_id_foreign` (`user_id`),
  ADD KEY `membership_growths_month_id_foreign` (`month_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `months`
--
ALTER TABLE `months`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `participations`
--
ALTER TABLE `participations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `participations_user_id_foreign` (`user_id`),
  ADD KEY `participations_month_id_foreign` (`month_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `promotions`
--
ALTER TABLE `promotions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `promotions_user_id_foreign` (`user_id`),
  ADD KEY `promotions_month_id_foreign` (`month_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- Indexes for table `water_supplies`
--
ALTER TABLE `water_supplies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `water_supplies_user_id_foreign` (`user_id`),
  ADD KEY `water_supplies_month_id_foreign` (`month_id`);

--
-- Indexes for table `water_supply_data`
--
ALTER TABLE `water_supply_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `water_supply_data_water_supply_id_foreign` (`water_supply_id`);

--
-- Indexes for table `youth_services`
--
ALTER TABLE `youth_services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `youth_services_user_id_foreign` (`user_id`),
  ADD KEY `youth_services_month_id_foreign` (`month_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `annual_funds`
--
ALTER TABLE `annual_funds`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `basic_education`
--
ALTER TABLE `basic_education`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `basic_education_data`
--
ALTER TABLE `basic_education_data`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `club_bulletins`
--
ALTER TABLE `club_bulletins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `club_flagships`
--
ALTER TABLE `club_flagships`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `club_flagship_data`
--
ALTER TABLE `club_flagship_data`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `club_meetings`
--
ALTER TABLE `club_meetings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `club_participations`
--
ALTER TABLE `club_participations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `club_participation_data`
--
ALTER TABLE `club_participation_data`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `club_registrations`
--
ALTER TABLE `club_registrations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `completes`
--
ALTER TABLE `completes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `disease_preventions`
--
ALTER TABLE `disease_preventions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `economic_communities`
--
ALTER TABLE `economic_communities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `maternal_children`
--
ALTER TABLE `maternal_children`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `media_coverages`
--
ALTER TABLE `media_coverages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `media_coverage_data`
--
ALTER TABLE `media_coverage_data`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `membership_classifications`
--
ALTER TABLE `membership_classifications`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `membership_classification_data`
--
ALTER TABLE `membership_classification_data`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `membership_developments`
--
ALTER TABLE `membership_developments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `membership_growths`
--
ALTER TABLE `membership_growths`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `months`
--
ALTER TABLE `months`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `participations`
--
ALTER TABLE `participations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `promotions`
--
ALTER TABLE `promotions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;

--
-- AUTO_INCREMENT for table `water_supplies`
--
ALTER TABLE `water_supplies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `water_supply_data`
--
ALTER TABLE `water_supply_data`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `youth_services`
--
ALTER TABLE `youth_services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `annual_funds`
--
ALTER TABLE `annual_funds`
  ADD CONSTRAINT `annual_funds_month_id_foreign` FOREIGN KEY (`month_id`) REFERENCES `months` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `annual_funds_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `basic_education`
--
ALTER TABLE `basic_education`
  ADD CONSTRAINT `basic_education_month_id_foreign` FOREIGN KEY (`month_id`) REFERENCES `months` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `basic_education_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `basic_education_data`
--
ALTER TABLE `basic_education_data`
  ADD CONSTRAINT `basic_education_data_basic_edu_id_foreign` FOREIGN KEY (`basic_edu_id`) REFERENCES `basic_education` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `club_bulletins`
--
ALTER TABLE `club_bulletins`
  ADD CONSTRAINT `club_bulletins_month_id_foreign` FOREIGN KEY (`month_id`) REFERENCES `months` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `club_bulletins_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `club_flagships`
--
ALTER TABLE `club_flagships`
  ADD CONSTRAINT `club_flagships_month_id_foreign` FOREIGN KEY (`month_id`) REFERENCES `months` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `club_flagships_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `club_flagship_data`
--
ALTER TABLE `club_flagship_data`
  ADD CONSTRAINT `club_flagship_data_club_flagship_id_foreign` FOREIGN KEY (`club_flagship_id`) REFERENCES `club_flagships` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `club_meetings`
--
ALTER TABLE `club_meetings`
  ADD CONSTRAINT `club_meetings_month_id_foreign` FOREIGN KEY (`month_id`) REFERENCES `months` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `club_meetings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `club_participations`
--
ALTER TABLE `club_participations`
  ADD CONSTRAINT `club_participations_month_id_foreign` FOREIGN KEY (`month_id`) REFERENCES `months` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `club_participations_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `club_participation_data`
--
ALTER TABLE `club_participation_data`
  ADD CONSTRAINT `club_participation_data_club_participation_id_foreign` FOREIGN KEY (`club_participation_id`) REFERENCES `club_participations` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `club_registrations`
--
ALTER TABLE `club_registrations`
  ADD CONSTRAINT `club_registrations_month_id_foreign` FOREIGN KEY (`month_id`) REFERENCES `months` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `club_registrations_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `completes`
--
ALTER TABLE `completes`
  ADD CONSTRAINT `completes_month_id_foreign` FOREIGN KEY (`month_id`) REFERENCES `months` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `completes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `disease_preventions`
--
ALTER TABLE `disease_preventions`
  ADD CONSTRAINT `disease_preventions_month_id_foreign` FOREIGN KEY (`month_id`) REFERENCES `months` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `disease_preventions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `economic_communities`
--
ALTER TABLE `economic_communities`
  ADD CONSTRAINT `economic_communities_month_id_foreign` FOREIGN KEY (`month_id`) REFERENCES `months` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `economic_communities_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `maternal_children`
--
ALTER TABLE `maternal_children`
  ADD CONSTRAINT `maternal_children_month_id_foreign` FOREIGN KEY (`month_id`) REFERENCES `months` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `maternal_children_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `media_coverages`
--
ALTER TABLE `media_coverages`
  ADD CONSTRAINT `media_coverages_month_id_foreign` FOREIGN KEY (`month_id`) REFERENCES `months` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `media_coverages_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `media_coverage_data`
--
ALTER TABLE `media_coverage_data`
  ADD CONSTRAINT `media_coverage_data_media_coverage_id_foreign` FOREIGN KEY (`media_coverage_id`) REFERENCES `media_coverages` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `membership_classifications`
--
ALTER TABLE `membership_classifications`
  ADD CONSTRAINT `membership_classifications_month_id_foreign` FOREIGN KEY (`month_id`) REFERENCES `months` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `membership_classifications_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `membership_classification_data`
--
ALTER TABLE `membership_classification_data`
  ADD CONSTRAINT `membership_classification_data_mem_class_id_foreign` FOREIGN KEY (`mem_class_id`) REFERENCES `membership_classifications` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `membership_developments`
--
ALTER TABLE `membership_developments`
  ADD CONSTRAINT `membership_developments_month_id_foreign` FOREIGN KEY (`month_id`) REFERENCES `months` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `membership_developments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `membership_growths`
--
ALTER TABLE `membership_growths`
  ADD CONSTRAINT `membership_growths_month_id_foreign` FOREIGN KEY (`month_id`) REFERENCES `months` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `membership_growths_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `participations`
--
ALTER TABLE `participations`
  ADD CONSTRAINT `participations_month_id_foreign` FOREIGN KEY (`month_id`) REFERENCES `months` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `participations_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `promotions`
--
ALTER TABLE `promotions`
  ADD CONSTRAINT `promotions_month_id_foreign` FOREIGN KEY (`month_id`) REFERENCES `months` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `promotions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `water_supplies`
--
ALTER TABLE `water_supplies`
  ADD CONSTRAINT `water_supplies_month_id_foreign` FOREIGN KEY (`month_id`) REFERENCES `months` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `water_supplies_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `water_supply_data`
--
ALTER TABLE `water_supply_data`
  ADD CONSTRAINT `water_supply_data_water_supply_id_foreign` FOREIGN KEY (`water_supply_id`) REFERENCES `water_supplies` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `youth_services`
--
ALTER TABLE `youth_services`
  ADD CONSTRAINT `youth_services_month_id_foreign` FOREIGN KEY (`month_id`) REFERENCES `months` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `youth_services_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
