<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClubMeetingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('club_meetings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('month_id');
            $table->foreign('month_id')->references('id')->on('months')->onDelete('cascade');
            $table->integer('no_of_members');
            $table->string('club_attendance')->nullable();
            $table->string('annual_target_percent')->nullable();

            $table->json('guest_speaker')->nullable();
            $table->json('classification_talks')->nullable();
            $table->json('rotary_program')->nullable();
            $table->json('meeting_combined')->nullable();
            $table->json('club_business')->nullable();
            $table->json('member_presence')->nullable();
            $table->json('total_percent')->nullable();
            $table->json('board')->nullable();
            $table->json('club_assembly')->nullable();
            $table->json('club_committee')->nullable();
            $table->string('month_attendance')->nullable();
            $table->string('annual_target')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('club_meetings');
    }
}
