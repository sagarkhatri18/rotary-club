<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClubParticipationDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('club_participation_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('club_participation_id');
            $table->foreign('club_participation_id')->references('id')->on('club_participations')->onDelete('cascade');
            $table->string('district_name');
            $table->date('date')->nullable();
            $table->string('no_of_participants')->nullable();
            $table->string('event_sponsored')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('club_participation_data');
    }
}
