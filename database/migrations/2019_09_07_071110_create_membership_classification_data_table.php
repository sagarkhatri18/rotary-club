<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembershipClassificationDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('membership_classification_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('mem_class_id');
            $table->foreign('mem_class_id')->references('id')->on('membership_classifications')->onDelete('cascade');
            $table->string('classification_categories_fulfilled');
            $table->string('no_of_club_members')->nullable();
            $table->string('classification_categories_unfulfilled')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('membership_classification_data');
    }
}
