<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembershipDevelopmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('membership_developments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('month_id');
            $table->foreign('month_id')->references('id')->on('months')->onDelete('cascade');

            $table->integer('no_of_members');
            $table->string('annual_target_no')->nullable();
            $table->string('annual_target_percent')->nullable();

            $table->json('new_members_orientation')->nullable();
            $table->json('club_officers')->nullable();
            $table->json('district_membership')->nullable();
            $table->json('membership_others')->nullable();

            $table->json('rotary_club')->nullable();
            $table->json('rotaract_club')->nullable();
            $table->json('interact_club')->nullable();
            $table->json('rotary_community')->nullable();

            $table->json('members_satisfaction')->nullable();
            $table->json('health_check')->nullable();
            $table->json('development_others')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('membership_developments');
    }
}
