<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWaterSupplyDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('water_supply_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('water_supply_id');
            $table->foreign('water_supply_id')->references('id')->on('water_supplies')->onDelete('cascade');

            $table->string('date');
            $table->string('beneficiaries')->nullable();
            $table->string('outputs')->nullable();
            $table->string('funding_type')->nullable();
            $table->string('fund_contributions')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('water_supply_data');
    }
}
