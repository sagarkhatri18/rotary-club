<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBasicEducationDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('basic_education_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('basic_edu_id');
            $table->foreign('basic_edu_id')->references('id')->on('basic_education')->onDelete('cascade');
            $table->string('project_activities');
            $table->string('date')->nullable();
            $table->string('beneficiaries')->nullable();
            $table->string('outputs')->nullable();
            $table->string('funding_type')->nullable();
            $table->string('fund_contributions')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('basic_education_data');
    }
}
