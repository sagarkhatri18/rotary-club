<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClubFlagshipDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('club_flagship_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('club_flagship_id');
            $table->foreign('club_flagship_id')->references('id')->on('club_flagships')->onDelete('cascade');

            $table->string('activities');
            $table->string('project_check')->nullable();
            $table->string('beneficiaries')->nullable();
            $table->string('area')->nullable();
            $table->string('funding_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('club_flagship_data');
    }
}
