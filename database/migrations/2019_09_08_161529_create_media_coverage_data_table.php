<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaCoverageDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media_coverage_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('media_coverage_id');
            $table->foreign('media_coverage_id')->references('id')->on('media_coverages')->onDelete('cascade');

            $table->string('message');
            $table->string('date')->nullable();
            $table->string('times_coverage')->nullable();
            $table->string('print_name')->nullable();
            $table->string('visual_name')->nullable();
            $table->string('audio_name')->nullable();
            $table->string('social_media')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media_coverage_data');
    }
}
