<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            ['username' => 'admin', 'password' => bcrypt('admin123'), 'club_name' => '', 'role' => 'admin'],

            ['username' => '16203', 'password' => bcrypt('user123'), 'club_name' => 'Biratnagar', 'role' => 'user'],
            ['username' => '16204', 'password' => bcrypt('user123'), 'club_name' => 'Kathmandu', 'role' => 'user'],
            ['username' => '22892', 'password' => bcrypt('user123'), 'club_name' => 'Dharan', 'role' => 'user'],
            ['username' => '23126', 'password' => bcrypt('user123'), 'club_name' => 'Patan', 'role' => 'user'],
            ['username' => '25212', 'password' => bcrypt('user123'), 'club_name' => 'Birganj', 'role' => 'user'],
            ['username' => '26776', 'password' => bcrypt('user123'), 'club_name' => 'Kathmandu Mid-Town', 'role' => 'user'],
            ['username' => '27964', 'password' => bcrypt('user123'), 'club_name' => 'Chitwan', 'role' => 'user'],
            ['username' => '28748', 'password' => bcrypt('user123'), 'club_name' => 'Hetauda', 'role' => 'user'],
            ['username' => '29857', 'password' => bcrypt('user123'), 'club_name' => 'Butwal', 'role' => 'user'],
            ['username' => '29915', 'password' => bcrypt('user123'), 'club_name' => 'Kathmandu North', 'role' => 'user'],

            ['username' => '31143', 'password' => bcrypt('user123'), 'club_name' => 'Patan West', 'role' => 'user'],
            ['username' => '31524', 'password' => bcrypt('user123'), 'club_name' => 'Lumbini Siddharthanagar', 'role' => 'user'],
            ['username' => '31764', 'password' => bcrypt('user123'), 'club_name' => 'Yala', 'role' => 'user'],
            ['username' => '50406', 'password' => bcrypt('user123'), 'club_name' => 'Itahari', 'role' => 'user'],
            ['username' => '50507', 'password' => bcrypt('user123'), 'club_name' => 'Pokhara', 'role' => 'user'],
            ['username' => '50576', 'password' => bcrypt('user123'), 'club_name' => 'Dhulikhel', 'role' => 'user'],
            ['username' => '50694', 'password' => bcrypt('user123'), 'club_name' => 'Patan South', 'role' => 'user'],
            ['username' => '50801', 'password' => bcrypt('user123'), 'club_name' => 'Lalitpur', 'role' => 'user'],
            ['username' => '51038', 'password' => bcrypt('user123'), 'club_name' => 'Pashupati Kathmandu', 'role' => 'user'],
            ['username' => '52113', 'password' => bcrypt('user123'), 'club_name' => 'Dillibazar-Kathmandu', 'role' => 'user'],

            ['username' => '53328', 'password' => bcrypt('user123'), 'club_name' => 'Kantipur Kathmandu', 'role' => 'user'],
            ['username' => '53734', 'password' => bcrypt('user123'), 'club_name' => 'Ratnanagar', 'role' => 'user'],
            ['username' => '53984', 'password' => bcrypt('user123'), 'club_name' => 'Damauli', 'role' => 'user'],
            ['username' => '53985', 'password' => bcrypt('user123'), 'club_name' => 'Mount Everest Lalitpur', 'role' => 'user'],
            ['username' => '55855', 'password' => bcrypt('user123'), 'club_name' => 'Balaju', 'role' => 'user'],
            ['username' => '56011', 'password' => bcrypt('user123'), 'club_name' => 'Kasthamandap Kathmandu', 'role' => 'user'],
            ['username' => '56012', 'password' => bcrypt('user123'), 'club_name' => 'Biratnagar Mid Town', 'role' => 'user'],
            ['username' => '56013', 'password' => bcrypt('user123'), 'club_name' => 'Kopundol', 'role' => 'user'],
            ['username' => '56014', 'password' => bcrypt('user123'), 'club_name' => 'Palpa Tansen', 'role' => 'user'],
            ['username' => '56015', 'password' => bcrypt('user123'), 'club_name' => 'Kathmandu West', 'role' => 'user'],

            ['username' => '56016', 'password' => bcrypt('user123'), 'club_name' => 'Butwal South', 'role' => 'user'],
            ['username' => '56780', 'password' => bcrypt('user123'), 'club_name' => 'Kavre-Banepa', 'role' => 'user'],
            ['username' => '57536', 'password' => bcrypt('user123'), 'club_name' => 'Thamel-Kathmandu', 'role' => 'user'],
            ['username' => '58448', 'password' => bcrypt('user123'), 'club_name' => 'Butwal-Downtown', 'role' => 'user'],
            ['username' => '60167', 'password' => bcrypt('user123'), 'club_name' => 'Rajdhani', 'role' => 'user'],
            ['username' => '60434', 'password' => bcrypt('user123'), 'club_name' => 'Narayangarh', 'role' => 'user'],
            ['username' => '60435', 'password' => bcrypt('user123'), 'club_name' => 'Gorkha', 'role' => 'user'],
            ['username' => '60675', 'password' => bcrypt('user123'), 'club_name' => 'Pokhara Mid Town', 'role' => 'user'],
            ['username' => '60815', 'password' => bcrypt('user123'), 'club_name' => 'Charumati Kathmandu', 'role' => 'user'],
            ['username' => '60878', 'password' => bcrypt('user123'), 'club_name' => 'Tripureswor', 'role' => 'user'],

            ['username' => '60968', 'password' => bcrypt('user123'), 'club_name' => 'Dharan Ghopa', 'role' => 'user'],
            ['username' => '62759', 'password' => bcrypt('user123'), 'club_name' => 'Jawalakhel', 'role' => 'user'],
            ['username' => '62760', 'password' => bcrypt('user123'), 'club_name' => 'Bhaktapur', 'role' => 'user'],
            ['username' => '62761', 'password' => bcrypt('user123'), 'club_name' => 'Patan Durbar Square', 'role' => 'user'],
            ['username' => '62762', 'password' => bcrypt('user123'), 'club_name' => 'Narayani Mid Town', 'role' => 'user'],
            ['username' => '62763', 'password' => bcrypt('user123'), 'club_name' => 'Mahabouddha', 'role' => 'user'],
            ['username' => '63296', 'password' => bcrypt('user123'), 'club_name' => 'The Himalayan Gurkhas', 'role' => 'user'],
            ['username' => '63378', 'password' => bcrypt('user123'), 'club_name' => 'Bharatpur', 'role' => 'user'],
            ['username' => '63380', 'password' => bcrypt('user123'), 'club_name' => 'Birgunj Metropolis', 'role' => 'user'],
            ['username' => '63381', 'password' => bcrypt('user123'), 'club_name' => 'Himalaya Patan', 'role' => 'user'],

            ['username' => '63636', 'password' => bcrypt('user123'), 'club_name' => 'Swoyambhu', 'role' => 'user'],
            ['username' => '63637', 'password' => bcrypt('user123'), 'club_name' => 'Bagmati Kathmandu', 'role' => 'user'],
            ['username' => '63638', 'password' => bcrypt('user123'), 'club_name' => 'Pokhra Fishtail', 'role' => 'user'],
            ['username' => '63859', 'password' => bcrypt('user123'), 'club_name' => 'Kathmandu North East', 'role' => 'user'],
            ['username' => '64060', 'password' => bcrypt('user123'), 'club_name' => 'Kathmandu Metro', 'role' => 'user'],
            ['username' => '64061', 'password' => bcrypt('user123'), 'club_name' => 'Budhanilkantha', 'role' => 'user'],
            ['username' => '64062', 'password' => bcrypt('user123'), 'club_name' => 'Rudramati Kathmandu', 'role' => 'user'],
            ['username' => '64357', 'password' => bcrypt('user123'), 'club_name' => 'Nagarjun', 'role' => 'user'],
            ['username' => '65321', 'password' => bcrypt('user123'), 'club_name' => 'New Road City Kathmandu', 'role' => 'user'],
            ['username' => '69937', 'password' => bcrypt('user123'), 'club_name' => 'Biratnagar Down Town', 'role' => 'user'],

            ['username' => '71663', 'password' => bcrypt('user123'), 'club_name' => 'Lamjung', 'role' => 'user'],
            ['username' => '71857', 'password' => bcrypt('user123'), 'club_name' => 'Pokhara Annapurna', 'role' => 'user'],
            ['username' => '74282', 'password' => bcrypt('user123'), 'club_name' => 'Baglung', 'role' => 'user'],
            ['username' => '76761', 'password' => bcrypt('user123'), 'club_name' => 'Tulsipur City', 'role' => 'user'],
            ['username' => '79555', 'password' => bcrypt('user123'), 'club_name' => 'Jawalakhel-Manjushree', 'role' => 'user'],
            ['username' => '79915', 'password' => bcrypt('user123'), 'club_name' => 'Baneshwor', 'role' => 'user'],
            ['username' => '80893', 'password' => bcrypt('user123'), 'club_name' => 'Chandragiri', 'role' => 'user'],
            ['username' => '81318', 'password' => bcrypt('user123'), 'club_name' => 'Lalitpur Mid Town', 'role' => 'user'],
            ['username' => '82616', 'password' => bcrypt('user123'), 'club_name' => 'Sainbu Bhainsepati', 'role' => 'user'],
            ['username' => '82923', 'password' => bcrypt('user123'), 'club_name' => 'Kakarvitta', 'role' => 'user'],

            ['username' => '83172', 'password' => bcrypt('user123'), 'club_name' => 'Durbarmarg', 'role' => 'user'],
            ['username' => '83175', 'password' => bcrypt('user123'), 'club_name' => 'Parbat', 'role' => 'user'],
            ['username' => '83243', 'password' => bcrypt('user123'), 'club_name' => 'Birtamode', 'role' => 'user'],
            ['username' => '83560', 'password' => bcrypt('user123'), 'club_name' => 'Lekhnath', 'role' => 'user'],
            ['username' => '83601', 'password' => bcrypt('user123'), 'club_name' => 'Pokhara Newroad', 'role' => 'user'],
            ['username' => '83999', 'password' => bcrypt('user123'), 'club_name' => 'Dang', 'role' => 'user'],
            ['username' => '84013', 'password' => bcrypt('user123'), 'club_name' => 'Madhyapur', 'role' => 'user'],
            ['username' => '84190', 'password' => bcrypt('user123'), 'club_name' => 'Damak', 'role' => 'user'],
            ['username' => '84382', 'password' => bcrypt('user123'), 'club_name' => 'Tilottama-Rupandehi', 'role' => 'user'],
            ['username' => '84427', 'password' => bcrypt('user123'), 'club_name' => 'Thimphu', 'role' => 'user'],

            ['username' => '84437', 'password' => bcrypt('user123'), 'club_name' => 'Rupandehi', 'role' => 'user'],
            ['username' => '85160', 'password' => bcrypt('user123'), 'club_name' => 'E-Club of District 3292', 'role' => 'user'],
            ['username' => '85219', 'password' => bcrypt('user123'), 'club_name' => 'Dhangadhi', 'role' => 'user'],
            ['username' => '85226', 'password' => bcrypt('user123'), 'club_name' => 'Kapilvastu', 'role' => 'user'],
            ['username' => '85767', 'password' => bcrypt('user123'), 'club_name' => 'Birtamode Mid-Town', 'role' => 'user'],
            ['username' => '85872', 'password' => bcrypt('user123'), 'club_name' => 'Bhadrapur', 'role' => 'user'],
            ['username' => '85919', 'password' => bcrypt('user123'), 'club_name' => 'Bhadgaon', 'role' => 'user'],
            ['username' => '86741', 'password' => bcrypt('user123'), 'club_name' => 'Makawanpur', 'role' => 'user'],
            ['username' => '87103', 'password' => bcrypt('user123'), 'club_name' => 'Sainamaina', 'role' => 'user'],
            ['username' => '87222', 'password' => bcrypt('user123'), 'club_name' => 'Biratnagar Central', 'role' => 'user'],

            ['username' => '87238', 'password' => bcrypt('user123'), 'club_name' => 'Urlabari', 'role' => 'user'],
            ['username' => '87245', 'password' => bcrypt('user123'), 'club_name' => 'Waling', 'role' => 'user'],
            ['username' => '87257', 'password' => bcrypt('user123'), 'club_name' => 'Central Butwal', 'role' => 'user'],
            ['username' => '87325', 'password' => bcrypt('user123'), 'club_name' => 'Dhading', 'role' => 'user'],
            ['username' => '87395', 'password' => bcrypt('user123'), 'club_name' => 'Panauti', 'role' => 'user'],
            ['username' => '87446', 'password' => bcrypt('user123'), 'club_name' => 'Arghakhanchi', 'role' => 'user'],
            ['username' => '87734', 'password' => bcrypt('user123'), 'club_name' => 'Shukalagandaki', 'role' => 'user'],
            ['username' => '87737', 'password' => bcrypt('user123'), 'club_name' => 'Biratnagar Fusion', 'role' => 'user'],
            ['username' => '87762', 'password' => bcrypt('user123'), 'club_name' => 'Gongabu', 'role' => 'user'],
            ['username' => '87769', 'password' => bcrypt('user123'), 'club_name' => 'Matribhumi Baluwatar', 'role' => 'user'],

            ['username' => '88035', 'password' => bcrypt('user123'), 'club_name' => 'Central Lumbini', 'role' => 'user'],
            ['username' => '88037', 'password' => bcrypt('user123'), 'club_name' => 'Sankhu', 'role' => 'user'],
            ['username' => '88039', 'password' => bcrypt('user123'), 'club_name' => 'Manigram', 'role' => 'user'],
            ['username' => '88040', 'password' => bcrypt('user123'), 'club_name' => 'Kapilvastu Mid Town', 'role' => 'user'],
            ['username' => '88042', 'password' => bcrypt('user123'), 'club_name' => 'Bajra Manasalu Gorkha', 'role' => 'user'],
            ['username' => '88043', 'password' => bcrypt('user123'), 'club_name' => 'Kapan', 'role' => 'user'],
            ['username' => '88111', 'password' => bcrypt('user123'), 'club_name' => 'Bhairahawa', 'role' => 'user'],
            ['username' => '88151', 'password' => bcrypt('user123'), 'club_name' => 'Tinau Butwal', 'role' => 'user'],
            ['username' => '88161', 'password' => bcrypt('user123'), 'club_name' => 'Devdaha', 'role' => 'user'],
            ['username' => '88163', 'password' => bcrypt('user123'), 'club_name' => 'Pokhara Lakeside', 'role' => 'user'],

            ['username' => '88281', 'password' => bcrypt('user123'), 'club_name' => 'Bardibas', 'role' => 'user'],
            ['username' => '88552', 'password' => bcrypt('user123'), 'club_name' => 'Gaidakot', 'role' => 'user'],
            ['username' => '89255', 'password' => bcrypt('user123'), 'club_name' => 'Maharajgunj', 'role' => 'user'],
            ['username' => '89317', 'password' => bcrypt('user123'), 'club_name' => 'Nawalpur Kawasoti', 'role' => 'user'],
            ['username' => '89336', 'password' => bcrypt('user123'), 'club_name' => 'Palpa Lumbini', 'role' => 'user'],
            ['username' => '89425', 'password' => bcrypt('user123'), 'club_name' => 'Surunga', 'role' => 'user'],
            ['username' => '89526', 'password' => bcrypt('user123'), 'club_name' => 'Kawasoti', 'role' => 'user'],

        ];
        foreach ($users as $user)
            User::create($user);
    }
}
