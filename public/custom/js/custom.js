$(document).ready(function() {

    var temp;
    /*
        For to dynamically add the td class in the table.
     */
    $(".addButtonClass").click(function(){
        var content = $('.clone_table tr'),
            size = $('.insert_more >tbody >tr').length + 1,
            element = null,
            element = content.clone();
        //element.attr('id', 'rec-'+size);
        element.appendTo('.insert_more');
        element.find('.sn').html(size+'.');
    });

    /*
        For to dynamically remove the selected td class in the table.
     */
    $("body").on("click",".removeButton",function(){
        var $this = $(this);
        swal({
            title: "Are you sure want to delete the row?",
            // text: "Your will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
            },
            function(){
                swal({
                    title: 'Deleted!',
                    text: 'The selected row has been deleted.',
                    type: "success",
                    timer: 700,
                    showCancelButton: false,
                    showConfirmButton: false
                });
                // swal("Deleted!", "The selected row has been deleted.", "success");
                $this.parents("tr").remove();
                $('.tbl_body tr').each(function(index) {
                    $(this).find('span.sn').html(index+1+'.');
                });
            });
    });

    $(".saveButton").on("click", function(){
        swal({
            title: 'Saved Successfully',
            text: 'The form has been saved successfully!',
            type: "success",
            timer: 700,
            showCancelButton: false,
            showConfirmButton: false
        });

        // $(`#${temp}`).find("input").val($("textarea#textHolder").val());

        $(`#${temp}`).closest("td").find("input").val($("textarea#textHolder").val());

    });

    $(document).on("click", ".tbl_body .checked", function(event) {

        // $(this).closest(event.target.nodeName).attr("id", Math.random().toString(36).substring(7));
        // temp = event.target.id;
        //
        // $('#modal-default').modal('show');
        // $("textarea#textHolder").val("");
        // $("textarea#textHolder").val($(this).find('input').val());

        $(this).closest("td").attr("id", Math.random().toString(36).substring(7));
        temp = $(this).closest("td").attr('id');
        $('#modal-default').modal('show');
        $("textarea#textHolder").val("");
        $("textarea#textHolder").val($(this).closest("td").find('input').val());

    });

    $(".appendIcon").on("click", function (event) {
        if($("textarea#textHolder").val().length === 0){
            $(`#${temp}`).find("span").html("");
        }else{
            $(`#${temp}`).find("span").html("<i class='fas fa-check'></i>");
        }
    });


    // modal for div check icon
    $(document).on("click", ".tbl_body .divChecked", function(event) {
        $(this).closest("div").attr("id", Math.random().toString(36).substring(7));
        temp = $(this).closest("div").attr('id');
        $('#div-modal').modal('show');
        $("textarea#divTextHolder").val("");
        $("textarea#divTextHolder").val($(this).closest("div").find('input').val());

    });
    $(".divSaveButton").on("click", function (event) {
        if($("textarea#divTextHolder").val().length === 0){
            $(`#${temp}`).find("span").html("");
        }else{
            $(`#${temp}`).find("span").html("<i class='fas fa-check'></i>");
        }

        swal({
            title: 'Saved Successfully',
            text: 'The form has been saved successfully!',
            type: "success",
            timer: 700,
            showCancelButton: false,
            showConfirmButton: false
        });
        $(`#${temp}`).closest("div").find("input").val($("textarea#divTextHolder").val());
    });


    $("#selectMonth").on("change", function (event) {
        event.preventDefault();
        // alert($(this).data('value'));
        if(event.target.value === "0"){
            window.location.href=$(this).data('value');
        }else{
            $("#submitForm").attr('action', '/'+$(this).data('value')+'/'+event.target.value);
            $("#submitForm").submit();
        }
    });



    // for the final submission of the form.
    $("#finalSubmit").on("click", function(){
        var month = $(this).data("value");
        var user_id = $(this).data("id");

        swal({
                title: "Are you sure want to submit the form finally?",
                text: "once submitted you can't edit or delete the data.",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-primary",
                confirmButtonText: "Yes, submit it!",
                closeOnConfirm: false
            },
            function(){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "/preview/submit",
                    type: 'POST',
                    dataType: 'JSON',
                    data: {month: month, user_id: user_id},
                    success: function (data) {
                        swal({
                            title: data.title,
                            text: data.text,
                            type: data.type,
                            timer: 700,
                            showCancelButton: false,
                            showConfirmButton: false
                        });
                    }
                });
            });
    });

    $('.print-window').click(function() {
        //$(this).closest('.card').attr('id', 'checker');
        $(this).closest('.card').siblings().toggle();
        window.print();
        $(this).closest('.card').siblings().toggle();
        // $('#checker').removeAttr('id');
    });

    $("#adminSubmitClick").on("click", function (event) {
        $("#adminClubSearch").attr('action', '/admin/'+$("#adminSelectMonth").find(":selected").val());
        $("#adminClubSearch").submit();
    });

});


