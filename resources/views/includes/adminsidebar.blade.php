<form method="get" id="adminClubSearch">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
        data-accordion="false">
        <li class="nav-link">
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="selectMonth">Month</label>
                    <select class="form-control" id="adminSelectMonth">
                        @foreach($month as $m)
                            <option @if($m->id == $month_id) selected @endif value="{{$m->id}}">{{$m->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </li>

{{--        <li class="nav-link">--}}
{{--            <div class="form-row">--}}
{{--                <div class="form-group col-md-12">--}}
{{--                    <label for="selectClub">Club</label>--}}
{{--                    <select class="form-control" id="selectClub" name="club_name">--}}
{{--                        @foreach($club as $c)--}}
{{--                            <option @if($c->id == $club_id) selected @endif value="{{$c->id}}">{{$c->club_name}}</option>--}}
{{--                        @endforeach--}}
{{--                    </select>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </li>--}}

        <li class="nav-link">
            <div class="form-row">
                <div class="form-group col-md-12">
                    <button type="button" id="adminSubmitClick" class="btn btn-primary btn-block"><i class='fas fa-search'></i> Find</button>
                </div>
            </div>
        </li>
    </ul>
</form>
