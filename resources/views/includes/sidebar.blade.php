
<aside class="main-sidebar" style="background-color: #F2F2F2">
    <div class="sidebar">
        <nav class="mt-2">
            @if(Auth::user()->role == 'user')
               @include('includes.usersidebar')
            @else
                @include('includes.adminsidebar')
            @endif
        </nav>
    </div>
</aside>
