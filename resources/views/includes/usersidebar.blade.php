<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
    data-accordion="false">
    <li class="nav-link">
        <label for="selectMonth">Month</label>
        <form method="GET" id="submitForm">
            <select class="form-control" id="selectMonth" data-value="{{$route}}">
                @foreach($month as $m)
                    <option @if($m->id == (int)$month_id) selected @endif value="{{$m->id}}">{{$m->name}}</option>
                @endforeach
            </select>
        </form>
    </li>

    <li class="nav-item has-treeview">
        <a href="#" class="nav-link">
            <i class="nav-icon fas fa-tag"></i>
            <p class="text-bold">Club Administration <i class="fas fa-angle-left right"></i></p>
        </a>
        <ul class="nav nav-treeview">
            <li class="nav-item">
                <a href="{{route('club_meetings.index')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Club Meetings Conducted</p>
                    {!! $tick['club_meeting']?"&nbsp;<i class='fas fa-check'></i>":"" !!}
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('club_bulletin.index')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Club Bulletin</p>
                    {!! $tick['club_bulletin']?"&nbsp;<i class='fas fa-check'></i>":"" !!}
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('club_participation.index')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Club Participation</p>
                    {!! $tick['club_participation']?"&nbsp;<i class='fas fa-check'></i>":"" !!}
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('club_registration.index')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Club Registration</p>
                    {!! $tick['club_registration']?"&nbsp;<i class='fas fa-check'></i>":"" !!}
                </a>
            </li>
        </ul>
    </li>

    <li class="nav-item has-treeview">
        <a href="#" class="nav-link">
            <i class="nav-icon fas fa-tag"></i>
            <p class="text-bold">Club Membership<i class="fas fa-angle-left right"></i></p>
        </a>
        <ul class="nav nav-treeview">
            <li class="nav-item">
                <a href="{{route('membership_classification.index')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Membership Classification</p>
                    {!! $tick['membership_class']?"&nbsp;<i class='fas fa-check'></i>":"" !!}
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('membership_growth.index')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Membership Growth</p>
                    {!! $tick['membership_growth']?"&nbsp;<i class='fas fa-check'></i>":"" !!}
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('membership_development.index')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Membership Development</p>
                    {!! $tick['membership_dev']?"&nbsp;<i class='fas fa-check'></i>":"" !!}
                </a>
            </li>
        </ul>
    </li>


    <li class="nav-item has-treeview">
        <a href="#" class="nav-link">
            <i class="nav-icon fas fa-tag"></i>
            <p class="text-bold">Service Project Committee<i class="fas fa-angle-left right"></i></p>
        </a>

        <ul class="nav nav-treeview">
            <li class="nav-item">
                <a href="{{route('water_supply.index')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Water Supply & Sanitation</p>
                    {!! $tick['water_supply']?"&nbsp;<i class='fas fa-check'></i>":"" !!}
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('disease_prevention.index')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Disease Prevention & Treatment</p>
                    {!! $tick['disease_prevention']?"&nbsp;<i class='fas fa-check'></i>":"" !!}
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('economic_community.index')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Economic & Community </p><br>
                    <p>&ensp; &ensp; &ensp; Development</p>
                    {!! $tick['eco_comm']?"&nbsp;<i class='fas fa-check'></i>":"" !!}
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('maternal_child.index')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Maternal & Child Health</p>
                    {!! $tick['maternal_child']?"&nbsp;<i class='fas fa-check'></i>":"" !!}
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('basic_education.index')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Basic Education & Literacy</p>
                    {!! $tick['basic_edu']?"&nbsp;<i class='fas fa-check'></i>":"" !!}
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('youth_service.index')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Youth Service</p>
                    {!! $tick['youth_service']?"&nbsp;<i class='fas fa-check'></i>":"" !!}
                </a>
            </li>
        </ul>
    </li>

    <li class="nav-item has-treeview">
        <a href="#" class="nav-link">
            <i class="nav-icon fas fa-tag"></i>
            <p class="text-bold">Club Public Relation Committee<i class="fas fa-angle-left right"></i></p>
        </a>

        <ul class="nav nav-treeview">
            <li class="nav-item">
                <a href="{{route('media_coverage.index')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Media Coverage of Club Activities</p>
                    {!! $tick['media_coverage']?"&nbsp;<i class='fas fa-check'></i>":"" !!}
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('club_flagship.index')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Club Flagship/Signature Project</p>
                    {!! $tick['club_flagship']?"&nbsp;<i class='fas fa-check'></i>":"" !!}
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('promotion.index')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Promotion of Four Way Tests</p>{!! $tick['promotion']?" &nbsp;<i class='fas fa-check'></i>":"" !!}<br>
                    <p>&ensp; &ensp; &ensp; & Project Information Board Display</p>
                </a>
            </li>
        </ul>
    </li>


    <li class="nav-item has-treeview">
        <a href="#" class="nav-link">
            <i class="nav-icon fas fa-tag"></i>
            <p class="text-bold">The Rotary Foundation Committee<i class="fas fa-angle-left right"></i></p>
        </a>

        <ul class="nav nav-treeview">
            <li class="nav-item">
                <a href="{{route('annual_fund.index')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Annual Fund Contribution</p>
                    {!! $tick['annual_fund']?"&nbsp;<i class='fas fa-check'></i>":"" !!}
                </a>
            </li>
            <li class="nav-item">
                <a href="{{route('participation_in_trf.index')}}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Participation in TRF Activities</p>
                    {!! $tick['participation']?"&nbsp;<i class='fas fa-check'></i>":"" !!}
                </a>
            </li>
        </ul>
    </li>

    <li class="nav-item">
        <a href="{{route('preview.index')}}" class="nav-link">
            <i class="nav-icon fas fa-tag"></i>
            <p class="text-bold">Form Preview</p>
        </a>
    </li>
</ul>