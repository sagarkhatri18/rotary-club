<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Club Monthly Report') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Styles -->
    <link media="screen" href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link rel="stylesheet" media="print" href="{{asset('custom/css/print.css')}}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css">
    <link rel="stylesheet" href="{{asset('custom/css/plugin.min.css')}}">
    <link rel="stylesheet" media="screen" href="{{asset('custom/css/style.css')}}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
{{--    <link rel="dns-prefetch" href="//fonts.gstatic.com">--}}
{{--    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">--}}

</head>
<body class="hold-transition sidebar-mini">
<nav class="navbar navbar-expand navbar-light top_navbar">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>
    </ul>
    <h4>Test Company</h4>
    <ul class="navbar-nav ml-auto">
        <img height="40" width="40" src="https://gravatar.com/avatar/5d17108b6bff72b438e0883b58502f28/?s=45&d=mm" class="img-circle elevation-2" alt="User Image"/>
        <li class="nav-item dropdown">
            <a id="navbarDropdown" class="nav-link dropdown-toggle text-white" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                <b>{{ Auth::user()->club_name }}</b> <span class="caret"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </li>
    </ul>
</nav>
<div class="wrapper">

    @include('includes.sidebar')

    <div class="content-wrapper">
        @yield('content')
    </div>
    <footer class="no-print main-footer">
        <h6>Footer Content</h6>
    </footer>
</div>

<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body p-3">
                <div class="mb-0">
                    <label for="text" style="font-size: large">Enter your text here:</label>
                    <textarea required id="textHolder" class="form-control" placeholder="enter here...." style="width: 100%; height: 150px; font-size: medium"></textarea>
                </div>
            </div>
            <div class="modal-footer justify-content-between p-2">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary saveButton appendIcon" data-dismiss="modal">Save changes</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="div-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body p-3">
                <div class="mb-0">
                    <label for="text" style="font-size: large">Enter your text here:</label>
                    <textarea required id="divTextHolder" class="form-control" placeholder="enter here...." style="width: 100%; height: 150px; font-size: medium"></textarea>
                </div>
            </div>
            <div class="modal-footer justify-content-between p-2">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary divSaveButton" data-dismiss="modal">Save changes</button>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="{{asset('custom/js/custom.js')}}"></script>
<script src="{{asset('custom/js/plugin.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>

<script>
    $(document).ready(function () {
    @if(Session::has('message'))
    var type = "{{ Session::get('type') }}";
    var message = "{{ Session::get('message') }}";

    swal({
        title: message,
        type: type,
        timer: 2000,
        showCancelButton: false,
        showConfirmButton: false
    });

    @endif
    });
</script>
    </body>
</html>
