@extends('layouts.app')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-left">
                        <li class="breadcrumb-item"><a href="{{route('admin.home')}}">Home</a></li>
                        <li class="breadcrumb-item active">Club lists</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12" style="margin-top: 4px;">
                    <div class="card">
                        <div class="card-header" style="background-color: gainsboro;">
                            <h3 class="card-title">Club Information - {{$month_name->name}}({{count($club)}} records)</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0" style="box-shadow: none !important;">
                            <table class="table table-hover" style="background-color: antiquewhite">
                                <thead>
                                <tr>
                                    <th width="10%" class="text-left">S.N</th>
                                    <th width="30%" class="text-left">Club Name</th>
                                    <th width="10%" class="text-left">Status</th>
                                    <th width="10%" class="text-left">Preview</th>
                                    <th class="text-left">Mark</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $i = 1; @endphp
                                @foreach($club as $c)
                                    <tr>
                                        <td>{{$i++}}.</td>
                                        <td>{{$c->club_name}}</td>
                                        <td>
                                            @if(\App\Model\Complete::adminClubCheck($month_id, $c->id))
                                                &nbsp; &nbsp;<i class='fas fa-check'></i>
                                            @else
                                                &nbsp; &nbsp;<i class='fas fa-times'></i>
                                            @endif
                                        </td>
                                        <td>
                                            @if(\App\Model\Complete::adminClubCheck($month_id, $c->id))
                                                <a href="{{route('admin.preview', ['month_id' => $month_id, 'user_id' => $c->id])}}">preview</a>
                                            @endif
                                        </td>
                                        <td>
                                            <form method="post" action="{{route('admin.mark')}}">
                                                @csrf
                                                <div class="row">
                                                    <input type="hidden" name="club_id" value="{{$c->id}}"/>
                                                    <input type="hidden" name="month_id" value="{{$month_id}}"/>
                                                    <div class="col-md-10">

                                                        <input type="text" required class="form-control" style="margin: 0; padding: 9px;" value="{{App\Model\AdminMark::getMark($c->id, $month_id)}}" name="mark" />
                                                    </div>
                                                    <div class="col-md-2">
                                                        <button type="submit" class="btn btn-primary mb-2">Save</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


