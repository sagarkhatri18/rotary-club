@extends('layouts.app')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-left">
                        <li class="breadcrumb-item"><a href="index.blade.php">Home</a></li>
                        <li class="breadcrumb-item active">Annual Fund Contribution</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header card_header p-0">
                            <p class="card-title ml-3"><small>Annual Fund Contribution</small></p>
                        </div>
                        <form class="mt-sm-3 mt-md-0" method="post" action="{{route('annual_fund.store')}}">
                            @csrf
                            <div class="card-body card_body pt-sm-0 pt-md-3 pb-0">
                                <div class="form-row">
                                    <div class="form-group col-xl-4 col-md-12 col-sm-6 mr-auto">
                                        <label for="club_name"><strong>Club Name</strong></label>
                                        <input type="text" readonly class="form-control-plaintext" id="club_name"
                                               value="{{Auth::user()->club_name}}">
                                    </div>
                                    <input type="hidden" name="id" value="{{$annual_fund?($annual_fund->id ? $annual_fund->id : ""):""}}"/>
                                    <input type="hidden" name="month_id" value="{{$month_id}}"/>
                                    <div class="form-group col-xl-4 col-md-12 col-sm-6">
                                        <label for="annual_target"><strong>Annual Target of TRF Contribution (USD):</strong></label>
                                        <input type="text" class="form-control" required name="annual_target" id="annual_target"
                                        value="{{$annual_fund?$annual_fund->annual_target:""}}">
                                    </div>
                                </div>

                                <div class="table-responsive mb-3">
                                    <table class="table table-bordered mb-0">
                                        <thead>
                                        <tr>
                                            <th>S.N</th>
                                            <th>TRF Contribution categories</th>
                                            <th>No of contributing Members</th>
                                            <th>Total Amount USD</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>1.</td>
                                            <td>100% Club Contribution: Small Gifts
                                                Minimum USD 10 to USD 99
                                            </td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="club_contribution_first"
                                                >{{$annual_fund?json_decode($annual_fund->club_contribution)->first:""}}</textarea></td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="club_contribution_second"
                                                >{{$annual_fund?json_decode($annual_fund->club_contribution)->second:""}}</textarea></td>
                                        </tr>
                                        <tr>
                                            <td>2.</td>
                                            <td>Rotary Foundation Sustaining Members (RFSM):
                                                USD 100 per year
                                            </td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="rotary_foundation_first"
                                                >{{$annual_fund?json_decode($annual_fund->rotary_foundation)->first:""}}</textarea></td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="rotary_foundation_second"
                                                >{{$annual_fund?json_decode($annual_fund->rotary_foundation)->second:""}}</textarea></td>
                                        </tr>
                                        <tr>
                                            <td>3.</td>
                                            <td>Paul Harris Fellow (PHF):
                                                USD 300 or USD 500 as directed by the District
                                            </td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="phf_first"
                                                >{{$annual_fund?json_decode($annual_fund->phf)->first:""}}</textarea></td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="phf_second"
                                                >{{$annual_fund?json_decode($annual_fund->phf)->second:""}}</textarea></td>
                                        </tr>
                                        <tr>
                                            <td>4.</td>
                                            <td>Multi Paul Harris Fellow (MPHF)
                                                USD 300 or USD 500 as directed by the District
                                            </td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="mphf_first"
                                                >{{$annual_fund?json_decode($annual_fund->mphf)->first:""}}</textarea></td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="mphf_second"
                                                >{{$annual_fund?json_decode($annual_fund->mphf)->second:""}}</textarea></td>
                                        </tr>
                                        <tr>
                                            <td>5.</td>
                                            <td>PAUL HARRIS SOCIETY (PHS)
                                                USD 1000.00 per year
                                            </td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="phs_first"
                                                >{{$annual_fund?json_decode($annual_fund->phs)->first:""}}</textarea></td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="phs_second"
                                                >{{$annual_fund?json_decode($annual_fund->phs)->second:""}}</textarea></td>
                                        </tr>
                                        <tr>
                                            <td>6.</td>
                                            <td>Major Donor
                                                USD 10000.00
                                            </td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="major_donor_first"
                                                >{{$annual_fund?json_decode($annual_fund->major_donor)->first:""}}</textarea></td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="major_donor_second"
                                                >{{$annual_fund?json_decode($annual_fund->major_donor)->second:""}}</textarea></td>
                                        </tr>
                                        <tr>
                                            <td>6.</td>
                                            <td>Others</td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="others_first"
                                                >{{$annual_fund?json_decode($annual_fund->others)->first:""}}</textarea></td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="others_second"
                                                >{{$annual_fund?json_decode($annual_fund->others)->second:""}}</textarea></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer text-white p-0">
                                <div class="form-row float-right p-2 mr-3 ml-3">
                                    <button type="submit" class="btn btn-block btn-primary">Save <i class="fas fa-check"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


