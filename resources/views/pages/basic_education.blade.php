@extends('layouts.app')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-left">
                        <li class="breadcrumb-item"><a href="index.blade.php">Home</a></li>
                        <li class="breadcrumb-item active">Basic Education & Literacy</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header card_header p-0">
                            <p class="card-title ml-3"><small>FOCUS AREA: BASIC EDUCATION AND LITERACY</small></p>
                        </div>
                        <form class="mt-sm-3 mt-md-0" method="post" action="{{route('basic_education.store')}}">
                            @csrf
                            <div class="card-body card_body pt-sm-0 pt-md-3 pb-0">
                                <div class="form-row">
                                    <div class="form-group col-xl-4 col-md-12 col-sm-6">
                                        <label for="club_name"><strong>Club Name</strong></label>
                                        <input type="text" readonly class="form-control-plaintext" id="club_name"
                                               value="{{Auth::user()->club_name}}">
                                    </div>
                                    <input type="hidden" name="id" value="{{$basic_education?($basic_education->id ? $basic_education->id : ""):""}}"/>
                                    <input type="hidden" name="month_id" value="{{$month_id}}"/>
                                    <div class="form-group col-xl-4 col-md-12 col-sm-6">
                                        <label for="name"><strong>Name of the Project:</strong></label>
                                        <input type="text" required name="project_name" class="form-control" id="name"
                                               value="{{$basic_education?$basic_education->project_name:""}}">
                                    </div>
                                    <div class="form-group col-xl-4 col-md-12 col-sm-6">
                                        <label for="address"><strong>Project Address (INSTITUTION/VDC/Municipality/Ward):</strong></label>
                                        <input type="text" name="project_address" class="form-control" id="address"
                                               value="{{$basic_education?$basic_education->project_address:""}}">
                                    </div>
                                </div>

                                <div class="table-responsive mb-3">
                                    <table class="table table-bordered insert_more mb-0">
                                        <thead>
                                        <tr>
                                            <th rowspan="2" class="align-middle">S.N</th>
                                            <th rowspan="2" class="align-middle" style="width: 20%">Project Activities <br>
                                                <small>(USE THE REPORT FORMAT DEVELOPED BY RNLM/TEACH)</small></th>
                                            <th rowspan="2" class="align-middle">Project Completion Date</th>
                                            <th colspan="2">M. Project Results</th>
                                            <th>N. Project Funding Type*</th>
                                            <th colspan="2">O. Total Fund Contributions*</th>
                                        </tr>
                                        <tr style="font-size: 13px;">
                                            <th class="font-weight-lighter">Beneficiaries <br>
                                                (h)	population , <br>
                                                or <br>
                                                &emsp;&emsp;(b)households <br>
                                                &emsp;(c) students <br>
                                                (i)&emsp;&emsp;&emsp;&emsp;&emsp;
                                            </th>
                                            <th class="font-weight-lighter" style="width: 10%">Outputs <br>
                                                outcomes
                                            </th>
                                            <th class="font-weight-lighter">•	Global Grant <br>
                                                •	District Grant <br>
                                                •	International partner <br>
                                                •	Club to Club  funding support <br>
                                                •	Internal Club fund <br>
                                                •	Others
                                            </th>
                                            <th colspan="2" class="font-weight-lighter" style="width: 17%">
                                                •	GG: <br>
                                                •	DG: <br>
                                                •	International Club/partner: <br>
                                                •	Club internal <br>
                                                •	Others
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody class="tbl_body">
                                        @if(!is_null($basic_education_data))
                                            @for($i=0; $i<count($basic_education_data); $i++)
                                                <tr>
                                                    <td>{{$i+1}}.</td>
                                                    <input type="hidden" name="basic_edu_data_id[]" value="{{$basic_education_data[$i]->id}}"/>
                                                    <td>
                                                        <textarea style="width: 100%; height: 40px;" name="project_activities[]" class="form-control"
                                                        >{{$basic_education_data[$i]->project_activities}}</textarea>
                                                    </td>
                                                    <td class="text-center"><input type="date" class="form-control" name="date[]" value="{{$basic_education_data[$i]->date}}"/></td>

                                                    <td class="position-relative">
                                                        <button type="button" data-value="{{$basic_education_data ? $basic_education_data[$i]->beneficiaries ? $basic_education_data[$i]->beneficiaries : "":""}}" class="checked text-center">
                                                            <span>{!! $basic_education_data ? $basic_education_data[$i]->beneficiaries ? "<i class='fas fa-check'></i>" : "":"" !!}</span>
                                                        </button>
                                                        <input type="text" hidden class="form-control" name="beneficiaries[]"
                                                               value="{{$basic_education_data ? $basic_education_data[$i]->beneficiaries ? $basic_education_data[$i]->beneficiaries : "":""}}"/>
                                                    </td>

                                                    <td class="position-relative">
                                                        <button type="button" data-value="{{$basic_education_data ? $basic_education_data[$i]->outputs ? $basic_education_data[$i]->outputs : "":""}}" class="checked text-center">
                                                            <span>{!! $basic_education_data ? $basic_education_data[$i]->outputs ? "<i class='fas fa-check'></i>" : "":"" !!}</span>
                                                        </button>
                                                        <input type="text" hidden class="form-control" name="outputs[]"
                                                               value="{{$basic_education_data ? $basic_education_data[$i]->outputs ? $basic_education_data[$i]->outputs : "":""}}"/>
                                                    </td>

                                                    <td class="position-relative">
                                                        <button type="button" data-value="{{$basic_education_data ? $basic_education_data[$i]->funding_type ? $basic_education_data[$i]->funding_type : "":""}}" class="checked text-center">
                                                            <span>{!! $basic_education_data ? $basic_education_data[$i]->funding_type ? "<i class='fas fa-check'></i>" : "":"" !!}</span>
                                                        </button>
                                                        <input type="text" hidden class="form-control" name="funding_type[]"
                                                               value="{{$basic_education_data ? $basic_education_data[$i]->funding_type ? $basic_education_data[$i]->funding_type : "":""}}"/>
                                                    </td>

                                                    <td class="position-relative">
                                                        <button type="button" data-value="{{$basic_education_data ? $basic_education_data[$i]->fund_contributions ? $basic_education_data[$i]->fund_contributions : "":""}}" class="checked text-center">
                                                            <span>{!! $basic_education_data ? $basic_education_data[$i]->fund_contributions ? "<i class='fas fa-check'></i>" : "":"" !!}</span>
                                                        </button>
                                                        <input type="text" hidden class="form-control" name="fund_contributions[]"
                                                               value="{{$basic_education_data ? $basic_education_data[$i]->fund_contributions ? $basic_education_data[$i]->fund_contributions : "":""}}"/>
                                                    </td>

                                                    {!!($i == 0)?'<td style="width: 5%"><button class="btn btn-primary btn-sm addButtonClass" type="button"><i class="fas fa-plus"></i></button></td>':
                                                    '<td class="text-center" style="width: 5%"><i class="fas fa-check"></i></td>'!!}
                                                </tr>
                                            @endfor
                                        @else
                                            <tr>
                                                <td>1.</td>
                                                <td>
                                                    <textarea style="width: 100%; height: 40px;" name="project_activities[]" class="form-control"></textarea>
                                                </td>
                                                <td class="text-center"><input class="form-control" type="date" name="date[]"/></td>

                                                <td class="position-relative">
                                                    <button type="button" data-value="" class="checked text-center">
                                                        <span></span>
                                                    </button>
                                                    <input type="text" hidden class="form-control" name="beneficiaries[]"/>
                                                </td>

                                                <td class="position-relative">
                                                    <button type="button" data-value="" class="checked text-center">
                                                        <span></span>
                                                    </button>
                                                    <input type="text" hidden class="form-control" name="outputs[]"/>
                                                </td>

                                                <td class="position-relative">
                                                    <button type="button" data-value="" class="checked text-center">
                                                        <span></span>
                                                    </button>
                                                    <input type="text" hidden class="form-control" name="funding_type[]"/>
                                                </td>

                                                <td class="position-relative">
                                                    <button type="button" data-value="" class="checked text-center">
                                                        <span></span>
                                                    </button>
                                                    <input type="text" hidden class="form-control" name="fund_contributions[]"/>
                                                </td>

                                                <td width="5%"><button class="btn btn-primary btn-sm addButtonClass" type="button"><i class="fas fa-plus"></i></button></td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                    <div class="d-none">
                                        <table class="clone_table">
                                            <tr>
                                                <td><span class="sn"></span></td>
                                                <input type="hidden" name="basic_edu_data_id[]" value=""/>
                                                <td>
                                                    <textarea style="width: 100%; height: 40px;" name="project_activities[]" class="form-control"></textarea>
                                                </td>
                                                <td class="text-center"><input class="form-control" type="date" name="date[]"/></td>
                                                <td class="position-relative">
                                                    <button type="button" data-value="" class="checked text-center">
                                                        <span></span>
                                                    </button>
                                                    <input type="text" hidden class="form-control" name="beneficiaries[]"/>
                                                </td>

                                                <td class="position-relative">
                                                    <button type="button" data-value="" class="checked text-center">
                                                        <span></span>
                                                    </button>
                                                    <input type="text" hidden class="form-control" name="outputs[]"/>
                                                </td>

                                                <td class="position-relative">
                                                    <button type="button" data-value="" class="checked text-center">
                                                        <span></span>
                                                    </button>
                                                    <input type="text" hidden class="form-control" name="funding_type[]"/>
                                                </td>

                                                <td class="position-relative">
                                                    <button type="button" data-value="" class="checked text-center">
                                                        <span></span>
                                                    </button>
                                                    <input type="text" hidden class="form-control" name="fund_contributions[]"/>
                                                </td>

                                                <td width="5%"><button class="btn btn-danger btn-sm removeButton" type="button"><i class="fas fa-times"></i></button></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-white p-0">
                                <div class="form-row float-right p-2 mr-3 ml-3">
                                    <button type="submit" class="btn btn-block btn-primary">Save <i class="fas fa-check"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


