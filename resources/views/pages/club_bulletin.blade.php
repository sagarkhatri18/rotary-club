@extends('layouts.app')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-left">
                        <li class="breadcrumb-item"><a href="index.blade.php">Home</a></li>
                        <li class="breadcrumb-item active">Club Bulletin</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header card_header p-0">
                            <p class="card-title ml-3"><small>Club Bulletin</small></p>
                        </div>
                        <form class="mt-sm-3 mt-md-0" method="post" action="{{route('club_bulletin.store')}}">
                            @csrf
                            <div class="card-body card_body pt-sm-0 pt-md-3 pb-0">
                                <div class="form-row">
                                    <div class="form-group col-xl-3 col-md-12 col-sm-6">
                                        <label for="club_name"><strong>Club Name</strong></label>
                                        <input type="text" readonly class="form-control-plaintext" id="club_name"
                                               value="{{Auth::user()->club_name}}">
                                    </div>
                                    <input type="hidden" name="id" value="{{$club_bulletins?($club_bulletins->id ? $club_bulletins->id : ""):""}}"/>
                                    <input type="hidden" name="month_id" value="{{$month_id}}"/>
                                    <div class="form-group col-xl-3 col-md-12 col-sm-6">
                                        <label for="members"><strong>Total Number of Members:</strong></label>
                                        <input type="text" required name="no_of_members" class="form-control"
                                               value="{{$club_bulletins?$club_bulletins->no_of_members:""}}" id="members">
                                    </div>
                                    <div class="form-group col-xl-3 col-md-12 col-sm-6">
                                        <label for="attendance"><strong>Club attendance:</strong></label>
                                        <input type="text" class="form-control" id="attendance" name="club_attendance"
                                               value="{{$club_bulletins?$club_bulletins->club_attendance:""}}">
                                    </div>
                                    <div class="form-group col-xl-3 col-md-12 col-sm-6">
                                        <label for="annual_target"><strong>Annual Target of attendance(%):</strong></label>
                                        <input type="text" id="annual_target" class="form-control" name="annual_target_percent"
                                               value="{{$club_bulletins?$club_bulletins->annual_target_percent:""}}">
                                    </div>
                                </div>

                                <div class="table-responsive mb-3">
                                    <table class="table table-bordered mb-0">
                                        <thead>
                                        <tr>
{{--                                            <th colspan="6" class="text-left">Title of the Club Bulletin: &nbsp;--}}
{{--                                                <textarea style="height:30px;" class="form-control" name="title">--}}
{{--                                                    {{$club_bulletins?$club_bulletins->title:""}}--}}
{{--                                                </textarea>--}}
{{--                                            </th>--}}
                                            <th colspan="6">
                                                <div class="form-group row mt-2 mb-0">
                                                    <label for="title" class="col-sm-2 col-form-label">Title of the Club Bulletin:</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" name="title" class="form-control" id="title" value="{{$club_bulletins?$club_bulletins->title:""}}">
                                                    </div>
                                                </div>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th rowspan="2"></th>
                                            <th colspan="3">Frequency of publication Published</th>
                                            <th colspan="2">Published in </th>
                                        </tr>
                                        <tr>
                                            <th width="15%">Weekly</th>
                                            <th width="15%">Fortnightly</th>
                                            <th width="15%">Monthly</th>
                                            <th width="15%">Hard Copy</th>
                                            <th width="15%">E-copy</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>No of issues published in this month</td>
{{--                                            <td><textarea style="width: 100%;" class="form-control" name="no_of_issue_first"--}}
{{--                                                >{{$club_bulletins?json_decode($club_bulletins->no_of_issue)->first:""}}</textarea></td>--}}
{{--                                            <td><textarea style="width: 100%;" class="form-control" name="no_of_issue_second"--}}
{{--                                                >{{$club_bulletins?json_decode($club_bulletins->no_of_issue)->second:""}}</textarea></td>--}}
{{--                                            <td><textarea style="width: 100%;" class="form-control" name="no_of_issue_third"--}}
{{--                                                >{{$club_bulletins?json_decode($club_bulletins->no_of_issue)->third:""}}</textarea></td>--}}
{{--                                            <td><textarea style="width: 100%;" class="form-control" name="no_of_issue_fourth"--}}
{{--                                                >{{$club_bulletins?json_decode($club_bulletins->no_of_issue)->fourth:""}}</textarea></td>--}}
{{--                                            <td><textarea style="width: 100%;" class="form-control" name="no_of_issue_fifth"--}}
{{--                                                >{{$club_bulletins?json_decode($club_bulletins->no_of_issue)->fifth:""}}</textarea></td>--}}

                                            <td>
                                                <select class="form-control" name="no_of_issue_first">
                                                    <option value="">Select..</option>
                                                    <option value="Yes" {!! $club_bulletins?json_decode($club_bulletins->no_of_issue)->first == 'Yes' ? "selected" :"" :"" !!}>YES</option>
                                                    <option value="No" {!! $club_bulletins?json_decode($club_bulletins->no_of_issue)->first == 'No' ? "selected" :"" :"" !!}>NO</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select class="form-control" name="no_of_issue_second">
                                                    <option value="">Select..</option>
                                                    <option value="Yes" {!! $club_bulletins?json_decode($club_bulletins->no_of_issue)->second == 'Yes' ? "selected" :"" :"" !!}>YES</option>
                                                    <option value="No" {!! $club_bulletins?json_decode($club_bulletins->no_of_issue)->second == 'No' ? "selected" :"" :"" !!}>NO</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select class="form-control" name="no_of_issue_third">
                                                    <option value="">Select..</option>
                                                    <option value="Yes" {!! $club_bulletins?json_decode($club_bulletins->no_of_issue)->third == 'Yes' ? "selected" :"" :"" !!}>YES</option>
                                                    <option value="No" {!! $club_bulletins?json_decode($club_bulletins->no_of_issue)->third == 'No' ? "selected" :"" :"" !!}>NO</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select class="form-control" name="no_of_issue_fourth">
                                                    <option value="">Select..</option>
                                                    <option value="Yes" {!! $club_bulletins?json_decode($club_bulletins->no_of_issue)->fourth == 'Yes' ? "selected" :"" :"" !!}>YES</option>
                                                    <option value="No" {!! $club_bulletins?json_decode($club_bulletins->no_of_issue)->fourth == 'No' ? "selected" :"" :"" !!}>NO</option>
                                                </select>
                                            </td>
                                            <td>
                                                <select class="form-control" name="no_of_issue_fifth">
                                                    <option value="">Select..</option>
                                                    <option value="Yes" {!! $club_bulletins?json_decode($club_bulletins->no_of_issue)->fifth == 'Yes' ? "selected" :"" :"" !!}>YES</option>
                                                    <option value="No" {!! $club_bulletins?json_decode($club_bulletins->no_of_issue)->fifth == 'No' ? "selected" :"" :"" !!}>NO</option>
                                                </select>
                                            </td>
                                        </tr>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th colspan="6" class="text-center">Attach e-copies in the monthly report</th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer text-white p-0">
                                <div class="form-row float-right p-2 mr-3 ml-3">
                                    <button type="submit" class="btn btn-block btn-primary">Save <i class="fas fa-check"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


