@extends('layouts.app')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-left">
                        <li class="breadcrumb-item"><a href="index.blade.php">Home</a></li>
                        <li class="breadcrumb-item active">Club Flagship/Signature Project</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header card_header p-0">
                            <p class="card-title ml-3"><small>CLUB FLAGSHIP/SIGNATURE PROJECT</small></p>
                        </div>
                        <form class="mt-sm-3 mt-md-0" method="post" action="{{route('club_flagship.store')}}">
                            @csrf
                            <div class="card-body card_body pt-sm-0 pt-md-3 pb-0">
                                <div class="form-row">
                                    <div class="form-group col-xl-4 col-md-12 col-sm-6">
                                        <label for="club_name"><strong>Club Name</strong></label>
                                        <input type="text" readonly class="form-control-plaintext" id="club_name"
                                               value="{{Auth::user()->club_name}}">
                                    </div>
                                    <input type="hidden" name="id" value="{{$club_flagship?($club_flagship->id ? $club_flagship->id : ""):""}}"/>
                                    <input type="hidden" name="month_id" value="{{$month_id}}"/>
                                </div>

                                <div class="table-responsive mb-3">
                                    <table class="table table-bordered insert_more mb-0">
                                        <thead class="tbl_body">
                                        <tr>
                                            <th colspan="3">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        Name of CLUB FLAGSHIP/SIGNATURE PROJECT:
                                                    </div>

                                                    <div class="position-relative" style="width: 50%; height: 40px; cursor:pointer;">
                                                        <button type="button" data-value="{{$club_flagship ? $club_flagship->name_of_club_flagship :""}}" class="divChecked text-center">
                                                            <span>{!! $club_flagship ? $club_flagship->name_of_club_flagship ? "<i class='fas fa-check'></i>" : "":"" !!}</span>
                                                        </button>
                                                        <input type="text" hidden class="form-control" name="name_of_club_flagship"
                                                               value="{{$club_flagship ? $club_flagship->name_of_club_flagship:""}}"/>
                                                    </div>
                                                </div>
                                            </th>
                                            <th colspan="4">Partner Name:
                                                <textarea style="width: 74%; height: 40px;" name="partner_name" class="form-control float-right">{{$club_flagship?($club_flagship->partner_name ? $club_flagship->partner_name : ""):""}}</textarea>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th rowspan="2" class="align-middle">S.N</th>
                                            <th rowspan="2" class="align-middle" style="width: 20%">Describe what activities have been implemented in the month</th>
                                            <th rowspan="2" class="align-middle">Project : Completed/
                                                Ongoing
                                            </th>
                                            <th rowspan="2" class="align-middle">Beneficiaries</th>
                                            <th rowspan="2" class="align-middle">Area/Location</th>
                                            <th colspan="2">Project Funding Type</th>
                                        </tr>
                                        <tr style="font-size: 13px;">
                                            <th class="font-weight-lighter" colspan="2" style="width: 20%">•	Global Grant <br>
                                                •	District Grant <br>
                                                •	International partner <br>
                                                •	Club to Club  funding support <br>
                                                •	Internal Club fund <br>
                                                •	Others
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody class="tbl_body">
                                        @if (!is_null($club_flagship_data))
                                            @for ($i=0; $i<count($club_flagship_data); $i++)
                                                <tr>
                                                    <td>{{$i+1}}.</td>
                                                    <input type="hidden" name="club_flagship_data_id[]" value="{{$club_flagship_data[$i]->id}}"/>

                                                    <td class="position-relative">
                                                        <button type="button" data-value="{{$club_flagship_data ? $club_flagship_data[$i]->activities ? $club_flagship_data[$i]->activities : "":""}}" class="checked text-center">
                                                            <span>{!! $club_flagship_data ? $club_flagship_data[$i]->activities ? "<i class='fas fa-check'></i>" : "":"" !!}</span>
                                                        </button>
                                                        <input type="text" hidden class="form-control" name="activities[]"
                                                               value="{{$club_flagship_data ? $club_flagship_data[$i]->activities ? $club_flagship_data[$i]->activities : "":""}}"/>
                                                    </td>

                                                    <td><textarea style="width: 100%; height: 40px;" class="form-control" name="project_check[]">{{$club_flagship_data[$i]->project_check}}</textarea></td>

                                                    <td class="position-relative">
                                                        <button type="button" data-value="{{$club_flagship_data ? $club_flagship_data[$i]->beneficiaries ? $club_flagship_data[$i]->beneficiaries : "":""}}" class="checked text-center">
                                                            <span>{!! $club_flagship_data ? $club_flagship_data[$i]->beneficiaries ? "<i class='fas fa-check'></i>" : "":"" !!}</span>
                                                        </button>
                                                        <input type="text" hidden class="form-control" name="beneficiaries[]"
                                                               value="{{$club_flagship_data ? $club_flagship_data[$i]->beneficiaries ? $club_flagship_data[$i]->beneficiaries : "":""}}"/>
                                                    </td>

                                                    <td><textarea style="width: 100%; height: 40px;" class="form-control" name="area[]">{{$club_flagship_data[$i]->area}}</textarea></td>

                                                    <td class="position-relative">
                                                        <button type="button" data-value="{{$club_flagship_data ? $club_flagship_data[$i]->funding_type ? $club_flagship_data[$i]->funding_type : "":""}}" class="checked text-center">
                                                            <span>{!! $club_flagship_data ? $club_flagship_data[$i]->funding_type ? "<i class='fas fa-check'></i>" : "":"" !!}</span>
                                                        </button>
                                                        <input type="text" hidden class="form-control" name="funding_type[]"
                                                               value="{{$club_flagship_data ? $club_flagship_data[$i]->funding_type ? $club_flagship_data[$i]->funding_type : "":""}}"/>
                                                    </td>

                                                    {!!($i == 0)?'<td style="width: 5%"><button class="btn btn-primary btn-sm addButtonClass" type="button"><i class="fas fa-plus"></i></button></td>':
                                                     '<td style="width: 5%" class="text-center"><i class="fas fa-check"></i></td>'!!}
                                                </tr>
                                            @endfor
                                        @else
                                            <tr>
                                                <td>1.</td>

                                                <td class="position-relative">
                                                    <button type="button" data-value="" class="checked text-center">
                                                        <span></span>
                                                    </button>
                                                    <input type="text" hidden class="form-control" name="activities[]"/>
                                                </td>

                                                <td><textarea style="width: 100%; height: 40px;" name="project_check[]" class="form-control"></textarea></td>

                                                <td class="position-relative">
                                                    <button type="button" data-value="" class="checked text-center">
                                                        <span></span>
                                                    </button>
                                                    <input type="text" hidden class="form-control" name="beneficiaries[]"/>
                                                </td>

                                                <td><textarea style="width: 100%; height: 40px;" name="area[]" class="form-control"></textarea></td>

                                                <td class="position-relative">
                                                    <button type="button" data-value="" class="checked text-center">
                                                        <span></span>
                                                    </button>
                                                    <input type="text" hidden class="form-control" name="funding_type[]"/>
                                                </td>

                                                <td style="width: 5%"><button class="btn btn-primary btn-sm addButtonClass" type="button"><i class="fas fa-plus"></i></button></td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                    <div class="d-none">
                                        <table class="clone_table">
                                            <tr>
                                                <td><span class="sn"></span></td>
                                                <input type="hidden" name="club_flagship_data_id[]" value=""/>

                                                <td class="position-relative">
                                                    <button type="button" data-value="" class="checked text-center">
                                                        <span></span>
                                                    </button>
                                                    <input type="text" hidden class="form-control" name="activities[]"/>
                                                </td>

                                                <td><textarea style="width: 100%; height: 40px;" name="project_check[]" class="form-control"></textarea></td>

                                                <td class="position-relative">
                                                    <button type="button" data-value="" class="checked text-center">
                                                        <span></span>
                                                    </button>
                                                    <input type="text" hidden class="form-control" name="beneficiaries[]"/>
                                                </td>

                                                <td><textarea style="width: 100%; height: 40px;" name="area[]" class="form-control"></textarea></td>

                                                <td class="position-relative">
                                                    <button type="button" data-value="" class="checked text-center">
                                                        <span></span>
                                                    </button>
                                                    <input type="text" hidden class="form-control" name="funding_type[]"/>
                                                </td>
                                                <td><button class="btn btn-danger btn-sm removeButton" type="button"><i class="fas fa-times"></i></button></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-white p-0">
                                <div class="form-row float-right p-2 mr-3 ml-3">
                                    <button type="submit" class="btn btn-block btn-primary">Save <i class="fas fa-check"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


