@extends('layouts.app')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-left">
                        <li class="breadcrumb-item"><a href="index.blade.php">Home</a></li>
                        <li class="breadcrumb-item active">Club Meetings Conducted</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header card_header p-0">
                            <p class="card-title ml-3"><small>Club Meetings Conducted</small></p>
                        </div>

                        <form class="mt-sm-3 mt-md-0" method="post" action="{{route('club_meetings.store')}}">
                            @csrf
                            <div class="card-body card_body pt-sm-0 pt-md-3 pb-0">
                                <div class="form-row">
                                    <div class="form-group col-xl-3 col-md-12 col-sm-6">
                                        <label for="club_name"><strong>Club Name:</strong></label>
                                        <input type="text" readonly class="form-control-plaintext" id="club_name"
                                               value="{{Auth::user()->club_name}}">
                                    </div>
                                    <div class="form-group col-xl-3 col-md-12 col-sm-6">
                                        <label for="members"><strong>Total Number of Members:</strong></label>
                                        <input type="text" required class="form-control" id="members" name="no_of_members"
                                               value="{{$club_meetings?$club_meetings->no_of_members:""}}">
                                    </div>
                                    <div class="form-group col-xl-3 col-md-12 col-sm-6">
                                        <label for="attendance"><strong>Club attendance:</strong></label>
                                        <input type="text" class="form-control" id="attendance" name="club_attendance"
                                               value="{{$club_meetings?$club_meetings->club_attendance:""}}">
                                    </div>
                                    <div class="form-group col-xl-3 col-md-12 col-sm-6">
                                        <label for="annual_target"><strong>Annual Target of
                                                attendance(%):</strong></label>
                                        <input type="text" id="annual_target" class="form-control" name="annual_target_percent"
                                               value="{{$club_meetings?$club_meetings->annual_target_percent:""}}">
                                    </div>
                                </div>

                                <div class="table-responsive mb-3">
                                    <table class="table table-bordered mb-0">
                                        <thead>
                                        <tr>
                                            <th rowspan="2" class="align-middle" style="width: 35%">A.1.1 Weekly
                                                Meetings focused on
                                            </th>
                                            <th colspan="5">Week</th>
                                        </tr>
                                        <tr>
                                            <th style="width: 10%">FIRST</th>
                                            <th style="width: 10%">SECOND</th>
                                            <th style="width: 10%">THIRD</th>
                                            <th style="width: 10%">FOURTH</th>
                                            <th style="width: 10%">FIFTH</th>
                                        </tr>
                                        </thead>
                                        <tbody class="tbl_body">
                                        <tr>
                                            <input type="hidden" name="id" value="{{$club_meetings?($club_meetings->id ? $club_meetings->id : ""):""}}"/>
                                            <input type="hidden" name="month_id" value="{{$month_id}}"/>

                                            <td>External Guest speaker (Mention the name of guest speaker and title)</td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->guest_speaker)->first ? json_decode($club_meetings->guest_speaker)->first : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->guest_speaker)->first ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="guest_speaker_first"
                                                       value="{{$club_meetings?(json_decode($club_meetings->guest_speaker)->first ? json_decode($club_meetings->guest_speaker)->first : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->guest_speaker)->second ? json_decode($club_meetings->guest_speaker)->second : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->guest_speaker)->second ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="guest_speaker_second"
                                                       value="{{$club_meetings?(json_decode($club_meetings->guest_speaker)->second ? json_decode($club_meetings->guest_speaker)->second : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->guest_speaker)->third ? json_decode($club_meetings->guest_speaker)->third : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->guest_speaker)->third ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="guest_speaker_third"
                                                       value="{{$club_meetings?(json_decode($club_meetings->guest_speaker)->third ? json_decode($club_meetings->guest_speaker)->third : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->guest_speaker)->fourth ? json_decode($club_meetings->guest_speaker)->fourth : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->guest_speaker)->fourth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="guest_speaker_fourth"
                                                       value="{{$club_meetings?(json_decode($club_meetings->guest_speaker)->fourth ? json_decode($club_meetings->guest_speaker)->fourth : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->guest_speaker)->fifth ? json_decode($club_meetings->guest_speaker)->fifth : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->guest_speaker)->fifth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="guest_speaker_fifth"
                                                       value="{{$club_meetings?(json_decode($club_meetings->guest_speaker)->fifth ? json_decode($club_meetings->guest_speaker)->fifth : ""):""}}"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Classification Talks: (Name of the Club member and his/her
                                                classification subject)
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->classification_talks)->first ? json_decode($club_meetings->classification_talks)->first : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->classification_talks)->first ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="classification_talks_first"
                                                       value="{{$club_meetings?(json_decode($club_meetings->classification_talks)->first ? json_decode($club_meetings->classification_talks)->first : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->classification_talks)->second ? json_decode($club_meetings->classification_talks)->second : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->classification_talks)->second ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="classification_talks_second"
                                                       value="{{$club_meetings?(json_decode($club_meetings->classification_talks)->second ? json_decode($club_meetings->classification_talks)->second : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->classification_talks)->third ? json_decode($club_meetings->classification_talks)->third : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->classification_talks)->third ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="classification_talks_third"
                                                       value="{{$club_meetings?(json_decode($club_meetings->classification_talks)->third ? json_decode($club_meetings->classification_talks)->third : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->classification_talks)->fourth ? json_decode($club_meetings->classification_talks)->fourth : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->classification_talks)->fourth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="classification_talks_fourth"
                                                       value="{{$club_meetings?(json_decode($club_meetings->classification_talks)->fourth ? json_decode($club_meetings->classification_talks)->fourth : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->classification_talks)->fifth ? json_decode($club_meetings->classification_talks)->fifth : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->classification_talks)->fifth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="classification_talks_fifth"
                                                       value="{{$club_meetings?(json_decode($club_meetings->classification_talks)->fifth ? json_decode($club_meetings->classification_talks)->fifth : ""):""}}"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Rotary program talks by District Leader
                                                (Name of the District Leader and topic)
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->rotary_program)->first ? json_decode($club_meetings->rotary_program)->first : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->rotary_program)->first ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="rotary_program_first"
                                                       value="{{$club_meetings?(json_decode($club_meetings->rotary_program)->first ? json_decode($club_meetings->rotary_program)->first : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->rotary_program)->second ? json_decode($club_meetings->rotary_program)->second : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->rotary_program)->second ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="rotary_program_second"
                                                       value="{{$club_meetings?(json_decode($club_meetings->rotary_program)->second ? json_decode($club_meetings->rotary_program)->second : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->rotary_program)->third ? json_decode($club_meetings->rotary_program)->third : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->rotary_program)->third ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="rotary_program_third"
                                                       value="{{$club_meetings?(json_decode($club_meetings->rotary_program)->third ? json_decode($club_meetings->rotary_program)->third : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->rotary_program)->fourth ? json_decode($club_meetings->rotary_program)->fourth : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->rotary_program)->fourth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="rotary_program_fourth"
                                                       value="{{$club_meetings?(json_decode($club_meetings->rotary_program)->fourth ? json_decode($club_meetings->rotary_program)->fourth : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->rotary_program)->fifth ? json_decode($club_meetings->rotary_program)->fifth : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->rotary_program)->fifth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="rotary_program_fifth"
                                                       value="{{$club_meetings?(json_decode($club_meetings->rotary_program)->fifth ? json_decode($club_meetings->rotary_program)->fifth : ""):""}}"/>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td>Meeting combined with service project activities visit (Name of the
                                                service project visited)
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->meeting_combined)->first ? json_decode($club_meetings->meeting_combined)->first : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->meeting_combined)->first ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="meeting_combined_first"
                                                       value="{{$club_meetings?(json_decode($club_meetings->meeting_combined)->first ? json_decode($club_meetings->meeting_combined)->first : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->meeting_combined)->second ? json_decode($club_meetings->meeting_combined)->second : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->meeting_combined)->second ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="meeting_combined_second"
                                                       value="{{$club_meetings?(json_decode($club_meetings->meeting_combined)->second ? json_decode($club_meetings->meeting_combined)->second : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->meeting_combined)->third ? json_decode($club_meetings->meeting_combined)->third : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->meeting_combined)->third ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="meeting_combined_third"
                                                       value="{{$club_meetings?(json_decode($club_meetings->meeting_combined)->third ? json_decode($club_meetings->meeting_combined)->third : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->meeting_combined)->fourth ? json_decode($club_meetings->meeting_combined)->fourth : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->meeting_combined)->fourth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="meeting_combined_fourth"
                                                       value="{{$club_meetings?(json_decode($club_meetings->meeting_combined)->fourth ? json_decode($club_meetings->meeting_combined)->fourth : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->meeting_combined)->fifth ? json_decode($club_meetings->meeting_combined)->fifth : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->meeting_combined)->fifth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="meeting_combined_fifth"
                                                       value="{{$club_meetings?(json_decode($club_meetings->meeting_combined)->fifth ? json_decode($club_meetings->meeting_combined)->fifth : ""):""}}"/>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td>Club business meeting or other focus
                                                (mention below)
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->club_business)->first ? json_decode($club_meetings->club_business)->first : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->club_business)->first ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="club_business_first"
                                                       value="{{$club_meetings?(json_decode($club_meetings->club_business)->first ? json_decode($club_meetings->club_business)->first : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->club_business)->second ? json_decode($club_meetings->club_business)->second : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->club_business)->second ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="club_business_second"
                                                       value="{{$club_meetings?(json_decode($club_meetings->club_business)->second ? json_decode($club_meetings->club_business)->second : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->club_business)->third ? json_decode($club_meetings->club_business)->third : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->club_business)->third ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="club_business_third"
                                                       value="{{$club_meetings?(json_decode($club_meetings->club_business)->third ? json_decode($club_meetings->club_business)->third : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->club_business)->fourth ? json_decode($club_meetings->club_business)->fourth : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->club_business)->fourth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="club_business_fourth"
                                                       value="{{$club_meetings?(json_decode($club_meetings->club_business)->fourth ? json_decode($club_meetings->club_business)->fourth : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->club_business)->fifth ? json_decode($club_meetings->club_business)->fifth : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->club_business)->fifth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="club_business_fifth"
                                                       value="{{$club_meetings?(json_decode($club_meetings->club_business)->fifth ? json_decode($club_meetings->club_business)->fifth : ""):""}}"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th colspan="6">Weekly meeting attendance including make ups within 14
                                                days
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>Members'Presence No.</td>
                                            <td>
                                                <textarea class="form-control" name="member_presence_first" style="width:100%; height: 40px;">{{$club_meetings?json_decode($club_meetings->member_presence)->first:""}}</textarea>
                                            </td>
                                            <td>
                                                <textarea class="form-control" name="member_presence_second"
                                                          style="width:100%; height: 40px;">{{$club_meetings?json_decode($club_meetings->member_presence)->second:""}}</textarea>
                                            </td>
                                            <td>
                                                <textarea class="form-control" name="member_presence_third"
                                                          style="width:100%; height: 40px;">{{$club_meetings?json_decode($club_meetings->member_presence)->third:""}}</textarea>
                                            </td>
                                            <td>
                                                <textarea class="form-control" name="member_presence_fourth"
                                                          style="width:100%; height: 40px;">{{$club_meetings?json_decode($club_meetings->member_presence)->fourth:""}}</textarea>
                                            </td>
                                            <td>
                                                <textarea class="form-control" name="member_presence_fifth"
                                                          style="width:100%; height: 40px;">{{$club_meetings?json_decode($club_meetings->member_presence)->fifth:""}}</textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>% of total</td>
                                            <td>
                                                <textarea class="form-control" name="total_percent_first"
                                                          style="width:100%; height: 40px;">{{$club_meetings?json_decode($club_meetings->total_percent)->first:""}}</textarea>
                                            </td>
                                            <td>
                                                <textarea class="form-control" name="total_percent_second"
                                                          style="width:100%; height: 40px;">{{$club_meetings?json_decode($club_meetings->total_percent)->second:""}}</textarea>
                                            </td>
                                            <td>
                                                <textarea class="form-control" name="total_percent_third"
                                                          style="width:100%; height: 40px;">{{$club_meetings?json_decode($club_meetings->total_percent)->third:""}}</textarea>
                                            </td>
                                            <td>
                                                <textarea class="form-control" name="total_percent_fourth"
                                                          style="width:100%; height: 40px;">{{$club_meetings?json_decode($club_meetings->total_percent)->fourth:""}}</textarea>
                                            </td>
                                            <td>
                                                <textarea class="form-control" name="total_percent_fifth"
                                                          style="width:100%; height: 40px;">{{$club_meetings?json_decode($club_meetings->total_percent)->fifth:""}}</textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th colspan="6">Other Club meetings ( No of attendance and date)</th>
                                        </tr>
                                        <tr>
                                            <td>A.1.2 Board (no. and date)</td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->board)->first ? json_decode($club_meetings->board)->first : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->board)->first ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="board_first"
                                                       value="{{$club_meetings?(json_decode($club_meetings->board)->first ? json_decode($club_meetings->board)->first : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->board)->second ? json_decode($club_meetings->board)->second : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->board)->second ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="board_second"
                                                       value="{{$club_meetings?(json_decode($club_meetings->board)->second ? json_decode($club_meetings->board)->second : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->board)->third ? json_decode($club_meetings->board)->third : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->board)->third ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="board_third"
                                                       value="{{$club_meetings?(json_decode($club_meetings->board)->third ? json_decode($club_meetings->board)->third : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->board)->fourth ? json_decode($club_meetings->board)->fourth : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->board)->fourth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="board_fourth"
                                                       value="{{$club_meetings?(json_decode($club_meetings->board)->fourth ? json_decode($club_meetings->board)->fourth : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->board)->fifth ? json_decode($club_meetings->board)->fifth : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->board)->fifth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="board_fifth"
                                                       value="{{$club_meetings?(json_decode($club_meetings->board)->fifth ? json_decode($club_meetings->board)->fifth : ""):""}}"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>A.1.3 Club Assembly (No. and date)</td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->club_assembly)->first ? json_decode($club_meetings->club_assembly)->first : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->club_assembly)->first ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="club_assembly_first"
                                                       value="{{$club_meetings?(json_decode($club_meetings->club_assembly)->first ? json_decode($club_meetings->club_assembly)->first : ""):""}}"/>
                                            </td>
                                            
                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->club_assembly)->second ? json_decode($club_meetings->club_assembly)->second : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->club_assembly)->second ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="club_assembly_second"
                                                       value="{{$club_meetings?(json_decode($club_meetings->club_assembly)->second ? json_decode($club_meetings->club_assembly)->second : ""):""}}"/>
                                            </td>
                                            
                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->club_assembly)->third ? json_decode($club_meetings->club_assembly)->third : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->club_assembly)->third ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="club_assembly_third"
                                                       value="{{$club_meetings?(json_decode($club_meetings->club_assembly)->third ? json_decode($club_meetings->club_assembly)->third : ""):""}}"/>
                                            </td>
                                            
                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->club_assembly)->fourth ? json_decode($club_meetings->club_assembly)->fourth : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->club_assembly)->fourth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="club_assembly_fourth"
                                                       value="{{$club_meetings?(json_decode($club_meetings->club_assembly)->fourth ? json_decode($club_meetings->club_assembly)->fourth : ""):""}}"/>
                                            </td>
                                            
                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->club_assembly)->fifth ? json_decode($club_meetings->club_assembly)->fifth : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->club_assembly)->fifth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="club_assembly_fifth"
                                                       value="{{$club_meetings?(json_decode($club_meetings->club_assembly)->fifth ? json_decode($club_meetings->club_assembly)->fifth : ""):""}}"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>A.1.4 Club Committee (Name of Committee)</td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->club_committee)->first ? json_decode($club_meetings->club_committee)->first : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->club_committee)->first ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="club_committee_first"
                                                       value="{{$club_meetings?(json_decode($club_meetings->club_committee)->first ? json_decode($club_meetings->club_committee)->first : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->club_committee)->second ? json_decode($club_meetings->club_committee)->second : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->club_committee)->second ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="club_committee_second"
                                                       value="{{$club_meetings?(json_decode($club_meetings->club_committee)->second ? json_decode($club_meetings->club_committee)->second : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->club_committee)->third ? json_decode($club_meetings->club_committee)->third : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->club_committee)->third ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="club_committee_third"
                                                       value="{{$club_meetings?(json_decode($club_meetings->club_committee)->third ? json_decode($club_meetings->club_committee)->third : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->club_committee)->fourth ? json_decode($club_meetings->club_committee)->fourth : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->club_committee)->fourth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="club_committee_fourth"
                                                       value="{{$club_meetings?(json_decode($club_meetings->club_committee)->fourth ? json_decode($club_meetings->club_committee)->fourth : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$club_meetings?(json_decode($club_meetings->club_committee)->fifth ? json_decode($club_meetings->club_committee)->fifth : ""):""}}" class="checked text-center">
                                                    <span>{!! $club_meetings?(json_decode($club_meetings->club_committee)->fifth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="club_committee_fifth"
                                                       value="{{$club_meetings?(json_decode($club_meetings->club_committee)->fifth ? json_decode($club_meetings->club_committee)->fifth : ""):""}}"/>
                                            </td>
                                        </tr>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th colspan="1">Total attendance in the month</th>
                                            <th colspan="5">
                                                <textarea class="form-control" name="month_attendance"
                                                                      style="width:100%; height: 40px; border: none;">{{$club_meetings?$club_meetings->month_attendance:""}}</textarea>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th colspan="1">Annual target for Club attendance</th>
                                            <th colspan="5">
                                                <textarea class="form-control" name="annual_target"
                                                                      style="width:100%; height: 40px; border: none;">{{$club_meetings?$club_meetings->annual_target:""}}</textarea>
                                            </th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer text-white p-0">
                                <div class="form-row float-right p-2 mr-3 ml-3">
                                    <button type="submit" class="btn btn-block btn-primary">Save <i
                                            class="fas fa-check"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


