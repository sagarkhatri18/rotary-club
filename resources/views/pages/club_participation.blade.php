@extends('layouts.app')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-left">
                        <li class="breadcrumb-item"><a href="index.blade.php">Home</a></li>
                        <li class="breadcrumb-item active">Club Participation in the District Events</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header card_header p-0">
                            <p class="card-title ml-3"><small>Club Participation in the District Events</small></p>
                        </div>
                        <form class="mt-sm-3 mt-md-0" method="post" action="{{route('club_participation.store')}}">
                            @csrf
                            <div class="card-body card_body pt-sm-0 pt-md-3 pb-0">
                                <div class="form-row">
                                    <div class="form-group col-xl-3 col-md-12 col-sm-6">
                                        <label for="club_name"><strong>Club Name</strong></label>
                                        <input type="text" readonly class="form-control-plaintext" id="club_name"
                                               value="{{Auth::user()->club_name}}">
                                    </div>
                                    <input type="hidden" name="id" value="{{$club_participation?($club_participation->id ? $club_participation->id : ""):""}}"/>
                                    <input type="hidden" name="month_id" value="{{$month_id}}"/>
                                    <div class="form-group col-xl-3 col-md-12 col-sm-6">
                                        <label for="members"><strong>Total Number of Members:</strong></label>
                                        <input type="text" required name="no_of_members" class="form-control" id="members"
                                               value="{{$club_participation?$club_participation->no_of_members:""}}">
                                    </div>
                                    <div class="form-group col-xl-3 col-md-12 col-sm-6">
                                        <label for="attendance"><strong>Club attendance:</strong></label>
                                        <input type="text" class="form-control" id="attendance" name="club_attendance"
                                               value="{{$club_participation?$club_participation->club_attendance:""}}">
                                    </div>
                                    <div class="form-group col-xl-3 col-md-12 col-sm-6">
                                        <label for="annual_target"><strong>Annual Target of attendance(%):</strong></label>
                                        <input type="text" id="annual_target" class="form-control" name="annual_target_percent"
                                               value="{{$club_participation?$club_participation->annual_target_percent:""}}">
                                    </div>
                                </div>

                                <div class="table-responsive mb-3">
                                    <table class="table table-bordered insert_more mb-0">
                                        <thead>
                                        <tr>
                                            <th rowspan="2" class="align-middle">S.N</th>
                                            <th rowspan="2" class="align-middle">Name of District Events in the Month</th>
                                            <th rowspan="2" class="align-middle">Date of Event</th>
                                            <th colspan="3">Club Events</th>
                                        </tr>
                                        <tr>
                                            <th>No of Club Participants</th>
                                            <th colspan="2">Event Sponsored (NRS)</th>
                                        </tr>
                                        </thead>
                                        <tbody class="tbl_body">
                                        @if (!is_null($club_participation_data))
                                            @for ($i=0; $i<count($club_participation_data); $i++)
                                                <tr>
                                                    <td>{{$i+1}}.</td>
                                                    <input type="hidden" name="participation_data_id[]" value="{{$club_participation_data[$i]->id}}"/>

                                                    <td class="position-relative">
                                                        <button type="button" data-value="{{$club_participation_data ? $club_participation_data[$i]->district_name ? $club_participation_data[$i]->district_name : "":""}}" class="block checked text-center">
                                                            <span>{!! $club_participation_data ? $club_participation_data[$i]->district_name ? "<i class='fas fa-check'></i>" : "":"" !!}</span>
                                                        </button>
                                                        <input type="text" hidden class="form-control" name="district_name[]"
                                                               value="{{$club_participation_data ? $club_participation_data[$i]->district_name ? $club_participation_data[$i]->district_name : "":""}}"/>
                                                    </td>

                                                    <td class="text-center"><input type="date" name="date[]" class="form-control" value="{{$club_participation_data[$i]->date}}"/></td>
                                                    <td><textarea style="width: 100%; height: 40px;" class="form-control" name="no_of_participants[]">{{$club_participation_data[$i]->no_of_participants}}</textarea></td>
                                                    <td><textarea style="width: 100%; height: 40px;" class="form-control" name="event_sponsored[]">{{$club_participation_data[$i]->event_sponsored}}</textarea></td>
                                                    {!!($i == 0)?'<td><button class="btn btn-primary btn-sm addButtonClass" type="button"><i class="fas fa-plus"></i></button></td>':
                                                     '<td class="text-center"><i class="fas fa-check"></i></td>'!!}
                                                </tr>
                                            @endfor
                                        @else
                                            <tr>
                                                <td>1.</td>

                                                <td class="position-relative">
                                                    <button type="button" data-value="" class="block checked text-center"><span></span></button>
                                                    <input type="text" hidden class="form-control" name="district_name[]"/>
                                                </td>

                                                <td class="text-center"><input type="date" name="date[]" class="form-control"/></td>
                                                <td><textarea style="width: 100%; height: 40px;" name="no_of_participants[]" class="form-control"></textarea></td>
                                                <td><textarea style="width: 100%; height: 40px;" name="event_sponsored[]" class="form-control"></textarea></td>
                                                <td><button class="btn btn-primary btn-sm addButtonClass" type="button"><i class="fas fa-plus"></i></button></td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                    <div class="d-none">
                                        <table class="clone_table sr-only">
                                            <tr>
                                                <td><span class="sn"></span></td>
                                                <input type="hidden" name="participation_data_id[]" value=""/>

                                                <td class="position-relative">
                                                    <button type="button" data-value="" class="checked text-center"><span></span></button>
                                                    <input type="text" hidden class="form-control" name="district_name[]"/>
                                                </td>

                                                <td class="text-center"><input type="date" name="date[]" class="form-control"/></td>
                                                <td><textarea style="width: 100%; height: 40px;" name="no_of_participants[]" class="form-control"></textarea></td>
                                                <td><textarea style="width: 100%; height: 40px;" name="event_sponsored[]" class="form-control"></textarea></td>
                                                <td><button class="btn btn-danger btn-sm removeButton" type="button"><i class="fas fa-times"></i></button></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-white p-0">
                                <div class="form-row float-right p-2 mr-3 ml-3">
                                    <button type="submit" class="btn btn-block btn-primary">Save <i class="fas fa-check"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


