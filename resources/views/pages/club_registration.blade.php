@extends('layouts.app')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-left">
                        <li class="breadcrumb-item"><a href="index.blade.php">Home</a></li>
                        <li class="breadcrumb-item active">Club Registration in Rotary Online</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header card_header p-0">
                            <p class="card-title ml-3"><small>Club Registration in Rotary Online</small></p>
                        </div>
                        <form class="mt-sm-3 mt-md-0" method="post" action="{{route('club_registration.store')}}">
                            @csrf
                            <div class="card-body card_body pt-sm-0 pt-md-3 pb-0">
                                <div class="form-row">
                                    <div class="form-group col-xl-3 col-md-12 col-sm-6">
                                        <label for="club_name"><strong>Club Name</strong></label>
                                        <input type="text" readonly class="form-control-plaintext" id="club_name"
                                               value="{{Auth::user()->club_name}}">
                                    </div>
                                    <input type="hidden" name="id" value="{{$club_registration?($club_registration->id ? $club_registration->id : ""):""}}"/>
                                    <input type="hidden" name="month_id" value="{{$month_id}}"/>
                                    <div class="form-group col-xl-3 col-md-12 col-sm-6">
                                        <label for="members"><strong>Total Number of Members:</strong></label>
                                        <input type="text" required name="no_of_members" class="form-control" id="members"
                                               value="{{$club_registration?$club_registration->no_of_members:""}}">
                                    </div>
                                    <div class="form-group col-xl-3 col-md-12 col-sm-6">
                                        <label for="attendance"><strong>Club attendance:</strong></label>
                                        <input type="text" class="form-control" id="attendance" name="club_attendance"
                                               value="{{$club_registration?$club_registration->club_attendance:""}}">
                                    </div>
                                    <div class="form-group col-xl-3 col-md-12 col-sm-6">
                                        <label for="annual_target"><strong>Annual Target of attendance(%):</strong></label>
                                        <input type="text" id="annual_target" class="form-control" name="annual_target_percent"
                                               value="{{$club_registration?$club_registration->annual_target_percent:""}}">
                                    </div>
                                </div>

                                <div class="table-responsive mb-3">
                                    <table class="table table-bordered mb-0" id="insert_more">
                                        <thead>
                                        <tr>
                                            <th colspan="2">Online Registration</th>
                                            <th>No.</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td style="width: 10%">1.</td>
                                            <td style="width: 70%">Total number of members registered  in MY ROTARY as of July 01, 2019</td>
                                            <td>
                                                <textarea style="width: 100%; height: 40px;" class="form-control" name="members_of_july">{{$club_registration?$club_registration->members_of_july:""}}</textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 10%">2.</td>
                                            <td style="width: 70%">Total number of members registered in MY ROTARY as of this reporting month</td>
                                            <td>
                                                <textarea style="width: 100%; height: 40px;" class="form-control" name="members_of_this_month">{{$club_registration?$club_registration->members_of_this_month:""}}</textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 10%">3.</td>
                                            <td style="width: 70%">Total number of Club Goals UPLOADED in the ROTARY CLUB CENTRAL</td>
                                            <td>
                                                <textarea style="width: 100%; height: 40px;" class="form-control" name="club_goals_uploaded">{{$club_registration?$club_registration->club_goals_uploaded:""}}</textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 10%">4.</td>
                                            <td style="width: 70%">Total number of Club Goals UPGRADED in the ROTARY CLUB CENTRAL</td>
                                            <td>
                                                <textarea style="width: 100%; height: 40px;" class="form-control" name="club_goals_upgraded">{{$club_registration?$club_registration->club_goals_upgraded:""}}</textarea>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer text-white p-0">
                                <div class="form-row float-right p-2 mr-3 ml-3">
                                    <button type="submit" class="btn btn-block btn-primary">Save <i class="fas fa-check"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


