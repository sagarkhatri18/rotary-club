@extends('layouts.app')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-left">
                        <li class="breadcrumb-item"><a href="index.blade.php">Home</a></li>
                        <li class="breadcrumb-item active">Disease Prevention & Treatment</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header card_header p-0">
                            <p class="card-title ml-3"><small>FOCUS AREA: DISEASE PREVENTION AND TREATMENT</small></p>
                        </div>
                        <form class="mt-sm-3 mt-md-0" method="post" action="{{route('disease_prevention.store')}}">
                            @csrf
                            <div class="card-body card_body pt-sm-0 pt-md-3 pb-0">
                                <div class="form-row">
                                    <div class="form-group col-xl-4 col-md-12 col-sm-6">
                                        <label for="club_name"><strong>Club Name</strong></label>
                                        <input type="text" readonly class="form-control-plaintext" id="club_name"
                                               value="{{Auth::user()->club_name}}">
                                    </div>
                                    <input type="hidden" name="id" value="{{$disease_prevention?($disease_prevention->id ? $disease_prevention->id : ""):""}}"/>
                                    <input type="hidden" name="month_id" value="{{$month_id}}"/>
                                    <div class="form-group col-xl-4 col-md-12 col-sm-6">
                                        <label for="wash"><strong>Name of the Project:</strong></label>
                                        <input type="text" required name="project_name" class="form-control" id="wash"
                                               value="{{$disease_prevention?$disease_prevention->project_name:""}}">
                                    </div>
                                    <div class="form-group col-xl-4 col-md-12 col-sm-6">
                                        <label for="address"><strong>Project Address (INSTITUTION/VDC/Municipality/Ward):</strong></label>
                                        <input type="text" name="project_address" class="form-control" id="address"
                                               value="{{$disease_prevention?$disease_prevention->project_address:""}}">
                                    </div>
                                </div>

                                <div class="table-responsive mb-3">
                                    <table class="table table-bordered mb-0">
                                        <thead>
                                        <tr>
                                            <th rowspan="2" class="align-middle">S.N</th>
                                            <th rowspan="2" class="align-middle" style="width: 30%">Project Activities</th>
                                            <th rowspan="2" class="align-middle">Project Completion Date</th>
                                            <th colspan="2">D. Project Results</th>
                                            <th>E. Project Funding Type*</th>
                                            <th>F. Total Fund Contributions*</th>
                                        </tr>
                                        <tr style="font-size: 13px;">
                                            <th class="font-weight-lighter" style="width: 10%">Beneficiaries <br>
                                                (b)	population , <br>
                                                or <br>
                                                &emsp;&emsp;(b)households <br>
                                                &emsp;(c) students <br>
                                                (c)&emsp;&emsp;&emsp;&emsp;&emsp;
                                            </th>
                                            <th class="font-weight-lighter" style="width: 10%">Outputs <br>
                                                outcomes
                                            </th>
                                            <th class="font-weight-lighter">•	Global Grant <br>
                                                •	District Grant <br>
                                                •	International partner <br>
                                                •	Club to Club  funding support <br>
                                                •	Internal Club fund <br>
                                                •	Others
                                            </th>
                                            <th class="font-weight-lighter">
                                                •	GG: <br>
                                                •	DG: <br>
                                                •	International Club/partner: <br>
                                                •	Club internal <br>
                                                •	Others
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody class="tbl_body">
                                        <tr>
                                            <td>1.</td>
                                            <td>Health Camp on:
                                                <textarea class="form-control" name="health_camp_first"
                                                          style="width:100%; height: 40px;">{{$disease_prevention?json_decode($disease_prevention->health_camp)->first:""}}</textarea>
                                            </td>
                                            <td class="text-center">
                                                <input type="date" name="health_camp_second" class="form-control" value="{{$disease_prevention?json_decode($disease_prevention->health_camp)->second:""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$disease_prevention?(json_decode($disease_prevention->health_camp)->third ? json_decode($disease_prevention->health_camp)->third : ""):""}}" class="checked text-center">
                                                    <span>{!! $disease_prevention?(json_decode($disease_prevention->health_camp)->third ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="health_camp_third"
                                                       value="{{$disease_prevention?(json_decode($disease_prevention->health_camp)->third ? json_decode($disease_prevention->health_camp)->third : ""):""}}"/>
                                            </td>
                                            
                                            <td class="position-relative">
                                                <button type="button" data-value="{{$disease_prevention?(json_decode($disease_prevention->health_camp)->fourth ? json_decode($disease_prevention->health_camp)->fourth : ""):""}}" class="checked text-center">
                                                    <span>{!! $disease_prevention?(json_decode($disease_prevention->health_camp)->fourth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="health_camp_fourth"
                                                       value="{{$disease_prevention?(json_decode($disease_prevention->health_camp)->fourth ? json_decode($disease_prevention->health_camp)->fourth : ""):""}}"/>
                                            </td>
                                            
                                            <td class="position-relative">
                                                <button type="button" data-value="{{$disease_prevention?(json_decode($disease_prevention->health_camp)->fifth ? json_decode($disease_prevention->health_camp)->fifth : ""):""}}" class="checked text-center">
                                                    <span>{!! $disease_prevention?(json_decode($disease_prevention->health_camp)->fifth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="health_camp_fifth"
                                                       value="{{$disease_prevention?(json_decode($disease_prevention->health_camp)->fifth ? json_decode($disease_prevention->health_camp)->fifth : ""):""}}"/>
                                            </td>
                                            
                                            <td class="position-relative">
                                                <button type="button" data-value="{{$disease_prevention?(json_decode($disease_prevention->health_camp)->sixth ? json_decode($disease_prevention->health_camp)->sixth : ""):""}}" class="checked text-center">
                                                    <span>{!! $disease_prevention?(json_decode($disease_prevention->health_camp)->sixth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="health_camp_sixth"
                                                       value="{{$disease_prevention?(json_decode($disease_prevention->health_camp)->sixth ? json_decode($disease_prevention->health_camp)->sixth : ""):""}}"/>
                                            </td>
                                            
                                        </tr>
                                        <tr>
                                            <td>2.</td>
                                            <td>Blood Donation Camp:
                                                <textarea class="form-control" name="blood_donation_first"
                                                          style="width:100%; height: 40px;">{{$disease_prevention?json_decode($disease_prevention->blood_donation)->first:""}}</textarea>
                                            </td>
                                            <td class="text-center">
                                                <input type="date" name="blood_donation_second" class="form-control" value="{{$disease_prevention?json_decode($disease_prevention->blood_donation)->second:""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$disease_prevention?(json_decode($disease_prevention->blood_donation)->third ? json_decode($disease_prevention->blood_donation)->third : ""):""}}" class="checked text-center">
                                                    <span>{!! $disease_prevention?(json_decode($disease_prevention->blood_donation)->third ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="blood_donation_third"
                                                       value="{{$disease_prevention?(json_decode($disease_prevention->blood_donation)->third ? json_decode($disease_prevention->blood_donation)->third : ""):""}}"/>
                                            </td>
                                            
                                            <td class="position-relative">
                                                <button type="button" data-value="{{$disease_prevention?(json_decode($disease_prevention->blood_donation)->fourth ? json_decode($disease_prevention->blood_donation)->fourth : ""):""}}" class="checked text-center">
                                                    <span>{!! $disease_prevention?(json_decode($disease_prevention->blood_donation)->fourth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="blood_donation_fourth"
                                                       value="{{$disease_prevention?(json_decode($disease_prevention->blood_donation)->fourth ? json_decode($disease_prevention->blood_donation)->fourth : ""):""}}"/>
                                            </td>
                                            
                                            <td class="position-relative">
                                                <button type="button" data-value="{{$disease_prevention?(json_decode($disease_prevention->blood_donation)->fifth ? json_decode($disease_prevention->blood_donation)->fifth : ""):""}}" class="checked text-center">
                                                    <span>{!! $disease_prevention?(json_decode($disease_prevention->blood_donation)->fifth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="blood_donation_fifth"
                                                       value="{{$disease_prevention?(json_decode($disease_prevention->blood_donation)->fifth ? json_decode($disease_prevention->blood_donation)->fifth : ""):""}}"/>
                                            </td>
                                            
                                            <td class="position-relative">
                                                <button type="button" data-value="{{$disease_prevention?(json_decode($disease_prevention->blood_donation)->sixth ? json_decode($disease_prevention->blood_donation)->sixth : ""):""}}" class="checked text-center">
                                                    <span>{!! $disease_prevention?(json_decode($disease_prevention->blood_donation)->sixth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="blood_donation_sixth"
                                                       value="{{$disease_prevention?(json_decode($disease_prevention->blood_donation)->sixth ? json_decode($disease_prevention->blood_donation)->sixth : ""):""}}"/>
                                            </td>
                                            
                                        </tr>
                                        <tr>
                                            <td>3.</td>
                                            <td>Training /capacity development on health related issues for:
                                                <textarea class="form-control" name="training_development_first"
                                                          style="width:100%; height: 40px;">{{$disease_prevention?json_decode($disease_prevention->training_development)->first:""}}</textarea>
                                            </td>
                                            <td class="text-center">
                                                <input type="date" name="training_development_second" class="form-control" value="{{$disease_prevention?json_decode($disease_prevention->training_development)->second:""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$disease_prevention?(json_decode($disease_prevention->training_development)->third ? json_decode($disease_prevention->training_development)->third : ""):""}}" class="checked text-center">
                                                    <span>{!! $disease_prevention?(json_decode($disease_prevention->training_development)->third ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="training_development_third"
                                                       value="{{$disease_prevention?(json_decode($disease_prevention->training_development)->third ? json_decode($disease_prevention->training_development)->third : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$disease_prevention?(json_decode($disease_prevention->training_development)->fourth ? json_decode($disease_prevention->training_development)->fourth : ""):""}}" class="checked text-center">
                                                    <span>{!! $disease_prevention?(json_decode($disease_prevention->training_development)->fourth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="training_development_fourth"
                                                       value="{{$disease_prevention?(json_decode($disease_prevention->training_development)->fourth ? json_decode($disease_prevention->training_development)->fourth : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$disease_prevention?(json_decode($disease_prevention->training_development)->fifth ? json_decode($disease_prevention->training_development)->fifth : ""):""}}" class="checked text-center">
                                                    <span>{!! $disease_prevention?(json_decode($disease_prevention->training_development)->fifth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="training_development_fifth"
                                                       value="{{$disease_prevention?(json_decode($disease_prevention->training_development)->fifth ? json_decode($disease_prevention->training_development)->fifth : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$disease_prevention?(json_decode($disease_prevention->training_development)->sixth ? json_decode($disease_prevention->training_development)->sixth : ""):""}}" class="checked text-center">
                                                    <span>{!! $disease_prevention?(json_decode($disease_prevention->training_development)->sixth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="training_development_sixth"
                                                       value="{{$disease_prevention?(json_decode($disease_prevention->training_development)->sixth ? json_decode($disease_prevention->training_development)->sixth : ""):""}}"/>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td>4.</td>
                                            <td>Health Equipment Supplies to:
                                                <textarea class="form-control" name="health_equipment_first" style="width:100%; height: 40px;"
                                                >{{$disease_prevention?json_decode($disease_prevention->health_equipment)->first:""}}</textarea></td>
                                            <td class="text-center">
                                                <input type="date" name="health_equipment_second" class="form-control" value="{{$disease_prevention?json_decode($disease_prevention->health_equipment)->second:""}}"/>
                                            </td>
                                            
                                            <td class="position-relative">
                                                <button type="button" data-value="{{$disease_prevention?(json_decode($disease_prevention->health_equipment)->third ? json_decode($disease_prevention->health_equipment)->third : ""):""}}" class="checked text-center">
                                                    <span>{!! $disease_prevention?(json_decode($disease_prevention->health_equipment)->third ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="health_equipment_third"
                                                       value="{{$disease_prevention?(json_decode($disease_prevention->health_equipment)->third ? json_decode($disease_prevention->health_equipment)->third : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$disease_prevention?(json_decode($disease_prevention->health_equipment)->fourth ? json_decode($disease_prevention->health_equipment)->fourth : ""):""}}" class="checked text-center">
                                                    <span>{!! $disease_prevention?(json_decode($disease_prevention->health_equipment)->fourth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="health_equipment_fourth"
                                                       value="{{$disease_prevention?(json_decode($disease_prevention->health_equipment)->fourth ? json_decode($disease_prevention->health_equipment)->fourth : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$disease_prevention?(json_decode($disease_prevention->health_equipment)->fifth ? json_decode($disease_prevention->health_equipment)->fifth : ""):""}}" class="checked text-center">
                                                    <span>{!! $disease_prevention?(json_decode($disease_prevention->health_equipment)->fifth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="health_equipment_fifth"
                                                       value="{{$disease_prevention?(json_decode($disease_prevention->health_equipment)->fifth ? json_decode($disease_prevention->health_equipment)->fifth : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$disease_prevention?(json_decode($disease_prevention->health_equipment)->sixth ? json_decode($disease_prevention->health_equipment)->sixth : ""):""}}" class="checked text-center">
                                                    <span>{!! $disease_prevention?(json_decode($disease_prevention->health_equipment)->sixth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="health_equipment_sixth"
                                                       value="{{$disease_prevention?(json_decode($disease_prevention->health_equipment)->sixth ? json_decode($disease_prevention->health_equipment)->sixth : ""):""}}"/>
                                            </td>
                                            
                                        </tr>
                                        <tr>
                                            <td>5.</td>
                                            <td>Health Hygiene promotional campaign on:
                                                <textarea class="form-control" name="health_hygiene_first"
                                                          style="width:100%; height: 40px;">{{$disease_prevention?json_decode($disease_prevention->health_hygiene)->first:""}}</textarea>
                                            </td>
                                            <td class="text-center">
                                                <input type="date" name="health_hygiene_second" class="form-control" value="{{$disease_prevention?json_decode($disease_prevention->health_hygiene)->second:""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$disease_prevention?(json_decode($disease_prevention->health_hygiene)->third ? json_decode($disease_prevention->health_hygiene)->third : ""):""}}" class="checked text-center">
                                                    <span>{!! $disease_prevention?(json_decode($disease_prevention->health_hygiene)->third ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="health_hygiene_third"
                                                       value="{{$disease_prevention?(json_decode($disease_prevention->health_hygiene)->third ? json_decode($disease_prevention->health_hygiene)->third : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$disease_prevention?(json_decode($disease_prevention->health_hygiene)->fourth ? json_decode($disease_prevention->health_hygiene)->fourth : ""):""}}" class="checked text-center">
                                                    <span>{!! $disease_prevention?(json_decode($disease_prevention->health_hygiene)->fourth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="health_hygiene_fourth"
                                                       value="{{$disease_prevention?(json_decode($disease_prevention->health_hygiene)->fourth ? json_decode($disease_prevention->health_hygiene)->fourth : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$disease_prevention?(json_decode($disease_prevention->health_hygiene)->fifth ? json_decode($disease_prevention->health_hygiene)->fifth : ""):""}}" class="checked text-center">
                                                    <span>{!! $disease_prevention?(json_decode($disease_prevention->health_hygiene)->fifth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="health_hygiene_fifth"
                                                       value="{{$disease_prevention?(json_decode($disease_prevention->health_hygiene)->fifth ? json_decode($disease_prevention->health_hygiene)->fifth : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$disease_prevention?(json_decode($disease_prevention->health_hygiene)->sixth ? json_decode($disease_prevention->health_hygiene)->sixth : ""):""}}" class="checked text-center">
                                                    <span>{!! $disease_prevention?(json_decode($disease_prevention->health_hygiene)->sixth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="health_hygiene_sixth"
                                                       value="{{$disease_prevention?(json_decode($disease_prevention->health_hygiene)->sixth ? json_decode($disease_prevention->health_hygiene)->sixth : ""):""}}"/>
                                            </td>
                                            
                                        </tr>
                                        <tr>
                                            <td>6.</td>
                                            <td>Others:
                                                <textarea class="form-control" name="others_first"
                                                          style="width:100%; height: 40px;">{{$disease_prevention?json_decode($disease_prevention->others)->first:""}}</textarea>
                                            </td>
                                            <td class="text-center">
                                                <input type="date" name="others_second" class="form-control" value="{{$disease_prevention?json_decode($disease_prevention->others)->second:""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$disease_prevention?(json_decode($disease_prevention->others)->third ? json_decode($disease_prevention->others)->third : ""):""}}" class="checked text-center">
                                                    <span>{!! $disease_prevention?(json_decode($disease_prevention->others)->third ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="others_third"
                                                       value="{{$disease_prevention?(json_decode($disease_prevention->others)->third ? json_decode($disease_prevention->others)->third : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$disease_prevention?(json_decode($disease_prevention->others)->fourth ? json_decode($disease_prevention->others)->fourth : ""):""}}" class="checked text-center">
                                                    <span>{!! $disease_prevention?(json_decode($disease_prevention->others)->fourth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="others_fourth"
                                                       value="{{$disease_prevention?(json_decode($disease_prevention->others)->fourth ? json_decode($disease_prevention->others)->fourth : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$disease_prevention?(json_decode($disease_prevention->others)->fifth ? json_decode($disease_prevention->others)->fifth : ""):""}}" class="checked text-center">
                                                    <span>{!! $disease_prevention?(json_decode($disease_prevention->others)->fifth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="others_fifth"
                                                       value="{{$disease_prevention?(json_decode($disease_prevention->others)->fifth ? json_decode($disease_prevention->others)->fifth : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$disease_prevention?(json_decode($disease_prevention->others)->sixth ? json_decode($disease_prevention->others)->sixth : ""):""}}" class="checked text-center">
                                                    <span>{!! $disease_prevention?(json_decode($disease_prevention->others)->sixth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="others_sixth"
                                                       value="{{$disease_prevention?(json_decode($disease_prevention->others)->sixth ? json_decode($disease_prevention->others)->sixth : ""):""}}"/>
                                            </td>

                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <p>OUTPUTS/OUTCOMES indicate : No of people served,
                                    No of blood donors/pints collected, No of people/health personnel
                                    trained, Name/No of health equipment supplied,  No of people reached with
                                    health messages etc.
                                </p>
                            </div>
                            <div class="card-footer text-white p-0">
                                <div class="form-row float-right p-2 mr-3 ml-3">
                                    <button type="submit" class="btn btn-block btn-primary">Save <i class="fas fa-check"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


