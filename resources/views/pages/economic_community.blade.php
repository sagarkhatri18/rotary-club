@extends('layouts.app')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-left">
                        <li class="breadcrumb-item"><a href="index.blade.php">Home</a></li>
                        <li class="breadcrumb-item active">Economic & Community Development</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header card_header p-0">
                            <p class="card-title ml-3"><small>FOCUS AREA: ECONOMIC AND COMMUNITY DEVELOPMENT</small></p>
                        </div>
                        <form class="mt-sm-3 mt-md-0" method="post" action="{{route('economic_community.store')}}">
                            @csrf
                            <div class="card-body card_body pt-sm-0 pt-md-3 pb-0">
                                <div class="form-row">
                                    <div class="form-group col-xl-4 col-md-12 col-sm-6">
                                        <label for="club_name"><strong>Club Name</strong></label>
                                        <input type="text" readonly class="form-control-plaintext" id="club_name"
                                               value="{{Auth::user()->club_name}}">
                                    </div>
                                    <input type="hidden" name="id" value="{{$economic_community?($economic_community->id ? $economic_community->id : ""):""}}"/>
                                    <input type="hidden" name="month_id" value="{{$month_id}}"/>
                                    <div class="form-group col-xl-4 col-md-12 col-sm-6">
                                        <label for="name"><strong>Name of the Project:</strong></label>
                                        <input type="text" required name="project_name" class="form-control" id="name"
                                               value="{{$economic_community?$economic_community->project_name:""}}">
                                    </div>
                                    <div class="form-group col-xl-4 col-md-12 col-sm-6">
                                        <label for="address"><strong>Project Address (INSTITUTION/VDC/Municipality/Ward):</strong></label>
                                        <input type="text" name="project_address" class="form-control" id="address"
                                               value="{{$economic_community?$economic_community->project_address:""}}">
                                    </div>
                                </div>

                                <div class="table-responsive mb-3">
                                    <table class="table table-bordered mb-0">
                                        <thead>
                                        <tr>
                                            <th rowspan="2" class="align-middle">S.N</th>
                                            <th rowspan="2" class="align-middle">Project Activities</th>
                                            <th rowspan="2" class="align-middle">Project Completion Date</th>
                                            <th colspan="2">G. Project Results</th>
                                            <th>H. Project Funding Type*</th>
                                            <th>I. Total Fund Contributions*</th>
                                        </tr>
                                        <tr style="font-size: 13px;">
                                            <th class="font-weight-lighter">Beneficiaries <br>
                                                (d)	population , <br>
                                                or <br>
                                                &emsp;&emsp;(b)households <br>
                                                &emsp;(c) students <br>
                                                (e)&emsp;&emsp;&emsp;&emsp;&emsp;
                                            </th>
                                            <th class="font-weight-lighter" style="width: 10%">Outputs <br>
                                                outcomes
                                            </th>
                                            <th class="font-weight-lighter">•	Global Grant <br>
                                                •	District Grant <br>
                                                •	International partner <br>
                                                •	Club to Club  funding support <br>
                                                •	Internal Club fund <br>
                                                •	Others
                                            </th>
                                            <th class="font-weight-lighter">
                                                •	GG: <br>
                                                •	DG: <br>
                                                •	International Club/partner: <br>
                                                •	Club internal <br>
                                                •	Others
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody class="tbl_body">
                                        <tr>
                                            <td>1.</td>
                                            <td>Skill Development /Vocational Training</td>
                                            <td class="text-center">
                                                <input type="date" name="skill_development_first" class="form-control" value="{{$economic_community?json_decode($economic_community->skill_development)->first:""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$economic_community?(json_decode($economic_community->skill_development)->second ? json_decode($economic_community->skill_development)->second : ""):""}}" class="checked text-center">
                                                    <span>{!! $economic_community?(json_decode($economic_community->skill_development)->second ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="skill_development_second"
                                                       value="{{$economic_community?(json_decode($economic_community->skill_development)->second ? json_decode($economic_community->skill_development)->second : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$economic_community?(json_decode($economic_community->skill_development)->third ? json_decode($economic_community->skill_development)->third : ""):""}}" class="checked text-center">
                                                    <span>{!! $economic_community?(json_decode($economic_community->skill_development)->third ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="skill_development_third"
                                                       value="{{$economic_community?(json_decode($economic_community->skill_development)->third ? json_decode($economic_community->skill_development)->third : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$economic_community?(json_decode($economic_community->skill_development)->fourth ? json_decode($economic_community->skill_development)->fourth : ""):""}}" class="checked text-center">
                                                    <span>{!! $economic_community?(json_decode($economic_community->skill_development)->fourth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="skill_development_fourth"
                                                       value="{{$economic_community?(json_decode($economic_community->skill_development)->fourth ? json_decode($economic_community->skill_development)->fourth : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$economic_community?(json_decode($economic_community->skill_development)->fifth ? json_decode($economic_community->skill_development)->fifth : ""):""}}" class="checked text-center">
                                                    <span>{!! $economic_community?(json_decode($economic_community->skill_development)->fifth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="skill_development_fifth"
                                                       value="{{$economic_community?(json_decode($economic_community->skill_development)->fifth ? json_decode($economic_community->skill_development)->fifth : ""):""}}"/>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td>2.</td>
                                            <td>Micro-Credit support</td>
                                            <td class="text-center">
                                                <input type="date" name="micro_credit_first" class="form-control" value="{{$economic_community?json_decode($economic_community->micro_credit)->first:""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$economic_community?(json_decode($economic_community->micro_credit)->second ? json_decode($economic_community->micro_credit)->second : ""):""}}" class="checked text-center">
                                                    <span>{!! $economic_community?(json_decode($economic_community->micro_credit)->second ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="micro_credit_second"
                                                       value="{{$economic_community?(json_decode($economic_community->micro_credit)->second ? json_decode($economic_community->micro_credit)->second : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$economic_community?(json_decode($economic_community->micro_credit)->third ? json_decode($economic_community->micro_credit)->third : ""):""}}" class="checked text-center">
                                                    <span>{!! $economic_community?(json_decode($economic_community->micro_credit)->third ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="micro_credit_third"
                                                       value="{{$economic_community?(json_decode($economic_community->micro_credit)->third ? json_decode($economic_community->micro_credit)->third : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$economic_community?(json_decode($economic_community->micro_credit)->fourth ? json_decode($economic_community->micro_credit)->fourth : ""):""}}" class="checked text-center">
                                                    <span>{!! $economic_community?(json_decode($economic_community->micro_credit)->fourth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="micro_credit_fourth"
                                                       value="{{$economic_community?(json_decode($economic_community->micro_credit)->fourth ? json_decode($economic_community->micro_credit)->fourth : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$economic_community?(json_decode($economic_community->micro_credit)->fifth ? json_decode($economic_community->micro_credit)->fifth : ""):""}}" class="checked text-center">
                                                    <span>{!! $economic_community?(json_decode($economic_community->micro_credit)->fifth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="micro_credit_fifth"
                                                       value="{{$economic_community?(json_decode($economic_community->micro_credit)->fifth ? json_decode($economic_community->micro_credit)->fifth : ""):""}}"/>
                                            </td>
                                            
                                        </tr>
                                        <tr>
                                            <td>3.</td>
                                            <td>Support for Income Generating Activities</td>
                                            <td class="text-center">
                                                <input type="date" name="support_for_income_first" class="form-control" value="{{$economic_community?json_decode($economic_community->support_for_income)->first:""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$economic_community?(json_decode($economic_community->support_for_income)->second ? json_decode($economic_community->support_for_income)->second : ""):""}}" class="checked text-center">
                                                    <span>{!! $economic_community?(json_decode($economic_community->support_for_income)->second ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="support_for_income_second"
                                                       value="{{$economic_community?(json_decode($economic_community->support_for_income)->second ? json_decode($economic_community->support_for_income)->second : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$economic_community?(json_decode($economic_community->support_for_income)->third ? json_decode($economic_community->support_for_income)->third : ""):""}}" class="checked text-center">
                                                    <span>{!! $economic_community?(json_decode($economic_community->support_for_income)->third ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="support_for_income_third"
                                                       value="{{$economic_community?(json_decode($economic_community->support_for_income)->third ? json_decode($economic_community->support_for_income)->third : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$economic_community?(json_decode($economic_community->support_for_income)->fourth ? json_decode($economic_community->support_for_income)->fourth : ""):""}}" class="checked text-center">
                                                    <span>{!! $economic_community?(json_decode($economic_community->support_for_income)->fourth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="support_for_income_fourth"
                                                       value="{{$economic_community?(json_decode($economic_community->support_for_income)->fourth ? json_decode($economic_community->support_for_income)->fourth : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$economic_community?(json_decode($economic_community->support_for_income)->fifth ? json_decode($economic_community->support_for_income)->fifth : ""):""}}" class="checked text-center">
                                                    <span>{!! $economic_community?(json_decode($economic_community->support_for_income)->fifth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="support_for_income_fifth"
                                                       value="{{$economic_community?(json_decode($economic_community->support_for_income)->fifth ? json_decode($economic_community->support_for_income)->fifth : ""):""}}"/>
                                            </td>
                                            
                                        </tr>
                                        <tr>
                                            <td>4.</td>
                                            <td>Tree Plantation and Preservation of Greenery/Planet</td>
                                            <td class="text-center">
                                                <input type="date" name="tree_plantation_first" class="form-control" value="{{$economic_community?json_decode($economic_community->tree_plantation)->first:""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$economic_community?(json_decode($economic_community->tree_plantation)->second ? json_decode($economic_community->tree_plantation)->second : ""):""}}" class="checked text-center">
                                                    <span>{!! $economic_community?(json_decode($economic_community->tree_plantation)->second ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="tree_plantation_second"
                                                       value="{{$economic_community?(json_decode($economic_community->tree_plantation)->second ? json_decode($economic_community->tree_plantation)->second : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$economic_community?(json_decode($economic_community->tree_plantation)->third ? json_decode($economic_community->tree_plantation)->third : ""):""}}" class="checked text-center">
                                                    <span>{!! $economic_community?(json_decode($economic_community->tree_plantation)->third ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="tree_plantation_third"
                                                       value="{{$economic_community?(json_decode($economic_community->tree_plantation)->third ? json_decode($economic_community->tree_plantation)->third : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$economic_community?(json_decode($economic_community->tree_plantation)->fourth ? json_decode($economic_community->tree_plantation)->fourth : ""):""}}" class="checked text-center">
                                                    <span>{!! $economic_community?(json_decode($economic_community->tree_plantation)->fourth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="tree_plantation_fourth"
                                                       value="{{$economic_community?(json_decode($economic_community->tree_plantation)->fourth ? json_decode($economic_community->tree_plantation)->fourth : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$economic_community?(json_decode($economic_community->tree_plantation)->fifth ? json_decode($economic_community->tree_plantation)->fifth : ""):""}}" class="checked text-center">
                                                    <span>{!! $economic_community?(json_decode($economic_community->tree_plantation)->fifth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="tree_plantation_fifth"
                                                       value="{{$economic_community?(json_decode($economic_community->tree_plantation)->fifth ? json_decode($economic_community->tree_plantation)->fifth : ""):""}}"/>
                                            </td>
                                            
                                        </tr>
                                        <tr>
                                            <td>5.</td>
                                            <td>Strengthening of community Institution</td>
                                            <td class="text-center">
                                                <input type="date" name="community_institution_first" class="form-control" value="{{$economic_community?json_decode($economic_community->community_institution)->first:""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$economic_community?(json_decode($economic_community->community_institution)->second ? json_decode($economic_community->community_institution)->second : ""):""}}" class="checked text-center">
                                                    <span>{!! $economic_community?(json_decode($economic_community->community_institution)->second ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="community_institution_second"
                                                       value="{{$economic_community?(json_decode($economic_community->community_institution)->second ? json_decode($economic_community->community_institution)->second : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$economic_community?(json_decode($economic_community->community_institution)->third ? json_decode($economic_community->community_institution)->third : ""):""}}" class="checked text-center">
                                                    <span>{!! $economic_community?(json_decode($economic_community->community_institution)->third ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="community_institution_third"
                                                       value="{{$economic_community?(json_decode($economic_community->community_institution)->third ? json_decode($economic_community->community_institution)->third : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$economic_community?(json_decode($economic_community->community_institution)->fourth ? json_decode($economic_community->community_institution)->fourth : ""):""}}" class="checked text-center">
                                                    <span>{!! $economic_community?(json_decode($economic_community->community_institution)->fourth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="community_institution_fourth"
                                                       value="{{$economic_community?(json_decode($economic_community->community_institution)->fourth ? json_decode($economic_community->community_institution)->fourth : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$economic_community?(json_decode($economic_community->community_institution)->fifth ? json_decode($economic_community->community_institution)->fifth : ""):""}}" class="checked text-center">
                                                    <span>{!! $economic_community?(json_decode($economic_community->community_institution)->fifth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="community_institution_fifth"
                                                       value="{{$economic_community?(json_decode($economic_community->community_institution)->fifth ? json_decode($economic_community->community_institution)->fifth : ""):""}}"/>
                                            </td>
                                            
                                        </tr>
                                        <tr>
                                            <td>6.</td>
                                            <td>Others</td>
                                            <td class="text-center">
                                                <input type="date" name="others_first" class="form-control" value="{{$economic_community?json_decode($economic_community->others)->first:""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$economic_community?(json_decode($economic_community->others)->second ? json_decode($economic_community->others)->second : ""):""}}" class="checked text-center">
                                                    <span>{!! $economic_community?(json_decode($economic_community->others)->second ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="others_second"
                                                       value="{{$economic_community?(json_decode($economic_community->others)->second ? json_decode($economic_community->others)->second : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$economic_community?(json_decode($economic_community->others)->third ? json_decode($economic_community->others)->third : ""):""}}" class="checked text-center">
                                                    <span>{!! $economic_community?(json_decode($economic_community->others)->third ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="others_third"
                                                       value="{{$economic_community?(json_decode($economic_community->others)->third ? json_decode($economic_community->others)->third : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$economic_community?(json_decode($economic_community->others)->fourth ? json_decode($economic_community->others)->fourth : ""):""}}" class="checked text-center">
                                                    <span>{!! $economic_community?(json_decode($economic_community->others)->fourth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="others_fourth"
                                                       value="{{$economic_community?(json_decode($economic_community->others)->fourth ? json_decode($economic_community->others)->fourth : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$economic_community?(json_decode($economic_community->others)->fifth ? json_decode($economic_community->others)->fifth : ""):""}}" class="checked text-center">
                                                    <span>{!! $economic_community?(json_decode($economic_community->others)->fifth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="others_fifth"
                                                       value="{{$economic_community?(json_decode($economic_community->others)->fifth ? json_decode($economic_community->others)->fifth : ""):""}}"/>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <p>OUTPUTS/OUTCOMES indicate : No of people served, No of people trained,
                                    Name/No of equipment supplied etc
                                </p>
                            </div>
                            <div class="card-footer text-white p-0">
                                <div class="form-row float-right p-2 mr-3 ml-3">
                                    <button type="submit" class="btn btn-block btn-primary">Save <i class="fas fa-check"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


