@extends('layouts.app')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-left">
                        <li class="breadcrumb-item">
                            @if(Auth::user()->role == 'user')
                                <a href="{{route('home')}}">Home</a>
                            @else
                                <a href="{{route('admin.home')}}">Home</a>
                            @endif
                        </li>
                        <li class="breadcrumb-item active">Form Preview</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row mb-2 ml-1">
                        <div class="row col-md-12">
                            <strong>Club Name: &nbsp;{{$club_name}}</strong>
                        </div>
                        <div class="row col-md-12">
                            <strong>Month: &nbsp;{{$month_name->name}}</strong>
                        </div>
                    </div>
                    <strong>A.CLUB ADMINISTRATION COMMITTEE REPORT</strong>

                    <!-- Club Meetings Conducted-->
                    <div class="card">
                        <div class="card-header card_header p-0">
                            <p class="card-title ml-3"><small>A.1 Club Meetings Conducted</small></p>
                        </div>
                        <div class="card-body card_body pt-sm-0 pt-md-3 pb-0">
                            <form class="mt-sm-3 mt-md-0">
                                <div class="row mb-2 ml-1">
                                    <div class="row col-md-6">
                                        <strong>Total Number of Members: {{$data['club_meetings']?$data['club_meetings']->no_of_members:""}}</strong>
                                    </div>
                                    <div class="row col-md-6">
                                        <strong>Club attendance: {{$data['club_meetings']?$data['club_meetings']->club_attendance:""}}</strong>
                                    </div>
                                    <div class="row col-md-6">
                                        <strong>Annual Target of attendance(%): {{$data['club_meetings']?$data['club_meetings']->annual_target_precent:""}}</strong>
                                    </div>
                                </div>
                            </form>
                            <div class="table-responsive mb-3">
                                <table class="table table-bordered mb-0">
                                    <thead>
                                    <tr>
                                        <th rowspan="2" class="align-middle" width="25%">A.1.1 Weekly Meetings focused on</th>
                                        <th colspan="5">Week</th>
                                    </tr>
                                    <tr>
                                        <th width="15%">FIRST</th>
                                        <th width="15%">SECOND</th>
                                        <th width="15%">THIRD</th>
                                        <th width="15%">FOURTH</th>
                                        <th>FIFTH</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <input type="hidden" id="hello"/>
                                    <tr>
                                        <td>External Guest speaker (Mention the name of guest speaker and title)</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->guest_speaker)->first:""}}</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->guest_speaker)->second:""}}</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->guest_speaker)->third:""}}</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->guest_speaker)->fourth:""}}</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->guest_speaker)->fifth:""}}</td>

                                    </tr>
                                    <tr>
                                        <td>Classification Talks: (Name of the Club member and his/her classification subject)</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->classification_talks)->first:""}}</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->classification_talks)->second:""}}</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->classification_talks)->third:""}}</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->classification_talks)->fourth:""}}</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->classification_talks)->fifth:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>Rotary  program talks by District Leader
                                            (Name of the District Leader and topic)
                                        </td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->rotary_program)->first:""}}</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->rotary_program)->second:""}}</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->rotary_program)->third:""}}</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->rotary_program)->fourth:""}}</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->rotary_program)->fifth:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>Meeting combined with service project activities visit (Name of the service project visited)</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->meeting_combined)->first:""}}</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->meeting_combined)->second:""}}</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->meeting_combined)->third:""}}</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->meeting_combined)->fourth:""}}</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->meeting_combined)->fifth:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>Club business meeting or other focus
                                            (mention below)
                                        </td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->club_business)->first:""}}</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->club_business)->second:""}}</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->club_business)->third:""}}</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->club_business)->fourth:""}}</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->club_business)->fifth:""}}</td>
                                    </tr>
                                    <tr>
                                        <th colspan="6">Weekly meeting attendance including make ups within 14 days</th>
                                    </tr>
                                    <tr>
                                        <td>Members'Presence No.</td>
                                        <td>{{$data['club_meetings']?json_decode($data['club_meetings']->member_presence)->first:""}}</td>
                                        <td>{{$data['club_meetings']?json_decode($data['club_meetings']->member_presence)->second:""}}</td>
                                        <td>{{$data['club_meetings']?json_decode($data['club_meetings']->member_presence)->third:""}}</td>
                                        <td>{{$data['club_meetings']?json_decode($data['club_meetings']->member_presence)->fourth:""}}</td>
                                        <td>{{$data['club_meetings']?json_decode($data['club_meetings']->member_presence)->fifth:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>% of total</td>
                                        <td>{{$data['club_meetings']?json_decode($data['club_meetings']->total_percent)->first:""}}</td>
                                        <td>{{$data['club_meetings']?json_decode($data['club_meetings']->total_percent)->second:""}}</td>
                                        <td>{{$data['club_meetings']?json_decode($data['club_meetings']->total_percent)->third:""}}</td>
                                        <td>{{$data['club_meetings']?json_decode($data['club_meetings']->total_percent)->fourth:""}}</td>
                                        <td>{{$data['club_meetings']?json_decode($data['club_meetings']->total_percent)->fifth:""}}</td>
                                    </tr>
                                    <tr>
                                        <th colspan="6">Other Club meetings ( No of attendance and date)</th>
                                    </tr>
                                    <tr>
                                        <td>A.1.2 Board (no. and date)</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->board)->first:""}}</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->board)->second:""}}</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->board)->third:""}}</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->board)->fourth:""}}</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->board)->fifth:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>A.1.3 Club Assembly (No. and date)</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->club_assembly)->first:""}}</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->club_assembly)->second:""}}</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->club_assembly)->third:""}}</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->club_assembly)->fourth:""}}</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->club_assembly)->fifth:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>A.1.4 Club Committee (Name of Committee)</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->club_committee)->first:""}}</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->club_committee)->second:""}}</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->club_committee)->third:""}}</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->club_committee)->fourth:""}}</td>
                                        <td class="align-top">{{$data['club_meetings']?json_decode($data['club_meetings']->club_committee)->fifth:""}}</td>
                                    </tr>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th colspan="1">Total attendance in the month</th>
                                        <th colspan="5">{{$data['club_meetings']?$data['club_meetings']->month_attendance:""}}</th>
                                    </tr>
                                    <tr>
                                        <th colspan="1">Annual target for Club attendance</th>
                                        <th colspan="5">{{$data['club_meetings']?$data['club_meetings']->annual_target:""}}</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer text-white p-0">
                            <form>
                                <div class="form-row float-right p-2 mr-3 ml-3">
                                    <button type="button" class="no-print btn btn-block btn-primary print-window">Print <i class="fas fa-print"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <!-- CLUB BULLETIN -->
                    <div class="card">
                        <div class="card-header card_header p-0">
                            <p class="card-title ml-3"><small>A.2. Club Bulletin</small></p>
                        </div>
                        <div class="card-body card_body pt-sm-0 pt-md-3 pb-0">
                            <form class="mt-sm-3 mt-md-0">
                                <div class="row mb-2 ml-1">
                                    <div class="row col-md-6">
                                        <strong>Total Number of Members: &nbsp;{{$data['club_bulletins']?$data['club_bulletins']->no_of_members:""}}</strong>
                                    </div>
                                    <div class="row col-md-6">
                                        <strong>Club attendance: &nbsp;{{$data['club_bulletins']?$data['club_bulletins']->club_attendance:""}}</strong>
                                    </div>
                                    <div class="row col-md-6">
                                        <strong>Annual Target of attendance(%): &nbsp;{{$data['club_bulletins']?$data['club_bulletins']->annual_target_percent:""}}</strong>
                                    </div>
                                </div>
                            </form>

                            <div class="table-responsive mb-3">
                                <table class="table table-bordered mb-0">
                                    <thead>
                                    <tr>
                                        <th colspan="6" class="text-left">Title of the Club Bulletin: &nbsp;{{$data['club_bulletins']?$data['club_bulletins']->title:""}}</th>
                                    </tr>
                                    <tr>
                                        <th rowspan="2"></th>
                                        <th colspan="3">Frequency of publication Published</th>
                                        <th colspan="2">Published in </th>
                                    </tr>
                                    <tr>
                                        <th width="15%">Weekly</th>
                                        <th width="15%">Fortnightly</th>
                                        <th width="15%">Monthly</th>
                                        <th width="15%">Hard Copy</th>
                                        <th width="15%">E-copy</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr class="text-center">
                                        <td>No of issues published in this month</td>
                                        <td>{{$data['club_bulletins']?json_decode($data['club_bulletins']->no_of_issue)->first:""}}</td>
                                        <td>{{$data['club_bulletins']?json_decode($data['club_bulletins']->no_of_issue)->second:""}}</td>
                                        <td>{{$data['club_bulletins']?json_decode($data['club_bulletins']->no_of_issue)->third:""}}</td>
                                        <td>{{$data['club_bulletins']?json_decode($data['club_bulletins']->no_of_issue)->fourth:""}}</td>
                                        <td>{{$data['club_bulletins']?json_decode($data['club_bulletins']->no_of_issue)->fifth:""}}</td>
                                    </tr>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th colspan="6" class="text-center">Attach e-copies in the monthly report</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer text-white p-0">
                            <form>
                                <div class="form-row float-right p-2 mr-3 ml-3">
                                    <button type="button" class="no-print btn btn-block btn-primary print-window">Print <i class="fas fa-print"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <!-- Club Participation in the District Events -->
                    <div class="card">
                        <div class="card-header card_header p-0">
                            <p class="card-title ml-3"><small>A.3. Club Participation in the District Events</small></p>
                        </div>
                        <div class="card-body card_body pt-sm-0 pt-md-3 pb-0">
                            <form class="mt-sm-3 mt-md-0">
                                <div class="row mb-2 ml-1">
                                    <div class="row col-md-6">
                                        <strong>Total Number of Members:  {{$data['club_participation']?$data['club_participation']->no_of_members:""}}</strong>
                                    </div>
                                    <div class="row col-md-6">
                                        <strong>Club attendance:  {{$data['club_participation']?$data['club_participation']->club_attendance:""}}</strong>
                                    </div>
                                    <div class="row col-md-6">
                                        <strong>Annual Target of attendance(%):  {{$data['club_participation']?$data['club_participation']->annual_target_percent:""}}</strong>
                                    </div>
                                </div>
                            </form>

                            <div class="table-responsive mb-3">
                                <table class="table table-bordered insert_more mb-0">
                                    <thead>
                                    <tr>
                                        <th rowspan="2" class="align-middle">S.N</th>
                                        <th rowspan="2" class="align-middle" width="20%">Name of District Events in the Month</th>
                                        <th rowspan="2" class="align-middle">Date of Event</th>
                                        <th colspan="3">Club Events</th>
                                    </tr>
                                    <tr>
                                        <th>No of Club Participants</th>
                                        <th colspan="2">Event Sponsored (NRS)</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($data['club_participation'])
                                        @php $i = 1 @endphp
                                        @foreach($data['club_participation']->club_participants_data as $val)
                                            <tr>
                                                <td>{{$i++}}.</td>
                                                <td>{{$val->district_name}}</td>
                                                <td>{{$val->date}}</td>
                                                <td>{{$val->no_of_participants}}</td>
                                                <td>{{$val->event_sponsored}}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer text-white p-0">
                            <form>
                                <div class="form-row float-right p-2 mr-3 ml-3">
                                    <button type="button" class="no-print btn btn-block btn-primary print-window">Print <i class="fas fa-print"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <!-- Club Registration in Rotary Online -->
                    <div class="card mb-5">
                        <div class="card-header card_header p-0">
                            <p class="card-title ml-3"><small>A.4. Club Registration in Rotary Online</small></p>
                        </div>
                        <div class="card-body card_body pt-sm-0 pt-md-3 pb-0">
                            <form class="mt-sm-3 mt-md-0">
                                <div class="row mb-2 ml-1">
                                    <div class="row col-md-6">
                                        <strong>Total Number of Members:  {{$data['club_registration']?$data['club_registration']->no_of_members:""}}</strong>
                                    </div>
                                    <div class="row col-md-6">
                                        <strong>Club attendance:  {{$data['club_registration']?$data['club_registration']->club_attendance:""}}</strong>
                                    </div>
                                    <div class="row col-md-6">
                                        <strong>Annual Target of attendance(%):  {{$data['club_registration']?$data['club_registration']->annual_target_percent:""}}</strong>
                                    </div>
                                </div>
                            </form>

                            <div class="table-responsive mb-3">
                                <table class="table table-bordered mb-0" id="insert_more">
                                    <thead>
                                    <tr>
                                        <th colspan="2">Online Registration</th>
                                        <th width="30%">No.</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>1.</td>
                                        <td>Total number of members registered  in MY ROTARY as of July 01, 2019</td>
                                        <td class="text-center">{{$data['club_registration']?$data['club_registration']->members_of_july:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>2.</td>
                                        <td>Total number of members registered in MY ROTARY as of this reporting month</td>
                                        <td class="text-center">{{$data['club_registration']?$data['club_registration']->members_of_this_month:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>3.</td>
                                        <td>Total number of Club Goals UPLOADED in the ROTARY CLUB CENTRAL</td>
                                        <td class="text-center">{{$data['club_registration']?$data['club_registration']->club_goals_uploaded:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>4.</td>
                                        <td>Total number of Club Goals UPGRADED in the ROTARY CLUB CENTRAL</td>
                                        <td class="text-center">{{$data['club_registration']?$data['club_registration']->club_goals_upgraded:""}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer text-white p-0">
                            <form>
                                <div class="form-row float-right p-2 mr-3 ml-3">
                                    <button type="button" class="no-print btn btn-block btn-primary print-window">Print <i class="fas fa-print"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>


                    <hr>
                    <!-- Membership Classification Survey -->
                    <div class="row mb-2 ml-1 mt-3">
                        <div class="row col-md-12">
                            <strong>Club Name: &nbsp;&nbsp;{{$club_name}}</strong>
                        </div>
                        <div class="row col-md-12">
                            <strong>Month: &nbsp;&nbsp;{{$month_name->name}}</strong>
                        </div>
                    </div>
                    <strong>B. CLUB MEMBERSHIP COMMITTEE</strong>
                    <div class="card">
                        <div class="card-header card_header p-0">
                            <p class="card-title ml-3"><small>B.1. Membership Classification Survey</small></p>
                        </div>
                        <div class="card-body card_body pt-sm-0 pt-md-3 pb-0">
                            <form class="mt-sm-3 mt-md-0">
                                <div class="row mb-2 ml-1">
                                    <div class="row col-md-6">
                                        <strong>Number of Membership in the club: {{$data['membership_class']?$data['membership_class']->no_of_members:""}}</strong>
                                    </div>
                                    <div class="row col-md-6">
                                        <strong>Annual Target for Membership Growth(No.): {{$data['membership_class']?$data['membership_class']->annual_target_no:""}}</strong>
                                    </div>
                                    <div class="row col-md-6">
                                        <strong>Annual Target for Membership Growth(%): {{$data['membership_class']?$data['membership_class']->annual_target_percent:""}}</strong>
                                    </div>
                                </div>
                            </form>

                            <div class="table-responsive mb-3">
                                <table class="table table-bordered insert_more mb-0">
                                    <thead>
                                    <tr>
                                        <th colspan="3">Classification Fulfilled</th>
                                        <th colspan="3">Classification identified/un-fulfilled</th>
                                    </tr>
                                    <tr>
                                        <th style="width: 5%">S.N</th>
                                        <th>Classification Categories</th>
                                        <th>No of Club members in the category</th>
                                        <th style="width: 5%">S.N</th>
                                        <th colspan="2">Classification categories</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($data['membership_class'])
                                        @php $i = 1 @endphp
                                        @foreach($data['membership_class']->membership_classification_data as $val)
                                            <tr>
                                                <td>{{$i++}}.</td>
                                                <td>{{$val->classification_categories_fulfilled}}</td>
                                                <td>{{$val->no_of_club_members}}</td>
                                                <td>{{$i++}}.</td>
                                                <td>{{$val->classification_categories_unfulfilled}}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer text-white p-0">
                            <form>
                                <div class="form-row float-right p-2 mr-3 ml-3">
                                    <button type="submit" class="no-print btn btn-block btn-primary print-window">Print <i class="fas fa-print"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <!-- Membership Growth -->
                    <div class="card">
                        <div class="card-header card_header p-0">
                            <p class="card-title ml-3"><small>B.2. Membership Growth</small></p>
                        </div>
                        <div class="card-body card_body pt-sm-0 pt-md-3 pb-0">
                            <form class="mt-sm-3 mt-md-0">
                                <div class="row mb-2 ml-1">
                                    <div class="row col-md-6">
                                        <strong>Number of Membership in the club: &nbsp;{{$data['membership_growth']?$data['membership_growth']->no_of_members:""}}</strong>
                                    </div>
                                    <div class="row col-md-6">
                                        <strong>Annual Target for Membership Growth(No.): &nbsp;{{$data['membership_growth']?$data['membership_growth']->annual_target_no:""}}</strong>
                                    </div>
                                    <div class="row col-md-6">
                                        <strong>Annual Target for Membership Growth(%): &nbsp;{{$data['membership_growth']?$data['membership_growth']->annual_target_percent:""}}</strong>
                                    </div>
                                </div>
                            </form>

                            <div class="table-responsive mb-3">
                                <table class="table table-bordered mb-0">
                                    <thead>
                                    <tr>
                                        <th class="text-left" width="60%">B.2.1 Total Net Growth/Loss of Members</th>
                                        <th>Number of members</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>(a)	Total numbers of members in the Club as of July 01, 2019</td>
                                        <td class="text-center">{{$data['membership_growth']?json_decode($data['membership_growth']->loss_of_members)->first:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>(a)	Total numbers of members terminated  in the reporting month</td>
                                        <td class="text-center">{{$data['membership_growth']?json_decode($data['membership_growth']->loss_of_members)->second:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>(a)	Total number of new members inducted in the reporting month</td>
                                        <td class="text-center">{{$data['membership_growth']?json_decode($data['membership_growth']->loss_of_members)->third:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>(a)	Total numbers of Net Gowth/Loss of Members in the reporting period
                                            = (a)-(b)+(c)
                                        </td>
                                        <td class="text-center">{{$data['membership_growth']?json_decode($data['membership_growth']->loss_of_members)->fourth:""}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="table-responsive mb-3">
                                <table class="table table-bordered mb-0">
                                    <thead>
                                    <tr>
                                        <th class="text-left" width="60%">B.2.2 Net Growth/Loss of Female Members</th>
                                        <th>Number of members</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>(a)	Total numbers of female members in the Club as of July 01, 2019</td>
                                        <td class="text-center">{{$data['membership_growth']?json_decode($data['membership_growth']->loss_of_female_members)->first:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>(a)	Total numbers of female members terminated by the reporting Month</td>
                                        <td class="text-center">{{$data['membership_growth']?json_decode($data['membership_growth']->loss_of_female_members)->second:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>(a)	Total numbers of new female members inducted by the reporting Month</td>
                                        <td class="text-center">{{$data['membership_growth']?json_decode($data['membership_growth']->loss_of_female_members)->third:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>(a)	Total numbers of Net Growth/Loss of Female Members by the
                                            reporting period=(a)-(b)+(c )
                                        </td>
                                        <td class="text-center">{{$data['membership_growth']?json_decode($data['membership_growth']->loss_of_female_members)->fourth:""}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="table-responsive mb-3">
                                <table class="table table-bordered mb-0">
                                    <thead>
                                    <tr>
                                        <th class="text-left" width="60%">B.2.3 Net Growth/Loss of Youth Members</th>
                                        <th>Number of members</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>(a)	Total numbers of youth members (<40 years of age) as of July 01, 2019</td>
                                        <td class="text-center">{{$data['membership_growth']?json_decode($data['membership_growth']->loss_of_youth_members)->first:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>(a)	Total numbers of youth members terminated by the reporting Month</td>
                                        <td class="text-center">{{$data['membership_growth']?json_decode($data['membership_growth']->loss_of_youth_members)->second:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>(a)	Total numbers of new youth members inducted by the reporting Month</td>
                                        <td class="text-center">{{$data['membership_growth']?json_decode($data['membership_growth']->loss_of_youth_members)->third:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>(a)	Total numbers of Net Growth/Loss of Youth Members
                                            by the reporting Month=(a)-(b)+(c)
                                        </td>
                                        <td class="text-center">{{$data['membership_growth']?json_decode($data['membership_growth']->loss_of_youth_members)->fourth:""}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer text-white p-0">
                            <form>
                                <div class="form-row float-right p-2 mr-3 ml-3">
                                    <button type="button" class="no-print btn btn-block btn-primary print-window">Print <i class="fas fa-print"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <!-- Membership Development -->
                    <div class="card mb-5">
                        <div class="card-header card_header p-0">
                            <p class="card-title ml-3"><small>B.3 Membership Development in the Month</small></p>
                        </div>
                        <div class="card-body card_body pt-sm-0 pt-md-3 pb-0">
                            <form class="mt-sm-3 mt-md-0">
                                <div class="row mb-2 ml-1">
                                    <div class="row col-md-6">
                                        <strong>Number of Membership in the club: &nbsp;{{$data['membership_development']?$data['membership_development']->no_of_members:""}}</strong>
                                    </div>
                                    <div class="row col-md-6">
                                        <strong>Annual Target for Membership Growth(No.): &nbsp;{{$data['membership_development']?$data['membership_development']->annual_target_no:""}}</strong>
                                    </div>
                                    <div class="row col-md-6">
                                        <strong>Annual Target for Membership Growth(%): &nbsp;{{$data['membership_development']?$data['membership_development']->annual_target_percent:""}}</strong>
                                    </div>
                                </div>
                            </form>

                            <div class="table-responsive mb-3">
                                <table class="table table-bordered mb-0">
                                    <thead>
                                    <tr>
                                        <th colspan="2" rowspan="2" class="align-middle text-left" width="35%">B.3.1 Membership Orientation Training </th>
                                        <th colspan="2">No of participants</th>
                                        <th rowspan="2" class="align-middle">Date</th>
                                    </tr>
                                    <tr>
                                        <th width="25%">Club Event</th>
                                        <th width="25%">District Event</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>1.</td>
                                        <td>New Members Orientation Training</td>
                                        <td>{{$data['membership_development']?json_decode($data['membership_development']->new_members_orientation)->club_event:""}}</td>
                                        <td>{{$data['membership_development']?json_decode($data['membership_development']->new_members_orientation)->district_event:""}}</td>
                                        <td class="text-center">{{$data['membership_development']?json_decode($data['membership_development']->new_members_orientation)->date:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>2.</td>
                                        <td>Club Officers Training Seminar</td>
                                        <td>{{$data['membership_development']?json_decode($data['membership_development']->club_officers)->club_event:""}}</td>
                                        <td>{{$data['membership_development']?json_decode($data['membership_development']->club_officers)->district_event:""}}</td>
                                        <td class="text-center">{{$data['membership_development']?json_decode($data['membership_development']->club_officers)->date:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>3.</td>
                                        <td>District Membership Seminar</td>
                                        <td>{{$data['membership_development']?json_decode($data['membership_development']->district_membership)->club_event:""}}</td>
                                        <td>{{$data['membership_development']?json_decode($data['membership_development']->district_membership)->district_event:""}}</td>
                                        <td class="text-center">{{$data['membership_development']?json_decode($data['membership_development']->district_membership)->date:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>4.</td>
                                        <td>Others (name)</td>
                                        <td>{{$data['membership_development']?json_decode($data['membership_development']->membership_others)->club_event:""}}</td>
                                        <td>{{$data['membership_development']?json_decode($data['membership_development']->membership_others)->district_event:""}}</td>
                                        <td class="text-center">{{$data['membership_development']?json_decode($data['membership_development']->membership_others)->date:""}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div class="table-responsive mb-3">
                                <table class="table table-bordered mb-0">
                                    <thead>
                                    <tr>
                                        <th colspan="2" class="text-left" width="35%">B.3.2 New Clubs Sponsored in the Month</th>
                                        <th>Charter Date</th>
                                        <th>No of Charter Members</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>1.</td>
                                        <td>Rotary Club of</td>
                                        <td class="text-center">{{$data['membership_development']?json_decode($data['membership_development']->rotary_club)->date:""}}</td>
                                        <td class="text-center">{{$data['membership_development']?json_decode($data['membership_development']->rotary_club)->charter_members:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>2.</td>
                                        <td>Rotaract Club of</td>
                                        <td class="text-center">{{$data['membership_development']?json_decode($data['membership_development']->rotaract_club)->date:""}}</td>
                                        <td class="text-center">{{$data['membership_development']?json_decode($data['membership_development']->rotaract_club)->charter_members:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>3.</td>
                                        <td>Interact Club of </td>
                                        <td class="text-center">{{$data['membership_development']?json_decode($data['membership_development']->interact_club)->date:""}}</td>
                                        <td class="text-center">{{$data['membership_development']?json_decode($data['membership_development']->interact_club)->charter_members:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>Rotary Community Corps</td>
                                        <td class="text-center">{{$data['membership_development']?json_decode($data['membership_development']->rotary_community)->date:""}}</td>
                                        <td class="text-center">{{$data['membership_development']?json_decode($data['membership_development']->rotary_community)->charter_members:""}}</td>
                                    </tr>
{{--                                    <tr>--}}
{{--                                        <td>4</td>--}}
{{--                                        <td></td>--}}
{{--                                        <td class="text-center"></td>--}}
{{--                                        <td></td>--}}
{{--                                    </tr>--}}
                                    </tbody>
                                </table>
                            </div>

                            <div class="table-responsive mb-3">
                                <table class="table table-bordered mb-0">
                                    <thead>
                                    <tr>
                                        <th colspan="2" class="text-left">B.3.3 Other Club Membership Development Activities </th>
                                        <th>Date/period of survey</th>
                                        <th>No of participants</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>1.</td>
                                        <td>Club Members Satisfaction Survey</td>
                                        <td class="text-center">{{$data['membership_development']?json_decode($data['membership_development']->members_satisfaction)->date:""}}</td>
                                        <td>{{$data['membership_development']?json_decode($data['membership_development']->members_satisfaction)->no:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>2.</td>
                                        <td>Club Health Check Survey</td>
                                        <td class="text-center">{{$data['membership_development']?json_decode($data['membership_development']->health_check)->date:""}}</td>
                                        <td>{{$data['membership_development']?json_decode($data['membership_development']->health_check)->no:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>3.</td>
                                        <td>Others</td>
                                        <td class="text-center">{{$data['membership_development']?json_decode($data['membership_development']->development_others)->date:""}}</td>
                                        <td>{{$data['membership_development']?json_decode($data['membership_development']->development_others)->no:""}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer text-white p-0">
                            <form>
                                <div class="form-row float-right p-2 mr-3 ml-3">
                                    <button type="submit" class="no-print btn btn-block btn-primary print-window">Print <i class="fas fa-print"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <hr>
                    <!-- Water Supply And Sanitation -->
                    <div class="row mb-2 ml-1 mt-3">
                        <div class="row col-md-12">
                            <strong>Club Name: &nbsp;{{$club_name}}</strong>
                        </div>
                        <div class="row col-md-12">
                            <strong>Month: &nbsp;{{$month_name->name}}</strong>
                        </div>
                    </div>
                    <strong>C. SERVICE PROJECT COMMITTEE</strong><br>
                    <strong>C.1 COMMUNITY SERVICE PROJECTS </strong>
                    <div class="card">
                        <div class="card-header card_header p-0">
                            <p class="card-title ml-3"><small>C.1.1 FOCUS AREA: WATER SUPPLY AND SANITATION (WASH)</small></p>
                        </div>
                        <div class="card-body card_body pt-sm-0 pt-md-3 pb-0">
                            <form class="mt-sm-3 mt-md-0">
                                <div class="row mb-2 ml-1">
                                    <div class="row col-md-12">
                                        <strong>Name of WASH Project: &nbsp;{{$data['water_supply']?$data['water_supply']->wash_project_name:""}}</strong>
                                    </div>
                                    <div class="row col-md-12">
                                        <strong>Project Address (VDC/Municipality/Ward): &nbsp;{{$data['water_supply']?$data['water_supply']->project_address:""}}</strong>
                                    </div>
                                </div>
                            </form>

                            <div class="table-responsive mb-3">
                                <table class="table table-bordered insert_more mb-0">
                                    <thead>
                                    <tr>
                                        <th rowspan="2" class="align-middle">S.N</th>
                                        <th rowspan="2" width="20%" class="align-middle">Project Completion Date</th>
                                        <th colspan="2">A. Project Results</th>
                                        <th>B. Project Funding Type*</th>
                                        <th>C. Total Fund Contributions*</th>
                                    </tr>
                                    <tr style="font-size: 13px;">
                                        <th class="font-weight-lighter">Beneficiaries <br>
                                            (a)	population, <br>
                                            or <br>
                                            (b)households <br>
                                            (c) students &ensp; &nbsp;
                                        </th>
                                        <th class="font-weight-lighter">Outputs <br>
                                            outcomes
                                        </th>
                                        <th class="font-weight-lighter">•	Global Grant <br>
                                            •	District Grant <br>
                                            •	Club to Club  funding support <br>
                                            •	Internal Club fund <br>
                                            •	Others
                                        </th>
                                        <th class="font-weight-lighter">
                                            •	GG: <br>
                                            •	DG: <br>
                                            •	International Club: <br>
                                            •	Club internal <br>
                                            •	Others <br>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($data['water_supply'])
                                        @php $i = 1 @endphp
                                        @foreach($data['water_supply']->water_supply_data as $val)
                                            <tr>
                                                <td>{{$i++}}.</td>
                                                <td class="text-center">{{$val->date}}</td>
                                                <td class="align-top">{{$val->beneficiaries}}</td>
                                                <td class="align-top">{{$val->outputs}}</td>
                                                <td class="align-top">{{$val->funding_type}}</td>
                                                <td class="align-top">{{$val->fund_contributions}}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            <p>OUTPUTS/OUTCOMES indicate : No of tapstands, No of handpumps, Length of pipeline installed,
                                total number of water reservoir tanks, number of household toilets, no of school latrines
                                built, no of people/staff trained, number of people reached etc
                            </p>
                        </div>
                        <div class="card-footer text-white p-0">
                            <form>
                                <div class="form-row float-right p-2 mr-3 ml-3">
                                    <button type="button" class="no-print btn btn-block btn-primary print-window">Print <i class="fas fa-print"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <!-- Disease Prevention And Treatment -->
                    <div class="card">
                        <div class="card-header card_header p-0">
                            <p class="card-title ml-3"><small>C.1.2 FOCUS AREA: DISEASE PREVENTION AND TREATMENT</small></p>
                        </div>
                        <div class="card-body card_body pt-sm-0 pt-md-3 pb-0">
                            <form class="mt-sm-3 mt-md-0">
                                <div class="row mb-2 ml-1">
                                    <div class="row col-md-12">
                                        <strong>Name of Project: &nbsp;{{$data['disease_prev']?$data['disease_prev']->project_name:""}}</strong>
                                    </div>
                                    <div class="row col-md-12">
                                        <strong>Project Address (VDC/Municipality/Ward): &nbsp;{{$data['disease_prev']?$data['disease_prev']->project_address:""}}</strong>
                                    </div>
                                </div>
                            </form>

                            <div class="table-responsive mb-3">
                                <table class="table table-bordered mb-0">
                                    <thead>
                                    <tr>
                                        <th rowspan="2" class="align-middle">S.N</th>
                                        <th rowspan="2" class="align-middle">Project Activities</th>
                                        <th rowspan="2" class="align-middle">Project Completion Date</th>
                                        <th colspan="2">D. Project Results</th>
                                        <th>E. Project Funding Type*</th>
                                        <th>F. Total Fund Contributions*</th>
                                    </tr>
                                    <tr style="font-size: 13px;">
                                        <th class="font-weight-lighter">Beneficiaries <br>
                                            (b)	population , <br>
                                            or <br>
                                            &emsp;&emsp;(b)households <br>
                                            &emsp;(c) students <br>
                                            (c)&emsp;&emsp;&emsp;&emsp;&emsp;
                                        </th>
                                        <th class="font-weight-lighter" style="width: 10%">Outputs <br>
                                            outcomes
                                        </th>
                                        <th class="font-weight-lighter">•	Global Grant <br>
                                            •	District Grant <br>
                                            •	International partner <br>
                                            •	Club to Club  funding support <br>
                                            •	Internal Club fund <br>
                                            •	Others
                                        </th>
                                        <th class="font-weight-lighter">
                                            •	GG: <br>
                                            •	DG: <br>
                                            •	International Club/partner: <br>
                                            •	Club internal <br>
                                            •	Others
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>1.</td>
                                        <td>Health Camp on: <br><strong>{{$data['disease_prev']?json_decode($data['disease_prev']->health_camp)->first:""}}</strong></td>
                                        <td class="text-center">{{$data['disease_prev']?json_decode($data['disease_prev']->health_camp)->second:""}}</td>
                                        <td class="align-top">{{$data['disease_prev']?json_decode($data['disease_prev']->health_camp)->third:""}}</td>
                                        <td class="align-top">{{$data['disease_prev']?json_decode($data['disease_prev']->health_camp)->fourth:""}}</td>
                                        <td class="align-top">{{$data['disease_prev']?json_decode($data['disease_prev']->health_camp)->fifth:""}}</td>
                                        <td class="align-top">{{$data['disease_prev']?json_decode($data['disease_prev']->health_camp)->sixth:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>2.</td>
                                        <td>Blood Donation Camp: <br><strong>{{$data['disease_prev']?json_decode($data['disease_prev']->blood_donation)->first:""}}</strong></td>
                                        <td class="text-center">{{$data['disease_prev']?json_decode($data['disease_prev']->blood_donation)->second:""}}</td>
                                        <td class="align-top">{{$data['disease_prev']?json_decode($data['disease_prev']->blood_donation)->third:""}}</td>
                                        <td class="align-top">{{$data['disease_prev']?json_decode($data['disease_prev']->blood_donation)->fourth:""}}</td>
                                        <td class="align-top">{{$data['disease_prev']?json_decode($data['disease_prev']->blood_donation)->fifth:""}}</td>
                                        <td class="align-top">{{$data['disease_prev']?json_decode($data['disease_prev']->blood_donation)->sixth:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>3.</td>
                                        <td>Training /capacity development on health related issues for: <br><strong>{{$data['disease_prev']?json_decode($data['disease_prev']->training_development)->first:""}}</strong></td>
                                        <td class="text-center">{{$data['disease_prev']?json_decode($data['disease_prev']->training_development)->second:""}}</td>
                                        <td class="align-top">{{$data['disease_prev']?json_decode($data['disease_prev']->training_development)->third:""}}</td>
                                        <td class="align-top">{{$data['disease_prev']?json_decode($data['disease_prev']->training_development)->fourth:""}}</td>
                                        <td class="align-top">{{$data['disease_prev']?json_decode($data['disease_prev']->training_development)->fifth:""}}</td>
                                        <td class="align-top">{{$data['disease_prev']?json_decode($data['disease_prev']->training_development)->sixth:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>4.</td>
                                        <td>Health Equipment Supplies to: <br><strong>{{$data['disease_prev']?json_decode($data['disease_prev']->health_equipment)->first:""}}</strong></td>
                                        <td class="text-center">{{$data['disease_prev']?json_decode($data['disease_prev']->health_equipment)->second:""}}</td>
                                        <td class="align-top">{{$data['disease_prev']?json_decode($data['disease_prev']->health_equipment)->third:""}}</td>
                                        <td class="align-top">{{$data['disease_prev']?json_decode($data['disease_prev']->health_equipment)->fourth:""}}</td>
                                        <td class="align-top">{{$data['disease_prev']?json_decode($data['disease_prev']->health_equipment)->fifth:""}}</td>
                                        <td class="align-top">{{$data['disease_prev']?json_decode($data['disease_prev']->health_equipment)->sixth:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>5.</td>
                                        <td>Health Hygiene promotional campaign on: <br><strong>{{$data['disease_prev']?json_decode($data['disease_prev']->health_hygiene)->first:""}}</strong></td>
                                        <td class="text-center">{{$data['disease_prev']?json_decode($data['disease_prev']->health_hygiene)->second:""}}</td>
                                        <td class="align-top">{{$data['disease_prev']?json_decode($data['disease_prev']->health_hygiene)->third:""}}</td>
                                        <td class="align-top">{{$data['disease_prev']?json_decode($data['disease_prev']->health_hygiene)->fourth:""}}</td>
                                        <td class="align-top">{{$data['disease_prev']?json_decode($data['disease_prev']->health_hygiene)->fifth:""}}</td>
                                        <td class="align-top">{{$data['disease_prev']?json_decode($data['disease_prev']->health_hygiene)->sixth:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>6.</td>
                                        <td>Others: <br><strong>{{$data['disease_prev']?json_decode($data['disease_prev']->others)->first:""}}</strong></td>
                                        <td class="text-center">{{$data['disease_prev']?json_decode($data['disease_prev']->others)->second:""}}</td>
                                        <td class="align-top">{{$data['disease_prev']?json_decode($data['disease_prev']->others)->third:""}}</td>
                                        <td class="align-top">{{$data['disease_prev']?json_decode($data['disease_prev']->others)->fourth:""}}</td>
                                        <td class="align-top">{{$data['disease_prev']?json_decode($data['disease_prev']->others)->fifth:""}}</td>
                                        <td class="align-top">{{$data['disease_prev']?json_decode($data['disease_prev']->others)->sixth:""}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <p>OUTPUTS/OUTCOMES indicate : No of people served,
                                No of blood donors/pints collected, No of people/health personnel
                                trained, Name/No of health equipment supplied,  No of people reached with
                                health messages etc.
                            </p>
                        </div>
                        <div class="card-footer text-white p-0">
                            <form>
                                <div class="form-row float-right p-2 mr-3 ml-3">
                                    <button type="button" class="no-print btn btn-block btn-primary print-window">Print <i class="fas fa-print"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <!-- FOCUS AREA: ECONOMIC AND COMMUNITY DEVELOPMENT -->
                    <div class="card">
                        <div class="card-header card_header p-0">
                            <p class="card-title ml-3"><small>C.1.3 FOCUS AREA: ECONOMIC AND COMMUNITY DEVELOPMENT</small></p>
                        </div>
                        <div class="card-body card_body pt-sm-0 pt-md-3 pb-0">
                            <form class="mt-sm-3 mt-md-0">
                                <div class="row mb-2 ml-1">
                                    <div class="row col-md-12">
                                        <strong>Name of Project: &nbsp;{{$data['economic_comm']?$data['economic_comm']->project_name:""}}</strong>
                                    </div>
                                    <div class="row col-md-12">
                                        <strong>Project Address (VDC/Municipality/Ward): &nbsp;{{$data['economic_comm']?$data['economic_comm']->project_address:""}}</strong>
                                    </div>
                                </div>
                            </form>

                            <div class="table-responsive mb-3">
                                <table class="table table-bordered mb-0">
                                    <thead>
                                    <tr>
                                        <th rowspan="2" class="align-middle">S.N</th>
                                        <th rowspan="2" class="align-middle">Project Activities</th>
                                        <th rowspan="2" class="align-middle">Project Completion Date</th>
                                        <th colspan="2">G. Project Results</th>
                                        <th>H. Project Funding Type*</th>
                                        <th>I. Total Fund Contributions*</th>
                                    </tr>
                                    <tr style="font-size: 13px;">
                                        <th class="font-weight-lighter">Beneficiaries <br>
                                            (d)	population , <br>
                                            or <br>
                                            &emsp;&emsp;(b)households <br>
                                            &emsp;(c) students <br>
                                            (e)&emsp;&emsp;&emsp;&emsp;&emsp;
                                        </th>
                                        <th class="font-weight-lighter" style="width: 10%">Outputs <br>
                                            outcomes
                                        </th>
                                        <th class="font-weight-lighter">•	Global Grant <br>
                                            •	District Grant <br>
                                            •	International partner <br>
                                            •	Club to Club  funding support <br>
                                            •	Internal Club fund <br>
                                            •	Others
                                        </th>
                                        <th class="font-weight-lighter">
                                            •	GG: <br>
                                            •	DG: <br>
                                            •	International Club/partner: <br>
                                            •	Club internal <br>
                                            •	Others
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>1.</td>
                                        <td>Skill Development /Vocational Training</td>
                                        <td class="text-center">{{$data['economic_comm']?json_decode($data['economic_comm']->skill_development)->first:""}}</td>
                                        <td class="align-top">{{$data['economic_comm']?json_decode($data['economic_comm']->skill_development)->second:""}}</td>
                                        <td class="align-top">{{$data['economic_comm']?json_decode($data['economic_comm']->skill_development)->third:""}}</td>
                                        <td class="align-top">{{$data['economic_comm']?json_decode($data['economic_comm']->skill_development)->fourth:""}}</td>
                                        <td class="align-top">{{$data['economic_comm']?json_decode($data['economic_comm']->skill_development)->fifth:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>2.</td>
                                        <td>Micro-Credit support</td>
                                        <td class="text-center">{{$data['economic_comm']?json_decode($data['economic_comm']->micro_credit)->first:""}}</td>
                                        <td class="align-top">{{$data['economic_comm']?json_decode($data['economic_comm']->micro_credit)->second:""}}</td>
                                        <td class="align-top">{{$data['economic_comm']?json_decode($data['economic_comm']->micro_credit)->third:""}}</td>
                                        <td class="align-top">{{$data['economic_comm']?json_decode($data['economic_comm']->micro_credit)->fourth:""}}</td>
                                        <td class="align-top">{{$data['economic_comm']?json_decode($data['economic_comm']->micro_credit)->fifth:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>3.</td>
                                        <td>Support for Income Generating Activities</td>
                                        <td class="text-center">{{$data['economic_comm']?json_decode($data['economic_comm']->support_for_income)->first:""}}</td>
                                        <td class="align-top">{{$data['economic_comm']?json_decode($data['economic_comm']->support_for_income)->second:""}}</td>
                                        <td class="align-top">{{$data['economic_comm']?json_decode($data['economic_comm']->support_for_income)->third:""}}</td>
                                        <td class="align-top">{{$data['economic_comm']?json_decode($data['economic_comm']->support_for_income)->fourth:""}}</td>
                                        <td class="align-top">{{$data['economic_comm']?json_decode($data['economic_comm']->support_for_income)->fifth:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>4.</td>
                                        <td>Tree Plantation and Preservation of Greenery/Planet</td>
                                        <td class="text-center">{{$data['economic_comm']?json_decode($data['economic_comm']->tree_plantation)->first:""}}</td>
                                        <td class="align-top">{{$data['economic_comm']?json_decode($data['economic_comm']->tree_plantation)->second:""}}</td>
                                        <td class="align-top">{{$data['economic_comm']?json_decode($data['economic_comm']->tree_plantation)->third:""}}</td>
                                        <td class="align-top">{{$data['economic_comm']?json_decode($data['economic_comm']->tree_plantation)->fourth:""}}</td>
                                        <td class="align-top">{{$data['economic_comm']?json_decode($data['economic_comm']->tree_plantation)->fifth:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>5.</td>
                                        <td>Strengthening of community Institution</td>
                                        <td class="text-center">{{$data['economic_comm']?json_decode($data['economic_comm']->community_institution)->first:""}}</td>
                                        <td class="align-top">{{$data['economic_comm']?json_decode($data['economic_comm']->community_institution)->second:""}}</td>
                                        <td class="align-top">{{$data['economic_comm']?json_decode($data['economic_comm']->community_institution)->third:""}}</td>
                                        <td class="align-top">{{$data['economic_comm']?json_decode($data['economic_comm']->community_institution)->fourth:""}}</td>
                                        <td class="align-top">{{$data['economic_comm']?json_decode($data['economic_comm']->community_institution)->fifth:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>6.</td>
                                        <td>Others</td>
                                        <td class="text-center">{{$data['economic_comm']?json_decode($data['economic_comm']->others)->first:""}}</td>
                                        <td class="align-top">{{$data['economic_comm']?json_decode($data['economic_comm']->others)->second:""}}</td>
                                        <td class="align-top">{{$data['economic_comm']?json_decode($data['economic_comm']->others)->third:""}}</td>
                                        <td class="align-top">{{$data['economic_comm']?json_decode($data['economic_comm']->others)->fourth:""}}</td>
                                        <td class="align-top">{{$data['economic_comm']?json_decode($data['economic_comm']->others)->fifth:""}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <p>OUTPUTS/OUTCOMES indicate : No of people served, No of people trained,
                                Name/No of equipment supplied etc
                            </p>
                        </div>
                        <div class="card-footer text-white p-0">
                            <form>
                                <div class="form-row float-right p-2 mr-3 ml-3">
                                    <button type="button" class="no-print btn btn-block btn-primary print-window">Print <i class="fas fa-print"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <!-- FOCUS AREA: MATERNAL AND CHILD HEALTH -->
                    <div class="card">
                        <div class="card-header card_header p-0">
                            <p class="card-title ml-3"><small>C.1.4 FOCUS AREA: MATERNAL AND CHILD HEALTH</small></p>
                        </div>
                        <div class="card-body card_body pt-sm-0 pt-md-3 pb-0">
                            <form class="mt-sm-3 mt-md-0">
                                <div class="row mb-2 ml-1">
                                    <div class="row col-md-12">
                                        <strong>Name of Project: &nbsp;{{$data['maternal_child']?$data['maternal_child']->project_name:""}}</strong>
                                    </div>
                                    <div class="row col-md-12">
                                        <strong>Project Address (VDC/Municipality/Ward): &nbsp;{{$data['maternal_child']?$data['maternal_child']->project_address:""}}</strong>
                                    </div>
                                </div>
                            </form>

                            <div class="table-responsive mb-3">
                                <table class="table table-bordered mb-0">
                                    <thead>
                                    <tr>
                                        <th rowspan="2" class="align-middle">S.N</th>
                                        <th rowspan="2" class="align-middle" style="width: 25%">Project Activities</th>
                                        <th rowspan="2" class="align-middle">Project Completion Date</th>
                                        <th colspan="2">J. Project Results</th>
                                        <th>K. Project Funding Type*</th>
                                        <th>L. Total Fund Contributions*</th>
                                    </tr>
                                    <tr style="font-size: 13px;">
                                        <th class="font-weight-lighter">Beneficiaries <br>
                                            (f)	population , <br>
                                            or <br>
                                            &emsp;&emsp;(b)households <br>
                                            &emsp;(c) students <br>
                                            (g)&emsp;&emsp;&emsp;&emsp;&emsp;
                                        </th>
                                        <th class="font-weight-lighter" style="width: 10%">Outputs <br>
                                            outcomes
                                        </th>
                                        <th class="font-weight-lighter">•	Global Grant <br>
                                            •	District Grant <br>
                                            •	International partner <br>
                                            •	Club to Club  funding support <br>
                                            •	Internal Club fund <br>
                                            •	Others
                                        </th>
                                        <th class="font-weight-lighter">
                                            •	GG: <br>
                                            •	DG: <br>
                                            •	International Club/partner: <br>
                                            •	Club internal <br>
                                            •	Others
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>1.</td>
                                        <td>Strengthening of Health Institution for Maternal and Child Health Service</td>
                                        <td class="text-center">{{$data['maternal_child']?json_decode($data['maternal_child']->maternal_health_service)->first:""}}</td>
                                        <td class="align-top">{{$data['maternal_child']?json_decode($data['maternal_child']->maternal_health_service)->second:""}}</td>
                                        <td class="align-top">{{$data['maternal_child']?json_decode($data['maternal_child']->maternal_health_service)->third:""}}</td>
                                        <td class="align-top">{{$data['maternal_child']?json_decode($data['maternal_child']->maternal_health_service)->fourth:""}}</td>
                                        <td class="align-top">{{$data['maternal_child']?json_decode($data['maternal_child']->maternal_health_service)->fifth:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>2.</td>
                                        <td>Supporting Polio Plus campaign</td>
                                        <td class="text-center">{{$data['maternal_child']?json_decode($data['maternal_child']->polio_campaign)->first:""}}</td>
                                        <td class="align-top">{{$data['maternal_child']?json_decode($data['maternal_child']->polio_campaign)->second:""}}</td>
                                        <td class="align-top">{{$data['maternal_child']?json_decode($data['maternal_child']->polio_campaign)->third:""}}</td>
                                        <td class="align-top">{{$data['maternal_child']?json_decode($data['maternal_child']->polio_campaign)->fourth:""}}</td>
                                        <td class="align-top">{{$data['maternal_child']?json_decode($data['maternal_child']->polio_campaign)->fifth:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>3.</td>
                                        <td>Supporting Health Campaign for child and maternal health</td>
                                        <td class="text-center">{{$data['maternal_child']?json_decode($data['maternal_child']->health_campaign)->first:""}}</td>
                                        <td class="align-top">{{$data['maternal_child']?json_decode($data['maternal_child']->health_campaign)->second:""}}</td>
                                        <td class="align-top">{{$data['maternal_child']?json_decode($data['maternal_child']->health_campaign)->third:""}}</td>
                                        <td class="align-top">{{$data['maternal_child']?json_decode($data['maternal_child']->health_campaign)->fourth:""}}</td>
                                        <td class="align-top">{{$data['maternal_child']?json_decode($data['maternal_child']->health_campaign)->fifth:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>4.</td>
                                        <td>Strengthen Child health services: breast feeding, nutritional supplements,
                                            child diarrhea disease control program, school health camp etc</td>
                                        <td class="text-center">{{$data['maternal_child']?json_decode($data['maternal_child']->child_health_services)->first:""}}</td>
                                        <td class="align-top">{{$data['maternal_child']?json_decode($data['maternal_child']->child_health_services)->second:""}}</td>
                                        <td class="align-top">{{$data['maternal_child']?json_decode($data['maternal_child']->child_health_services)->third:""}}</td>
                                        <td class="align-top">{{$data['maternal_child']?json_decode($data['maternal_child']->child_health_services)->fourth:""}}</td>
                                        <td class="align-top">{{$data['maternal_child']?json_decode($data['maternal_child']->child_health_services)->fifth:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>5.</td>
                                        <td>Sustainable immunization program support</td>
                                        <td class="text-center">{{$data['maternal_child']?json_decode($data['maternal_child']->sustainable_immunization)->first:""}}</td>
                                        <td class="align-top">{{$data['maternal_child']?json_decode($data['maternal_child']->sustainable_immunization)->second:""}}</td>
                                        <td class="align-top">{{$data['maternal_child']?json_decode($data['maternal_child']->sustainable_immunization)->third:""}}</td>
                                        <td class="align-top">{{$data['maternal_child']?json_decode($data['maternal_child']->sustainable_immunization)->fourth:""}}</td>
                                        <td class="align-top">{{$data['maternal_child']?json_decode($data['maternal_child']->sustainable_immunization)->fifth:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>6.</td>
                                        <td>Others</td>
                                        <td class="text-center">{{$data['maternal_child']?json_decode($data['maternal_child']->others)->first:""}}</td>
                                        <td class="align-top">{{$data['maternal_child']?json_decode($data['maternal_child']->others)->second:""}}</td>
                                        <td class="align-top">{{$data['maternal_child']?json_decode($data['maternal_child']->others)->third:""}}</td>
                                        <td class="align-top">{{$data['maternal_child']?json_decode($data['maternal_child']->others)->fourth:""}}</td>
                                        <td class="align-top">{{$data['maternal_child']?json_decode($data['maternal_child']->others)->fifth:""}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer text-white p-0">
                            <form>
                                <div class="form-row float-right p-2 mr-3 ml-3">
                                    <button type="button" class="no-print btn btn-block btn-primary print-window">Print <i class="fas fa-print"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <!-- FOCUS AREA: BASIC EDUCATION AND LITERACY -->
                    <div class="card">
                        <div class="card-header card_header p-0">
                            <p class="card-title ml-3"><small>C.1.5 FOCUS AREA: BASIC EDUCATION AND LITERACY</small></p>
                        </div>
                        <div class="card-body card_body pt-sm-0 pt-md-3 pb-0">
                            <form class="mt-sm-3 mt-md-0">
                                <div class="row mb-2 ml-1">
                                    <div class="row col-md-12">
                                        <strong>Name of Project: &nbsp;{{$data['basic_edu']?$data['basic_edu']->project_name:""}}</strong>
                                    </div>
                                    <div class="row col-md-12">
                                        <strong>Project Address (VDC/Municipality/Ward): &nbsp;{{$data['basic_edu']?$data['basic_edu']->project_address:""}}</strong>
                                    </div>
                                </div>
                            </form>

                            <div class="table-responsive mb-3">
                                <table class="table table-bordered insert_more mb-0">
                                    <thead>
                                    <tr>
                                        <th rowspan="2" class="align-middle">S.N</th>
                                        <th rowspan="2" class="align-middle" style="width: 20%">Project Activities <br>
                                            <small>(USE THE REPORT FORMAT DEVELOPED BY RNLM/TEACH)</small></th>
                                        <th rowspan="2" class="align-middle">Project Completion Date</th>
                                        <th colspan="2">M. Project Results</th>
                                        <th>N. Project Funding Type*</th>
                                        <th colspan="2">O. Total Fund Contributions*</th>
                                    </tr>
                                    <tr style="font-size: 13px;">
                                        <th class="font-weight-lighter">Beneficiaries <br>
                                            (h)	population , <br>
                                            or <br>
                                            &emsp;&emsp;(b)households <br>
                                            &emsp;(c) students <br>
                                            (i)&emsp;&emsp;&emsp;&emsp;&emsp;
                                        </th>
                                        <th class="font-weight-lighter" style="width: 10%">Outputs <br>
                                            outcomes
                                        </th>
                                        <th class="font-weight-lighter">•	Global Grant <br>
                                            •	District Grant <br>
                                            •	International partner <br>
                                            •	Club to Club  funding support <br>
                                            •	Internal Club fund <br>
                                            •	Others
                                        </th>
                                        <th class="font-weight-lighter" style="width: 17%">
                                            •	GG: <br>
                                            •	DG: <br>
                                            •	International Club/partner: <br>
                                            •	Club internal <br>
                                            •	Others
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($data['basic_edu'])
                                        @php $i = 1 @endphp
                                        @foreach($data['basic_edu']->basic_education_data as $val)
                                            <tr>
                                                <td>{{$i++}}.</td>
                                                <td class="align-top">{{$val->project_activities}}</td>
                                                <td class="text-center">{{$val->date}}</td>
                                                <td class="align-top">{{$val->beneficiaries}}</td>
                                                <td class="align-top">{{$val->outputs}}</td>
                                                <td class="align-top">{{$val->funding_type}}</td>
                                                <td class="align-top">{{$val->fund_contributions}}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer text-white p-0">
                            <form>
                                <div class="form-row float-right p-2 mr-3 ml-3">
                                    <button type="button" class="no-print btn btn-block btn-primary print-window">Print <i class="fas fa-print"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <!-- YOUTH SERVICE -->
                    <div class="card mb-5">
                        <div class="card-header card_header p-0">
                            <p class="card-title ml-3"><small>C.1.6 YOUTH SERVICE</small></p>
                        </div>
                        <div class="card-body card_body pt-sm-0 pt-md-3 pb-0">
                            <form class="mt-sm-3 mt-md-0">
                                <div class="row mb-2 ml-1">
                                    <div class="row col-md-12">
                                        <strong>Name of Project: &nbsp;{{$data['youth_service']?$data['youth_service']->project_name:""}}</strong>
                                    </div>
                                    <div class="row col-md-12">
                                        <strong>Project Address (VDC/Municipality/Ward): &nbsp;{{$data['youth_service']?$data['youth_service']->project_address:""}}</strong>
                                    </div>
                                </div>
                            </form>

                            <div class="table-responsive mb-3">
                                <table class="table table-bordered mb-0">
                                    <thead>
                                    <tr>
                                        <th rowspan="2" class="align-middle">S.N</th>
                                        <th rowspan="2" class="align-middle" style="width: 25%;">Project Activities</th>
                                        <th rowspan="2" class="align-middle">Project Completion Date</th>
                                        <th colspan="2">P. Project Results</th>
                                        <th>Q. Project Funding Type*</th>
                                        <th>R. Total Fund Contributions*</th>
                                    </tr>
                                    <tr style="font-size: 13px;">
                                        <th class="font-weight-lighter">Beneficiaries <br>
                                            (j)	No of youth
                                        </th>
                                        <th class="font-weight-lighter" style="width: 10%">Outputs <br>
                                            outcomes
                                        </th>
                                        <th class="font-weight-lighter">•	District Grant <br>
                                            •	International partner <br>
                                            •	Club to Club funding support <br>
                                            •	Internal Club fund <br>
                                            •	Others
                                        </th>
                                        <th class="font-weight-lighter">
                                            •	GG: <br>
                                            •	DG: <br>
                                            •	International Club/partner: <br>
                                            •	Club internal <br>
                                            •	Others
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>1.</td>
                                        <td>Sponsoring RYLA</td>
                                        <td class="text-center">{{$data['youth_service']?json_decode($data['youth_service']->sponsoring)->first:""}}</td>
                                        <td class="align-top">{{$data['youth_service']?json_decode($data['youth_service']->sponsoring)->second:""}}</td>
                                        <td class="align-top">{{$data['youth_service']?json_decode($data['youth_service']->sponsoring)->third:""}}</td>
                                        <td class="align-top">{{$data['youth_service']?json_decode($data['youth_service']->sponsoring)->fourth:""}}</td>
                                        <td class="align-top">{{$data['youth_service']?json_decode($data['youth_service']->sponsoring)->fifth:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>2.</td>
                                        <td>Joint Service Project with ROTARACTS/INTERACTS</td>
                                        <td class="text-center">{{$data['youth_service']?json_decode($data['youth_service']->joint_service)->first:""}}</td>
                                        <td class="align-top">{{$data['youth_service']?json_decode($data['youth_service']->joint_service)->second:""}}</td>
                                        <td class="align-top">{{$data['youth_service']?json_decode($data['youth_service']->joint_service)->third:""}}</td>
                                        <td class="align-top">{{$data['youth_service']?json_decode($data['youth_service']->joint_service)->fourth:""}}</td>
                                        <td class="align-top">{{$data['youth_service']?json_decode($data['youth_service']->joint_service)->fifth:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>3.</td>
                                        <td>Career Counseling service</td>
                                        <td class="text-center">{{$data['youth_service']?json_decode($data['youth_service']->career_counseling)->first:""}}</td>
                                        <td class="align-top">{{$data['youth_service']?json_decode($data['youth_service']->career_counseling)->second:""}}</td>
                                        <td class="align-top">{{$data['youth_service']?json_decode($data['youth_service']->career_counseling)->third:""}}</td>
                                        <td class="align-top">{{$data['youth_service']?json_decode($data['youth_service']->career_counseling)->fourth:""}}</td>
                                        <td class="align-top">{{$data['youth_service']?json_decode($data['youth_service']->career_counseling)->fifth:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>4.</td>
                                        <td>Professional skill development training</td>
                                        <td class="text-center">{{$data['youth_service']?json_decode($data['youth_service']->professional_skills)->first:""}}</td>
                                        <td class="align-top">{{$data['youth_service']?json_decode($data['youth_service']->professional_skills)->second:""}}</td>
                                        <td class="align-top">{{$data['youth_service']?json_decode($data['youth_service']->professional_skills)->third:""}}</td>
                                        <td class="align-top">{{$data['youth_service']?json_decode($data['youth_service']->professional_skills)->fourth:""}}</td>
                                        <td class="align-top">{{$data['youth_service']?json_decode($data['youth_service']->professional_skills)->fifth:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>5.</td>
                                        <td>Others</td>
                                        <td class="text-center">{{$data['youth_service']?json_decode($data['youth_service']->others)->first:""}}</td>
                                        <td class="align-top">{{$data['youth_service']?json_decode($data['youth_service']->others)->second:""}}</td>
                                        <td class="align-top">{{$data['youth_service']?json_decode($data['youth_service']->others)->third:""}}</td>
                                        <td class="align-top">{{$data['youth_service']?json_decode($data['youth_service']->others)->fourth:""}}</td>
                                        <td class="align-top">{{$data['youth_service']?json_decode($data['youth_service']->others)->fifth:""}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer text-white p-0">
                            <form>
                                <div class="form-row float-right p-2 mr-3 ml-3">
                                    <button type="button" class="no-print btn btn-block btn-primary print-window">Print <i class="fas fa-print"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>


                    <hr>
                    <!-- MEDIA COVERAGE of CLUB ACTIVITIES in the Reporting Month -->
                    <div class="row mb-2 ml-1 mt-3">
                        <div class="row col-md-12">
                            <strong>Club Name: &nbsp;{{$club_name}}</strong>
                        </div>
                        <div class="row col-md-12">
                            <strong>Month: &nbsp;{{$month_name->name}}</strong>
                        </div>
                    </div>
                    <strong>D. CLUB PUBLIC RELATION COMMITTEE</strong>
                    <div class="card">
                        <div class="card-header card_header p-0">
                            <p class="card-title ml-3"><small>D.1 MEDIA COVERAGE of CLUB ACTIVITIES in the Reporting Month</small></p>
                        </div>
                        <div class="card-body card_body pt-sm-0 pt-md-3 pb-0">
                            <div class="table-responsive mb-3">
                                <table class="table table-bordered insert_more mb-0">
                                    <thead>
                                    <tr>
                                        <th colspan="9">No of events covered by Media  ( sample of coverage to be attached in the report)</th>
                                    </tr>
                                    <tr>
                                        <th>S.N</th>
                                        <th>Description of message  disseminated </th>
                                        <th>Date Covered</th>
                                        <th>No of times coverage</th>
                                        <th>Name of Print media</th>
                                        <th>Name of Visual Media</th>
                                        <th>Name of Audio/Radio</th>
                                        <th style="width: 18%">Name  of Social Media</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($data['media_coverage'])
                                        @php $i = 1 @endphp
                                        @foreach($data['media_coverage']->media_coverage_data as $val)
                                            <tr>
                                                <td>{{$i++}}.</td>
                                                <td class="align-top">{{$val->message}}</td>
                                                <td class="text-center">{{$val->date}}</td>
                                                <td class="align-top">{{$val->times_coverage}}</td>
                                                <td class="align-top">{{$val->print_name}}</td>
                                                <td class="align-top">{{$val->visual_name}}</td>
                                                <td class="align-top">{{$val->audio_name}}</td>
                                                <td class="align-top">{{$val->social_media}}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer text-white p-0">
                            <form>
                                <div class="form-row float-right p-2 mr-3 ml-3">
                                    <button type="button" class="no-print btn btn-block btn-primary print-window">Print <i class="fas fa-print"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>


                    <!-- CLUB FLAGSHIP/SIGNATURE PROJECT -->
                    <div class="card">
                        <div class="card-header card_header p-0">
                            <p class="card-title ml-3"><small>D.2 CLUB FLAGSHIP/SIGNATURE PROJECT</small></p>
                        </div>
                        <div class="card-body card_body pt-sm-0 pt-md-3 pb-0">
                            <div class="table-responsive mb-3">
                                <table class="table table-bordered insert_more mb-0">
                                    <thead>
                                    <tr>
                                        <th colspan="3" class="text-left">Name of CLUB FLAGSHIP/SIGNATURE PROJECT: &ensp;&nbsp;{{$data['club_flagship']?$data['club_flagship']->name_of_club_flagship:""}}</th>
                                        <th colspan="3" class="text-left">Partner Name: &ensp;&nbsp;{{$data['club_flagship']?$data['club_flagship']->partner_name:""}}</th>
                                    </tr>
                                    <tr>
                                        <th rowspan="2" class="align-middle">S.N</th>
                                        <th rowspan="2" class="align-middle">Describe what activities have been implemented in the month</th>
                                        <th rowspan="2" class="align-middle">Project : Completed/
                                            Ongoing
                                        </th>
                                        <th rowspan="2" class="align-middle">Beneficiaries</th>
                                        <th rowspan="2" class="align-middle">Area/Location</th>
                                        <th colspan="2">Project Funding Type</th>
                                    </tr>
                                    <tr style="font-size: 13px;">
                                        <th class="font-weight-lighter" colspan="2" style="width: 20%">•	Global Grant <br>
                                            •	District Grant <br>
                                            •	International partner <br>
                                            •	Club to Club  funding support <br>
                                            •	Internal Club fund <br>
                                            •	Others
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($data['club_flagship'])
                                        @php $i = 1 @endphp
                                        @foreach($data['club_flagship']->club_flagship_data as $val)
                                            <tr>
                                                <td>{{$i++}}.</td>
                                                <td class="align-top">{{$val->activities}}</td>
                                                <td class="text-center">{{$val->project_check}}</td>
                                                <td class="align-top">{{$val->beneficiaries}}</td>
                                                <td class="align-top">{{$val->area}}</td>
                                                <td class="align-top">{{$val->funding_type}}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer text-white p-0">
                            <form>
                                <div class="form-row float-right p-2 mr-3 ml-3">
                                    <button type="button" class="no-print btn btn-block btn-primary print-window">Print <i class="fas fa-print"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <!-- PROMOTION of FOUR WAY TESTS and PROJECT INFORMATION BOARD DISPLAY -->
                    <div class="card mb-5">
                        <div class="card-header card_header p-0">
                            <p class="card-title ml-3"><small>D.3 PROMOTION of FOUR WAY TESTS and PROJECT INFORMATION BOARD DISPLAY</small></p>
                        </div>
                        <div class="card-body card_body pt-sm-0 pt-md-3 pb-0">
                            <div class="table-responsive mb-3">
                                <table class="table table-bordered mb-0">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>Name of School/Institutions: &ensp;<br>{{$data['promotion']?$data['promotion']->name_of_school:""}}<br></th>
                                        <th>Name of Business Houses: &ensp;<br>{{$data['promotion']?$data['promotion']->name_of_business:""}}<br></th>
                                        <th>Name of Public Places: &ensp;<br>{{$data['promotion']?$data['promotion']->name_of_public_places:""}}<br></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>No of 4-Way Test Banners Installed</td>
                                        <td>{{$data['promotion']?json_decode($data['promotion']->test_banners)->first:""}}</td>
                                        <td>{{$data['promotion']?json_decode($data['promotion']->test_banners)->second:""}}</td>
                                        <td>{{$data['promotion']?json_decode($data['promotion']->test_banners)->third:""}}</td>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <th>School/Institutions</th>
                                        <th>Community sites</th>
                                        <th>Other sites</th>
                                    </tr>
                                    <tr>
                                        <td>No of Project Information Board installed</td>
                                        <td>{{$data['promotion']?json_decode($data['promotion']->project_information)->first:""}}</td>
                                        <td>{{$data['promotion']?json_decode($data['promotion']->project_information)->second:""}}</td>
                                        <td>{{$data['promotion']?json_decode($data['promotion']->project_information)->third:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>Club Brochures</td>
                                        <td colspan="3">{{$data['promotion']?$data['promotion']->club_brochures:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>Club Pamphlets</td>
                                        <td colspan="3">{{$data['promotion']?$data['promotion']->club_pamphlets:""}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer text-white p-0">
                            <form>
                                <div class="form-row float-right p-2 mr-3 ml-3">
                                    <button type="button" class="no-print btn btn-block btn-primary print-window">Print <i class="fas fa-print"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>


                    <hr>
                    <!-- Annual Fund Contribution -->
                    <div class="row mb-2 ml-1 mt-3">
                        <div class="row col-md-12">
                            <strong>Club Name: &nbsp;{{$club_name}}</strong>
                        </div>
                        <div class="row col-md-12">
                            <strong>Month: &nbsp;{{$month_name->name}}</strong>
                        </div>
                    </div>
                    <strong>E. THE ROTARY FOUNDATION COMMITTEE</strong>
                    <div class="card">
                        <div class="card-header card_header p-0">
                            <p class="card-title ml-3"><small>E.1 Annual Fund Contribution</small></p>
                        </div>
                        <div class="card-body card_body pt-sm-0 pt-md-3 pb-0">
                            <form class="mt-sm-3 mt-md-0">
                                <div class="row mb-2 ml-1">
                                    <div class="row col-md-12">
                                        <strong>Annual Target of TRF Contribution (USD): &nbsp;{{$data['annual_fund']?$data['annual_fund']->annual_target:""}}</strong>
                                    </div>
                                </div>
                            </form>
                            <div class="table-responsive mb-3">
                                <table class="table table-bordered mb-0">
                                    <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <th>TRF Contribution categories</th>
                                        <th>No of contributing Members</th>
                                        <th>Total Amount USD</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>1.</td>
                                        <td>100% Club Contribution: Small Gifts
                                            Minimum USD 10 to USD 99
                                        </td>
                                        <td class="text-center">{{$data['annual_fund']?json_decode($data['annual_fund']->club_contribution)->first:""}}</td>
                                        <td class="text-center">{{$data['annual_fund']?json_decode($data['annual_fund']->club_contribution)->second:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>2.</td>
                                        <td>Rotary Foundation Sustaining Members (RFSM):
                                            USD 100 per year
                                        </td>
                                        <td class="text-center">{{$data['annual_fund']?json_decode($data['annual_fund']->rotary_foundation)->first:""}}</td>
                                        <td class="text-center">{{$data['annual_fund']?json_decode($data['annual_fund']->rotary_foundation)->second:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>3.</td>
                                        <td>Paul Harris Fellow (PHF):
                                            USD 300 or USD 500 as directed by the District
                                        </td>
                                        <td class="text-center">{{$data['annual_fund']?json_decode($data['annual_fund']->phf)->first:""}}</td>
                                        <td class="text-center">{{$data['annual_fund']?json_decode($data['annual_fund']->phf)->second:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>4.</td>
                                        <td>Multi Paul Harris Fellow (MPHF)
                                            USD 300 or USD 500 as directed by the District
                                        </td>
                                        <td class="text-center">{{$data['annual_fund']?json_decode($data['annual_fund']->mphf)->first:""}}</td>
                                        <td class="text-center">{{$data['annual_fund']?json_decode($data['annual_fund']->mphf)->second:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>5.</td>
                                        <td>PAUL HARRIS SOCIETY (PHS)
                                            USD 1000.00 per year
                                        </td>
                                        <td class="text-center">{{$data['annual_fund']?json_decode($data['annual_fund']->phs)->first:""}}</td>
                                        <td class="text-center">{{$data['annual_fund']?json_decode($data['annual_fund']->phs)->second:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>6.</td>
                                        <td>Major Donor
                                            USD 10000.00
                                        </td>
                                        <td class="text-center">{{$data['annual_fund']?json_decode($data['annual_fund']->major_donor)->first:""}}</td>
                                        <td class="text-center">{{$data['annual_fund']?json_decode($data['annual_fund']->major_donor)->second:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>6.</td>
                                        <td>Others</td>
                                        <td class="text-center">{{$data['annual_fund']?json_decode($data['annual_fund']->others)->first:""}}</td>
                                        <td class="text-center">{{$data['annual_fund']?json_decode($data['annual_fund']->others)->second:""}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer text-white p-0">
                            <form>
                                <div class="form-row float-right p-2 mr-3 ml-3">
                                    <button type="button" class="no-print btn btn-block btn-primary print-window">Print <i class="fas fa-print"></i></button>
                                </div>
                            </form>
                        </div>
                    </div>


                    <!-- Participation in TRF ACTIVITIES -->
                    <div class="card">
                        <div class="card-header card_header p-0">
                            <p class="card-title ml-3"><small>E.2 Participation in TRF ACTIVITIES</small></p>
                        </div>
                        <div class="card-body card_body pt-sm-0 pt-md-3 pb-0">
                            <form class="mt-sm-3 mt-md-0">
                                <div class="row mb-2 ml-1">
                                    <div class="row col-md-12">
                                        <strong>Annual Target of TRF Contribution (USD): &nbsp;{{$data['participation']?$data['participation']->annual_target:""}}</strong>
                                    </div>
                                </div>
                            </form>
                            <div class="table-responsive mb-3">
                                <table class="table table-bordered mb-0">
                                    <thead>
                                    <tr>
                                        <th rowspan="2" class="align-middle" width="5%">S.N</th>
                                        <th rowspan="2" class="align-middle" width="30%">TRF Activities</th>
                                        <th rowspan="2" class="align-middle">Participants</th>
                                        <th colspan="2">No of Project proposals</th>
                                    </tr>
                                    <tr>
                                        <th>Submitted</th>
                                        <th>Approved</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>1.</td>
                                        <td>Global  Grant Projects</td>
                                        <td>{{$data['participation']?json_decode($data['participation']->global_grant)->first:""}}</td>
                                        <td>{{$data['participation']?json_decode($data['participation']->global_grant)->second:""}}</td>
                                        <td>{{$data['participation']?json_decode($data['participation']->global_grant)->third:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>2.</td>
                                        <td>District Grant Projects</td>
                                        <td>{{$data['participation']?json_decode($data['participation']->district_grant)->first:""}}</td>
                                        <td>{{$data['participation']?json_decode($data['participation']->district_grant)->second:""}}</td>
                                        <td>{{$data['participation']?json_decode($data['participation']->district_grant)->third:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>3.</td>
                                        <td>Club level Orientation on TRF</td>
                                        <td>{{$data['participation']?json_decode($data['participation']->club_level)->first:""}}</td>
                                        <td>{{$data['participation']?json_decode($data['participation']->club_level)->second:""}}</td>
                                        <td>{{$data['participation']?json_decode($data['participation']->club_level)->third:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>4.</td>
                                        <td>District Grand Management Seminar</td>
                                        <td>{{$data['participation']?json_decode($data['participation']->district_grand)->first:""}}</td>
                                        <td>{{$data['participation']?json_decode($data['participation']->district_grand)->second:""}}</td>
                                        <td>{{$data['participation']?json_decode($data['participation']->district_grand)->third:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>5.</td>
                                        <td>District TRF Seminar</td>
                                        <td>{{$data['participation']?json_decode($data['participation']->district_trf)->first:""}}</td>
                                        <td>{{$data['participation']?json_decode($data['participation']->district_trf)->second:""}}</td>
                                        <td>{{$data['participation']?json_decode($data['participation']->district_trf)->third:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>6.</td>
                                        <td>No of Peace Scholarship sponsored</td>
                                        <td>{{$data['participation']?json_decode($data['participation']->no_of_peace)->first:""}}</td>
                                        <td>{{$data['participation']?json_decode($data['participation']->no_of_peace)->second:""}}</td>
                                        <td>{{$data['participation']?json_decode($data['participation']->no_of_peace)->third:""}}</td>
                                    </tr>
                                    <tr>
                                        <td>7.</td>
                                        <td>No of youth Exchange students sponsored/hosted</td>
                                        <td>{{$data['participation']?json_decode($data['participation']->no_of_youth)->first:""}}</td>
                                        <td>{{$data['participation']?json_decode($data['participation']->no_of_youth)->second:""}}</td>
                                        <td>{{$data['participation']?json_decode($data['participation']->no_of_youth)->third:""}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer text-white p-0">

                            <div class="form-row float-right pt-4 mr-3 ml-3">
                                <form>
                                    <div class="form-group mr-2">
                                        <button type="submit" class="no-print btn btn-primary print-window">Print <i class="fas fa-print"></i></button>
                                    </div>
                                </form>

                                @if(Auth::user()->role !== 'admin')
                                    <div class="form-group mr-2">
                                        <button id="finalSubmit" type="button" data-value="{{$month_id}}" data-id="{{Auth::user()->id}}" class="no-print btn btn-secondary">Final Submit <i class="fas fa-check"></i></button>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

