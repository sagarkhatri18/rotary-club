@extends('layouts.app')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-left">
                        <li class="breadcrumb-item"><a href="index.blade.php">Home</a></li>
                        <li class="breadcrumb-item active">Maternal & Child Health</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header card_header p-0">
                            <p class="card-title ml-3"><small>FOCUS AREA: MATERNAL AND CHILD HEALTH</small></p>
                        </div>
                        <form class="mt-sm-3 mt-md-0" method="post" action="{{route('maternal_child.store')}}">
                            @csrf
                            <div class="card-body card_body pt-sm-0 pt-md-3 pb-0">
                                <div class="form-row">
                                    <div class="form-group col-xl-4 col-md-12 col-sm-6">
                                        <label for="club_name"><strong>Club Name</strong></label>
                                        <input type="text" readonly class="form-control-plaintext" id="club_name"
                                               value="{{Auth::user()->club_name}}">
                                    </div>
                                    <input type="hidden" name="id" value="{{$maternal_child?($maternal_child->id ? $maternal_child->id : ""):""}}"/>
                                    <input type="hidden" name="month_id" value="{{$month_id}}"/>
                                    <div class="form-group col-xl-4 col-md-12 col-sm-6">
                                        <label for="name"><strong>Name of the Project:</strong></label>
                                        <input type="text" required name="project_name" class="form-control" id="name"
                                               value="{{$maternal_child?$maternal_child->project_name:""}}">
                                    </div>
                                    <div class="form-group col-xl-4 col-md-12 col-sm-6">
                                        <label for="address"><strong>Project Address (INSTITUTION/VDC/Municipality/Ward):</strong></label>
                                        <input type="text" name="project_address" class="form-control" id="address"
                                               value="{{$maternal_child?$maternal_child->project_address:""}}">
                                    </div>
                                </div>

                                <div class="table-responsive mb-3">
                                    <table class="table table-bordered mb-0">
                                        <thead>
                                        <tr>
                                            <th rowspan="2" class="align-middle">S.N</th>
                                            <th rowspan="2" class="align-middle" style="width: 25%">Project Activities</th>
                                            <th rowspan="2" class="align-middle">Project Completion Date</th>
                                            <th colspan="2">J. Project Results</th>
                                            <th>K. Project Funding Type*</th>
                                            <th>L. Total Fund Contributions*</th>
                                        </tr>
                                        <tr style="font-size: 13px;">
                                            <th class="font-weight-lighter">Beneficiaries <br>
                                                (f)	population , <br>
                                                or <br>
                                                &emsp;&emsp;(b)households <br>
                                                &emsp;(c) students <br>
                                                (g)&emsp;&emsp;&emsp;&emsp;&emsp;
                                            </th>
                                            <th class="font-weight-lighter" style="width: 10%">Outputs <br>
                                                outcomes
                                            </th>
                                            <th class="font-weight-lighter">•	Global Grant <br>
                                                •	District Grant <br>
                                                •	International partner <br>
                                                •	Club to Club  funding support <br>
                                                •	Internal Club fund <br>
                                                •	Others
                                            </th>
                                            <th class="font-weight-lighter">
                                                •	GG: <br>
                                                •	DG: <br>
                                                •	International Club/partner: <br>
                                                •	Club internal <br>
                                                •	Others
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody class="tbl_body">
                                        <tr>
                                            <td>1.</td>
                                            <td>Strengthening of Health Institution for Maternal and Child Health Service</td>
                                            <td class="text-center">
                                                <input type="date" name="maternal_health_service_first" class="form-control" value="{{$maternal_child?json_decode($maternal_child->maternal_health_service)->first:""}}"/>
                                            </td>
                                            
                                            <td class="position-relative">
                                                <button type="button" data-value="{{$maternal_child?(json_decode($maternal_child->maternal_health_service)->second ? json_decode($maternal_child->maternal_health_service)->second : ""):""}}" class="checked text-center">
                                                    <span>{!! $maternal_child?(json_decode($maternal_child->maternal_health_service)->second ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="maternal_health_service_second"
                                                       value="{{$maternal_child?(json_decode($maternal_child->maternal_health_service)->second ? json_decode($maternal_child->maternal_health_service)->second : ""):""}}"/>
                                            </td>
                                            
                                            <td class="position-relative">
                                                <button type="button" data-value="{{$maternal_child?(json_decode($maternal_child->maternal_health_service)->third ? json_decode($maternal_child->maternal_health_service)->third : ""):""}}" class="checked text-center">
                                                    <span>{!! $maternal_child?(json_decode($maternal_child->maternal_health_service)->third ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="maternal_health_service_third"
                                                       value="{{$maternal_child?(json_decode($maternal_child->maternal_health_service)->third ? json_decode($maternal_child->maternal_health_service)->third : ""):""}}"/>
                                            </td>
                                            
                                            <td class="position-relative">
                                                <button type="button" data-value="{{$maternal_child?(json_decode($maternal_child->maternal_health_service)->fourth ? json_decode($maternal_child->maternal_health_service)->fourth : ""):""}}" class="checked text-center">
                                                    <span>{!! $maternal_child?(json_decode($maternal_child->maternal_health_service)->fourth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="maternal_health_service_fourth"
                                                       value="{{$maternal_child?(json_decode($maternal_child->maternal_health_service)->fourth ? json_decode($maternal_child->maternal_health_service)->fourth : ""):""}}"/>
                                            </td>
                                            
                                            <td class="position-relative">
                                                <button type="button" data-value="{{$maternal_child?(json_decode($maternal_child->maternal_health_service)->fifth ? json_decode($maternal_child->maternal_health_service)->fifth : ""):""}}" class="checked text-center">
                                                    <span>{!! $maternal_child?(json_decode($maternal_child->maternal_health_service)->fifth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="maternal_health_service_fifth"
                                                       value="{{$maternal_child?(json_decode($maternal_child->maternal_health_service)->fifth ? json_decode($maternal_child->maternal_health_service)->fifth : ""):""}}"/>
                                            </td>
                                            
                                        </tr>
                                        <tr>
                                            <td>2.</td>
                                            <td>Supporting Polio Plus campaign</td>
                                            <td class="text-center">
                                                <input type="date" name="polio_campaign_first" class="form-control" value="{{$maternal_child?json_decode($maternal_child->polio_campaign)->first:""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$maternal_child?(json_decode($maternal_child->polio_campaign)->second ? json_decode($maternal_child->polio_campaign)->second : ""):""}}" class="checked text-center">
                                                    <span>{!! $maternal_child?(json_decode($maternal_child->polio_campaign)->second ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="polio_campaign_second"
                                                       value="{{$maternal_child?(json_decode($maternal_child->polio_campaign)->second ? json_decode($maternal_child->polio_campaign)->second : ""):""}}"/>
                                            </td>
                                            
                                            <td class="position-relative">
                                                <button type="button" data-value="{{$maternal_child?(json_decode($maternal_child->polio_campaign)->third ? json_decode($maternal_child->polio_campaign)->third : ""):""}}" class="checked text-center">
                                                    <span>{!! $maternal_child?(json_decode($maternal_child->polio_campaign)->third ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="polio_campaign_third"
                                                       value="{{$maternal_child?(json_decode($maternal_child->polio_campaign)->third ? json_decode($maternal_child->polio_campaign)->third : ""):""}}"/>
                                            </td>
                                            
                                            <td class="position-relative">
                                                <button type="button" data-value="{{$maternal_child?(json_decode($maternal_child->polio_campaign)->fourth ? json_decode($maternal_child->polio_campaign)->fourth : ""):""}}" class="checked text-center">
                                                    <span>{!! $maternal_child?(json_decode($maternal_child->polio_campaign)->fourth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="polio_campaign_fourth"
                                                       value="{{$maternal_child?(json_decode($maternal_child->polio_campaign)->fourth ? json_decode($maternal_child->polio_campaign)->fourth : ""):""}}"/>
                                            </td>
                                            
                                            <td class="position-relative">
                                                <button type="button" data-value="{{$maternal_child?(json_decode($maternal_child->polio_campaign)->fifth ? json_decode($maternal_child->polio_campaign)->fifth : ""):""}}" class="checked text-center">
                                                    <span>{!! $maternal_child?(json_decode($maternal_child->polio_campaign)->fifth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="polio_campaign_fifth"
                                                       value="{{$maternal_child?(json_decode($maternal_child->polio_campaign)->fifth ? json_decode($maternal_child->polio_campaign)->fifth : ""):""}}"/>
                                            </td>
                                            
                                        </tr>
                                        <tr>
                                            <td>3.</td>
                                            <td>Supporting Health Campaign for child and maternal health</td>
                                            <td class="text-center">
                                                <input type="date" name="health_campaign_first" class="form-control" value="{{$maternal_child?json_decode($maternal_child->health_campaign)->first:""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$maternal_child?(json_decode($maternal_child->health_campaign)->second ? json_decode($maternal_child->health_campaign)->second : ""):""}}" class="checked text-center">
                                                    <span>{!! $maternal_child?(json_decode($maternal_child->health_campaign)->second ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="health_campaign_second"
                                                       value="{{$maternal_child?(json_decode($maternal_child->health_campaign)->second ? json_decode($maternal_child->health_campaign)->second : ""):""}}"/>
                                            </td>
                                            
                                            <td class="position-relative">
                                                <button type="button" data-value="{{$maternal_child?(json_decode($maternal_child->health_campaign)->third ? json_decode($maternal_child->health_campaign)->third : ""):""}}" class="checked text-center">
                                                    <span>{!! $maternal_child?(json_decode($maternal_child->health_campaign)->third ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="health_campaign_third"
                                                       value="{{$maternal_child?(json_decode($maternal_child->health_campaign)->third ? json_decode($maternal_child->health_campaign)->third : ""):""}}"/>
                                            </td>
                                            
                                            <td class="position-relative">
                                                <button type="button" data-value="{{$maternal_child?(json_decode($maternal_child->health_campaign)->fourth ? json_decode($maternal_child->health_campaign)->fourth : ""):""}}" class="checked text-center">
                                                    <span>{!! $maternal_child?(json_decode($maternal_child->health_campaign)->fourth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="health_campaign_fourth"
                                                       value="{{$maternal_child?(json_decode($maternal_child->health_campaign)->fourth ? json_decode($maternal_child->health_campaign)->fourth : ""):""}}"/>
                                            </td>
                                            
                                            <td class="position-relative">
                                                <button type="button" data-value="{{$maternal_child?(json_decode($maternal_child->health_campaign)->fifth ? json_decode($maternal_child->health_campaign)->fifth : ""):""}}" class="checked text-center">
                                                    <span>{!! $maternal_child?(json_decode($maternal_child->health_campaign)->fifth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="health_campaign_fifth"
                                                       value="{{$maternal_child?(json_decode($maternal_child->health_campaign)->fifth ? json_decode($maternal_child->health_campaign)->fifth : ""):""}}"/>
                                            </td>
                                            
                                        </tr>
                                        <tr>
                                            <td>4.</td>
                                            <td>Strengthen Child health services: breast feeding, nutritional supplements,
                                                child diarrhea disease control program, school health camp etc</td>
                                            <td class="text-center">
                                                <input type="date" name="child_health_services_first" class="form-control" value="{{$maternal_child?json_decode($maternal_child->child_health_services)->first:""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$maternal_child?(json_decode($maternal_child->child_health_services)->second ? json_decode($maternal_child->child_health_services)->second : ""):""}}" class="checked text-center">
                                                    <span>{!! $maternal_child?(json_decode($maternal_child->child_health_services)->second ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="child_health_services_second"
                                                       value="{{$maternal_child?(json_decode($maternal_child->child_health_services)->second ? json_decode($maternal_child->child_health_services)->second : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$maternal_child?(json_decode($maternal_child->child_health_services)->third ? json_decode($maternal_child->child_health_services)->third : ""):""}}" class="checked text-center">
                                                    <span>{!! $maternal_child?(json_decode($maternal_child->child_health_services)->third ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="child_health_services_third"
                                                       value="{{$maternal_child?(json_decode($maternal_child->child_health_services)->third ? json_decode($maternal_child->child_health_services)->third : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$maternal_child?(json_decode($maternal_child->child_health_services)->fourth ? json_decode($maternal_child->child_health_services)->fourth : ""):""}}" class="checked text-center">
                                                    <span>{!! $maternal_child?(json_decode($maternal_child->child_health_services)->fourth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="child_health_services_fourth"
                                                       value="{{$maternal_child?(json_decode($maternal_child->child_health_services)->fourth ? json_decode($maternal_child->child_health_services)->fourth : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$maternal_child?(json_decode($maternal_child->child_health_services)->fifth ? json_decode($maternal_child->child_health_services)->fifth : ""):""}}" class="checked text-center">
                                                    <span>{!! $maternal_child?(json_decode($maternal_child->child_health_services)->fifth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="child_health_services_fifth"
                                                       value="{{$maternal_child?(json_decode($maternal_child->child_health_services)->fifth ? json_decode($maternal_child->child_health_services)->fifth : ""):""}}"/>
                                            </td>
                                            
                                        </tr>
                                        <tr>
                                            <td>5.</td>
                                            <td>Sustainable immunization program support</td>
                                            <td class="text-center">
                                                <input type="date" name="sustainable_immunization_first" class="form-control" value="{{$maternal_child?json_decode($maternal_child->sustainable_immunization)->first:""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$maternal_child?(json_decode($maternal_child->sustainable_immunization)->second ? json_decode($maternal_child->sustainable_immunization)->second : ""):""}}" class="checked text-center">
                                                    <span>{!! $maternal_child?(json_decode($maternal_child->sustainable_immunization)->second ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="sustainable_immunization_second"
                                                       value="{{$maternal_child?(json_decode($maternal_child->sustainable_immunization)->second ? json_decode($maternal_child->sustainable_immunization)->second : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$maternal_child?(json_decode($maternal_child->sustainable_immunization)->third ? json_decode($maternal_child->sustainable_immunization)->third : ""):""}}" class="checked text-center">
                                                    <span>{!! $maternal_child?(json_decode($maternal_child->sustainable_immunization)->third ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="sustainable_immunization_third"
                                                       value="{{$maternal_child?(json_decode($maternal_child->sustainable_immunization)->third ? json_decode($maternal_child->sustainable_immunization)->third : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$maternal_child?(json_decode($maternal_child->sustainable_immunization)->fourth ? json_decode($maternal_child->sustainable_immunization)->fourth : ""):""}}" class="checked text-center">
                                                    <span>{!! $maternal_child?(json_decode($maternal_child->sustainable_immunization)->fourth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="sustainable_immunization_fourth"
                                                       value="{{$maternal_child?(json_decode($maternal_child->sustainable_immunization)->fourth ? json_decode($maternal_child->sustainable_immunization)->fourth : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$maternal_child?(json_decode($maternal_child->sustainable_immunization)->fifth ? json_decode($maternal_child->sustainable_immunization)->fifth : ""):""}}" class="checked text-center">
                                                    <span>{!! $maternal_child?(json_decode($maternal_child->sustainable_immunization)->fifth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="sustainable_immunization_fifth"
                                                       value="{{$maternal_child?(json_decode($maternal_child->sustainable_immunization)->fifth ? json_decode($maternal_child->sustainable_immunization)->fifth : ""):""}}"/>
                                            </td>
                                            
                                        </tr>
                                        <tr>
                                            <td>6.</td>
                                            <td>Others</td>
                                            <td class="text-center">
                                                <input type="date" name="others_first" class="form-control" value="{{$maternal_child?json_decode($maternal_child->others)->first:""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$maternal_child?(json_decode($maternal_child->others)->second ? json_decode($maternal_child->others)->second : ""):""}}" class="checked text-center">
                                                    <span>{!! $maternal_child?(json_decode($maternal_child->others)->second ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="others_second"
                                                       value="{{$maternal_child?(json_decode($maternal_child->others)->second ? json_decode($maternal_child->others)->second : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$maternal_child?(json_decode($maternal_child->others)->third ? json_decode($maternal_child->others)->third : ""):""}}" class="checked text-center">
                                                    <span>{!! $maternal_child?(json_decode($maternal_child->others)->third ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="others_third"
                                                       value="{{$maternal_child?(json_decode($maternal_child->others)->third ? json_decode($maternal_child->others)->third : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$maternal_child?(json_decode($maternal_child->others)->fourth ? json_decode($maternal_child->others)->fourth : ""):""}}" class="checked text-center">
                                                    <span>{!! $maternal_child?(json_decode($maternal_child->others)->fourth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="others_fourth"
                                                       value="{{$maternal_child?(json_decode($maternal_child->others)->fourth ? json_decode($maternal_child->others)->fourth : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$maternal_child?(json_decode($maternal_child->others)->fifth ? json_decode($maternal_child->others)->fifth : ""):""}}" class="checked text-center">
                                                    <span>{!! $maternal_child?(json_decode($maternal_child->others)->fifth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="others_fifth"
                                                       value="{{$maternal_child?(json_decode($maternal_child->others)->fifth ? json_decode($maternal_child->others)->fifth : ""):""}}"/>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer text-white p-0">
                                <div class="form-row float-right p-2 mr-3 ml-3">
                                    <button type="submit" class="btn btn-block btn-primary">Save <i class="fas fa-check"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


