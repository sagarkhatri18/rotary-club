@extends('layouts.app')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-left">
                        <li class="breadcrumb-item"><a href="index.blade.php">Home</a></li>
                        <li class="breadcrumb-item active">Media Coverage of Club Activities</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header card_header p-0">
                            <p class="card-title ml-3"><small>MEDIA COVERAGE of CLUB ACTIVITIES in the Reporting Month</small></p>
                        </div>
                        <form class="mt-sm-3 mt-md-0" method="post" action="{{route('media_coverage.store')}}">
                            @csrf
                            <div class="card-body card_body pt-sm-0 pt-md-3 pb-0">
                                <div class="form-row">
                                    <div class="form-group col-xl-4 col-md-12 col-sm-6">
                                        <label for="club_name" class="col-form-label">Club Name</label>
                                        <input type="text" readonly class="form-control-plaintext" id="club_name"
                                               value="{{Auth::user()->club_name}}">
                                    </div>
                                    <input type="hidden" name="id" value="{{$media_coverage?($media_coverage->id ? $media_coverage->id : ""):""}}"/>
                                    <input type="hidden" name="month_id" value="{{$month_id}}"/>
                                </div>

                                <div class="table-responsive mb-3">
                                    <table class="table table-bordered insert_more mb-0">
                                        <thead>
                                        <tr>
                                            <th colspan="9">No of events covered by Media  ( sample of coverage to be attached in the report)</th>
                                        </tr>
                                        <tr>
                                            <th>S.N</th>
                                            <th width="14%">Description of message  disseminated </th>
                                            <th>Date Covered</th>
                                            <th>No of times coverage</th>
                                            <th>Name of Print media</th>
                                            <th>Name of Visual Media</th>
                                            <th>Name of Audio/Radio</th>
                                            <th colspan="2" style="width: 18%">Name  of Social Media</th>
                                        </tr>
                                        </thead>
                                        <tbody class="tbl_body">

                                        @if (!is_null($media_coverage_data))
                                            @for ($i=0; $i<count($media_coverage_data); $i++)
                                                <tr>
                                                    <td>{{$i+1}}.</td>
                                                    <input type="hidden" name="media_coverage_data_id[]" value="{{$media_coverage_data[$i]->id}}"/>

                                                    <td class="position-relative">
                                                        <button type="button" data-value="{{$media_coverage_data ? $media_coverage_data[$i]->message ? $media_coverage_data[$i]->message : "":""}}" class="checked text-center">
                                                            <span>{!! $media_coverage_data ? $media_coverage_data[$i]->message ? "<i class='fas fa-check'></i>" : "":"" !!}</span>
                                                        </button>
                                                        <input type="text" hidden class="form-control" name="message[]"
                                                               value="{{$media_coverage_data ? $media_coverage_data[$i]->message ? $media_coverage_data[$i]->message : "":""}}"/>
                                                    </td>

                                                    <td><input type="date" class="form-control" name="date[]" value="{{$media_coverage_data[$i]->date}}"/></td>

                                                    <td><textarea style="width: 100%; height: 40px;" class="form-control" name="times_coverage[]">{{$media_coverage_data[$i]->times_coverage}}</textarea></td>

                                                    <td class="position-relative">
                                                        <button type="button" data-value="{{$media_coverage_data ? $media_coverage_data[$i]->print_name ? $media_coverage_data[$i]->print_name : "":""}}" class="checked text-center">
                                                            <span>{!! $media_coverage_data ? $media_coverage_data[$i]->print_name ? "<i class='fas fa-check'></i>" : "":"" !!}</span>
                                                        </button>
                                                        <input type="text" hidden class="form-control" name="print_name[]"
                                                               value="{{$media_coverage_data ? $media_coverage_data[$i]->print_name ? $media_coverage_data[$i]->print_name : "":""}}"/>
                                                    </td>

                                                    <td class="position-relative">
                                                        <button type="button" data-value="{{$media_coverage_data ? $media_coverage_data[$i]->visual_name ? $media_coverage_data[$i]->visual_name : "":""}}" class="checked text-center">
                                                            <span>{!! $media_coverage_data ? $media_coverage_data[$i]->visual_name ? "<i class='fas fa-check'></i>" : "":"" !!}</span>
                                                        </button>
                                                        <input type="text" hidden class="form-control" name="visual_name[]"
                                                               value="{{$media_coverage_data ? $media_coverage_data[$i]->visual_name ? $media_coverage_data[$i]->visual_name : "":""}}"/>
                                                    </td>

                                                    <td class="position-relative">
                                                        <button type="button" data-value="{{$media_coverage_data ? $media_coverage_data[$i]->audio_name ? $media_coverage_data[$i]->audio_name : "":""}}" class="checked text-center">
                                                            <span>{!! $media_coverage_data ? $media_coverage_data[$i]->audio_name ? "<i class='fas fa-check'></i>" : "":"" !!}</span>
                                                        </button>
                                                        <input type="text" hidden class="form-control" name="audio_name[]"
                                                               value="{{$media_coverage_data ? $media_coverage_data[$i]->audio_name ? $media_coverage_data[$i]->audio_name : "":""}}"/>
                                                    </td>

                                                    <td><textarea style="width: 100%; height: 40px;" class="form-control" name="social_media[]">{{$media_coverage_data[$i]->social_media}}</textarea></td>

                                                    {!!($i == 0)?'<td style="width: 5%"><button class="btn btn-primary btn-sm addButtonClass" type="button"><i class="fas fa-plus"></i></button></td>':
                                                     '<td style="width: 5%" class="text-center"><i class="fas fa-check"></i></td>'!!}
                                                </tr>
                                            @endfor
                                        @else
                                            <tr>
                                                <td>1.</td>

                                                <td class="position-relative">
                                                    <button type="button" data-value="" class="checked text-center">
                                                        <span></span>
                                                    </button>
                                                    <input type="text" hidden class="form-control" name="message[]"/>
                                                </td>

                                                <td class="text-center"><input type="date" class="form-control" name="date[]"/></td>
                                                <td><textarea style="width: 100%; height: 40px;" name="times_coverage[]" class="form-control"></textarea></td>

                                                <td class="position-relative">
                                                    <button type="button" data-value="" class="checked text-center">
                                                        <span></span>
                                                    </button>
                                                    <input type="text" hidden class="form-control" name="print_name[]"/>
                                                </td>

                                                <td class="position-relative">
                                                    <button type="button" data-value="" class="checked text-center">
                                                        <span></span>
                                                    </button>
                                                    <input type="text" hidden class="form-control" name="visual_name[]"/>
                                                </td>

                                                <td class="position-relative">
                                                    <button type="button" data-value="" class="checked text-center">
                                                        <span></span>
                                                    </button>
                                                    <input type="text" hidden class="form-control" name="audio_name[]"/>
                                                </td>

                                                <td><textarea style="width: 100%; height: 40px;" name="social_media[]" class="form-control"></textarea></td>
                                                <td style="width: 5%"><button class="btn btn-primary btn-sm addButtonClass" type="button"><i class="fas fa-plus"></i></button></td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                    <div class="d-none">
                                        <table class="clone_table">
                                            <tr>
                                                <td><span class="sn"></span></td>
                                                <input type="hidden" name="media_coverage_data_id[]" value=""/>

                                                <td class="position-relative">
                                                    <button type="button" data-value="" class="checked text-center">
                                                        <span></span>
                                                    </button>
                                                    <input type="text" hidden class="form-control" name="message[]"/>
                                                </td>

                                                <td class="text-center"><input type="date" class="form-control" name="date[]"/></td>
                                                <td><textarea style="width: 100%; height: 40px;" name="times_coverage[]" class="form-control"></textarea></td>
                                                <td class="position-relative">
                                                    <button type="button" data-value="" class="checked text-center">
                                                        <span></span>
                                                    </button>
                                                    <input type="text" hidden class="form-control" name="print_name[]"/>
                                                </td>

                                                <td class="position-relative">
                                                    <button type="button" data-value="" class="checked text-center">
                                                        <span></span>
                                                    </button>
                                                    <input type="text" hidden class="form-control" name="visual_name[]"/>
                                                </td>

                                                <td class="position-relative">
                                                    <button type="button" data-value="" class="checked text-center">
                                                        <span></span>
                                                    </button>
                                                    <input type="text" hidden class="form-control" name="audio_name[]"/>
                                                </td>
                                                <td><textarea style="width: 100%; height: 40px;" name="social_media[]" class="form-control"></textarea></td>
                                                <td style="width: 5%"><button class="btn btn-danger btn-sm removeButton" type="button"><i class="fas fa-times"></i></button></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-white p-0">
                                <div class="form-row float-right p-2 mr-3 ml-3">
                                    <button type="submit" class="btn btn-block btn-primary">Save <i class="fas fa-check"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


