@extends('layouts.app')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-left">
                        <li class="breadcrumb-item"><a href="index.blade.php">Home</a></li>
                        <li class="breadcrumb-item active">Membership Classification Survey</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header card_header p-0">
                            <p class="card-title ml-3"><small>Membership Classification Survey</small></p>
                        </div>
                        <form class="mt-sm-3 mt-md-0" method="post" action="{{route('membership_classification.store')}}">
                            @csrf
                            <div class="card-body card_body pt-sm-0 pt-md-3 pb-0">
                                <div class="form-row">
                                    <div class="form-group col-xl-3 col-md-12 col-sm-6">
                                        <label for="club_name"><strong>Club Name</strong></label>
                                        <input type="text" readonly class="form-control-plaintext" id="club_name"
                                               value="{{Auth::user()->club_name}}">
                                    </div>
                                    <input type="hidden" name="id" value="{{$membership_classification?($membership_classification->id ? $membership_classification->id : ""):""}}"/>
                                    <input type="hidden" name="month_id" value="{{$month_id}}"/>
                                    <div class="form-group col-xl-3 col-md-12 col-sm-6">
                                        <label for="members"><strong>Number of Membership in th club:</strong></label>
                                        <input type="text" required name="no_of_members" class="form-control"
                                               value="{{$membership_classification?$membership_classification->no_of_members:""}}" id="members">
                                    </div>
                                    <div class="form-group col-xl-3 col-md-12 col-sm-6">
                                        <label for="attendance"><strong>Annual Target for Membership Growth(No.):</strong></label>
                                        <input type="text" name="annual_target_no" class="form-control"
                                               value="{{$membership_classification?$membership_classification->annual_target_no:""}}" id="attendance">
                                    </div>
                                    <div class="form-group col-xl-3 col-md-12 col-sm-6">
                                        <label for="annual_target"><strong>Annual Target for Membership Growth(%):</strong></label>
                                        <input type="text" name="annual_target_percent" id="annual_target"
                                               value="{{$membership_classification?$membership_classification->annual_target_percent:""}}" class="form-control">
                                    </div>
                                </div>

                                <div class="table-responsive mb-3">
                                    <table class="table table-bordered insert_more mb-0">
                                        <thead>
                                        <tr>
                                            <th colspan="3">Classification Fulfilled</th>
                                            <th colspan="3">Classification identified/un-fulfilled</th>
                                        </tr>
                                        <tr>
                                            <th style="width: 5%">S.N</th>
                                            <th>Classification Categories</th>
                                            <th>No of Club members in the category</th>
                                            <th style="width: 5%">S.N</th>
                                            <th colspan="2">Classification categories</th>
                                        </tr>
                                        </thead>
                                        <tbody class="tbl_body">
                                        @if(!is_null($membership_classification_data))
                                            @for($i=0; $i<count($membership_classification_data); $i++)
                                                <tr>
                                                    <td>{{$i+1}}.</td>
                                                    <input type="hidden" name="classification_data_id[]" value="{{$membership_classification_data[$i]->id}}"/>
                                                    <td>
                                                        <textarea style="width: 100%; height: 40px;" name="classification_categories_fulfilled[]" class="form-control"
                                                        >{{$membership_classification_data[$i]->classification_categories_fulfilled}}</textarea>
                                                    </td>
                                                    <td>
                                                        <textarea style="width: 100%; height: 40px;" name="no_of_club_members[]" class="form-control"
                                                        >{{$membership_classification_data[$i]->no_of_club_members}}</textarea>
                                                    </td>
                                                    <td>{{$i+1}}.</td>
                                                    <td>
                                                        <textarea style="width: 100%; height: 40px;" name="classification_categories_unfulfilled[]" class="form-control"
                                                        >{{$membership_classification_data[$i]->classification_categories_unfulfilled}}</textarea>
                                                    </td>
                                                    {!! ($i == 0)? '<td><button class="btn btn-primary btn-sm addButtonClass" type="button"><i class="fas fa-plus"></i></button></td>':
                                                    '<td class="text-center"><i class="fas fa-check"></i></td>' !!}
                                                </tr>
                                            @endfor
                                        @else
                                            <tr>
                                                <td>1.</td>
                                                <td>
                                                    <textarea style="width: 100%; height: 40px;" name="classification_categories_fulfilled[]" class="form-control"></textarea>
                                                </td>
                                                <td>
                                                    <textarea style="width: 100%; height: 40px;" name="no_of_club_members[]" class="form-control"></textarea>
                                                </td>
                                                <td>1.</td>
                                                <td>
                                                    <textarea style="width: 100%; height: 40px;" name="classification_categories_unfulfilled[]" class="form-control"></textarea>
                                                </td>
                                                <td><button class="btn btn-primary btn-sm addButtonClass" type="button"><i class="fas fa-plus"></i></button></td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                    <div class="d-none">
                                        <table class="clone_table">
                                            <tr>
                                                <input type="hidden" name="classification_data_id[]" value=""/>
                                                <td><span class="sn"></span></td>
                                                <td>
                                                    <textarea style="width: 100%; height: 40px;" name="classification_categories_fulfilled[]" class="form-control"></textarea>
                                                </td>
                                                <td>
                                                    <textarea style="width: 100%; height: 40px;" name="no_of_club_members[]" class="form-control"></textarea>
                                                </td>
                                                <td><span class="sn"></span></td>
                                                <td>
                                                    <textarea style="width: 100%; height: 40px;" name="classification_categories_unfulfilled[]" class="form-control"></textarea>
                                                </td>
                                                <td><button class="btn btn-danger btn-sm removeButton" type="button"><i class="fas fa-times"></i></button></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-white p-0">
                                <div class="form-row float-right p-2 mr-3 ml-3">
                                    <button type="submit" class="btn btn-block btn-primary">Save <i class="fas fa-check"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


