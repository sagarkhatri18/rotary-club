@extends('layouts.app')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-left">
                        <li class="breadcrumb-item"><a href="index.blade.php">Home</a></li>
                        <li class="breadcrumb-item active">Membership Development in the Month</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header card_header p-0">
                            <p class="card-title ml-3"><small>Membership Development in the Month</small></p>
                        </div>
                        <form class="mt-sm-3 mt-md-0" method="post" action="{{route('membership_development.store')}}">
                            @csrf
                            <div class="card-body card_body pt-sm-0 pt-md-3 pb-0">
                                <div class="form-row">
                                    <div class="form-group col-xl-3 col-md-12 col-sm-6">
                                        <label for="club_name"><strong>Club Name</strong></label>
                                        <input type="text" readonly class="form-control-plaintext" id="club_name"
                                               value="{{Auth::user()->club_name}}">
                                    </div>
                                    <input type="hidden" name="id" value="{{$membership_development?($membership_development->id ? $membership_development->id : ""):""}}"/>
                                    <input type="hidden" name="month_id" value="{{$month_id}}"/>
                                    <div class="form-group col-xl-3 col-md-12 col-sm-6">
                                        <label for="members"><strong>Number of Membership in the club:</strong></label>
                                        <input type="text" required name="no_of_members" class="form-control" id="members"
                                               value="{{$membership_development?$membership_development->no_of_members:""}}">
                                    </div>
                                    <div class="form-group col-xl-3 col-md-12 col-sm-6">
                                        <label for="attendance"><strong>Annual Target for Membership Growth(No.):</strong></label>
                                        <input type="text" name="annual_target_no" class="form-control" id="attendance"
                                               value="{{$membership_development?$membership_development->annual_target_no:""}}">
                                    </div>
                                    <div class="form-group col-xl-3 col-md-12 col-sm-6">
                                        <label for="annual_target"><strong>Annual Target for Membership Growth(%):</strong></label>
                                        <input type="text" id="annual_target" class="form-control" name="annual_target_percent"
                                               value="{{$membership_development?$membership_development->annual_target_percent:""}}">
                                    </div>
                                </div>

                                <div class="table-responsive mb-3">
                                    <table class="table table-bordered mb-0">
                                        <thead>
                                        <tr>
                                            <th colspan="2" rowspan="2" class="align-middle">B.3.1 Membership Orientation Training </th>
                                            <th colspan="2">No of participants</th>
                                            <th rowspan="2" class="align-middle">Date</th>
                                        </tr>
                                        <tr>
                                            <th>Club Event</th>
                                            <th>District Event</th>
                                        </tr>
                                        </thead>
                                        <tbody class="tbl_body">
                                        <tr>
                                            <td>1.</td>
                                            <td>New Members Orientation Training</td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$membership_development?(json_decode($membership_development->new_members_orientation)->club_event ? json_decode($membership_development->new_members_orientation)->club_event : ""):""}}" class="checked text-center">
                                                    <span>{!! $membership_development?(json_decode($membership_development->new_members_orientation)->club_event ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="new_members_orientation_club_event"
                                                       value="{{$membership_development?(json_decode($membership_development->new_members_orientation)->club_event ? json_decode($membership_development->new_members_orientation)->club_event : ""):""}}"/>
                                            </td>
                                            
                                            <td class="position-relative">
                                                <button type="button" data-value="{{$membership_development?(json_decode($membership_development->new_members_orientation)->district_event ? json_decode($membership_development->new_members_orientation)->district_event : ""):""}}" class="checked text-center">
                                                    <span>{!! $membership_development?(json_decode($membership_development->new_members_orientation)->district_event ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="new_members_orientation_district_event"
                                                       value="{{$membership_development?(json_decode($membership_development->new_members_orientation)->district_event ? json_decode($membership_development->new_members_orientation)->district_event : ""):""}}"/>
                                            </td>
                                            
                                            <td class="text-center"><input type="date" class="form-control" name="new_members_orientation_date" value="{{$membership_development?json_decode($membership_development->new_members_orientation)->date:""}}"/></td>
                                        </tr>
                                        <tr>
                                            <td>2.</td>
                                            <td>Club Officers Training Seminar</td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$membership_development?(json_decode($membership_development->club_officers)->club_event ? json_decode($membership_development->club_officers)->club_event : ""):""}}" class="checked text-center">
                                                    <span>{!! $membership_development?(json_decode($membership_development->club_officers)->club_event ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="club_officers_club_event"
                                                       value="{{$membership_development?(json_decode($membership_development->club_officers)->club_event ? json_decode($membership_development->club_officers)->club_event : ""):""}}"/>
                                            </td>
                                            
                                            <td class="position-relative">
                                                <button type="button" data-value="{{$membership_development?(json_decode($membership_development->club_officers)->district_event ? json_decode($membership_development->club_officers)->district_event : ""):""}}" class="checked text-center">
                                                    <span>{!! $membership_development?(json_decode($membership_development->club_officers)->district_event ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="club_officers_district_event"
                                                       value="{{$membership_development?(json_decode($membership_development->club_officers)->district_event ? json_decode($membership_development->club_officers)->district_event : ""):""}}"/>
                                            </td>
                                            
                                            <td class="text-center"><input type="date" class="form-control" name="club_officers_date" value="{{$membership_development?json_decode($membership_development->club_officers)->date:""}}"/></td>
                                        </tr>
                                        <tr>
                                            <td>3.</td>
                                            <td>District Membership Seminar</td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$membership_development?(json_decode($membership_development->district_membership)->club_event ? json_decode($membership_development->district_membership)->club_event : ""):""}}" class="checked text-center">
                                                    <span>{!! $membership_development?(json_decode($membership_development->district_membership)->club_event ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="district_membership_club_event"
                                                       value="{{$membership_development?(json_decode($membership_development->district_membership)->club_event ? json_decode($membership_development->district_membership)->club_event : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$membership_development?(json_decode($membership_development->district_membership)->district_event ? json_decode($membership_development->district_membership)->district_event : ""):""}}" class="checked text-center">
                                                    <span>{!! $membership_development?(json_decode($membership_development->district_membership)->district_event ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="district_membership_district_event"
                                                       value="{{$membership_development?(json_decode($membership_development->district_membership)->district_event ? json_decode($membership_development->district_membership)->district_event : ""):""}}"/>
                                            </td>
                                            
                                            <td class="text-center"><input type="date" class="form-control" name="district_membership_date" value="{{$membership_development?json_decode($membership_development->district_membership)->date:""}}"/></td>
                                        </tr>
                                        <tr>
                                            <td>4.</td>
                                            <td>Others (name)</td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$membership_development?(json_decode($membership_development->membership_others)->club_event ? json_decode($membership_development->membership_others)->club_event : ""):""}}" class="checked text-center">
                                                    <span>{!! $membership_development?(json_decode($membership_development->membership_others)->club_event ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="membership_others_club_event"
                                                       value="{{$membership_development?(json_decode($membership_development->membership_others)->club_event ? json_decode($membership_development->membership_others)->club_event : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$membership_development?(json_decode($membership_development->membership_others)->district_event ? json_decode($membership_development->membership_others)->district_event : ""):""}}" class="checked text-center">
                                                    <span>{!! $membership_development?(json_decode($membership_development->membership_others)->district_event ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="membership_others_district_event"
                                                       value="{{$membership_development?(json_decode($membership_development->membership_others)->district_event ? json_decode($membership_development->membership_others)->district_event : ""):""}}"/>
                                            </td>
                                            
                                            <td class="text-center"><input type="date" class="form-control" name="membership_others_date" value="{{$membership_development?json_decode($membership_development->membership_others)->date:""}}"/></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="table-responsive mb-3">
                                    <table class="table table-bordered mb-0">
                                        <thead>
                                        <tr>
                                            <th colspan="2">B.3.2 New Clubs Sponsored in the Month</th>
                                            <th>Charter Date</th>
                                            <th>No of Charter Members</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>1.</td>
                                            <td>Rotary Club of</td>
                                            <td class="text-center"><input type="date" class="form-control" name="rotary_club_date" value="{{$membership_development?json_decode($membership_development->rotary_club)->date:""}}"/></td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="rotary_club_charter_members"
                                                >{{$membership_development?json_decode($membership_development->rotary_club)->charter_members:""}}</textarea></td>
                                        </tr>
                                        <tr>
                                            <td>2.</td>
                                            <td>Rotaract Club of</td>
                                            <td class="text-center"><input type="date" class="form-control" name="rotaract_club_date" value="{{$membership_development?json_decode($membership_development->rotaract_club)->date:""}}"/></td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="rotaract_club_charter_members"
                                                >{{$membership_development?json_decode($membership_development->rotaract_club)->charter_members:""}}</textarea></td>
                                        </tr>
                                        <tr>
                                            <td>3.</td>
                                            <td>Interact Club of </td>
                                            <td class="text-center"><input type="date" class="form-control" name="interact_club_date" value="{{$membership_development?json_decode($membership_development->interact_club)->date:""}}"/></td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="interact_club_charter_members"
                                                >{{$membership_development?json_decode($membership_development->interact_club)->charter_members:""}}</textarea></td>
                                        </tr>
                                        <tr>
                                            <td>4.</td>
                                            <td>Rotary Community Corps</td>
                                            <td class="text-center"><input type="date" class="form-control" name="rotary_community_date" value="{{$membership_development?json_decode($membership_development->rotary_community)->date:""}}"/></td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="rotary_community_charter_members"
                                                >{{$membership_development?json_decode($membership_development->rotary_community)->charter_members:""}}</textarea></td>
                                        </tr>
{{--                                        <tr>--}}
{{--                                            <td>4</td>--}}
{{--                                            <td></td>--}}
{{--                                            <td class="text-center"><input type="date" class="form-control"/></td>--}}
{{--                                            <td><textarea style="width: 100%; height: 30px;"></textarea></td>--}}
{{--                                        </tr>--}}
                                        </tbody>
                                    </table>
                                </div>

                                <div class="table-responsive mb-3">
                                    <table class="table table-bordered mb-0">
                                        <thead>
                                        <tr>
                                            <th colspan="2">B.3.3 Other Club Membership Development Activities </th>
                                            <th>Date/period of survey</th>
                                            <th>No of participants</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>1.</td>
                                            <td>Club Members Satisfaction Survey</td>
                                            <td class="text-center"><input type="date" class="form-control" name="members_satisfaction_date" value="{{$membership_development?json_decode($membership_development->members_satisfaction)->date:""}}"/></td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="members_satisfaction_no"
                                                >{{$membership_development?json_decode($membership_development->members_satisfaction)->no:""}}</textarea></td>
                                        </tr>
                                        <tr>
                                            <td>2.</td>
                                            <td>Club Health Check Survey</td>
                                            <td class="text-center"><input type="date" class="form-control" name="health_check_date" value="{{$membership_development?json_decode($membership_development->health_check)->date:""}}"/></td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="health_check_no"
                                                >{{$membership_development?json_decode($membership_development->health_check)->no:""}}</textarea></td>
                                        </tr>
                                        <tr>
                                            <td>3.</td>
                                            <td>Others</td>
                                            <td class="text-center"><input type="date" class="form-control" name="development_others_date" value="{{$membership_development?json_decode($membership_development->development_others)->date:""}}"/></td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="development_others_no"
                                                >{{$membership_development?json_decode($membership_development->development_others)->no:""}}</textarea></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer text-white p-0">
                                <div class="form-row float-right p-2 mr-3 ml-3">
                                    <button type="submit" class="btn btn-block btn-primary">Save <i class="fas fa-check"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


