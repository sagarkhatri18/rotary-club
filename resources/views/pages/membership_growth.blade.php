@extends('layouts.app')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-left">
                        <li class="breadcrumb-item"><a href="index.blade.php">Home</a></li>
                        <li class="breadcrumb-item active">Membership Growth</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header card_header p-0">
                            <p class="card-title ml-3"><small>Membership Growth</small></p>
                        </div>
                        <form class="mt-sm-3 mt-md-0" method="post" action="{{route('membership_growth.store')}}">
                            @csrf
                            <div class="card-body card_body pt-sm-0 pt-md-3 pb-0">
                                <div class="form-row">
                                    <div class="form-group col-xl-3 col-md-12 col-sm-6">
                                        <label for="club_name"><strong>Club Name</strong></label>
                                        <input type="text" readonly class="form-control-plaintext" id="club_name"
                                               value="{{Auth::user()->club_name}}">
                                    </div>
                                    <input type="hidden" name="id" value="{{$membership_growth?($membership_growth->id ? $membership_growth->id : ""):""}}"/>
                                    <input type="hidden" name="month_id" value="{{$month_id}}"/>
                                    <div class="form-group col-xl-3 col-md-12 col-sm-6">
                                        <label for="members"><strong>Number of Membership in the club:</strong></label>
                                        <input type="text" required name="no_of_members" class="form-control" id="members"
                                               value="{{$membership_growth?$membership_growth->no_of_members:""}}">
                                    </div>
                                    <div class="form-group col-xl-3 col-md-12 col-sm-6">
                                        <label for="attendance"><strong>Annual Target for Membership Growth(No.):</strong></label>
                                        <input type="text" name="annual_target_no" class="form-control" id="attendance"
                                               value="{{$membership_growth?$membership_growth->annual_target_no:""}}">
                                    </div>
                                    <div class="form-group col-xl-3 col-md-12 col-sm-6">
                                        <label for="annual_target"><strong>Annual Target for Membership Growth(%):</strong></label>
                                        <input type="text" name="annual_target_percent" id="annual_target" class="form-control"
                                               value="{{$membership_growth?$membership_growth->annual_target_percent:""}}">
                                    </div>
                                </div>

                                <div class="table-responsive mb-3">
                                    <table class="table table-bordered mb-0">
                                        <thead>
                                        <tr>
                                            <th>B.2.1 Total Net Growth/Loss of Members</th>
                                            <th style="width: 20%">Number of members</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>(a)	Total numbers of members in the Club as of July 01, 2019</td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="loss_of_members_first"
                                                >{{$membership_growth?json_decode($membership_growth->loss_of_members)->first:""}}</textarea></td>
                                        </tr>
                                        <tr>
                                            <td>(a)	Total numbers of members terminated  in the reporting month</td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="loss_of_members_second"
                                                >{{$membership_growth?json_decode($membership_growth->loss_of_members)->second:""}}</textarea></td>
                                        </tr>
                                        <tr>
                                            <td>(a)	Total number of new members inducted in the reporting month</td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="loss_of_members_third"
                                                >{{$membership_growth?json_decode($membership_growth->loss_of_members)->third:""}}</textarea></td>
                                        </tr>
                                        <tr>
                                            <td>(a)	Total numbers of Net Gowth/Loss of Members in the reporting period
                                                = (a)-(b)+(c)
                                            </td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="loss_of_members_fourth"
                                                >{{$membership_growth?json_decode($membership_growth->loss_of_members)->fourth:""}}</textarea></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="table-responsive mb-3">
                                    <table class="table table-bordered mb-0">
                                        <thead>
                                        <tr>
                                            <th>B.2.2 Net Growth/Loss of Female Members</th>
                                            <th style="width: 20%">Number of members</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>(a)	Total numbers of female members in the Club as of July 01, 2019</td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="loss_of_female_members_first"
                                                >{{$membership_growth?json_decode($membership_growth->loss_of_female_members)->first:""}}</textarea></td>
                                        </tr>
                                        <tr>
                                            <td>(a)	Total numbers of female members terminated by the reporting Month</td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="loss_of_female_members_second"
                                                >{{$membership_growth?json_decode($membership_growth->loss_of_female_members)->second:""}}</textarea></td>
                                        </tr>
                                        <tr>
                                            <td>(a)	Total numbers of new female members inducted by the reporting Month</td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="loss_of_female_members_third"
                                                >{{$membership_growth?json_decode($membership_growth->loss_of_female_members)->third:""}}</textarea></td>
                                        </tr>
                                        <tr>
                                            <td>(a)	Total numbers of Net Growth/Loss of Female Members by the
                                                reporting period=(a)-(b)+(c )
                                            </td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="loss_of_female_members_fourth"
                                                >{{$membership_growth?json_decode($membership_growth->loss_of_female_members)->fourth:""}}</textarea></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="table-responsive mb-3">
                                    <table class="table table-bordered mb-0">
                                        <thead>
                                        <tr>
                                            <th>B.2.3 Net Growth/Loss of Youth Members</th>
                                            <th style="width: 20%">Number of members</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>(a)	Total numbers of youth members (<40 years of age) as of July 01, 2019</td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="loss_of_youth_members_first"
                                                >{{$membership_growth?json_decode($membership_growth->loss_of_youth_members)->first:""}}</textarea></td>
                                        </tr>
                                        <tr>
                                            <td>(a)	Total numbers of youth members terminated by the reporting Month</td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="loss_of_youth_members_second"
                                                >{{$membership_growth?json_decode($membership_growth->loss_of_youth_members)->second:""}}</textarea></td>
                                        </tr>
                                        <tr>
                                            <td>(a)	Total numbers of new youth members inducted by the reporting Month</td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="loss_of_youth_members_third"
                                                >{{$membership_growth?json_decode($membership_growth->loss_of_youth_members)->third:""}}</textarea></td>
                                        </tr>
                                        <tr>
                                            <td>(a)	Total numbers of Net Growth/Loss of Youth Members
                                                by the reporting Month=(a)-(b)+(c)
                                            </td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="loss_of_youth_members_fourth"
                                                >{{$membership_growth?json_decode($membership_growth->loss_of_youth_members)->fourth:""}}</textarea></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer text-white p-0">
                                <div class="form-row float-right p-2 mr-3 ml-3">
                                    <button type="submit" class="btn btn-block btn-primary">Save <i class="fas fa-check"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


