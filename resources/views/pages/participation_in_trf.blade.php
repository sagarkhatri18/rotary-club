@extends('layouts.app')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-left">
                        <li class="breadcrumb-item"><a href="index.blade.php">Home</a></li>
                        <li class="breadcrumb-item active">Participation in TRF Activities</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header card_header p-0">
                            <p class="card-title ml-3"><small>Participation in TRF ACTIVITIES</small></p>
                        </div>
                        <form class="mt-sm-3 mt-md-0" method="post" action="{{route('participation_in_trf.store')}}">
                            @csrf
                            <div class="card-body card_body pt-sm-0 pt-md-3 pb-0">
                                <div class="form-row">
                                    <div class="form-group col-xl-4 col-md-12 col-sm-6 mr-auto">
                                        <label for="club_name"><strong>Club Name</strong></label>
                                        <input type="text" readonly class="form-control-plaintext" id="club_name"
                                               value="{{Auth::user()->club_name}}">
                                    </div>
                                    <input type="hidden" name="id" value="{{$participation?($participation->id ? $participation->id : ""):""}}"/>
                                    <input type="hidden" name="month_id" value="{{$month_id}}"/>
                                    <div class="form-group col-xl-4 col-md-12 col-sm-6">
                                        <label for="annual_target"><strong>Annual Target of TRF Contribution (USD):</strong></label>
                                        <input type="text" class="form-control" required name="annual_target" id="annual_target"
                                        value="{{$participation?$participation->annual_target:""}}">
                                    </div>
                                </div>

                                <div class="table-responsive mb-3">
                                    <table class="table table-bordered mb-0">
                                        <thead>
                                        <tr>
                                            <th rowspan="2" class="align-middle">S.N</th>
                                            <th rowspan="2" class="align-middle">TRF Activities</th>
                                            <th rowspan="2" class="align-middle">Participants</th>
                                            <th colspan="2">No of Project proposals</th>
                                        </tr>
                                        <tr>
                                            <th>Submitted</th>
                                            <th>Approved</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>1.</td>
                                            <td>Global  Grant Projects</td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="global_grant_first"
                                                >{{$participation?json_decode($participation->global_grant)->first:""}}</textarea></td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="global_grant_second"
                                                >{{$participation?json_decode($participation->global_grant)->second:""}}</textarea></td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="global_grant_third"
                                                >{{$participation?json_decode($participation->global_grant)->third:""}}</textarea></td>
                                        </tr>
                                        <tr>
                                            <td>2.</td>
                                            <td>District Grant Projects</td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="district_grant_first"
                                                >{{$participation?json_decode($participation->district_grant)->first:""}}</textarea></td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="district_grant_second"
                                                >{{$participation?json_decode($participation->district_grant)->second:""}}</textarea></td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="district_grant_third"
                                                >{{$participation?json_decode($participation->district_grant)->third:""}}</textarea></td>
                                        </tr>
                                        <tr>
                                            <td>3.</td>
                                            <td>Club level Orientation on TRF</td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="club_level_first"
                                                >{{$participation?json_decode($participation->club_level)->first:""}}</textarea></td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="club_level_second"
                                                >{{$participation?json_decode($participation->club_level)->second:""}}</textarea></td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="club_level_third"
                                                >{{$participation?json_decode($participation->club_level)->third:""}}</textarea></td>
                                        </tr>
                                        <tr>
                                            <td>4.</td>
                                            <td>District Grand Management Seminar</td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="district_grand_first"
                                                >{{$participation?json_decode($participation->district_grand)->first:""}}</textarea></td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="district_grand_second"
                                                >{{$participation?json_decode($participation->district_grand)->second:""}}</textarea></td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="district_grand_third"
                                                >{{$participation?json_decode($participation->district_grand)->third:""}}</textarea></td>
                                        </tr>
                                        <tr>
                                            <td>5.</td>
                                            <td>District TRF Seminar</td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="district_trf_first"
                                                >{{$participation?json_decode($participation->district_trf)->first:""}}</textarea></td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="district_trf_second"
                                                >{{$participation?json_decode($participation->district_trf)->second:""}}</textarea></td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="district_trf_third"
                                                >{{$participation?json_decode($participation->district_trf)->third:""}}</textarea></td>
                                        </tr>
                                        <tr>
                                            <td>6.</td>
                                            <td>No of Peace Scholarship sponsored</td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="no_of_peace_first"
                                                >{{$participation?json_decode($participation->no_of_peace)->first:""}}</textarea></td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="no_of_peace_second"
                                                >{{$participation?json_decode($participation->no_of_peace)->second:""}}</textarea></td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="no_of_peace_third"
                                                >{{$participation?json_decode($participation->no_of_peace)->third:""}}</textarea></td>
                                        </tr>
                                        <tr>
                                            <td>7.</td>
                                            <td>No of youth Exchange students sponsored/hosted</td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="no_of_youth_first"
                                                >{{$participation?json_decode($participation->no_of_youth)->first:""}}</textarea></td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="no_of_youth_second"
                                                >{{$participation?json_decode($participation->no_of_youth)->second:""}}</textarea></td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="no_of_youth_third"
                                                >{{$participation?json_decode($participation->no_of_youth)->third:""}}</textarea></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer text-white p-0">
                                <div class="form-row float-right p-2 mr-3 ml-3">
                                    <button type="submit" class="btn btn-block btn-primary">Save <i class="fas fa-check"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


