@extends('layouts.app')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-left">
                        <li class="breadcrumb-item"><a href="index.blade.php">Home</a></li>
                        <li class="breadcrumb-item active">Promotion of Four Way Tests & Project Information Board Display</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header card_header p-0">
                            <p class="card-title ml-3"><small>PROMOTION of FOUR WAY TESTS and PROJECT INFORMATION BOARD DISPLAY</small></p>
                        </div>
                        <form class="mt-sm-3 mt-md-0" method="post" action="{{route('promotion.store')}}">
                            @csrf
                            <div class="card-body card_body pt-sm-0 pt-md-3 pb-0">
                                <div class="form-row">
                                    <div class="form-group col-xl-4 col-md-12 col-sm-6">
                                        <label for="club_name"><strong>Club Name</strong></label>
                                        <input type="text" readonly class="form-control-plaintext" id="club_name"
                                               value="{{Auth::user()->club_name}}">
                                    </div>
                                    <input type="hidden" name="id" value="{{$promotion?($promotion->id ? $promotion->id : ""):""}}"/>
                                    <input type="hidden" name="month_id" value="{{$month_id}}"/>
                                </div>

                                <div class="table-responsive mb-3">
                                    <table class="table table-bordered mb-0">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th>Name of School/Institutions: &ensp;
                                                <textarea style="width: 100%; height: 50px;" class="form-control" required name="name_of_school"
                                                >{{$promotion?$promotion->name_of_school:""}}</textarea>
                                            </th>
                                            <th>Name of Business Houses:
                                                &ensp;<textarea style="width: 100%; height: 50px;" class="form-control" name="name_of_business"
                                                >{{$promotion?$promotion->name_of_business:""}}</textarea>
                                            </th>
                                            <th>Name of Public Places:
                                                &ensp;<textarea style="width: 100%; height: 50px;" class="form-control" name="name_of_public_places"
                                                >{{$promotion?$promotion->name_of_public_places:""}}</textarea>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>No of 4-Way Test Banners Installed</td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="test_banners_first"
                                                >{{$promotion?json_decode($promotion->test_banners)->first:""}}</textarea></td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="test_banners_second"
                                                >{{$promotion?json_decode($promotion->test_banners)->second:""}}</textarea></td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="test_banners_third"
                                                >{{$promotion?json_decode($promotion->test_banners)->third:""}}</textarea></td>
                                        </tr>
                                        <tr>
                                            <th></th>
                                            <th>School/Institutions</th>
                                            <th>Community sites</th>
                                            <th>Other sites</th>
                                        </tr>
                                        <tr>
                                            <td>No of Project Information Board installed</td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="project_information_first"
                                                >{{$promotion?json_decode($promotion->project_information)->first:""}}</textarea></td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="project_information_second"
                                                >{{$promotion?json_decode($promotion->project_information)->second:""}}</textarea></td>
                                            <td><textarea style="width: 100%; height: 40px;" class="form-control" name="project_information_third"
                                                >{{$promotion?json_decode($promotion->project_information)->third:""}}</textarea></td>
                                        </tr>
                                        <tr>
                                            <td>Club Brochures</td>
                                            <td colspan="3">
                                                <textarea style="width: 40%; height: 50px;" class="form-control float-left" name="club_brochures"
                                                >{{$promotion?$promotion->club_brochures:""}}</textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Club Pamphlets</td>
                                            <td colspan="3">
                                                <textarea style="width: 40%; height: 50px;" class="form-control float-left" name="club_pamphlets"
                                                >{{$promotion?$promotion->club_pamphlets:""}}</textarea>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer text-white p-0">
                                <div class="form-row float-right p-2 mr-3 ml-3">
                                    <button type="submit" class="btn btn-block btn-primary">Save <i class="fas fa-check"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


