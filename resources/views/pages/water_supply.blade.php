@extends('layouts.app')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-left">
                        <li class="breadcrumb-item"><a href="index.blade.php">Home</a></li>
                        <li class="breadcrumb-item active">Water Supply & Sanitation</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header card_header p-0">
                            <p class="card-title ml-3"><small>FOCUS AREA: WATER SUPPLY AND SANITATION (WASH)</small></p>
                        </div>
                        <form class="mt-sm-3 mt-md-0" method="post" action="{{route('water_supply.store')}}">
                            @csrf
                            <div class="card-body card_body pt-sm-0 pt-md-3 pb-0">
                                <div class="form-row">
                                    <div class="form-group col-xl-4 col-md-12 col-sm-6">
                                        <label for="club_name"><strong>Club Name</strong></label>
                                        <input type="text" readonly class="form-control-plaintext" id="club_name"
                                               value="{{Auth::user()->club_name}}">
                                    </div>
                                    <input type="hidden" name="id" value="{{$water_supply?($water_supply->id ? $water_supply->id : ""):""}}"/>
                                    <input type="hidden" name="month_id" value="{{$month_id}}"/>
                                    <div class="form-group col-xl-4 col-md-12 col-sm-6">
                                        <label for="wash"><strong>Name of WASH Project:</strong></label>
                                        <input type="text" required name="wash_project_name" class="form-control" id="wash"
                                               value="{{$water_supply?$water_supply->wash_project_name:""}}">
                                    </div>
                                    <div class="form-group col-xl-4 col-md-12 col-sm-6">
                                        <label for="address"><strong>Project Address (VDC/Municipality/Ward):</strong></label>
                                        <input type="text" name="project_address" class="form-control" id="address"
                                               value="{{$water_supply?$water_supply->project_address:""}}">
                                    </div>
                                </div>

                                <div class="table-responsive mb-3">
                                    <table class="table table-bordered insert_more mb-0">
                                        <thead>
                                        <tr>
                                            <th rowspan="2" class="align-middle" width="5%">S.N</th>
                                            <th rowspan="2" class="align-middle" width="20%">Project Completion Date</th>
                                            <th colspan="2" width="30%">A. Project Results</th>
                                            <th width="20%">B. Project Funding Type*</th>
                                            <th colspan="2" width="25%">C. Total Fund Contributions*</th>
                                        </tr>
                                        <tr style="font-size: 13px;">
                                            <th class="font-weight-lighter w-10">Beneficiaries <br>
                                                (a)	population, <br>
                                                or <br>
                                                (b)households <br>
                                                (c) students &ensp; &nbsp;
                                            </th>
                                            <th class="font-weight-lighter w-10">Outputs <br>
                                                outcomes
                                            </th>
                                            <th class="font-weight-lighter w-10">•	Global Grant <br>
                                                •	District Grant <br>
                                                •	Club to Club  funding support <br>
                                                •	Internal Club fund <br>
                                                •	Others
                                            </th>
                                            <th class="font-weight-lighter" colspan="2">
                                                •	GG: <br>
                                                •	DG: <br>
                                                •	International Club: <br>
                                                •	Club internal <br>
                                                •	Others <br>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody class="tbl_body">
                                        @if(!is_null($water_supply_data))
                                            @for($i=0; $i<count($water_supply_data); $i++)
                                                <tr>
                                                    <td>{{$i+1}}.</td>
                                                    <input type="hidden" name="water_supply_data_id[]" value="{{$water_supply_data[$i]->id}}"/>
                                                    <td class="text-center"><input type="date" class="form-control" name="date[]" value="{{$water_supply_data[$i]->date}}"/></td>

                                                    <td class="position-relative">
                                                        <button type="button" data-value="{{$water_supply_data ? $water_supply_data[$i]->beneficiaries ? $water_supply_data[$i]->beneficiaries : "":""}}" class="checked text-center">
                                                            <span>{!! $water_supply_data ? $water_supply_data[$i]->beneficiaries ? "<i class='fas fa-check'></i>" : "":"" !!}</span>
                                                        </button>
                                                        <input type="text" hidden class="form-control" name="beneficiaries[]"
                                                               value="{{$water_supply_data ? $water_supply_data[$i]->beneficiaries ? $water_supply_data[$i]->beneficiaries : "":""}}"/>
                                                    </td>

                                                    <td class="position-relative">
                                                        <button type="button" data-value="{{$water_supply_data ? $water_supply_data[$i]->outputs ? $water_supply_data[$i]->outputs : "":""}}" class="checked text-center">
                                                            <span>{!! $water_supply_data ? $water_supply_data[$i]->outputs ? "<i class='fas fa-check'></i>" : "":"" !!}</span>
                                                        </button>
                                                        <input type="text" hidden class="form-control" name="outputs[]"
                                                               value="{{$water_supply_data ? $water_supply_data[$i]->outputs ? $water_supply_data[$i]->outputs : "":""}}"/>
                                                    </td>

                                                    <td class="position-relative">
                                                        <button type="button" data-value="{{$water_supply_data ? $water_supply_data[$i]->funding_type ? $water_supply_data[$i]->funding_type : "":""}}" class="checked text-center">
                                                            <span>{!! $water_supply_data ? $water_supply_data[$i]->funding_type ? "<i class='fas fa-check'></i>" : "":"" !!}</span>
                                                        </button>
                                                        <input type="text" hidden class="form-control" name="funding_type[]"
                                                               value="{{$water_supply_data ? $water_supply_data[$i]->funding_type ? $water_supply_data[$i]->funding_type : "":""}}"/>
                                                    </td>

                                                    <td class="position-relative">
                                                        <button type="button" data-value="{{$water_supply_data ? $water_supply_data[$i]->fund_contributions ? $water_supply_data[$i]->fund_contributions : "":""}}" class="checked text-center">
                                                            <span>{!! $water_supply_data ? $water_supply_data[$i]->fund_contributions ? "<i class='fas fa-check'></i>" : "":"" !!}</span>
                                                        </button>
                                                        <input type="text" hidden class="form-control" name="fund_contributions[]"
                                                               value="{{$water_supply_data ? $water_supply_data[$i]->fund_contributions ? $water_supply_data[$i]->fund_contributions : "":""}}"/>
                                                    </td>
                                                    
                                                    {!!($i == 0)?'<td width="5%" class="text-center"><button class="btn btn-primary btn-sm addButtonClass" type="button"><i class="fas fa-plus"></i></button></td>':
                                                    '<td class="text-center" width="5%"><i class="fas fa-check"></i></td>'!!}
                                                </tr>
                                            @endfor
                                        @else
                                            <tr>
                                                <td>1.</td>
                                                <td class="text-center"><input class="form-control" type="date" name="date[]"/></td>

                                                <td class="position-relative">
                                                    <button type="button" data-value="" class="checked text-center">
                                                        <span></span>
                                                    </button>
                                                    <input type="text" hidden class="form-control" name="beneficiaries[]"/>
                                                </td>

                                                <td class="position-relative">
                                                    <button type="button" data-value="" class="checked text-center">
                                                        <span></span>
                                                    </button>
                                                    <input type="text" hidden class="form-control" name="outputs[]"/>
                                                </td>

                                                <td class="position-relative">
                                                    <button type="button" data-value="" class="checked text-center">
                                                        <span></span>
                                                    </button>
                                                    <input type="text" hidden class="form-control" name="funding_type[]"/>
                                                </td>

                                                <td class="position-relative">
                                                    <button type="button" data-value="" class="checked text-center">
                                                        <span></span>
                                                    </button>
                                                    <input type="text" hidden class="form-control" name="fund_contributions[]"/>
                                                </td>

                                                <td width="5%" class="text-center"><button class="btn btn-primary btn-sm addButtonClass" type="button"><i class="fas fa-plus"></i></button></td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                    <div class="d-none">
                                        <table class="clone_table">
                                            <tr>
                                                <td><span class="sn"></span></td>
                                                <input type="hidden" name="water_supply_data_id[]" value=""/>
                                                <td class="text-center"><input class="form-control" type="date" name="date[]"/></td>
                                                <td class="position-relative">
                                                    <button type="button" data-value="" class="checked text-center">
                                                        <span></span>
                                                    </button>
                                                    <input type="text" hidden class="form-control" name="beneficiaries[]"/>
                                                </td>

                                                <td class="position-relative">
                                                    <button type="button" data-value="" class="checked text-center">
                                                        <span></span>
                                                    </button>
                                                    <input type="text" hidden class="form-control" name="outputs[]"/>
                                                </td>

                                                <td class="position-relative">
                                                    <button type="button" data-value="" class="checked text-center">
                                                        <span></span>
                                                    </button>
                                                    <input type="text" hidden class="form-control" name="funding_type[]"/>
                                                </td>

                                                <td class="position-relative">
                                                    <button type="button" data-value="" class="checked text-center">
                                                        <span></span>
                                                    </button>
                                                    <input type="text" hidden class="form-control" name="fund_contributions[]"/>
                                                </td>
                                                <td width="5%" class="text-center"><button class="btn btn-danger btn-sm removeButton" type="button"><i class="fas fa-times"></i></button></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <p>OUTPUTS/OUTCOMES indicate : No of tapstands, No of handpumps, Length of pipeline installed,
                                    total number of water reservoir tanks, number of household toilets, no of school latrines
                                    built, no of people/staff trained, number of people reached etc
                                </p>
                            </div>
                            <div class="card-footer text-white p-0">
                                <div class="form-row float-right p-2 mr-3 ml-3">
                                    <button type="submit" class="btn btn-block btn-primary">Save <i class="fas fa-check"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

