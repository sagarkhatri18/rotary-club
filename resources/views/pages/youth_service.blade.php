@extends('layouts.app')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-left">
                        <li class="breadcrumb-item"><a href="index.blade.php">Home</a></li>
                        <li class="breadcrumb-item active">Youth Service</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header card_header p-0">
                            <p class="card-title ml-3"><small>YOUTH SERVICE</small></p>
                        </div>
                        <form class="mt-sm-3 mt-md-0" method="post" action="{{route('youth_service.store')}}">
                            @csrf
                            <div class="card-body card_body pt-sm-0 pt-md-3 pb-0">
                                <div class="form-row">
                                    <div class="form-group col-xl-4 col-md-12 col-sm-6">
                                        <label for="club_name"><strong>Club Name</strong></label>
                                        <input type="text" readonly class="form-control-plaintext" id="club_name"
                                               value="{{Auth::user()->club_name}}">
                                    </div>
                                    <input type="hidden" name="id" value="{{$youth_service?($youth_service->id ? $youth_service->id : ""):""}}"/>
                                    <input type="hidden" name="month_id" value="{{$month_id}}"/>
                                    <div class="form-group col-xl-4 col-md-12 col-sm-6">
                                        <label for="name"><strong>Name of the Project:</strong></label>
                                        <input type="text" required name="project_name" class="form-control" id="name"
                                               value="{{$youth_service?$youth_service->project_name:""}}">
                                    </div>
                                    <div class="form-group col-xl-4 col-md-12 col-sm-6">
                                        <label for="address"><strong>Project Address (INSTITUTION/VDC/Municipality/Ward):</strong></label>
                                        <input type="text" name="project_address" class="form-control" id="address"
                                               value="{{$youth_service?$youth_service->project_address:""}}">
                                    </div>
                                </div>

                                <div class="table-responsive mb-3">
                                    <table class="table table-bordered mb-0">
                                        <thead>
                                        <tr>
                                            <th rowspan="2" class="align-middle">S.N</th>
                                            <th rowspan="2" class="align-middle" style="width: 25%;">Project Activities</th>
                                            <th rowspan="2" class="align-middle">Project Completion Date</th>
                                            <th colspan="2">P. Project Results</th>
                                            <th>Q. Project Funding Type*</th>
                                            <th>R. Total Fund Contributions*</th>
                                        </tr>
                                        <tr style="font-size: 13px;">
                                            <th class="font-weight-lighter" style="width: 10%">Beneficiaries <br>
                                                (j)	No of youth
                                            </th>
                                            <th class="font-weight-lighter" style="width: 10%">Outputs <br>
                                                outcomes
                                            </th>
                                            <th class="font-weight-lighter">•	District Grant <br>
                                                •	International partner <br>
                                                •	Club to Club funding support <br>
                                                •	Internal Club fund <br>
                                                •	Others
                                            </th>
                                            <th class="font-weight-lighter">
                                                •	GG: <br>
                                                •	DG: <br>
                                                •	International Club/partner: <br>
                                                •	Club internal <br>
                                                •	Others
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody class="tbl_body">
                                        <tr>
                                            <td>1.</td>
                                            <td>Sponsoring RYLA</td>
                                            <td class="text-center">
                                                <input type="date" name="sponsoring_first" class="form-control" value="{{$youth_service?json_decode($youth_service->sponsoring)->first:""}}"/>
                                            </td>
                                            
                                            <td class="position-relative">
                                                <button type="button" data-value="{{$youth_service?(json_decode($youth_service->sponsoring)->second ? json_decode($youth_service->sponsoring)->second : ""):""}}" class="checked text-center">
                                                    <span>{!! $youth_service?(json_decode($youth_service->sponsoring)->second ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="sponsoring_second"
                                                       value="{{$youth_service?(json_decode($youth_service->sponsoring)->second ? json_decode($youth_service->sponsoring)->second : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$youth_service?(json_decode($youth_service->sponsoring)->third ? json_decode($youth_service->sponsoring)->third : ""):""}}" class="checked text-center">
                                                    <span>{!! $youth_service?(json_decode($youth_service->sponsoring)->third ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="sponsoring_third"
                                                       value="{{$youth_service?(json_decode($youth_service->sponsoring)->third ? json_decode($youth_service->sponsoring)->third : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$youth_service?(json_decode($youth_service->sponsoring)->fourth ? json_decode($youth_service->sponsoring)->fourth : ""):""}}" class="checked text-center">
                                                    <span>{!! $youth_service?(json_decode($youth_service->sponsoring)->fourth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="sponsoring_fourth"
                                                       value="{{$youth_service?(json_decode($youth_service->sponsoring)->fourth ? json_decode($youth_service->sponsoring)->fourth : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$youth_service?(json_decode($youth_service->sponsoring)->fifth ? json_decode($youth_service->sponsoring)->fifth : ""):""}}" class="checked text-center">
                                                    <span>{!! $youth_service?(json_decode($youth_service->sponsoring)->fifth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="sponsoring_fifth"
                                                       value="{{$youth_service?(json_decode($youth_service->sponsoring)->fifth ? json_decode($youth_service->sponsoring)->fifth : ""):""}}"/>
                                            </td>
                                            
                                        </tr>
                                        <tr>
                                            <td>2.</td>
                                            <td>Joint Service Project with ROTARACTS/INTERACTS</td>
                                            <td class="text-center">
                                                <input type="date" name="joint_service_first" class="form-control" value="{{$youth_service?json_decode($youth_service->joint_service)->first:""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$youth_service?(json_decode($youth_service->joint_service)->second ? json_decode($youth_service->joint_service)->second : ""):""}}" class="checked text-center">
                                                    <span>{!! $youth_service?(json_decode($youth_service->joint_service)->second ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="joint_service_second"
                                                       value="{{$youth_service?(json_decode($youth_service->joint_service)->second ? json_decode($youth_service->joint_service)->second : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$youth_service?(json_decode($youth_service->joint_service)->third ? json_decode($youth_service->joint_service)->third : ""):""}}" class="checked text-center">
                                                    <span>{!! $youth_service?(json_decode($youth_service->joint_service)->third ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="joint_service_third"
                                                       value="{{$youth_service?(json_decode($youth_service->joint_service)->third ? json_decode($youth_service->joint_service)->third : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$youth_service?(json_decode($youth_service->joint_service)->fourth ? json_decode($youth_service->joint_service)->fourth : ""):""}}" class="checked text-center">
                                                    <span>{!! $youth_service?(json_decode($youth_service->joint_service)->fourth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="joint_service_fourth"
                                                       value="{{$youth_service?(json_decode($youth_service->joint_service)->fourth ? json_decode($youth_service->joint_service)->fourth : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$youth_service?(json_decode($youth_service->joint_service)->fifth ? json_decode($youth_service->joint_service)->fifth : ""):""}}" class="checked text-center">
                                                    <span>{!! $youth_service?(json_decode($youth_service->joint_service)->fifth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="joint_service_fifth"
                                                       value="{{$youth_service?(json_decode($youth_service->joint_service)->fifth ? json_decode($youth_service->joint_service)->fifth : ""):""}}"/>
                                            </td>
                                            
                                        </tr>
                                        <tr>
                                            <td>3.</td>
                                            <td>Career Counseling service</td>
                                            <td class="text-center">
                                                <input type="date" name="career_counseling_first" class="form-control" value="{{$youth_service?json_decode($youth_service->career_counseling)->first:""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$youth_service?(json_decode($youth_service->career_counseling)->second ? json_decode($youth_service->career_counseling)->second : ""):""}}" class="checked text-center">
                                                    <span>{!! $youth_service?(json_decode($youth_service->career_counseling)->second ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="career_counseling_second"
                                                       value="{{$youth_service?(json_decode($youth_service->career_counseling)->second ? json_decode($youth_service->career_counseling)->second : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$youth_service?(json_decode($youth_service->career_counseling)->third ? json_decode($youth_service->career_counseling)->third : ""):""}}" class="checked text-center">
                                                    <span>{!! $youth_service?(json_decode($youth_service->career_counseling)->third ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="career_counseling_third"
                                                       value="{{$youth_service?(json_decode($youth_service->career_counseling)->third ? json_decode($youth_service->career_counseling)->third : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$youth_service?(json_decode($youth_service->career_counseling)->fourth ? json_decode($youth_service->career_counseling)->fourth : ""):""}}" class="checked text-center">
                                                    <span>{!! $youth_service?(json_decode($youth_service->career_counseling)->fourth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="career_counseling_fourth"
                                                       value="{{$youth_service?(json_decode($youth_service->career_counseling)->fourth ? json_decode($youth_service->career_counseling)->fourth : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$youth_service?(json_decode($youth_service->career_counseling)->fifth ? json_decode($youth_service->career_counseling)->fifth : ""):""}}" class="checked text-center">
                                                    <span>{!! $youth_service?(json_decode($youth_service->career_counseling)->fifth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="career_counseling_fifth"
                                                       value="{{$youth_service?(json_decode($youth_service->career_counseling)->fifth ? json_decode($youth_service->career_counseling)->fifth : ""):""}}"/>
                                            </td>
                                            
                                        </tr>
                                        <tr>
                                            <td>4.</td>
                                            <td>Professional skill development training</td>
                                            <td class="text-center">
                                                <input type="date" name="professional_skills_first" class="form-control" value="{{$youth_service?json_decode($youth_service->professional_skills)->first:""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$youth_service?(json_decode($youth_service->professional_skills)->second ? json_decode($youth_service->professional_skills)->second : ""):""}}" class="checked text-center">
                                                    <span>{!! $youth_service?(json_decode($youth_service->professional_skills)->second ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="professional_skills_second"
                                                       value="{{$youth_service?(json_decode($youth_service->professional_skills)->second ? json_decode($youth_service->professional_skills)->second : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$youth_service?(json_decode($youth_service->professional_skills)->third ? json_decode($youth_service->professional_skills)->third : ""):""}}" class="checked text-center">
                                                    <span>{!! $youth_service?(json_decode($youth_service->professional_skills)->third ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="professional_skills_third"
                                                       value="{{$youth_service?(json_decode($youth_service->professional_skills)->third ? json_decode($youth_service->professional_skills)->third : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$youth_service?(json_decode($youth_service->professional_skills)->fourth ? json_decode($youth_service->professional_skills)->fourth : ""):""}}" class="checked text-center">
                                                    <span>{!! $youth_service?(json_decode($youth_service->professional_skills)->fourth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="professional_skills_fourth"
                                                       value="{{$youth_service?(json_decode($youth_service->professional_skills)->fourth ? json_decode($youth_service->professional_skills)->fourth : ""):""}}"/>
                                            </td>
                                            
                                            <td class="position-relative">
                                                <button type="button" data-value="{{$youth_service?(json_decode($youth_service->professional_skills)->fifth ? json_decode($youth_service->professional_skills)->fifth : ""):""}}" class="checked text-center">
                                                    <span>{!! $youth_service?(json_decode($youth_service->professional_skills)->fifth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="professional_skills_fifth"
                                                       value="{{$youth_service?(json_decode($youth_service->professional_skills)->fifth ? json_decode($youth_service->professional_skills)->fifth : ""):""}}"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>5.</td>
                                            <td>Others</td>
                                            <td class="text-center">
                                                <input type="date" name="others_first" class="form-control" value="{{$youth_service?json_decode($youth_service->others)->first:""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$youth_service?(json_decode($youth_service->others)->second ? json_decode($youth_service->others)->second : ""):""}}" class="checked text-center">
                                                    <span>{!! $youth_service?(json_decode($youth_service->others)->second ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="others_second"
                                                       value="{{$youth_service?(json_decode($youth_service->others)->second ? json_decode($youth_service->others)->second : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$youth_service?(json_decode($youth_service->others)->third ? json_decode($youth_service->others)->third : ""):""}}" class="checked text-center">
                                                    <span>{!! $youth_service?(json_decode($youth_service->others)->third ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="others_third"
                                                       value="{{$youth_service?(json_decode($youth_service->others)->third ? json_decode($youth_service->others)->third : ""):""}}"/>
                                            </td>

                                            <td class="position-relative">
                                                <button type="button" data-value="{{$youth_service?(json_decode($youth_service->others)->fourth ? json_decode($youth_service->others)->fourth : ""):""}}" class="checked text-center">
                                                    <span>{!! $youth_service?(json_decode($youth_service->others)->fourth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="others_fourth"
                                                       value="{{$youth_service?(json_decode($youth_service->others)->fourth ? json_decode($youth_service->others)->fourth : ""):""}}"/>
                                            </td>
                                            
                                            <td class="position-relative">
                                                <button type="button" data-value="{{$youth_service?(json_decode($youth_service->others)->fifth ? json_decode($youth_service->others)->fifth : ""):""}}" class="checked text-center">
                                                    <span>{!! $youth_service?(json_decode($youth_service->others)->fifth ? "<i class='fas fa-check'></i>" : ""):"" !!}</span>
                                                </button>
                                                <input type="text" hidden class="form-control" name="others_fifth"
                                                       value="{{$youth_service?(json_decode($youth_service->others)->fifth ? json_decode($youth_service->others)->fifth : ""):""}}"/>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="card-footer text-white p-0">
                                <div class="form-row float-right p-2 mr-3 ml-3">
                                    <button type="submit" class="btn btn-block btn-primary">Save <i class="fas fa-check"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


