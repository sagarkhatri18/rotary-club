<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

//Auth::routes();

Route::GET('/', 'Auth\LoginController@showLoginForm')->name('login');
Route::POST('/', 'Auth\LoginController@login');
Route::POST('/logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['middleware' => ['auth', 'userMiddleware']], function () {

    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/home/{id}', 'HomeController@view')->name('home.view');

    /*
    * CLUB ADMINISTRATION ROUTE
    */
    Route::resource('club_meetings', 'ClubAdministration\ClubMeetingsController', ['only' => ['index', 'show', 'store']]);
    Route::resource('club_bulletin', 'ClubAdministration\ClubBulletinController', ['only' => ['index', 'show', 'store']]);
    Route::resource('club_participation', 'ClubAdministration\ClubParticipationController', ['only' => ['index', 'show', 'store']]);
    Route::resource('club_registration', 'ClubAdministration\ClubRegistrationController', ['only' => ['index', 'show', 'store']]);

    /*
    * CLUB MEMBERSHIP ROUTE
    */
    Route::resource('membership_classification', 'ClubMembership\MembershipClassificationController', ['only' => ['index', 'show', 'store']]);
    Route::resource('membership_growth', 'ClubMembership\MembershipGrowthController', ['only' => ['index', 'show', 'store']]);
    Route::resource('membership_development', 'ClubMembership\MembershipDevelopmentController', ['only' => ['index', 'show', 'store']]);

    /*
    * SERVICE PROJECT COMMITTEE ROUTE
    */
    Route::resource('water_supply', 'ServiceProjectCommittee\WaterSupplySanitationController', ['only' => ['index', 'show', 'store']]);
    Route::resource('disease_prevention', 'ServiceProjectCommittee\DiseasePreventionController', ['only' => ['index', 'show', 'store']]);
    Route::resource('economic_community', 'ServiceProjectCommittee\EconomicCommunityController', ['only' => ['index', 'show', 'store']]);
    Route::resource('maternal_child', 'ServiceProjectCommittee\MaternalChildController', ['only' => ['index', 'show', 'store']]);
    Route::resource('basic_education', 'ServiceProjectCommittee\BasicEducationController', ['only' => ['index', 'show', 'store']]);
    Route::resource('youth_service', 'ServiceProjectCommittee\YouthServiceController', ['only' => ['index', 'show', 'store']]);

    /*
    * CLUB PUBLIC RELATION COMMITTEE ROUTE
    */
    Route::resource('media_coverage', 'ClubPublicRelationCommittee\MediaCoverageController', ['only' => ['index', 'show', 'store']]);
    Route::resource('club_flagship', 'ClubPublicRelationCommittee\ClubFlagshipController', ['only' => ['index', 'show', 'store']]);
    Route::resource('promotion', 'ClubPublicRelationCommittee\PromotionController', ['only' => ['index', 'show', 'store']]);

    /*
    * THE ROTARY FOUNDATION COMMITTEE ROUTE
    */
    Route::resource('annual_fund', 'RotaryFoundationCommittee\AnnualFundController', ['only' => ['index', 'show', 'store']]);
    Route::resource('participation_in_trf', 'RotaryFoundationCommittee\ParticipationInTRFController', ['only' => ['index', 'show', 'store']]);

    /*
   * FORM PREVIEW
   */
    Route::resource('preview', 'FormPreviewController', ['only' => ['index', 'show']]);
    Route::post('preview/submit', 'FormPreviewController@finalSubmit')->name('final.submit');

});

Route::group(['middleware' => ['auth', 'adminMiddleware']], function () {
    /*
    * ADMIN ROUTE
    */
    Route::get('/admin', 'AdminController@index')->name('admin.home');
    Route::get('/admin/{month_id}/{user_id}', 'AdminController@reportSearch')->name('admin.preview');
    Route::get('/admin/{month_id}', 'AdminController@clubView')->name('admin.club.view');
    Route::post('/admin/mark', 'AdminController@mark')->name('admin.mark');

});
